$.ajax({
    url: 'evaluate', method: "POST", data: {
        query: JSON.stringify({
            "symbols": [
                "x",
                "y",
                "z"
            ],
            "formula": "x-y-z",
            "substitutions": [
                {
                    "var": "x",
                    "value": "1/z + 1/(z**2)"
                },
                {
                    "var": "y",
                    "value": "2"
                },
                {
                    "var": "z",
                    "value": "3"
                },
                {
                    "var": "x",
                    "value": "5"
                }
            ]
        })
    }, success: function (d) {
        console.log(d)
    }
});
