# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Revelo website and python sympy server
* Version 0.1.0

### How do I get set up? ###

####Local####
* git clone https://artpar@bitbucket.org/artpar/revelo.git
* cd revelo
* mvn clean install -Plocal -DskipTests

In another terminal window
* cd sympy-server/
* python src/main.py 8088

In another terminal window
* cd src/main/docker
* docker-compose -f weedfs-local-docker-compose.yml up -d

Setup mysql
* load the mysql dump in db inf

In the ide set following
* java -jar target/crawler.jar server target/classes/config.yml
main clas: com.inf.drop.App

####Server

### Contribution guidelines ###

* None for now

### Who do I talk to? ###

* Parth Mudgal 
* Vivek Mehta
