-- Create syntax for TABLE 'scope'
CREATE TABLE `scope` (
  `scope` VARCHAR(50) NOT NULL DEFAULT '',
  `role`  VARCHAR(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`scope`, `role`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- Create syntax for TABLE 'user'
CREATE TABLE `user` (
  `id`      INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` VARCHAR(100)              DEFAULT NULL,
  `scope`   VARCHAR(50)               DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 3
  DEFAULT CHARSET = utf8;

INSERT INTO `scope` (`scope`, `role`)
VALUES
  ('ADMIN', 'GET_ADMIN_CATEGORY'),
  ('ADMIN', 'TEMP');
INSERT INTO `user` (`id`, `user_id`, `scope`)
VALUES
  (1, 'fundoobuddy@gmail.com', 'ADMIN'),
  (2, 'vivekm@gmail.com', 'ADMIN');
