<project xmlns="http://maven.apache.org/POM/4.0.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <groupId>revelo</groupId>
    <artifactId>revelo</artifactId>
    <version>1.0-SNAPSHOT</version>
    <name>revelo</name>

    <properties>
        <spring.version>4.0.2.RELEASE</spring.version>
        <dropwizard.version>0.9.2</dropwizard.version>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    </properties>

    <profiles>
        <profile>
            <id>local</id>
            <properties>
                <appserver.port>8082</appserver.port>
            </properties>
            <build>
                <filters>
                    <filter>src/main/filters/local.properties</filter>
                </filters>
            </build>
        </profile>
        <profile>
            <id>prod</id>
            <properties>
                <appserver.port>19501</appserver.port>
            </properties>
            <build>
                <filters>
                    <filter>src/main/filters/prod.properties</filter>
                </filters>
            </build>
        </profile>
    </profiles>

    <repositories>
        <repository>
            <id>hypergraphdb</id>
            <url>http://hypergraphdb.org/maven</url>
        </repository>
    </repositories>

    <dependencies>
        <dependency>
            <groupId>io.artpar</groupId>
            <artifactId>autocrud</artifactId>
            <version>1.0-SNAPSHOT</version>
        </dependency>

        <dependency>
            <groupId>commons-codec</groupId>
            <artifactId>commons-codec</artifactId>
            <version>1.11</version>
        </dependency>
        <dependency>
            <groupId>commons-io</groupId>
            <artifactId>commons-io</artifactId>
            <version>2.5</version>
        </dependency>
        <dependency>
            <groupId>xalan</groupId>
            <artifactId>xalan</artifactId>
            <version>2.7.1</version>
        </dependency>
        <dependency>
            <groupId>org.apache.poi</groupId>
            <artifactId>poi-ooxml</artifactId>
            <version>3.8</version>
        </dependency>

        <dependency>
            <groupId>org.hypergraphdb</groupId>
            <artifactId>hgdb</artifactId>
            <version>1.3</version>
        </dependency>
        <dependency>
            <groupId>org.hypergraphdb</groupId>
            <artifactId>hgbdbje</artifactId>
            <version>1.3</version>
        </dependency>
        <dependency>
            <groupId>com.github.messenger4j</groupId>
            <artifactId>messenger4j</artifactId>
            <version>1.0.0-M2</version>
        </dependency>

        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>5.1.44</version>
        </dependency>
        <dependency>
            <groupId>org.jsoup</groupId>
            <artifactId>jsoup</artifactId>
            <version>1.7.2</version>
        </dependency>
        <dependency>
            <groupId>org.apache.velocity</groupId>
            <artifactId>velocity</artifactId>
            <version>1.7</version>
        </dependency>

        <dependency>
            <groupId>io.dropwizard</groupId>
            <artifactId>dropwizard-forms</artifactId>
            <version>${dropwizard.version}</version>
        </dependency>
        <dependency>
            <groupId>io.dropwizard</groupId>
            <artifactId>dropwizard-assets</artifactId>
            <version>${dropwizard.version}</version>
        </dependency>
        <dependency>
            <groupId>io.dropwizard</groupId>
            <artifactId>dropwizard-auth</artifactId>
            <version>${dropwizard.version}</version>
        </dependency>
        <dependency>
            <groupId>io.dropwizard</groupId>
            <artifactId>dropwizard-views</artifactId>
            <version>${dropwizard.version}</version>
        </dependency>
        <dependency>
            <groupId>com.bendb.dropwizard</groupId>
            <artifactId>dropwizard-redis</artifactId>
            <version>0.9.1-1</version>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-jdbc</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-web</artifactId>
            <version>4.1.6.RELEASE</version>
        </dependency>

        <dependency>
            <groupId>net.sf.json-lib</groupId>
            <artifactId>json-lib</artifactId>
            <version>2.4</version>
            <classifier>jdk15</classifier>
        </dependency>
        <dependency>
            <groupId>com.auth0</groupId>
            <artifactId>java-jwt</artifactId>
            <version>2.1.0</version>
        </dependency>
        <dependency>
            <groupId>com.auth0</groupId>
            <artifactId>auth0</artifactId>
            <version>0.1.1</version>
            <exclusions>
                <exclusion>
                    <groupId>com.fasterxml.jackson.core</groupId>
                    <artifactId>jackson-databind</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>com.fasterxml.jackson.core</groupId>
                    <artifactId>jackson-core</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>com.fasterxml.jackson.core</groupId>
                    <artifactId>jackson-annotations</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>com.zaxxer</groupId>
            <artifactId>HikariCP</artifactId>
            <version>2.4.5</version>
        </dependency>
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-api</artifactId>
            <version>5.0.2</version>
        </dependency>
    </dependencies>

    <build>

        <resources>
            <resource>
                <directory>src/main/resources/</directory>
                <filtering>true</filtering>
                <includes>
                    <include>config.yml</include>
                    <include>favicon.ico</include>
                    <include>homepage/index.html</include>
                    <include>homepage/favicon.png</include>
                    <include>homepage/css/</include>
                    <include>homepage/img/</include>
                    <include>homepage/js/</include>
                    <include>homepage/vendor/</include>
                    <include>root/index.html</include>
                    <include>root/app/</include>
                    <include>root/app/favicon.png</include>
                    <include>root/assets/</include>
                    <include>root/bower_components/angulartics/dist/</include>
                    <include>root/bower_components/angulartics-google-analytics/dist/</include>
                    <include>root/bower_components/angulartics-mixpanel/dist/</include>
                    <include>root/bower_components/zoomooz/</include>
                    <include>root/bower_components/angulartics-flurry/dist/</include>
                    <include>root/bower_components/fontawesome-iconpicker/dist/</include>
                </includes>
            </resource>
        </resources>
        <finalName>crawler</finalName>
        <plugins>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-resources-plugin</artifactId>
                <version>3.1.0</version>
                <configuration>
                    <nonFilteredFileExtensions>
                        <nonFilteredFileExtension>svg</nonFilteredFileExtension>
                        <nonFilteredFileExtension>woff</nonFilteredFileExtension>
                        <nonFilteredFileExtension>woff2</nonFilteredFileExtension>
                        <nonFilteredFileExtension>ttf</nonFilteredFileExtension>
                        <nonFilteredFileExtension>eot</nonFilteredFileExtension>
                        <nonFilteredFileExtension>otf</nonFilteredFileExtension>
                    </nonFilteredFileExtensions>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-shade-plugin</artifactId>
                <version>2.3</version>
                <configuration>
                    <createDependencyReducedPom>true</createDependencyReducedPom>
                    <filters>
                        <filter>
                            <artifact>*:*</artifact>
                            <excludes>
                                <exclude>META-INF/*.SF</exclude>
                                <exclude>META-INF/*.DSA</exclude>
                                <exclude>META-INF/*.RSA</exclude>
                            </excludes>
                        </filter>
                    </filters>
                </configuration>
                <executions>
                    <execution>
                        <phase>package</phase>
                        <goals>
                            <goal>shade</goal>
                        </goals>
                        <configuration>
                            <transformers>
                                <transformer
                                    implementation="org.apache.maven.plugins.shade.resource.ServicesResourceTransformer" />
                                <transformer
                                    implementation="org.apache.maven.plugins.shade.resource.ManifestResourceTransformer">
                                    <mainClass>com.inf.drop.App</mainClass>
                                </transformer>
                            </transformers>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.3</version>
                <configuration>
                    <source>1.8</source>
                    <target>1.8</target>
                    <executable>{JAVA_HOME_1_8}</executable>
                    <encoding>UTF-8</encoding>
                </configuration>

            </plugin>

            <plugin>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>2.22.1</version>
                <configuration>
                    <includes>
                        <include>**/*Tests.java</include>
                    </includes>
                </configuration>
            </plugin>

            <plugin>
                <groupId>io.fabric8</groupId>
                <artifactId>docker-maven-plugin</artifactId>
                <version>0.19.0</version>

                <configuration>
                    <images>
                        <!-- A single's image configuration -->
                        <image>
                            <name>909032783227.dkr.ecr.ap-southeast-1.amazonaws.com/revelo</name>
                            <build>
                                <dockerFile>Dockerfile</dockerFile>
                            </build>
                        </image>
                    </images>
                </configuration>
            </plugin>

        </plugins>
    </build>
</project>
