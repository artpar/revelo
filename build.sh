mvn clean install -Pprod
cp target/crawler.jar src/main/docker/revelo.jar 
cd src/main/docker
docker build -t 909032783227.dkr.ecr.ap-southeast-1.amazonaws.com/revelo .
cd -
docker push 909032783227.dkr.ecr.ap-southeast-1.amazonaws.com/revelo
