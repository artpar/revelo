#!/usr/bin/env bash
ps aux | grep "target/crawler.jar" | grep -v grep | awk '{print $2}' | xargs kill
ps aux | grep "sympy-server/main.py" | grep -v grep | awk '{print $2}' | xargs kill
