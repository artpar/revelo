#!/usr/bin/env bash
host=`hostname`
if [[ $host == *"planendo"* ]]
then
    echo "running in prod"
    export MAVEN_OPTS='-Xmx512m -Xms128m'
    mvn clean install -Pprod -DskipTests
    java -jar target/crawler.jar server target/classes/config.yml &
    python2.7 sympy-server/main.py 30824 &
else
    echo "running in local"
    mvn -o clean install -Plocal -DskipTests
    java -jar target/crawler.jar server target/classes/config.yml &
    python sympy-server/main.py 8080 &
fi
