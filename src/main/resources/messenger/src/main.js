// polyfill
// import 'babel-polyfill';

import Vue from 'vue';
import $ from 'jquery';
window.jQuery = $;
window.$ = $;

require("semantic-ui/dist/semantic.min");

require("typeface-ubuntu");

var scrollToBottom = function () {

    var $container = $('.messages'),
        $scrollTo = $('.messages ul li:last');

    $container.scrollTop(
        $scrollTo.offset().top - $container.offset().top + $container.scrollTop()
    );

    $container.animate({
        scrollTop: $scrollTo.offset().top - $container.offset().top + $container.scrollTop()
    }, "slow");
}
import App from './App';

import store from './store';

// require("semantic-ui")
Vue.config.devtools = true;

new Vue({
    el: 'body',
    components: { App },
    store: store
});
