/**
 * Vuex
 * http://vuex.vuejs.org/zh-cn/intro.html
 */
import Vue from 'vue';
import Vuex from 'vuex';
import chatapi from './api/chatapi'
import jQuery from 'jquery';

var $ = jQuery;

function scrollToBottom() {

    var $container = $('.messages'),
        $scrollTo = $('.messages ul li:last');
    $container.animate({
        duration: 2000,
        scrollTop: $scrollTo.offset().top - $container.offset().top + $container.scrollTop()
    },);
}

Vue.use(Vuex);

/**
 * Returns a random integer between min (inclusive) and max (inclusive)
 * Using Math.round() will give you a non-uniform distribution!
 */
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

const now = new Date();
const store = new Vuex.Store({
    state: {
        // 当前用户
        user: {
            name: '',
            img: 'dist/images/user.gif'
        },
        // 会话列表
        sessions: [
            {
                id: 1,
                user: {
                    name: 'Mobiles',
                    img: 'dist/images/bot/bot2head2x.png'
                },
                messages: [{
                    content: 'Hello, are you looking for a mobile ?',
                    date: now
                }]
            },
        ],
        // 当前选中的会话
        currentSessionId: 1,
        userId: "<PSID>",
        // 过滤出只包含这个key的会话
        filterKey: ''
    },
    mutations: {
        INIT_DATA(state) {
            let data = localStorage.getItem('vue-chat-session');
            if (data) {
                state.sessions = JSON.parse(data);
            }
        },
        // 发送消息
        SEND_MESSAGE({sessions, currentSessionId, userId}, content) {
            let session = sessions.find(item => item.id === currentSessionId);

            session.messages.push({
                content: content,
                date: new Date(),
                self: true
            });
            chatapi.sendMessage(currentSessionId, content, userId).then(function (responses) {

                var data = responses.data;
                console.log("responses", data);


                (function (data) {
                    if (data.length == 0) {
                        return;
                    }
                    var i = 0;
                    var tm = setInterval(function () {

                        session.messages.push({
                            content: data[i].text,
                            date: new Date(),
                            self: false
                        });
                        i += 1;
                        if (i == data.length) {
                            clearInterval(tm);
                        }


                    }, getRandomInt(600, 2000));


                })(data);
            });
        },
        // 选择会话
        SET_USER_ID(state, userId) {
            state.userId = userId;
        },
        SELECT_SESSION(state, id) {
            state.currentSessionId = id;
        },
        // 搜索
        SET_FILTER_KEY(state, value) {
            state.filterKey = value;
        }
    }
});

store.watch(
    (state) => state.sessions,
    (val) => {
        console.log("value", val[0].messages);
        var msgs = val[0].messages;
        if (msgs.length > 300) {
            msgs = msgs.slice(msgs.length - 300);
            val[0].messages = msgs;
        }
        console.log('CHANGE: ', val);
        localStorage.setItem('vue-chat-session', JSON.stringify(val));
        scrollToBottom();
    },
    {
        deep: true
    }
);

export default store;
export const actions = {
    initData: ({dispatch}) => dispatch('INIT_DATA'),
    sendMessage: ({dispatch}, content) => dispatch('SEND_MESSAGE', content),
    setUserId: ({dispatch}, userId) => dispatch('SET_USER_ID', userId),
    selectSession: ({dispatch}, id) => dispatch('SELECT_SESSION', id),
    search: ({dispatch}, value) => dispatch('SET_FILTER_KEY', value)
};
