import axios from 'axios'

var jsSHA = require("jssha");
var paramsList = window.location.search.substring(1).split("&").map(function (e) {
    var x = e.split("=");
    return {"name": x[0], "value": x[1]}
});

var questionnaireId = paramsList.filter(function (e) {
    return e.name == "q";
})[0].value;

export default {
    // Convert a byte array to a hex string
    bytesToHex(bytes) {
        for (var hex = [], i = 0; i < bytes.length; i++) {
            hex.push((bytes[i] >>> 4).toString(16));
            hex.push((bytes[i] & 0xF).toString(16));
        }
        return hex.join("");
    },

    sendMessage(number, message, userId) {

        var promise = new Promise(function (resolve, reject) {
            console.log("Send message");


            let dataString = JSON.stringify({
                "object": "page",
                "entry": [
                    {
                        "id": number,
                        "time": new Date().getTime(),
                        "messaging": [
                            {
                                "sender": {
                                    "id": "r-" + userId
                                },
                                "recipient": {
                                    "id": "0"
                                },
                                "timestamp": new Date().getTime(),
                                "message": {
                                    "mid": "mid." + new Date().getTime() + ".message1",
                                    "text": message.trim(),
                                }
                            }
                        ]
                    }
                ]
            });
            var shaObj = new jsSHA("SHA-1", "TEXT");
            shaObj.setHMACKey("d78e237a0b18b33e4f40803489d72459", "TEXT");
            shaObj.update(dataString);
            var hmac = shaObj.getHMAC("HEX");

            axios({
                url: '/messenger/' + questionnaireId,
                method: "POST",
                headers: {
                    "X-Hub-Signature": "....." + hmac
                },
                data: dataString,
            }).then(resolve, reject)
        });


        return promise;


    }
}