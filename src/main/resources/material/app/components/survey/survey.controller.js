/**
 * @ngdoc function
 * @name revelo.controller.ListCtrl
 * @description controller for Flight listing page
 * @author Ashish Mishra <ashishjpm@gmail.com>
 */

;
(function () {
    revelo
        .controller('surveyCtrl', ['$scope', 'Survey', '$state', 'DataProvider', function ($scope, Survey, $state, DataProvider) {
            var classMap = {
                1: ['col-md-12', 'col-sm-12', 'col-xs-12'],
                2: ['col-md-6', 'col-sm-6', 'col-xs-6'],
                3: ['col-md-4', 'col-sm-4', 'col-xs-4'],
                4: ['col-md-3', 'col-sm-3', 'col-xs-3']
            };

            $scope.selectedAns = null;
            $scope.suggestions = [];

            $scope.imageServerRoot = "https://images.revelo.revlo.club/";

            if (window.location.hostname == "localhost") {
              $scope.imageServerRoot = "http://localhost:9333";
            }
            var surveyId = $state.params.surveyId;

            function init() {


                DataProvider.get('survey', {where: "id:" + surveyId, children: 'questionnaire_id'}).then(function (e) {
                    $scope.survey = e.data.data[0];
                    console.log("survey is ", e);
                });

                Survey.getQuestions(surveyId).then(function (response) {
                    response = response.data;
                    console.log("survey details", response);
                    $scope.surveyDetails = response;

                    $scope.surveyTypes = getSurveyTypes($scope.surveyDetails);
                    $scope.classForSurveyTypes = classMap[$scope.surveyTypes.length];
                    $scope.updateCurrentQue(0);
                }, function (err) {
                });
                // $scope.surveyDetails = SurveyService.processRawSurveyDetails(SurveyService.getDummyData());
                // $scope.surveyDetails = SurveyService.getDummyData();
            }

            $scope.scrollToResult = function () {
                $('html, body').animate({
                    scrollTop: $("#result").offset().top
                }, 2000);
            }

            $scope.updateAnswer = function (questionNumber, answer, multiple) {

                var question = $scope.surveyDetails[questionNumber];

                if (!multiple) {
                    $scope.surveyDetails[questionNumber].answer = answer;
                } else {
                    if ($scope.surveyDetails[questionNumber].answer instanceof Array) {
                        $scope.surveyDetails[questionNumber].answer.push(answer)
                    } else {
                        if ($scope.surveyDetails[questionNumber].answer) {
                            $scope.surveyDetails[questionNumber].answer = [$scope.surveyDetails[questionNumber].answer, answer]
                        } else {
                            $scope.surveyDetails[questionNumber].answer = [answer];
                        }
                    }
                }

                Survey.updateAnswer(surveyId, question.id, $scope.surveyDetails[questionNumber].answer).then(function (w) {
                    Survey.getSuggestions(surveyId).then(function (d) {
                        d = d.data;
                        $scope.suggestions = d;

                        if (d.length > 0) {
                            function loadQuestion(i) {
                                var interval = setTimeout(function () {
                                    clearTimeout(interval);
                                    var product = $scope.suggestions[i];
                                    Survey.getProductSplit(surveyId, product.product.name, product.product.companyName).then(function (a) {
                                        a = a.data;
                                        console.log("split", a);
                                        var scoreMap = a.scoreMap;
                                        var scoreMapKeys = Object.keys(scoreMap);
                                        scoreMapKeys.map(function (scoreKey) {
                                            if (scoreMap[scoreKey].reason) {
                                                var reasonType = scoreMap[scoreKey].reason[0];
                                                var reasonColor = "black"
                                                switch (reasonType) {
                                                    case '*':
                                                        reasonColor = "green";
                                                        break;
                                                    case '+':
                                                        reasonColor = 'orange';
                                                        break;
                                                    case '-':
                                                        reasonColor = 'red';
                                                }
                                                scoreMap[scoreKey].reason = scoreMap[scoreKey].reason.substring(1);
                                                scoreMap[scoreKey].reasonColor = reasonColor;
                                            }
                                        });
                                        $scope.suggestions[i].split = scoreMap;
                                        // splitBody.html(makeTable(a.scoreMap, ["Feature", "Score"]))
                                    })


                                    var productName = d[i].product.name;
                                    productName = productName.replace(/[\-\/\(\)]/g, " ").split(" ");
                                    var strSearch = "";
                                    var added = 0;
                                    for (var e = 0; e < productName.length && added < 3; e++) {
                                        if (productName[e].trim().length > 0) {
                                            added += 1;
                                            strSearch += productName[e].trim() + " ";
                                        }
                                    }


                                    var keyName = strSearch + " " + d[i].product.companyName;


                                    (function (index, keyName) {
                                        DataProvider.getImages(keyName).then(function (c) {
                                            // $scope.images[keyName] = c;
                                            $scope.suggestions[index].image = random(c.data);
                                            // $scope.$digest();


                                            //setInterval(function () {
                                            //    var next = random(c);
                                            //    console.log("update image for " + category.name + " to " + next.url);
                                            //    $scope.categoryList[i].image = next;
                                            //    $("#image-" + category.referenceId).attr("src", "/image/" + next.url);
                                            //}, Math.random() * 20000 + 10000);
                                        });
                                    }(i, keyName));

                                    loadQuestion(i + 1);

                                }, 100)
                            }

                            loadQuestion(0);


                        }


                    })
                })

            }

            function getSurveyTypes(surveyDetails) {
                var surveyTypes = [];
                for (var i = 0; i < surveyDetails.length; i++) {
                    var name = surveyDetails[i].questionGroup.split(';')[0];
                    var icon = surveyDetails[i].questionGroup.split(';')[1];
                    if (surveyTypes.map(function (obj) {
                            return obj.name
                        }).indexOf(name) == -1) {
                        var temp = {
                            name: name,
                            icon: icon,
                            status: 'inactive'
                        }
                        surveyTypes.push(temp);
                    }
                }
                return surveyTypes;
            }

            $scope.updateImgSelected = function (imgNumber) {
                $scope.surveyDetails[currentQue].selectedAns = imgNumber; // to maintain history
                $scope.selectedAns = $scope.surveyDetails[currentQue].internalObject[imgNumber].value;
            }

            $scope.updateAnsMultiSelect = function (type, number) {
                if (type == 'image') {
                    if ($scope.surveyDetails[currentQue].selectedAns == undefined) {
                        $scope.surveyDetails[currentQue].selectedAns = [];
                    }
                    else if ($scope.surveyDetails[currentQue].selectedAns.indexOf($scope.surveyDetails[currentQue].internalObject[number].value) == -1) {
                        $scope.surveyDetails[currentQue].selectedAns.push($scope.surveyDetails[currentQue].internalObject[number].value)
                    }
                    else if ($scope.surveyDetails[currentQue].selectedAns.indexOf($scope.surveyDetails[currentQue].internalObject[number].value) != -1) {
                        $scope.surveyDetails[currentQue].selectedAns.splice($scope.surveyDetails[currentQue].selectedAns.indexOf($scope.surveyDetails[currentQue].internalObject[number].value, 1));
                    }
                }
            }

            $scope.updateCurrentQue = function (number) {
                if (number <= $scope.surveyDetails.length - 1) {
                    var currentName = $scope.surveyDetails[number].questionGroup.split(';')[0];
                    var currentIndex = $scope.surveyTypes.map(function (obj) {
                        return obj.name
                    }).indexOf(currentName);
                    for (i = 0; i < $scope.surveyTypes.length; i++) {
                        if (i < currentIndex) {
                            $scope.surveyTypes[i].status = 'done';
                        }
                        else if (i == currentIndex) {
                            $scope.surveyTypes[i].status = 'active';
                        }
                        else if (i > currentIndex) {
                            $scope.surveyTypes[i].status = 'inactive';
                        }
                    }
                    $scope.currentQue = number;
                } else {
                    for (i = 0; i < $scope.surveyTypes.length; i++) {
                        $scope.surveyTypes[i].status = 'done';
                    }
                }
                $(document).ready(function () {
                    $.material.init();
                });
            }

            $scope.surveyTypesChange = function (index, currentName) {
                var queNo = null;
                for (var i = 0; i < $scope.surveyDetails.length; i++) {
                    // $scope.surveyTypes[i].status = null;
                    // if(i == index)
                    //   $scope.surveyTypes[i].status = 'active';
                    if ((queNo == null) && ($scope.surveyDetails[i].questionGroup.split(';')[0] == currentName))
                        queNo = i;
                }
                $scope.updateCurrentQue(queNo);
            }

            init();

        }]);
}());
