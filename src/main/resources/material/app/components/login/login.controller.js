/**
* @ngdoc function
* @name revelo.controller.ListCtrl
* @description controller for Flight listing page
* @author Ashish Mishra <ashishjpm@gmail.com>
*/

;
(function() {
 revelo
    .controller('LoginCtrl', ['$scope', function($scope) {


        var token = window.localStorage.getItem("token");
        if (!token || token.length < 5) {
            window.location = "/root/app/#/login";
        } else {
            window.location = "/material/#!/home"
        }

    }]);
}());