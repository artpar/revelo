/**
 * @ngdoc function
 * @name revelo.controller.ListCtrl
 * @description controller for Flight listing page
 * @author Ashish Mishra <ashishjpm@gmail.com>
 */

;
(function () {
    revelo
        .controller('ItemSelectCtrl', ['$scope', '$state', 'DataProvider', 'Survey', function ($scope, $state, DataProvider, Survey) {
            var categoryId = $state.params.categoryId;
            console.log("loaded item select", $state, $scope, categoryId);


            $scope.startSurvey = function (params) {
                var referenceId = params.surveyId;
                Survey.start(referenceId).then(function (survey) {
                    survey = survey.data;
                    console.log("survey started", survey);
                    window.location = window.location.href.split("#")[0] + "#!/survey/" + survey.id + "/" + survey.categoryName
                })
            };


            DataProvider.get('questionnaire', {
                children: 'category_id,user_id',
                limit: 50
            }).then(function (questionnaireListResponse) {
                questionnaireListResponse = questionnaireListResponse.data;
                console.log("got all questionnaire list", questionnaireListResponse);

                function setImage(name, i) {
                    DataProvider.getImages(name).then(function (e) {
                        e = e.data;
                        $scope.questionnaireList[i].images = e;
                        $scope.questionnaireList[i].image = random(e);
                        $scope.questionnaireList[i].image_url = "https://images.revelo.revlo.club/" + $scope.questionnaireList[i].image.token;
                        $scope.questionnaireList[i].color = pastel_colour(name)
                    });
                }

                var questionnaireList = questionnaireListResponse.data;
                var finalList = [];
                for (var i = 0; i < questionnaireList.length; i++) {
                    var questionnaire = questionnaireList[i];
                    if (questionnaire.category[0].reference_id != categoryId) {
                        continue;
                    }
                    finalList.push(questionnaire);
                    setImage(questionnaire.category[0].name, i);
                }

                console.log("final list", finalList)
                $scope.questionnaireList = finalList;

                // $("select").select2();

            });


        }]);
}());