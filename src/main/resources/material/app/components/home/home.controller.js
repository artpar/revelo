/**
 * @ngdoc function
 * @name revelo.controller.ListCtrl
 * @description controller for Flight listing page
 * @author Ashish Mishra <ashishjpm@gmail.com>
 */

;
(function () {
    revelo
        .controller('HomeCtrl', ['$scope', 'CategoryFactory', 'DataProvider', function ($scope, CategoryFactory, DataProvider) {


            function setImage(name, i) {
                DataProvider.getImages(name).then(function (e) {
                    e = e.data;
                    $scope.catList[i].images = e;
                    $scope.catList[i].image = random(e);
                    $scope.catList[i].image_url = "https://images.revelo.revlo.club/" + $scope.catList[i].image.token;
                    $scope.catList[i].color = pastel_colour(name)
                });
            }


            CategoryFactory.getCategoryList().then(function (response) {
                $scope.catList = response.data.data;

                for (var i = 0; i < $scope.catList.length; i++) {
                    var category = $scope.catList[i];
                    setImage(category.name, i);
                }


            }, function (err) {
            });

        }]);
}());