/**
 * Properties of the revelo app
 *
 * @class revelo.service.SurveyService
 * @memberOf revelo.SurveyService
 * @author Ashish Mishra <ashishjpm@gmail.com>
 */

;
(function() {
    revelo
        .factory('HomeService', function($http, AppConstant) {
            function getCatagory(surveyId) {
            	return $http({
                    url: 
                        AppConstant.url.base +  
                        AppConstant.url.category,
                    method: 'GET'
                });
            };

            return {
            	getCatagory: getCatagory
            };
        });
}());