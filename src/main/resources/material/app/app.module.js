/**
 * Main module for the applications
 * @namespace revelo
 * @author Ashish Mishra <ashishjpm@gmail.com>
 */
'use strict';

function random(items, length) {
  if (!length || length < 2) {

    return items[Math.floor(Math.random() * items.length)];
  } else {

    return items[Math.floor(Math.random() * items.length)] + random(items, length - 1);
  }
}

function pastel_colour(input_str) {

  //TODO: adjust base colour values below based on theme
  var baseRed = 128;
  var baseGreen = 128;
  var baseBlue = 128;

  //lazy seeded random hack to get values from 0 - 256
  //for seed just take bitwise XOR of first two chars
  var seed = input_str.charCodeAt(0) ^ input_str.charCodeAt(1);
  var rand_1 = Math.abs((Math.sin(seed++) * 10000)) % 256;
  var rand_2 = Math.abs((Math.sin(seed++) * 10000)) % 256;
  var rand_3 = Math.abs((Math.sin(seed++) * 10000)) % 256;

  //build colour
  var red = Math.round((rand_1 + baseRed) / 2);
  var green = Math.round((rand_2 + baseGreen) / 2);
  var blue = Math.round((rand_3 + baseBlue) / 2);

  return "#" + red.toString(16) + green.toString(16) + blue.toString(16);
}

var revelo = angular.module('revelo', ['ui.router', 'AdminModule', 'SurveyFactory', 'angular-jwt']);

revelo.run(function($rootScope){
	$rootScope.current = {};
    $rootScope.$on('$stateChangeSuccess', function(){
        $(document).ready(function(){
            $.material.init();
        });
    });
})