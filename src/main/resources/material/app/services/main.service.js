revelo.config(['$httpProvider', 'jwtInterceptorProvider',
  function ($httpProvider, jwtInterceptorProvider) {

    jwtInterceptorProvider.tokenGetter = ['$state', function ($state) {
      // console.log("token getter", $state);

      console.log("token", localStorage.getItem("user"));
      console.log("token", localStorage.getItem("token"));
      console.log("token", localStorage.getItem("profile"));

      var t = null;
      try {

        t = JSON.parse(localStorage.getItem("token"));
      } catch (e) {
        localStorage.setItem("token", null)
        return
      }
      if (t) {
        return t;
      }

      if (!$state.requiresLogin) {
        return null;
      }

      var promise = function (resolve, reject) {
        var ob = {
          resolve: resolve,
          reject: reject
        };
        tokenQueue.push(ob);
      };
      return $q(promise);
    }];

    $httpProvider.interceptors.push('jwtInterceptor');
  }
]);