;
(function () {
  revelo
      .constant('AppConstant', {
        url: {
          base: '/',
          apiRootPath: '/',
          newApiRoot: '/api/v2',
          category: '/api/v2/category',
          adminApiRootPath: '/api/v2/category',
          survey: '/survey',
          details: '/details'
        }
      });
}());