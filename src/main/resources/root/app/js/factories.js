/**
 * Created by parth on 28/4/16.
 */
var AdminModule = angular.module('AdminModule', ['DataFactory']);

AdminModule.factory('DataProvider', ['$http', 'Data', 'Log', '$q', function ($http, Data, Log, $q) {
  var factory = {};
  factory.apiRoot = Data.apiRoot("");
  factory.newApiRoot = Data.newApiRoot("/");
  factory.getNewApiRoot = function () {
    return factory.newApiRoot;
  }


  factory.getQuestionnaireList = function () {
    return $http({
      url: factory.apiRoot + 'questionnaire/list'
    })
  };

  factory.getQuestionnaireStats = function (referenceId) {
    return $http({
      url: factory.apiRoot + "questionnaire/" + referenceId + "/stats"
    })
  };

  factory.get = function (type, obj) {
    return $http({
      url: factory.newApiRoot + type + "?" + makeQS(obj)
    })
  };


  factory.delete = function (type, obj) {
    return $http({
      url: factory.newApiRoot + type + "?" + makeQS(obj),
      method: "DELETE",
      data: JSON.stringify(obj)
    })
  };

  factory.new = function (type, obj) {
    return $http({
      url: factory.newApiRoot + type,
      method: "POST",
      data: JSON.stringify(obj)
    })
  };

  factory.uploadXls = function (type, referenceId, obj) {
    return $http.post(factory.apiRoot + type + "/" + referenceId + "/xls",
      obj, {
        transformRequest: angular.identity,
        headers: {'Content-Type': undefined}
      }
    );
  };


  factory.uploadImage = function (obj) {
    return $http.post(factory.apiRoot + "image/upload",
      obj, {
        transformRequest: angular.identity,
        headers: {'Content-Type': undefined}
      }
    );
  };

  factory.deleteImage = function (imageServerRoot, img) {
    return $http.post(imageServerRoot + img);
  }


  factory.edit = function (type, obj) {
    return $http({
      url: factory.newApiRoot + type,
      method: "PUT",
      data: JSON.stringify(obj)

    })
  };

  factory.getImages = function (token) {
    return $http({
      url: factory.apiRoot + "image/search?query=" + token,
    })
  };

  factory.getProductImages = function (category, company, product) {
    return $http({
      url: factory.apiRoot + "image/lookup?category=" + category + "&company=" + company + "&product=" + product,
    })
  };

  factory.getUserStats = function (referenceId) {
    if (referenceId && referenceId.length > 1) {
      referenceId = referenceId + "/";
    } else {
      referenceId = "";
    }
    return $http({
      url: factory.apiRoot + "user/" + referenceId + "stats"
    })
  };
  factory.getCategoryList = function (params) {
    return factory.get('category', params);
  };


  factory.getList = function (type, params) {
    var deferred = $q.defer();
    deferred.success = function (c) {
      deferred.successCallback = c;
      return deferred;
    };
    deferred.error = function (e) {
      deferred.errorCallback = e;
      return deferred;
    };
    if (!params) {
      params = {};
    }
    params["limit"] = 50;


    var query = makeQS(params);
    $http({
      url: factory.newApiRoot + type + "?" + query,
      data: params
    }).success(function (c) {
      if (c && c.data && c.data instanceof Array) {
        deferred.successCallback(c.data);
      } else {
        deferred.successCallback(c);

      }
      deferred.resolve(c);
    }).error(function (e) {
      console.log("Error", e);
      deferred.errorCallback(e);
      deferred.reject(e)
    });
    return deferred;

  };


  factory.getSingle = function (type, params) {
    var deferred = $q.defer();
    deferred.success = function (c) {
      deferred.successCallback = c;
    };
    deferred.error = function (e) {
      deferred.errorCallback = e;
    };
    if (!params) {
      params = {};
    }
    params["limit"] = 1;


    var query = makeQS(params);
    $http({
      url: factory.newApiRoot + type + "?" + query,
      data: params
    }).success(function (c) {
      if (c && c.data && c.data instanceof Array) {
        deferred.successCallback(c.data[0]);
      } else {
        deferred.successCallback(c);

      }
      deferred.resolve(c);
    }).error(function (e) {
      console.log("Error", e);
      deferred.errorCallback(e);
      deferred.reject(e)
    });
    return deferred;
  };


  factory.getUser = function (referenceId) {
    return factory.getSingle("user", {"query": "reference_id:" + referenceId});
  };

  factory.getMe = function () {
    return factory.getSingle("user/mine", {});
  };

  factory.getMyGroups = function (userReferenceId) {
    return factory.getList("user_usergroup/mine", {"user_id": userReferenceId, "children": ["usergroup_id"]});
  };

  factory.getCategoryStats = function (referenceId) {
    return $http({
      url: factory.apiRoot + "category/" + referenceId + "/stats"
    })
  };


  factory.addResultToCategory = function (referenceId, result, companyName) {
    return $http({
      'url': factory.apiRoot + "category/result",
      'method': 'POST',
      'data': {
        referenceId: referenceId,
        name: result,
        companyName: companyName
      }
    })
  };


  function makeQS(ar) {
    if (!ar) {
      return "";
    }
    var s = "";
    if (ar instanceof Array) {

      for (var i = 0; i < ar.length; i++) {
        s = s + ar[i][0] + "=" + ar[i][1] + "&";
      }
    } else {
      var keys = Object.keys(ar);
      for (var i = 0; i < keys.length; i++) {
        s = s + keys[i] + "=" + encodeURIComponent(ar[keys[i]]) + "&";
      }

    }

    return s;
  }

  factory.deleteResultFromCategory = function (name, referenceId, companyName) {
    var queryString = makeQS([['name', name], ['referenceId', referenceId], ['companyName', companyName]]);
    return $http({
      url: factory.apiRoot + 'category/result/delete',
      method: 'POST',
      data: {
        name: name,
        referenceId: referenceId,
        companyName: companyName
      }
    })
  };

  factory.getResultsForCategory = function (referenceId) {
    return $http({
      url: factory.apiRoot + "category/" + referenceId + "/result"
    })
  };


  factory.getCategoryFeatures = function (referenceId) {
    return $http({
      url: factory.apiRoot + "category/" + referenceId + "/features"
    })
  };


  //factory.getValuesForFeatureName = function (featureName, referenceId) {
  //    return $http({
  //        url: factory.apiRoot + "category/" + referenceId + "/featureValues?featureName=" + encodeURI(featureName)
  //    })
  //};
  factory.getValuesForFeatureName = function (featureName, referenceId) {
    return $http({
      url: factory.apiRoot + "category/" + referenceId + "/originalFeatureValues?featureName=" + encodeURI(featureName)
    })
  };


  factory.addResultFeatureValue = function (r) {
    return $http({
      url: factory.apiRoot + "category/product/edit",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      method: 'POST',
      data: makeQS(r)
    })
  };

  factory.getProductFeatures = function (r) {
    var keys = Object.keys(r);
    var arr = [];
    for (var i = 0; i < keys.length; i++) {
      arr.push([keys[i], r[keys[i]]])
    }
    return $http({
      url: factory.apiRoot + "category/product?" + makeQS(arr),
    })
  };


  factory.getCategory = function (referenceId) {
    return $http({
      url: factory.apiRoot + "category/reference/" + referenceId
    })
  }
  ;

  factory.newQuestionnaire = function (title, category) {
    return $http({
      'url': factory.newApiRoot + "questionnaire",
      method: 'POST',
      data: {
        name: title,
        category: category
      }
    })
  };


  factory.updateNumberValue = function (categoryReferenceId, value, newNumberValue) {
    var ar = [['newNumberValue', newNumberValue],
      ['value', value]];
    return $http({
      url: factory.apiRoot + "category/" + categoryReferenceId + "/value/edit?" + makeQS(ar),
      method: 'POST',
      data: {}
    })
  };

  factory.getQuestionnaireListByCategory = function (referenceId) {
    return $http({
      url: factory.apiRoot + "category/" + referenceId + "/questionnaire"
    });
  };

  factory.getQuestionnaireByReferenceId = function (referenceId) {
    return $http({
      url: factory.apiRoot + "questionnaire/" + referenceId
    })
  };

  factory.getQuestionnaireCategory = function (referenceId) {
    return $http({
      'url': factory.apiRoot + "questionnaire/" + referenceId + "/category"
    })
  };


  factory.getQuestionnaireFeatures = function (referenceId) {
    return $http({
      url: factory.apiRoot + "questionnaire/" + referenceId + "/features"
    })
  };

  factory.getUserSurveysByQuestionnaireId = function (referenceId) {
    return $http({
      url: factory.apiRoot + "questionnaire/" + referenceId + "/features"
    })
  };


  factory.export = function (item) {
    window.location = factory.apiRoot + item + "/export";
    // return $http({
    //   url: factory.apiRoot + item + "/export",
    //   method: "POST"
    // })
  };

  factory.updateCategory = function (r) {
    return $http({
      url: factory.apiRoot + "category/update",
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      data: JSON.stringify(r)
    });
  };


  factory.getCategoryFeatures = function (category) {
    return $http({
      url: factory.apiRoot + 'category/' + category + '/features'
    })
  };

  factory.getAnswerData = function (referenceId, questionId, answer) {
    return $http({
      url: factory.apiRoot + "category/answerData",
      method: 'GET',
      params: {
        referenceId: referenceId,
        questionId: questionId,
        answer: answer
      }
    })
  };

  factory.updateAnswerFunctions = function (referenceId, questionId, answer, data) {
    return $http({
      url: factory.apiRoot + 'question/answerFunctions',
      method: "POST",
      data: {
        referenceId: referenceId,
        questionId: questionId,
        answer: answer,
        data: JSON.stringify(data)
      }
    })
  };

  factory.getQuestionnaireQuestions = function (category) {
    return $http({
      url: factory.apiRoot + 'questionnaire/' + category + '/questions',
      method: 'GET'
    })
  };

  factory.getAllQuestions = function (filter) {
    return $http({
      url: factory.apiRoot + 'question/search',
      params: {
        question: filter
      },
      method: 'GET'
    })
  };

  factory.getQuestionTypes = function () {
    return $http({
      url: factory.apiRoot + 'question/types'
    })
  };

  factory.updateQuestionType = function (questionId, type) {
    return $http({
      url: factory.apiRoot + 'question/type',
      method: 'POST',
      data: {
        questionId: questionId,
        type: type
      }
    })
  };


  factory.updateQuestionData = function (questionId, data) {
    var options = data.options;
    if (options != undefined && options instanceof Array) {
      for (var i = 0; i < options.length; i++) {
        delete options[i]['$$hashKey'];
      }
    }
    data.options = options;
    return $http({
      url: factory.apiRoot + 'question/data',
      method: 'POST',
      data: {
        questionId: questionId,
        data: JSON.stringify(data)
      }
    })
  };

  factory.deleteAnswerByReferenceId = function (referenceId) {
    return $http({
      url: factory.newApiRoot + "answer",
      method: "DELETE",
      data: {
        reference_id: referenceId,
      }
    })
  };

  factory.deleteAnswer = function (questionId, answerValue, callback) {
    return $http({
      url: factory.newApiRoot + 'answer?' + makeQS({
        "where": "answer:" + answerValue + ",question_id:" + questionId
      }),
      method: 'GET'
    }).then(function (r) {
      console.log("answer search respones: ", r)
      var answer = r.data.data[0];
      factory.deleteAnswerByReferenceId(answer.reference_id).then(function (r) {
        callback(r);
      });
      console.log("need to delete this answer", r)
    })
  };

  factory.deleteQuestionFromQuestionnaire = function (questionnaireReferenceId, questionId) {
    return $http({
      url: factory.apiRoot + 'questionnaire/' + questionnaireReferenceId + '/question/' + questionId,
      data: {
        category: questionnaireReferenceId,
        questionId: questionId
      },
      method: 'DELETE'
    })
  };
  factory.addQuestionToQuestionnaire = function (referenceId, questionId) {
    return $http({
      url: factory.newApiRoot + 'questionnaire_question',
      method: "POST",
      data: {
        reference_id: referenceId,
        question_id: questionId
      }
    })
  };

  factory.addQuestion = function (question) {
    return $http({
      url: factory.newApiRoot + 'question',
      method: 'POST',
      data: {
        question: question,
        type: 'single_string_choice'
      }
    });
  };
  return factory;
}]);

AdminModule.factory('Log', ['Data', function (Data) {
  var factory = {};
  factory.log = function () {
    console.log.apply(console, arguments);
  };

  factory.error = function () {
    bootbox.alert(JSON.stringify(arguments));
  };

  factory.info = function () {
    console.log.apply(console, arguments);
  };

  return factory;


}]);
var SurveyFactory = angular.module('SurveyFactory', []);

SurveyFactory.factory("Survey", ['$http', 'Data', function ($http, Data) {
  var factory = {};
  factory.root = Data.apiRoot("survey");
  factory.start = function (referenceId) {
    return $http({
      url: factory.root + "/start/" + referenceId,
      method: 'POST'
    });
  };
  factory.getSuggestions = function (surveyId) {
    return $http({
      url: factory.root + "/getSuggestions",
      params: {
        surveyId: surveyId
      }
    })
  };


  factory.getProductSplit = function (surveyId, productName, subcategory) {
    return $http({
      url: factory.root + "/getSplit",
      params: {
        surveyId: surveyId,
        productName: productName,
        subcategory: subcategory,
      }
    })
  };
  factory.updateAnswer = function (surveyId, questionId, answer) {
    return $http({
      url: factory.root + "/updateAnswer",
      method: 'POST',
      params: {
        surveyId: parseInt(surveyId),
        questionId: parseInt(questionId),
        answer: answer
      }
    })
  };

  factory.getBotResponse = function(surveyId, questionnaireId, questionId, answer) {
    return $http({
      url:  "/messenger/getBotResponse",
      method: 'POST',
      params: {
        surveyId: parseInt(surveyId),
        questionId: parseInt(questionId),
        questionnaireId: questionnaireId, 
        answer: answer
      }
    })
  }

  factory.getQuestions = function (surveyId) {
    console.log("get questions for ", surveyId);
    return $http({
      url: factory.root + "/details",
      params: {
        surveyId: surveyId
      }
    });
  };

  factory.list = function () {
    return $http({
      url: factory.root + "/list"
    })
  };
  return factory;
}]);

var DataFactory = angular.module("DataFactory", []);
DataFactory.factory('Data', function () {
  var factory = {};
  factory.partials = function () {
    return Constants.rootPath + "app/views/";
  };

  factory.root = function (path) {
    return Constants.rootPath + path;
  };

  factory.adminApiRoot = function () {
    return Constants.adminApiRootPath + "/";
  };
  factory.apiRoot = function (path) {
    return Constants.apiRootPath + path;
  };
  factory.newApiRoot = function (path) {
    return Constants.newApiRoot + path;
  };


  return factory;
});


mainModule.factory("categoryFactory", ['$http', 'Data', function ($http, Data) {
  var factory = {};
  factory.root = Data.apiRoot("category");
  factory.getCategoryList = function (params) {

    var urlParams = "?";
    if (params) {
      var keys = Object.keys(params);
      for (var i = 0; i < keys.length; i++) {
        urlParams = urlParams + keys[i] + "=" + params[keys[i]] + "&"
      }
    }

    return $http.get(factory.root + "/list" + urlParams);
  };
  return factory;
}]);


