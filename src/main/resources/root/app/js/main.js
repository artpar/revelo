/***
 Metronic AngularJS App Main Script
 ***/

function random(items, length) {
  if (!length || length < 2) {

    return items[Math.floor(Math.random() * items.length)];
  } else {

    return items[Math.floor(Math.random() * items.length)] + random(items, length - 1);
  }
}

function pastel_colour(input_str) {

  //TODO: adjust base colour values below based on theme
  var baseRed = 128;
  var baseGreen = 128;
  var baseBlue = 128;

  //lazy seeded random hack to get values from 0 - 256
  //for seed just take bitwise XOR of first two chars
  var seed = input_str.charCodeAt(0) ^ input_str.charCodeAt(1);
  var rand_1 = Math.abs((Math.sin(seed++) * 10000)) % 256;
  var rand_2 = Math.abs((Math.sin(seed++) * 10000)) % 256;
  var rand_3 = Math.abs((Math.sin(seed++) * 10000)) % 256;

  //build colour
  var red = Math.round((rand_1 + baseRed) / 2);
  var green = Math.round((rand_2 + baseGreen) / 2);
  var blue = Math.round((rand_3 + baseBlue) / 2);

  return "#" + red.toString(16) + green.toString(16) + blue.toString(16);
}

/* Metronic App */
var MetronicApp = angular.module("MetronicApp", [
  "ui.router",
  "ui.bootstrap",
  "oc.lazyLoad",
  "ngSanitize",
  'ngRoute',
  'mainModule',
  'ngCookies',
  'auth0',
  'angular-storage',
  'angular-jwt',
  'angulartics', 'angulartics.google.analytics',
  'angularUtils.directives.dirPagination'
]);

var app = MetronicApp;

var mainModule = angular.module('mainModule', ['SurveyFactory', 'DataFactory', 'auth0', 'angular-storage', 'angular-jwt', 'AdminModule', 'ui.bootstrap']);


/* Configure ocLazyLoader(refer: https://github.com/ocombe/ocLazyLoad) */
MetronicApp.config(['$ocLazyLoadProvider', function ($ocLazyLoadProvider) {
  $ocLazyLoadProvider.config({
    // global configs go here
  });
}]);

var tokenQueue = [];

MetronicApp.config(['$routeProvider', 'authProvider', '$httpProvider',
  '$locationProvider', 'jwtInterceptorProvider', '$analyticsProvider',
  function ($routeProvider, authProvider, $httpProvider,
            $locationProvider, jwtInterceptorProvider, $analyticsProvider) {
    var root = Constants.rootPath + "resources/js/app/partials/";
    $analyticsProvider.virtualPageviews(true);
    authProvider.init({
      domain: 'revelo.auth0.com',
      clientID: 'Ag9rX5XhV6YqyF11jnSyj2JqmAqmluxT',
      loginUrl: "/login",
      scope: 'openid profile email name'
    });

    //app.js
    authProvider.on('loginSuccess', function ($location, profilePromise, idToken, store, $rootScope) {
      console.log("Login Success, resolve this queue: ", tokenQueue, tokenQueue.length);
      store.set('token', idToken);
      for (var i = 0; i < tokenQueue.length; i++) {
        tokenQueue[i].resolve(idToken);
      }
      profilePromise.then(function (profile) {
        store.set('profile', profile);
        //$rootScope.settings.profile = profile;
        $rootScope.$broadcast('gotProfile', profile);
        window.location = "/root/";
      });
      $location.path('/');
    });

    authProvider.on('loginFailure', function () {
      for (var i = 0; i < tokenQueue.length; i++) {
        tokenQueue[i].reject(null);
      }
      $location.path("/login");
      // Error Callback
    });

    jwtInterceptorProvider.tokenGetter = ['store', '$q', '$rootScope', '$state', function (store, $q, $rootScope, $state) {
      // console.log("token getter", $state)

      var t = store.get("token");
      if (t) {
        return t;
      }


      if (!$state.requiresLogin) {
        return null;
      }
      var promise = function (resolve, reject) {
        var ob = {
          resolve: resolve,
          reject: reject
        };
        tokenQueue.push(ob);
      };
      return $q(promise);
    }];

    // Add a simple interceptor that will fetch all requests and add the jwt token to its authorization header.
    // NOTE: in case you are calling APIs which expect a token signed with a different secret, you might
    // want to check the delegation-token example
    $httpProvider.interceptors.push('jwtInterceptor');
  }
]).run(['auth', 'jwtInterceptor', function (auth, jwtInterceptor) {
  auth.hookEvents();

}]).run(function ($rootScope, auth, store, jwtHelper, $location, $analytics, DataProvider, GroupService, $state) {
  //if (!$rootScope.settings) {
  //    $rootScope.settings = {};
  //}
  $rootScope.$on('gotProfile', function (e, profile) {
    $rootScope.settings.profile = profile;
  });
  //$rootScope.settings.profile = store.get('profile');
  $rootScope.$on('$locationChangeEnd', function () {
    console.log("location change end")
  });
  $rootScope.$on('$locationChangeStart', function (eve, currRoute, prevRoute) {
    console.log("location change start", currRoute.$$route, $state);
    console.log("Route object", $state);


    if (!auth.isAuthenticated) {
      var token = store.get('token');
      if (token) {
        if (!jwtHelper.isTokenExpired(token)) {
          auth.authenticate(store.get('profile'), token);
          $rootScope.settings.profile = store.get('profile');
          DataProvider.getMe().success(function (me) {
            console.log("get me", me);
            $rootScope.reveloUser = me;
            DataProvider.getMyGroups(me.reference_id).success(function (groups) {
              var groupNames = groups.map(function (e, i) {
                return e.usergroup[0].name;
              });
              console.log("user groups", groups, groupNames);
              $rootScope.reveloUserGroups = groupNames;
              GroupService.setGroups(groupNames);
              $rootScope.$broadcast("permissions.changed")
            }).error(function (e) {
              console.log("failed to get usergroups", e, arguments);
            });

          });
        } else {
          store.set("token", null);
          $location.path('/login');
        }
      } else {
        if (!$state.requiresLogin) {
          return;
        } else {
          $location.path('/login');
        }
      }
    } else {

    }
  });
  $rootScope.$on('app.unauthorised', function () {
    console.log("app is unauthorized", arguments);
    //window.location = "/root/app/index.html#/dashboard.html";
  })
}).config(function ($analyticsProvider) {
  $analyticsProvider.firstPageview(true);
  /* Records pages that don't use $state or $route */
  $analyticsProvider.withAutoBase(true);
  /* Records full path */
});
;

/********************************************
 BEGIN: BREAKING CHANGE in AngularJS v1.3.x:
 *********************************************/
/**
 `$controller` will no longer look for controllers on `window`.
 The old behavior of looking on `window` for controllers was originally intended
 for use in examples, demos, and toy apps. We found that allowing global controller
 functions encouraged poor practices, so we resolved to disable this behavior by
 default.

 To migrate, register your controllers with modules rather than exposing them
 as globals:

 Before:

 ```javascript
 function MyController() {
  // ...
}
 ```

 After:

 ```javascript
 angular.module('myApp', []).controller('MyController', [function() {
  // ...
}]);

 Although it's not recommended, you can re-enable the old behavior like this:

 ```javascript
 angular.module('myModule').config(['$controllerProvider', function($controllerProvider) {
  // this option might be handy for migrating old apps, but please don't use it
  // in new ones!
  $controllerProvider.allowGlobals();
}]);
 **/

//AngularJS v1.3.x workaround for old style controller declarition in HTML
MetronicApp.config(['$controllerProvider', function ($controllerProvider) {
  // this option might be handy for migrating old apps, but please don't use it
  // in new ones!
  $controllerProvider.allowGlobals();
}]);

/********************************************
 END: BREAKING CHANGE in AngularJS v1.3.x:
 *********************************************/

/* Setup global settings */
MetronicApp.factory('settings', ['$rootScope', function ($rootScope) {
  // supported languages
  var settings = {
    layout: {
      pageSidebarClosed: false, // sidebar menu state
      pageContentWhite: true, // set page content layout
      pageBodySolid: false, // solid body color state
      pageAutoScrollOnLoad: 1000 // auto scroll to top on page load
    },
    assetsPath: '../assets',
    globalPath: '../assets/global',
    layoutPath: '../assets/layouts/layout4',
  };

  $rootScope.settings = settings;

  return settings;
}]);

/* Setup App Main Controller */
MetronicApp.controller('AppController', ['$scope', '$rootScope', function ($scope, $rootScope) {
  $scope.$on('$viewContentLoaded', function () {
    App.initComponents(); // init core components
    //Layout.init(); //  Init entire layout(header, footer, sidebar, etc) on page load if the partials included in server side instead of loading with ng-include directive
  });
}]);

/***
 Layout Partials.
 By default the partials are loaded through AngularJS ng-include directive. In case they loaded in server side(e.g: PHP include function) then below partial
 initialization can be disabled and Layout.init() should be called on page load complete as explained above.
 ***/

/* Setup Layout Part - Header */
MetronicApp.controller('HeaderController', ['$scope', function ($scope) {
  $scope.$on('$includeContentLoaded', function () {
    Layout.initHeader(); // init header
  });
}]);

/* Setup Layout Part - Sidebar */
MetronicApp.controller('SidebarController', ['$scope', function ($scope) {
  $scope.$on('$includeContentLoaded', function () {
    Layout.initSidebar(); // init sidebar
  });
}]);

/* Setup Layout Part - Sidebar */
MetronicApp.controller('PageHeadController', ['$scope', function ($scope) {
  $scope.$on('$includeContentLoaded', function () {
    Demo.init(); // init theme panel
  });
}]);

/* Setup Layout Part - Quick Sidebar */
MetronicApp.controller('QuickSidebarController', ['$scope', function ($scope) {
  $scope.$on('$includeContentLoaded', function () {
    setTimeout(function () {
      QuickSidebar.init(); // init quick sidebar
    }, 2000)
  });
}]);

/* Setup Layout Part - Theme Panel */
MetronicApp.controller('ThemePanelController', ['$scope', function ($scope) {
  $scope.$on('$includeContentLoaded', function () {
    Demo.init(); // init theme panel
  });
}]);

/* Setup Layout Part - Footer */
MetronicApp.controller('FooterController', ['$scope', function ($scope) {
  $scope.$on('$includeContentLoaded', function () {
    Layout.initFooter(); // init footer
  });
}]);

/* Setup Rounting For All Pages */
MetronicApp.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
  // Redirect any unmatched url
  $urlRouterProvider.otherwise("/dashboard.html");
  var root = Constants.rootPath + "app/views/";

  $stateProvider

  // Dashboard
    .state('dashboard', {
      url: "/dashboard.html",
      templateUrl: "views/dashboard.html",
      data: {pageTitle: 'Home'},
      controller: "HomeController",
      resolve: {
        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
          return $ocLazyLoad.load({
            name: 'ReveloApp',
            insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
            files: [
              "../assets/pages/css/profile.min.css",
              'js/controllers/HomeController.js',
            ]
          });
        }]
      }
    })
    .state('crud', {
      url: "/crud/:type",
      templateUrl: root + "crud.html",
      data: {pageTitle: 'Crud'},
      controller: "CrudController",
      resolve: {
        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
          return $ocLazyLoad.load({
            name: 'ReveloApp',
            insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
            files: [
              "../assets/global/plugins/datatables/datatables.min.js",
              "../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css",
              "../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css",
              "../assets/global/plugins/datatables/datatables.min.css",
              'js/controllers/CrudController.js',
              "../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js",
              "../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js",
            ],
            serie: true
          });
        }]
      }
    })

    .state('surveys', {
      url: "/surveys",
      templateUrl: "views/surveys.html",
      data: {pageTitle: 'Home'},
      controller: "DashboardController",
      resolve: {
        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
          return $ocLazyLoad.load({
            name: 'ReveloApp',
            insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
            files: [
              'js/controllers/DashboardController.js',
            ]
          });
        }]
      }
    })
    .state('questionnaires', {
      url: "/questionnaires",
      templateUrl: "views/questionnaires.html",
      data: {pageTitle: 'Home'},
      controller: "DashboardController",
      resolve: {
        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
          return $ocLazyLoad.load({
            name: 'ReveloApp',
            insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
            files: [
              'js/controllers/IndexController.js',
              'js/controllers/DashboardController.js',
            ]
          });
        }]
      }
    })
    .state("login", {
      url: "/login",
      templateUrl: "views/login.html",
      data: {pageTitle: 'Login'},
      controller: "LoginController",
      resolve: {
        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
          return $ocLazyLoad.load({
            name: 'MetronicApp',
            insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
            files: [
              'js/controllers/LoginController.js',
            ]
          });
        }]
      }
    })

    .state("categories", {
      url: '/categories',
      templateUrl: root + 'categories.html',
      controller: 'CategoryController',
      requiresLogin: true,
      data: {pageTitle: 'Categories'},
      resolve: {
        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
          return $ocLazyLoad.load({
            name: 'MetronicApp',
            insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
            files: [
              'js/controllers/CategoryController.js',
            ]
          });
        }]
      }
    })

    .state("category", {
      url: '/category/:referenceId',
      templateUrl: root + 'category.html',
      controller: 'CategoryController',
      requiresLogin: true,
      data: {pageTitle: 'Category'},
      resolve: {
        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
          return $ocLazyLoad.load({
            name: 'MetronicApp',
            insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
            files: [
              'js/controllers/CategoryController.js',
            ]
          });
        }]
      }
    })

    .state("questionnaire", {
      url: '/questionnaire/:referenceId',
      templateUrl: root + 'questionnaire.html',
      controller: 'QuestionnaireController',
      requiresLogin: true,
      data: {pageTitle: 'Questionnaire'},
      resolve: {
        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
          return $ocLazyLoad.load({
            name: 'MetronicApp',
            insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
            files: [
              "../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js",
              "../assets/global/plugins/typeahead/typeahead.bundle.min.js",
              'js/controllers/QuestionnaireController.js',
            ]
          });
        }]
      }
    })
    .state('user', {
      url: '/user',
      templateUrl: root + "user.html",
      controller: "UserProfileController",
      requiresLogin: true,
      data: {pageTitle: "User Profile"},
      resolve: {
        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
          return $ocLazyLoad.load({
            name: "Revelo",
            insertBefore: "#ng_load_plugins_before",
            files: [
              "../assets/pages/css/profile.min.css",
              "js/controllers/UserProfileController.js"
            ]
          })
        }]
      }
    })
    .state('survey', {
      url: '/survey/:id/:name',
      templateUrl: root + "survey.html",
      controller: 'SurveyController',
      requiresLogin: false,
      data: {pageTitle: 'Survey'},
      resolve: {
        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
          return $ocLazyLoad.load({
            name: 'MetronicApp',
            insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
            files: [
              "../assets/global/plugins/bootstrap-multiselect/js/bootstrap-multiselect.js",
              "../assets/global/plugins/bootstrap-multiselect/css/bootstrap-multiselect.css",
              "../assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js",
              'views/question.html',
              'js/controllers/IndexController.js',
              'js/controllers/SurveyController.js'
            ]
          });
        }]
      }
    })

    .state("logout", {
      url: "/logout",
      controller: 'LogoutController',
      templateUrl: "views/logout.html",
      data: {pageTitle: 'Logout'},
      resolve: {
        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
          return $ocLazyLoad.load({
            name: 'MetronicApp',
            insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
            files: [
              'js/controllers/LogoutController.js',
            ]
          });
        }]
      }
    });
  // User Profile Account
}]);

/* Init global settings and run the app */
MetronicApp.run(["$rootScope", "settings", "$state", function ($rootScope, settings, $state) {
  $rootScope.$state = $state; // state to be accessed from view
  $rootScope.$settings = settings; // state to be accessed from view
}]);