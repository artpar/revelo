/**
 * Created by parth on 28/4/16.
 */



angular.module('MetronicApp').controller('LogoutController', function ($scope, auth, $location, store) {
    //auth.getToken({
    //    scope: 'openid name email photo'
    //});
    auth.signout();
    store.remove('profile');
    store.remove('token');
    $location.path('/login');
});
