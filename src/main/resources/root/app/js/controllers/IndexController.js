/**
 * Created by parth.mudgal on 11/07/14.
 */


angular.module("MetronicApp").directive("selector", ['Data', function (Data) {
    function link($scope, element, attrs) {
        element.ready(function () {
            $scope.select = function (item) {
                console.log("do select", item);
                $scope.itemChanged({item: item});
            };
        });
    }

    return {
        restrict: 'E',
        link: link,
        templateUrl: Data.partials() + "selector/selector.html",
        scope: {
            data: '=data',
            itemChanged: '&'
        },
    }
}]);


angular.module('MetronicApp').directive('question', ['Data', '$timeout', function (Data, $timeout) {
    function link(scope, element, attrs) {
        element.ready(function () {
            var promise;
            scope.$watch('data.answer', function (newVal, oldVal) {
                if (newVal == oldVal) {
                    return;
                }
                $timeout.cancel(promise);
                promise = $timeout(function () {
                    scope.answerChanged({arg1: scope.data});
                }, 300);
            })
        })
    }

    return {
        restrict: 'E',
        link: link,
        templateUrl: Data.partials() + "question.html",
        scope: {
            data: '=data',
            imageServerRoot: '=',
            answerChanged: '&'
        }
    }
}]);

angular.module('MetronicApp').directive('answerSelection', ['Data', function (Data) {
    function link(scope, element, attrs) {
        scope.contentUrl = Data.partials() + 'answer/' + scope.data.type + '.html';

        console.log("current answer", scope.data.answer);

        scope.selectedAnswers = [];

        var answerOptions = scope.data.internalObject.options;
        if (!answerOptions) {
            answerOptions = [];
        }

        if (scope.data.answer instanceof Array) {
            for (var i = 0; i < scope.data.answer.length; i++) {
                var t = scope.data.answer[i];
                for (var j = 0; j < answerOptions.length; j++) {
                    if (answerOptions[j].value == t) {
                        answerOptions[j].selected = true;
                        scope.selectedAnswers.push(scope.data.internalObject.options[j]);
                    }
                }
            }
        } else {
            var t = scope.data.answer;
            for (var j = 0; j < answerOptions.length; j++) {
                if (answerOptions[j].value == t) {
                    answerOptions[j].selected = true;
                    scope.selectedAnswers = [scope.data.internalObject.options[j]];
                    break;
                }
            }
        }

        scope.addAnswer = function (a) {
            console.log("Add answer", a);
            if (a.selected) {
                a.selected = false;
                // scope.selectedAnswers.forEach(function (e) {
                //   if (e.value == a.value) {
                //     e.selected = false;
                //   }
                // });

                var indexToRemove = scope.data.answer.indexOf(a.value);
                scope.data.answer.splice(indexToRemove, 1);
                // scope.answerChanged({arg1: scope.data});
                // return;

            } else {
              if (!scope.data.answer) {
                scope.data.answer = [];
              }

              if (scope.data.answer.push) {

                var newArray = [];
                for (var e = 0; e < scope.data.answer.length; e++) {
                  var answer = scope.data.answer[e];
                  if (answer == null) {
                    continue;
                  }
                  newArray.push(answer)
                }
                newArray.push(a.value);

                scope.data.answer = newArray;
              } else {
                if (a !== null) {
                  scope.data.answer = [a]
                } else {
                  scope.data.answer = [];
                }
              }

              scope.selectedAnswers.push(a);
              a.selected = true;
            }


            scope.answerChanged({arg1: scope.data});
        };


        scope.setAnswer = function (a) {


            if (a.selected) {
                a.selected = false;
                var toremove = -1;
                for (var u = 0; u < scope.selectedAnswers.length; u++) {
                    if (scope.selectedAnswers[u].value == a.value) {
                        toremove = u;
                        break;
                    }
                }
                if (toremove > -1) {
                    scope.selectedAnswers.splice(toremove, 1);
                }


                var indexToRemove = scope.data.answer.indexOf(a.value);
                scope.data.answer = null;
                scope.answerChanged({arg1: scope.data});
                return;

            }


            if (scope.selectedAnswers) {
                for (var e = 0; e < scope.selectedAnswers.length; e++) {
                    scope.selectedAnswers[e].selected = false;
                }
            }

            scope.data.answer = a.value;
            scope.selectedAnswers = [a];
            a.selected = true;
            scope.answerChanged({arg1: scope.data});
        };

        scope.$watch('$data.answer', function () {
            console.log("answer changed", arguments);
        });
    }

    return {
        restrict: 'E',
        link: link,
        template: '<div ng-include="contentUrl"></div>',
        scope: {
            data: '=',
            answerChanged: '&',
            imageServerRoot: '=',
        }
    }
}]);
