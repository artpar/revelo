angular.module('MetronicApp').controller('UserProfileController', function ($rootScope, $scope, $http, $timeout, DataProvider) {
    $scope.$on('$viewContentLoaded', function () {
        App.initAjax(); // initialize core components
        Layout.setSidebarMenuActiveLink('set', $('#sidebar_menu_link_profile')); // set profile link active in sidebar menu 
    });


    $scope.user = $rootScope.settings.profile;

    DataProvider.getMe().success(function(u){
        $scope.user = u;
    });

    DataProvider.getUserStats().success(function(s){
        $scope.stats = s;
    });

    console.log("user", $rootScope.settings);
});
