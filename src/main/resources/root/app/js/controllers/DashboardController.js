angular.module('MetronicApp').controller("DashboardController", function (auth, $rootScope, $scope, $location, Survey, DataProvider, Data, Log) {
    $scope.listUrl = Data.partials() + "survey_list.html";
    $scope.$on('$viewContentLoaded', function () {
        // initialize core components
        App.initAjax();
    });

    $scope.imageServerRoot = "https://images.revlo.inscripta.ai";

    if (window.location.hostname == "localhost") {
        $scope.imageServerRoot = "http://localhost:9333";
    }

    function loadSurveys() {
        DataProvider.get("survey/mine", {children: "questionnaire_id"}).success(function (d) {
            $scope.surveyList = d.data;
        });
    }

    loadSurveys();
    $scope.selectCategory = function(category) {
        console.log("category", arguments);
    };
    $scope.newQuestionnaire = function(c) {
        DataProvider.new("questionnaire", { category_id: c.reference_id, name: name})
    };


    $scope.updateQuestionnaireList = function () {
        DataProvider.get('questionnaire/mine', {children: 'category_id', limit: 50}).error(function () {
            Log.error("Error - " + arguments);
        }).success(function (data) {
            console.log("response", data);
            $scope.questionnaireList = data.data;
        });
    };


    $scope.updateQuestionnaireList();

    $scope.start = function () {
        console.log("selection - ", $scope.selection);
        Survey.start($scope.selection.name).error(function () {
            alert("failed");
        }).success(function (data) {
            console.log(data);
            $location.path("/survey/" + data.id);
            loadSurveys();
        });
    }
});

