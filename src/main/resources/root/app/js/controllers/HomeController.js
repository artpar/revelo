/**
 * Created by parth on 28/4/16.
 */


angular.module('MetronicApp').controller("HomeController",
  function (auth, $rootScope, $scope, $location, Survey, DataProvider, Data, Log, $analytics) {
    console.log("home controller");
    $analytics.eventTrack('HomePageLoad');
    $rootScope.settings.layout.pageSidebarClosed = true;
    $scope.imageServerRoot = "https://images.revlo.inscripta.ai";

    if (window.location.hostname == "localhost") {
      $scope.imageServerRoot = "http://localhost:9333";
    }


    $scope.startSurvey = function (referenceId) {
      Survey.start(referenceId).success(function (survey) {
        window.location = window.location.href.split("#")[0] + "#/survey/" + survey.id + "/" + survey.categoryName
      }).error(function (error) {
        Log.error("Failed to start survey", error)
      });
    };

    function gotoTop(element) {
      element.scrollTop = 0
    };

    $scope.setCategory = function (c) {
      $scope.category = c;
      gotoTop(document.documentElement);
    }


    // get all the categories
    DataProvider.get("category", {limit: 100}).success(function (d) {
      console.log("got category list", d.data);
      $scope.categoryList = d.data;
    });


    // get recent surveys of the user
    DataProvider.get("survey/mine", {children: "questionnaire_id", limit: 30, order: "created_at:desc"}).success(function (d) {
      $scope.userSurveyList = d.data;
    });

    $scope.categories = [];


    DataProvider.get('questionnaire', {
      children: 'category_id,user_id',
      limit: 50
    }).success(function (questionnaireListResponse) {

      function setImage(name, i) {
        DataProvider.getImages(name).success(function (e) {
          $scope.questionnaireList[i].images = e;
          $scope.questionnaireList[i].image = random(e);
          $scope.questionnaireList[i].color = pastel_colour(name)
        });
      }

      var questionnaireList = questionnaireListResponse.data;
      var categoryMap = {};
      for (var i = 0; i < questionnaireList.length; i++) {
        var questionnaire = questionnaireList[i];
        setImage(questionnaire.category[0].name, i);
        if (!categoryMap[questionnaire.category[0].name]) {
          $scope.categories.push(questionnaire.category[0]);
          categoryMap[questionnaire.category[0].name] = true;
        }
      }

      $scope.refreshImages = function (i, category) {
        DataProvider.getImages(category.name).success(function (c) {
          $scope.categories[i].image = random(c);
          // category.image = random(c);
        });
      };

      function refreshCategories() {
        var c = $scope.categories;
        for (var i = 0; i < c.length; i++) {
          $scope.refreshImages(i, c[i]);
        }
      }

      refreshCategories();

      $scope.questionnaireList = questionnaireListResponse.data;

      // $("select").select2();

    });




    $scope.category = null;

    $scope.resumeSurvey = function (survey) {
      window.location = window.location.href.split("#")[0] + "#/survey/" + survey.id + "/" + $scope.selectedCategory.name;
    };

    $scope.$watch('category', function (category) {
      if (!category) {
        return;
      }
      if (!$scope.questionnaireList) {
        return;
      }

      var selectedCategory = $scope.categoryList.filter(function (e, i) {
        return e.name == category;
      })[0];
      console.log("ca changed", category, selectedCategory);
      $scope.selectedCategory = category;


      if ($scope.userSurveyList) {
        $scope.surveyList = $scope.userSurveyList.filter(function (e, i) {
          return e.questionnaire[0].category_id == $scope.selectedCategory.id;
        });
      }

      $scope.questionnaires = $scope.questionnaireList.filter(function (e, i) {
        return e.category[0].id == $scope.selectedCategory.id;
      })
    })
  }
);
