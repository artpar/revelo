/**
 * Created by parth on 28/4/16.
 */

function makeTable(obj, titles) {
  var table = $("<table class='table table-bordered table-hover'></table>");
  var thead = $("<thead></thead>");
  var tr = $("<tr></tr>");
  for (var i = 0; i < titles.length; i++) {
    tr.append($("<th></th>").text(titles[i]));
  }
  thead.append(tr);
  var keys = Object.keys(obj);
  var tbody = $("<tbody></tbody>");
  for (var i = 0; i < keys.length; i++) {
    var tr = $("<tr></tr>");
    tr.append($("<td></td>").html(keys[i]));
    tr.append($("<td></td>").html(objToText(obj[keys[i]])));
    tbody.append(tr);
  }
  table.append(thead);
  table.append(tbody);
  return table;
}


function objToText(obj) {
  var str = "";
  if (!obj) {
    return "<empty>"
  }
  var keys = Object.keys(obj);
  for (var i = 0; i < keys.length; i++) {
    str = str + keys[i] + ": " + obj[keys[i]] + "<br>\n"
  }
  return str;
}


angular.module('MetronicApp').filter('cut', function () {
  return function (value, wordwise, max, tail) {
    if (!value) return '';

    max = parseInt(max, 10);
    if (!max) return value;
    if (value.length <= max) return value;

    value = value.substr(0, max);
    if (wordwise) {
      var lastspace = value.lastIndexOf(' ');
      if (lastspace !== -1) {
        //Also remove . and , so its gives a cleaner result.
        if (value.charAt(lastspace-1) === '.' || value.charAt(lastspace-1) === ',') {
          lastspace = lastspace - 1;
        }
        value = value.substr(0, lastspace);
      }
    }

    return value + (tail || ' …');
  };
});

angular.module('MetronicApp').controller('SurveyController', ['$rootScope', 'auth', '$scope', '$stateParams', 'Survey', 'DataProvider', function ($rootScope, auth, $scope, $stateParams, Survey, DataProvider) {
  $scope.auth = auth;
  // $scope.name = $stateParams.id;
  $scope.surveyId = $stateParams.id;
  $scope.name = $stateParams.name.replace(/ /g, "+");
  $rootScope.settings.layout.pageSidebarClosed = true;
  $scope.suggestions = [];
  $scope.imageServerRoot = "https://images.revlo.inscripta.ai";
  $scope.restOfTheUrl = window.location.host;

  $scope.chatHistory = [];
  $scope.chatInput = ""; 

  if (window.location.hostname == "localhost") {
    $scope.imageServerRoot = "http://localhost:9333";
  }

  DataProvider.get('survey', {where: "id:" + $scope.surveyId, children: 'questionnaire_id'}).success(function (e) {
    $scope.survey = e.data[0];
    console.log("survey is ", e);
  });

  var input = document.getElementById("chatMessage");
  input.addEventListener("keyup", function(event) {
    event.preventDefault();
    if (event.keyCode === 13) {
        document.getElementById("chatButton").click();
    }
  });



  /**
   * AmCharts plugin: Auto-calculate color based on value
   * The plugin relies on custom chart propety: `colorRanges`
   */
  AmCharts.addInitHandler(function(chart) {

    var dataProvider = chart.dataProvider;
    var colorRanges = chart.colorRanges;

    // Based on https://www.sitepoint.com/javascript-generate-lighter-darker-color/
    function ColorLuminance(hex, lum) {

      // validate hex string
      hex = String(hex).replace(/[^0-9a-f]/gi, '');
      if (hex.length < 6) {
        hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
      }
      lum = lum || 0;

      // convert to decimal and change luminosity
      var rgb = "#",
        c, i;
      for (i = 0; i < 3; i++) {
        c = parseInt(hex.substr(i * 2, 2), 16);
        c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
        rgb += ("00" + c).substr(c.length);
      }

      return rgb;
    }

    if (colorRanges) {

      var item;
      var range;
      var valueProperty;
      var value;
      var average;
      var variation;
      for (var i = 0, iLen = dataProvider.length; i < iLen; i++) {

        item = dataProvider[i];

        for (var x = 0, xLen = colorRanges.length; x < xLen; x++) {

          range = colorRanges[x];
          valueProperty = range.valueProperty;
          value = item[valueProperty];

          if (value >= range.start && value <= range.end) {
            average = (range.start - range.end) / 2;

            if (value <= average)
              variation = (range.variation * -1) / value * average;
            else if (value > average)
              variation = range.variation / value * average;

            item[range.colorProperty] = ColorLuminance(range.color, variation.toFixed(2));
          }
        }
      }
    }

  }, ["serial"]);


  document.getElementById("chatMessage").addEventListener("keyup", function(event) {
    event.preventDefault();
    var matchIndexArr = [];
    if (event.keyCode === 13) {
      document.getElementById("chatButton").click();
    }
    else if($scope.questions[0] && !($scope.questions[0].type=="single_string" || $scope.questions[0].type=="single_integer")){
      var searchStr = $(this).val();
      var answers1 = $scope.questions[0].internalObject.options;
      for (i = 0; i < answers1.length; i++) {
        if (answers1[i].label.toLowerCase().indexOf(searchStr.toLowerCase()) != -1) {
          matchIndexArr.push(i);
        }
      }
      if (matchIndexArr.length == 1) {
        $scope.questions[0].answer = (matchIndexArr[0]+1).toString();
        $scope.updateAnswer($scope.questions[0]);
      }
    }
  });

  window.onresize = function(){
    $scope.gotoBottom("chatz");
    //$scope.chatWindowHeight = window.innerHeight - 240;
    //document.getElementById("chatWindow").style.max-height = $scope.chatWindowHeight +";
  };

  window.onload = function() {
     $scope.gotoBottom("chatz");
     $scope.nextQuestion();
  }


  $scope.showSplit = function (e, s_index) {
    if (e.split) {
      $("#product_" + s_index).toggle();
      return;
    }

    var chartBoxId = "product_chart_" + s_index;
    var chartBox = document.getElementById(chartBoxId)
    console.log("suggestion", e);
    Survey.getProductSplit($scope.survey.id, e.product.name, e.product.companyName).success(function (a) {
      console.log("split", a);
      e.split = a.featureScoreMap;
      e.positiveAnswerSplit = a.positiveAnswerFeaturewiseScoreList;
      e.negativeAnswerSplit = a.negativeAnswerFeaturewiseScoreList;

      var splitData = [];
      var scoreMapKeys = Object.keys(a.featureScoreMap);
      for (var i = 0; i < scoreMapKeys.length; i++) {
        var key = scoreMapKeys[i];
        var keyData = a.featureScoreMap[key];
        splitData.push({
          name: key,
          value: keyData.value,
          score: keyData.score,
          weightedScore: keyData.weightedScore,
        })
      }


      setTimeout(function () {
        var chart = AmCharts.makeChart(chartBoxId, {
          "type": "serial",
          "theme": "light",

          "colorRanges": [{
            "start": -10000,
            "end": 10000,
            "color": "#0080FF",
            "variation": 0.6,
            "valueProperty": "weightedScore",
            "colorProperty": "color"
          }],

          "dataDateFormat": "YYYY-MM-DD",
          "dataProvider": splitData,

          "addClassNames": true,
          "startDuration": 1,
          //"color": "#FFFFFF",
          "marginLeft": 0,
          "categoryField": "name",
          "categoryAxis": {
            "parseDates": false,
            "autoGridCount": false,
            "gridCount": 50,
            "autoRotateCount": 4,
            "autoRotateAngle": -35,
            "gridAlpha": 0.1,
            "gridColor": "#FFFFFF",
            "axisColor": "#555555"
          },

          "valueAxes": [{
            "id": "a1",
            "title": "Score",
            "gridAlpha": 0,
            "axisAlpha": 0
          }, {
            "id": "a2",
            "position": "right",
            "gridAlpha": 0,
            "axisAlpha": 0,
            "labelsEnabled": false
          }, {
            "id": "a3",
            "title": "Weighted Score",
            "position": "right",
            "gridAlpha": 0,
            "axisAlpha": 0,
            "inside": false
          }],
          "graphs": [{
            "id": "g1",
            "valueField": "score",
            "title": "score",
            "type": "column",
            "fillAlphas": 0.9,
            "valueAxis": "a1",
            "balloonText": "[[value]] points",
            "legendValueText": "[[value]] pts",
            "legendPeriodValueText": ": [[value.sum]]",
            "lineColor": "#94d478",
            "alphaField": "alpha"
          }, {
            "id": "g2",
            "valueField": "weightedScore",
            "classNameField": "bulletClass",
            "title": "feature",
            "type": "line",
            "valueAxis": "a2",
            "lineColor": "#786c56",
            "lineThickness": 1,
            "legendValueText": "[[value]]/[[description]]",
            "descriptionField": "feature",
            "bullet": "round",
            "bulletSizeField": "townSize",
            "bulletBorderColor": "#786c56",
            "bulletBorderAlpha": 1,
            "bulletBorderThickness": 2,
            "bulletColor": "#000000",
            "labelText": "[[name]]",
            "labelPosition": "right",
            "balloonText": "value:[[value]]",
            "showBalloon": true,
            "animationPlayed": true
          }, {
            "id": "g3",
            "title": "weightedScore",
            "valueField": "weightedScore",
            "type": "line",
            "valueAxis": "a3",
            "lineColor": "#ff5755",
            "balloonText": "[[value]]",
            "lineThickness": 0.4,
            "legendValueText": "[[value]]",
            "legendPeriodValueText": ": [[value.sum]]",
            "bullet": "square",
            "bulletBorderColor": "#ff5755",
            "bulletBorderThickness": 1,
            "bulletBorderAlpha": 1,
            "dashLengthField": "dashLength",
            "animationPlayed": true
          }],

          "chartCursor": {
            "zoomable": false,
            "categoryBalloonDateFormat": "DD",
            "cursorAlpha": 0,
            "valueBalloonsEnabled": false
          },
          "legend": {
            "bulletType": "round",
            "equalWidths": false,
            "valueWidth": 120,
            "useGraphSettings": true,
            //"color": "#FFFFFF"
          }
        });
      }, 300);
    });
  };


  $scope.clearAnswer = function (question) {
    console.log("clear answer for ", question);
    Survey.updateAnswer($scope.surveyId, question.id, null).success(function (w) {
      Survey.getSuggestions($scope.surveyId).success(function (e) {
        $scope.suggestions = e;
      })
    })
  };

  $scope.updateAnswer = function (question) {

    console.log("question answer updated - ", question, $scope.questionGroups);
    var updatedGroup = $scope.questionGroups[question.questionGroup];

    var questions = updatedGroup.questions;
    var attempted = false;
    for (var r = 0; r < questions.length; r++) {
      if (questions[r].answer !== null) {
        attempted = true;
        break;
      }
    }

    if (attempted) {
      updatedGroup.currentColor = "#23C773"; //updatedGroup.color;
    } else {
      updatedGroup.currentColor = "#9E9E9E";
    }

    Survey.updateAnswer($scope.surveyId, question.id, question.answer).success(function (w) {
      Survey.getSuggestions($scope.surveyId).success(function (d) {
        $scope.suggestions = d;
        if (d.length > 0) {
          (function (i) {
            var interval = setInterval(function () {

              if (i >= d.length) {
                clearInterval(interval);
                return;
              }

              if (i < 3) {
                $scope.showSplit(d[i], i);
              }

              // $scope.showSplit(d[i], i);
              // Survey.getProductSplit($scope.surveyId, d[i].product.name, d[i].product.companyName).then(function (a) {
              //   a = a.data;
              //   console.log("split", a);
              //
              //   d[i].split = a.featureScoreMap;
              //   d[i].answerSplit = a.answerFeaturewiseScoreList;
              //
              // })

              var productName = d[i].product.name;
              productName = productName.replace(/[\-\/\(\)]/g, " ").split(" ");
              var strSearch = "";
              var added = 0;
              for (var e = 0; e < productName.length && added < 3; e++) {
                if (productName[e].trim().length > 0) {
                  added += 1;
                  strSearch += productName[e].trim() + " ";
                }
              }

              (function (index, prod) {
                console.log("get images for ", index, prod);
                DataProvider.getProductImages(prod.categoryId, prod.companyName, prod.name).success(function (c) {
                  // $scope.images[keyName] = c;
                  $scope.suggestions[index].image = random(c);
                  // $scope.$digest();

                });
              }(i, d[i].product));
              i += 1;

            }, 1000)
          })(0);
        }
      })
    });
  };

  Survey.getQuestions($scope.surveyId).success(function (questionList) {

    var groupQuestionMap = {};

    $scope.chatHistory = [];

    var chosenGroup = null;
    var chosenIndex = -1;

    for (var i = 0; i < questionList.length; i++) {
      var question = questionList[i];

      var questionGroup = question.questionGroup;

      if (!groupQuestionMap[questionGroup]) {
        var parts = questionGroup.split(";");
        if (parts.length < 2) {
          parts.push("fa-lemon-o")
        }
        if (parts.length < 3) {
          parts.push("#FF9800")
        }

        groupQuestionMap[questionGroup] = {
          questions: [],
          icon: parts[1],
          name: parts[0],
          color: parts[2],
          currentColor: "#9E9E9E"
        };
      }
      groupQuestionMap[questionGroup].questions.push(question);


      if (question.answer !== null) {

        groupQuestionMap[questionGroup].currentColor = groupQuestionMap[questionGroup].color;


        $scope.chatHistory.push({
          type: "in",
          message: question.question,
        });


        var answers = question.internalObject.options;
        var currentAnswers = question.answer.split(",")

        for (var i1 = 0; i1 < answers.length; i1++) {
          var answer1 = answers[i1];
          if (currentAnswers.indexOf(answer1.value) > -1) {

            $scope.chatHistory.push({
              type: "out",
              message: answer1.label,
            });
          }
        }


      } else {

        if (chosenGroup == null) {
          chosenGroup = questionGroup;
          chosenIndex = groupQuestionMap[questionGroup].questions.length - 1;
        }

      }


      var type = question.type;

      if (type.indexOf("multi_string_choice") != 0 || type.indexOf("multi_image_choice") != 0) {
        continue;
      }

      var answer = question.answer;
      try {
        var a = JSON.parse(answer);
        if (a instanceof Array) {
          answer = a;
        } else {
          answer = [answer];
        }
      } catch (e) {
        answer = [answer];
      }
      //questionList[0].answer = [answer];
      question.answer = answer;

    }
    console.log("got response", questionList);

    $scope.questionGroups = groupQuestionMap;


    $scope.questions = null;

    /*$scope.gotoBottom = function (id) {
      var element = document.getElementById(id);
      element.scrollTop = element.scrollHeight - element.clientHeight;
      document.documentElement.scrollTop = 0;
    };*/

    $scope.gotoBottom = function (id) {
      var element = document.getElementById(id);
      $('#' + id).animate({
        scrollTop: element.scrollHeight
      }, 800);
      document.documentElement.scrollTop = 0;
    };


    if (chosenGroup != null) {
      $scope.questions = [$scope.questionGroups[chosenGroup].questions[chosenIndex]];
    }

    $scope.currentQuestionIndex = chosenIndex;
    $scope.selectedGroup = groupQuestionMap[chosenGroup];

    $scope.setGroup = function (key, index) {
      index = index || 0;
      $scope.currentQuestionIndex = 0;
      $scope.questions = [groupQuestionMap[key].questions[$scope.currentQuestionIndex]];
      console.log("questions: ", $scope.questions)
      $scope.selectedGroup = groupQuestionMap[key];
      setTimeout(function () {
        $("select").select2();
        $(".zoomTarget").zoomTarget();
      }, 300);
    };

    $scope.nextQuestion = function () {

      $scope.chatHistory.push({
        type: "in",
        message: $scope.questions[0].question,
      });

      var answers1 = $scope.questions[0].internalObject.options;
      var currentAnswers1;

      if($scope.questions[0].type=="single_string" || $scope.questions[0].type=="single_integer") {
        currentAnswers1 = $scope.chatInput;
        $scope.chatHistory.push({
          type: "out",
          message: currentAnswers1,
        });
      }
      else { 
        if ($scope.questions[0].answer) {
          if($scope.questions[0].answer.split) {
            currentAnswers1 = $scope.questions[0].answer.split(",");
          }
          else {
            currentAnswers1 = $scope.questions[0].answer;
          }
        
          for (var i = 0; i < answers1.length; i++) {
            var answer = answers1[i];
            if (currentAnswers1.indexOf(answer.value) > -1) {

              $scope.chatHistory.push({
                type: "out",
                message: answer.label,
              });
            }
          }
        }
        else {

        }
      }

      if ($scope.currentQuestionIndex + 1 == $scope.selectedGroup.questions.length) {
        $scope.selectedGroup = null;
        $scope.questions = null;
        $scope.currentQuestionIndex = -1;
      } else {
        $scope.questions = [$scope.selectedGroup.questions[$scope.currentQuestionIndex + 1]];
        $scope.currentQuestionIndex += 1;
      }
      $scope.chatInput = "";

      setTimeout(function () {
        $(".zoomTarget").zoomTarget();
        $("select").select2();
        $scope.gotoBottom("chatz")
      }, 300);
    };

  }).error(function (e) {
    console.log("failed to get survey", e);
  });
}]);
