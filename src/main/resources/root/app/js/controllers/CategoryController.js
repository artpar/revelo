/**
 * Created by parth on 28/4/16.
 */


angular.module('MetronicApp').controller('CategoryController', ['$scope', '$stateParams', 'DataProvider', 'Log', 'Data', 'Survey', '$location',
    function ($scope, $stateParams, DataProvider, Log, Data, Survey, $location) {


        $scope.imageServerRoot = "https://images.revlo.inscripta.ai";

        if (window.location.hostname == "localhost") {
            $scope.imageServerRoot = "http://localhost:9333";
        }



        $scope.addCategory = function () {
            //var name = window.prompt("New category name");

            bootbox.prompt("New category name", function (result) {
                if (result) {
                    DataProvider.new("category", {name: result}).success(function (e) {
                        refreshCategories();
                        Log.info("created category", e);
                    }).error(function (e) {
                        Log.error("failed to make category");
                    });
                }
            });
        };
        $scope.newQuestionnaire = function (categoryReferenceId) {
            bootbox.prompt("New questionnaire title", function (name) {

                if (!name || name.trim().length < 5) {
                    return;
                }

                DataProvider.new("questionnaire", {
                    category_id: categoryReferenceId,
                    name: name.trim()
                }).success(function (r) {
                    Log.info("Created new questionnaire [" + name + "] in category");
                    $location.path("/questionnaire/" + r.reference_id);
                }).error(function (e) {
                    Log.error("Failed to create new questionnaire", e)
                })
            })
        };


        $scope.deleteCategory = function (c) {

            bootbox.confirm("Are you sure ? This cannot be undone.", function (result) {
                if (!result) {
                    //alert("Prompt dismissed");
                } else {
                    DataProvider.delete("category", {reference_id: c.reference_id}).success(function () {
                        Log.info("Deleted category");
                        refreshCategories();
                    }).error(function (e) {
                        Log.error("Failed to delete category", e)
                    });
                }
            });


        };


      $scope.refreshImages = function (i, category) {
        DataProvider.getImages(category.name).success(function (c) {
          $scope.images = c;
          category.image = random(c);


          //setInterval(function () {
          //    var next = random(c);
          //    console.log("update image for " + category.name + " to " + next.url);
          //    $scope.categoryList[i].image = next;
          //    $("#image-" + category.referenceId).attr("src", "/image/" + next.url);
          //}, Math.random() * 20000 + 10000);
        });
      };

        function refreshCategories() {
            DataProvider.getCategoryList({limit: 50}).success(function (c) {
                c = c.data;
                $scope.categoryList = c;
                for (var i = 0; i < c.length; i++) {
                    $scope.refreshImages(i, c[i]);
                }
            });
        }

        if (!$stateParams.referenceId) {
            refreshCategories();
            return;
        }

        $scope.referenceId = $stateParams.referenceId;

        $scope.startSurvey = function (referenceId) {
            Survey.start(referenceId).success(function (survey) {
                window.open(window.location.href.split("#")[0] + "#/survey/" + survey.id)
            }).error(function (error) {
                Log.error("Failed to start survey", error)
            });
        };


        $scope.exportXls = function () {

            DataProvider.export("category/" + $stateParams.referenceId)

        };

        $scope.startQuestionnaire = function () {

        };


        $scope.setType = function () {
            console.log("set data source for category: ", $stateParams.referenceId);


            bootbox.prompt("Source of product data: (Dbpedia::data / Database::custom)", function (newSource) {
                if (newSource) {
                    DataProvider.updateCategory({
                        referenceId: $stateParams.referenceId,
                        "key": "datasource",
                        "value": newSource
                    })
                        .success(function (r) {
                            $scope.category.datasource = newSource;
                        })
                }
            });
        };


        $("#fupload").on("change", function () {
            var file = $("#fupload")[0].files[0];

            bootbox.confirm("Are you sure you want to uploadXls the category description from: " + file.name, function (confirm) {
                if (confirm) {
                    var formData = new FormData();
                    formData.append('file', file, file.name);

                    DataProvider.uploadXls("category", $stateParams.referenceId, formData).then(function (response) {

                        var msg = "";
                        if (response.data.status == "failed") {
                            msg = response.data.errors.join("\n   ")
                        }

                        bootbox.alert("Upload response: " + response.data.status + ". " + msg);
                        $scope.reloadData();
                    });

                }
                $("#fupload")[0].value = null;
            });
        });


        $scope.deleteResultFromCategory = function (name, referenceId, companyName) {

            bootbox.confirm("Are you sure you want to delete this result ? cannot be undone ", function (confirm) {

                if (!confirm) {
                    return;
                }

                DataProvider.deleteResultFromCategory(name, referenceId, companyName).success(function (c) {
                    Log.info("Deleted " + name + " from category " + $scope.category.name);
                    DataProvider.getResultsForCategory($stateParams.referenceId).success(function (c) {
                        $scope.results = c;
                    });
                })
            });
        };

        $scope.results = [];


        $scope.reloadData = function () {

            DataProvider.getCategory($stateParams.referenceId).success(function (c) {
                $scope.category = c;
                DataProvider.get('questionnaire', {where: 'category_id:' + $stateParams.referenceId}).success(function (c) {
                    $scope.questionnaireList = c.data;
                })
            });


            DataProvider.getResultsForCategory($stateParams.referenceId).success(function (c) {
                $scope.results = c;
            });

            DataProvider.getCategoryFeatures($stateParams.referenceId).success(function (c) {
                $scope.features = c;
            });

        };
        $scope.reloadData();


        $scope.getValues = function (featureName, referenceId, e) {
            DataProvider.getValuesForFeatureName(featureName, referenceId).success(function (c) {
                $(e.target).parent().parent().find(".value").html("");
                $(e.target).parent().parent().parent().find('div.value').html(jsonToTable1(c, featureName, referenceId, e.target, ['key', 'numberValue', 'companyName', 'originalNumberValue']));

            })
        };
        $scope.closeFeatures = function (e) {
            $(e.target).parent().parent().parent().find("div.features").html("");
        };
        $scope.closeValues = function (e) {
            $(e.target).parent().parent().parent().find("div.value").html("");
        };
        function jsonToTable1(c, featureName, referenceId, target, skip) {
            var t = $("<table class='table'></table>");
            var thead = $("<thead></thead>");
            var tr = $("<tr></tr>");


            var objectKeys = Object.keys(c[0]);
            for (var i = 0; i < objectKeys.length; i++) {
                if (skip.indexOf(objectKeys[i]) > -1) {
                    //console.log("Skip: " + objectKeys[j]);
                    continue;
                }

                tr.append("<th>" + objectKeys[i] + "</th>");
            }
            thead.append(tr);
            var tbody = $("<tbody></tbody>");
            for (var i = 0; i < c.length; i++) {
                var tr = $("<tr></tr>");

                for (var j = 0; j < objectKeys.length; j++) {
                    if (skip.indexOf(objectKeys[j]) > -1) {
                        //console.log("Skip: " + objectKeys[j]);
                        continue;
                    }
                    if (j < objectKeys.length - 1) {
                        tr.append($("<td></td>").text(c[i][objectKeys[j]]));
                    } else {

                        var span = $('<span class="label label-default"></span>');
                        span.text(c[i][objectKeys[j]]);
                        span.append($('<i class="glyphicon glyphicon-pencil" aria-hidden="true"></i>'));
                        var clickHandler = function (featureName, referenceId, key, oldValue, oldNumericValue) {
                            return function (e) {
                                console.log("edit", featureName, referenceId, key);

                                bootbox.prompt("New value for numeric value of [" + featureName + "] for " + ": ", c[objectKeys[j - 1]], function (value) {
                                    if (value && value != oldNumericValue) {
                                        DataProvider.updateNumberValue(referenceId, oldValue, value).success(function (c) {
                                            $scope.getValues(featureName, referenceId, {'e': {'target': target}});
                                        });
                                    }

                                });
                            }
                        };


                        span.on("click", clickHandler(featureName, referenceId, objectKeys[j], c[i]['value'], c[i]['numericValue']));
                        tr.append($("<td></td>").html(span));
                    }
                }


                tbody.append(tr);
            }
            t.append(thead);
            t.append(tbody);
            return t;
        }


        $scope.getFeatures = function (name, companyName, referenceId, e) {
            DataProvider.getProductFeatures({
                name: name,
                companyName: companyName,
                referenceId: referenceId
            }).success(function (c) {
                var div = $(e.target).parent().parent().parent().find("div.features");
                //$(document).find(".features").html("");
                div.html(jsonToTable(c, name, companyName, referenceId, e.target));
            })
        };

        function jsonToTable(c, name, companyName, referenceId, target, skip) {
            skip = skip || [];
            var t = $("<table class='table'></table>");
            var thead = $("<thead></thead>");
            var tr = $("<tr></tr>");


            var objectKeys = Object.keys(c[0]);
            for (var i = 0; i < objectKeys.length; i++) {
                if (skip.indexOf(objectKeys[i]) > -1) {
                    console.log("Skip: " + featureName);
                    continue;
                }

                tr.append("<th>" + objectKeys[i] + "</th>");
            }
            thead.append(tr);
            var tbody = $("<tbody></tbody>");
            for (var i = 0; i < c.length; i++) {
                var tr = $("<tr></tr>");

                for (var j = 0; j < objectKeys.length; j++) {
                    var featureName = c[i]['key'];
                    var cellValue = c[i]['value'];
                    if (skip.indexOf(featureName) > -1) {
                        console.log("Skip: " + featureName);
                        continue;
                    }

                    if (j < objectKeys.length - 1) {
                        tr.append($("<td></td>").text(featureName));
                    } else {
                        var span = $('<span></span>');
                        span.text(cellValue);
                        span.append($('<i class="glyphicon glyphicon-pencil" aria-hidden="true"></i>'));
                        var clickHandler = function (name, companyName, referenceId, key, oldValue, oldNumericValue) {
                            return function (e) {
                                console.log("edit", name, companyName, referenceId, key);

                                bootbox.prompt("New value for [" + key + "] for [" + companyName + "]" + name + " (" + c[key] + "): ", function (value) {
                                    if (value && value != oldValue) {
                                        DataProvider.addResultFeatureValue({
                                            name: name,
                                            companyName: companyName,
                                            referenceId: referenceId,
                                            key: key,
                                            value: value,
                                            numericValue: oldNumericValue
                                        }).success(function (c) {
                                            $scope.getFeatures(name, companyName, referenceId, {'target': target});
                                        });
                                    }
                                });
                            }
                        };


                        span.on("click", clickHandler(name, companyName, referenceId, featureName, cellValue, c[i]['numericValue']));
                        tr.append($("<td></td>").html(span));
                    }
                }


                tbody.append(tr);
            }
            t.append(thead);
            t.append(tbody);
            return t;
        }

        $scope.addSpecToResult = function (name, companyName, referenceId) {
            bootbox.prompt("What is the name of the feature (try to use a known standard term)", function (name1) {
                if (!name1) {
                    return
                }
                bootbox.prompt("What is the value of the feature (with units)", function (value) {
                    if (!value) {
                        return
                    }

                    bootbox.prompt("[CAN SKIP THIS PART]   Can you write a regex to extract number from the standard notation (eg 34 from '34 mts.' -> '(\\d+) ?mts.') for your value ?", function (regex) {

                        if (!regex) {
                            regex = "";
                        }
                        DataProvider.addResultFeatureValue({
                            name: name,
                            companyName: companyName,
                            referenceId: referenceId,
                            key: name1,
                            value: value,
                            regex: regex
                        }).success(function (a) {
                            Log.info("Added a feature to the product");
                        })
                    });


                });

            });


        };

        $scope.addResult = function () {

            bootbox.prompt("Result...", function (result) {
                if (!result) {
                    return;
                }
                bootbox.prompt("Group name...", function (companyName) {
                    if (result && companyName) {
                        DataProvider.addResultToCategory($stateParams.referenceId, result, companyName).success(function (c) {
                            $scope.results = c;
                        });
                    }

                });
            });

        }
    }
])
;
