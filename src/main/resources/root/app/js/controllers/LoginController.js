/**
 * Created by parth on 28/4/16.
 */



angular.module('MetronicApp').controller('LoginController', function ($scope, auth) {
    $scope.auth = auth;
    $scope.signin = function () {
        auth.signin({
            scope: 'openid email name'
        })
    };
});
