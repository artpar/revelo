/**
 * Created by parth on 28/4/16.
 */
function toTitleCase(input) {
  var output = input
    .split(' ')  // 'HOw aRe YOU' => ['HOw' 'aRe' 'YOU']
    .map(function (letter) {
      var firstLetter = letter[0].toUpperCase() // H , a , Y  => H , A , Y
      var restLetters = letter.substring(1).toLowerCase() // Ow, Re, OU => ow, re, ou
      return firstLetter + restLetters // conbine together
    })
    .join(' ') //['How' 'Are' 'You'] => 'How Are You'
  return output
}

angular.module('MetronicApp').controller('QuestionnaireController',
  ['$scope', '$stateParams', 'DataProvider', 'Log', '$modal', 'Data', 'Survey', '$location',
    function ($scope, $stateParams, DataProvider, Log, $modal, Data, Survey, $location) {

      $scope.imageServerRoot = "https://images.revlo.inscripta.ai";

      if (window.location.hostname == "localhost") {
        $scope.imageServerRoot = "http://localhost:9333";
      }
      $scope.isOpen = false;
      $scope.$watch("isOpen", function (e) {
        setTimeout(function () {
          var bswitch = $(".make-switch");
          bswitch.bootstrapSwitch();
        }, 2000);
      });


      $scope.deleteQuestionnaire = function (referenceId) {

        bootbox.confirm("Are you sure you want to delete this questionnaire ? People who have already started the survey wont be affected", function (res) {
          if (res) {
            DataProvider.delete('questionnaire', {reference_id: referenceId}).success(function () {
              Log.info("Deleted questionnaire " + $scope.questionnaire.name);
              $location.path("/questionnaires")
              bootbox.alert("Deleted the questionnaire");
            }).error(function (e) {
              Log.error("Failed  to delete questionnaire: " + e)
              bootbox.alert("Failed to delete the questionnaire");
            })
          }
        });


      };

      $scope.deleteAllQuestions = function (referenceId) {

        bootbox.confirm("Are you sure you want to delete all questions from this questionnaire ? People who have already started the survey wont be affected", function (res) {
          if (res) {

            var questionCount = $scope.questions.length;
            for (var i = 0; i < questionCount; i++) {
              var question = $scope.questions[i];
              $scope.deleteQuestionFromQuestionnaire(referenceId, question.question[0].id)
            }


          }
        });

      };

      // $scope.exportQuestionnaire = function (referenceId) {
      //     DataProvider.export('questionnaire', {reference_id: referenceId}).success(function () {
      //         Log.info("Deleted questionnaire " + $scope.questionnaire.name);
      //         $location.path("/questionnaires")
      //     }).error(function (e) {
      //         Log.error("Failed  to export questionnaire: " + e)
      //     })
      // };


      $scope.setStatus = function (question, newStatus) {
        console.log("set status", arguments);
        question.status = newStatus;
        DataProvider.edit("questionnaire_question", question).success(function (e) {
          Log.info("Status updated to " + e.status);
        }).error(function (e) {
          Log.error("Failed to update status", e);
        })
      };

      DataProvider.get("questionnaire/mine").success(function (d) {
        $scope.questionnaireList = d.data;
      }).error(function (d) {
        Log.log(d);
      });


      $scope.removeCategory = function (c) {
        Log.log("delete", c);
      };


      $scope.updateStats = function () {
        DataProvider.getQuestionnaireStats($stateParams.referenceId).success(function (d) {
          console.log(d);
          $scope.stats = d;
        });
      };

      $scope.updateStats();

      if (!$stateParams.referenceId) {
        return;
      }

      $scope.questionnaireReferenceId = $stateParams.referenceId;

      DataProvider.get('questionnaire', {
        where: "reference_id:" + $stateParams.referenceId,
        children: "category_id"
      }).success(function (q) {
        q = q.data[0];
        $scope.questionnaire = q;
        $scope.category = q.category[0].name;
        $scope.category0 = q.category[0];

        //DataProvider.getQuestionnaireCategory($stateParams.referenceId).success(function (d) {
        //    $scope.categoryO = d;
        //});

        DataProvider.getQuestionnaireFeatures(q.reference_id).success(function (d) {
          $scope.featureList = d;
          setTimeout(function () {
            $("select[name=featureListSingleSelect]").select2();
          }, 2000)
        }).error(function (d) {
          Log.error("Failed to get category features", d);
        });
      });
      Log.log("show questionnaire", $stateParams.referenceId);
      $scope.referenceId = $stateParams.referenceId;
      $scope.loadQuestions = function () {

        DataProvider.get('questionnaire_question', {
          where: "questionnaire_id:" + $scope.referenceId,
          limit: 50,
          'children': 'question_id'
        }).success(function (d) {
          $scope.questions = d.data;
          var questions = $scope.questions;
          for (var i = 0; i < questions.length; i++) {
            var question = questions[i];
            var que = question.question[0];
            que.internalObject = JSON.parse(que.data);
            if (!que.question_group) {
              que.question_group = "Work Life;fa-question;efefef"
            }
            var parts = que.question_group.split(";");
            que.question_group = {name: parts[0], icon: parts[1], color: parts[2]};
            question.tid = random("qwerqwerqwer", 10);
          }
        }).error(function (d) {
          Log.error(d);
        });

        $scope.updateStats();
      };

      $scope.startSurvey = function (referenceId) {
        Survey.start(referenceId).success(function (survey) {
          window.open(window.location.href.split("#")[0] + "#/survey/" + survey.id + "/" + $scope.category)
        }).error(function (error) {
          Log.error("Failed to start survey", error)
        });
      };


      $scope.loadQuestions();
      $scope.changeQuestionType = function (question) {
        Log.log("Change question type for", question.question[0].id);
        DataProvider.get('question_type').success(function (types) {
          types = types.data;
          Log.log("types", types);

          var buttons = [];
          var msg = "Select new type <br /><br />";
          for (var i = 0; i < types.length; i++) {
            buttons.push({
              text: toTitleCase(types[i].name.replace(/[_]/g, " ")),
              value: types[i].name
            });
          }

          bootbox.prompt({
            title: "Select the new question type.",
            inputOptions: buttons,
            inputType: 'select',
            callback: function (result) {
              console.log('This was logged in the callback: ' + result);
              if (!result) {
                return;
              }
              DataProvider.edit("question", {
                "reference_id": question.question[0].reference_id,
                type: result
              }).success(function () {
                question.question[0].type = result;
              }).error(function (d) {
                Log.error(d);
              })
            }
          })
        })
      };

      $scope.editQuestionnaire = function (questionObj) {
        var question = questionObj.question[0];
        console.log(question);
        if (!question.question_group) {
          question.question_group = {
            name: 'Work Life',
            icon: 'fa-question',
            color: '#9E9E9E'
          }
        }
        var name = question.question_group.name;
        var icon = question.question_group.icon;
        var color = question.question_group.color;


        var form = $('<form class="form"></form>');

        var formGroup = $("<div class='form-group'></div>");

        formGroup.append($("<input class='form-control' name='name'>").val(name));

        formGroup.append($("<input id='icon-pick' class='form-control' name='icon' />").val(icon));

        formGroup.append($("<input name='color' class='form-control'>").val(color));
        form.append(formGroup);
        bootbox.alert(form, function () {
          var username = form.find('input[name=name]').val();
          var icon = form.find('input[name=icon]').val();
          var color = form.find('input[name=color]').val();

          question.question_group = username + ";" + icon + ";" + color;
          questionObj.question[0] = question;
          DataProvider.edit("question", question).success(function (e) {
            bootbox.alert("Updated question details");
          });
          questionObj.question[0].question_group = {name: username, icon: icon, color: color};
          console.log(username);
        });

        setTimeout(function () {
          form.find('#icon-pick').iconpicker({
            icons: ["fa-glass", "fa-music", "fa-search", "fa-envelope-o", "fa-heart", "fa-star", "fa-star-o", "fa-user", "fa-film", "fa-th-large", "fa-th", "fa-th-list", "fa-check", "fa-remove", "fa-search-plus", "fa-search-minus", "fa-power-off", "fa-signal", "fa-gear", "fa-trash-o", "fa-home", "fa-file-o", "fa-clock-o", "fa-road", "fa-download", "fa-arrow-circle-o-down", "fa-arrow-circle-o-up", "fa-inbox", "fa-play-circle-o", "fa-rotate-right", "fa-refresh", "fa-list-alt", "fa-lock", "fa-flag", "fa-headphones", "fa-volume-off", "fa-volume-down", "fa-volume-up", "fa-qrcode", "fa-barcode", "fa-tag", "fa-tags", "fa-book", "fa-bookmark", "fa-print", "fa-camera", "fa-font", "fa-bold", "fa-italic", "fa-text-height", "fa-text-width", "fa-align-left", "fa-align-center", "fa-align-right", "fa-align-justify", "fa-list", "fa-dedent", "fa-indent", "fa-video-camera", "fa-photo", "fa-pencil", "fa-map-marker", "fa-adjust", "fa-tint", "fa-edit", "fa-share-square-o", "fa-check-square-o", "fa-arrows", "fa-step-backward", "fa-fast-backward", "fa-backward", "fa-play", "fa-pause", "fa-stop", "fa-forward", "fa-fast-forward", "fa-step-forward", "fa-eject", "fa-chevron-left", "fa-chevron-right", "fa-plus-circle", "fa-minus-circle", "fa-times-circle", "fa-check-circle", "fa-question-circle", "fa-info-circle", "fa-crosshairs", "fa-times-circle-o", "fa-check-circle-o", "fa-ban", "fa-arrow-left", "fa-arrow-right", "fa-arrow-up", "fa-arrow-down", "fa-mail-forward", "fa-expand", "fa-compress", "fa-plus", "fa-minus", "fa-asterisk", "fa-exclamation-circle", "fa-gift", "fa-leaf", "fa-fire", "fa-eye", "fa-eye-slash", "fa-warning", "fa-plane", "fa-calendar", "fa-random", "fa-comment", "fa-magnet", "fa-chevron-up", "fa-chevron-down", "fa-retweet", "fa-shopping-cart", "fa-folder", "fa-folder-open", "fa-arrows-v", "fa-arrows-h", "fa-bar-chart-o", "fa-twitter-square", "fa-facebook-square", "fa-camera-retro", "fa-key", "fa-gears", "fa-comments", "fa-thumbs-o-up", "fa-thumbs-o-down", "fa-star-half", "fa-heart-o", "fa-sign-out", "fa-linkedin-square", "fa-thumb-tack", "fa-external-link", "fa-sign-in", "fa-trophy", "fa-github-square", "fa-upload", "fa-lemon-o", "fa-phone", "fa-square-o", "fa-bookmark-o", "fa-phone-square", "fa-twitter", "fa-facebook-f", "fa-github", "fa-unlock", "fa-credit-card", "fa-feed", "fa-hdd-o", "fa-bullhorn", "fa-bell", "fa-certificate", "fa-hand-o-right", "fa-hand-o-left", "fa-hand-o-up", "fa-hand-o-down", "fa-arrow-circle-left", "fa-arrow-circle-right", "fa-arrow-circle-up", "fa-arrow-circle-down", "fa-globe", "fa-wrench", "fa-tasks", "fa-filter", "fa-briefcase", "fa-arrows-alt", "fa-group", "fa-chain", "fa-cloud", "fa-flask", "fa-cut", "fa-copy", "fa-paperclip", "fa-save", "fa-square", "fa-navicon", "fa-list-ul", "fa-list-ol", "fa-strikethrough", "fa-underline", "fa-table", "fa-magic", "fa-truck", "fa-pinterest", "fa-pinterest-square", "fa-google-plus-square", "fa-google-plus", "fa-money", "fa-caret-down", "fa-caret-up", "fa-caret-left", "fa-caret-right", "fa-columns", "fa-unsorted", "fa-sort-down", "fa-sort-up", "fa-envelope", "fa-linkedin", "fa-rotate-left", "fa-legal", "fa-dashboard", "fa-comment-o", "fa-comments-o", "fa-flash", "fa-sitemap", "fa-umbrella", "fa-paste", "fa-lightbulb-o", "fa-exchange", "fa-cloud-download", "fa-cloud-upload", "fa-user-md", "fa-stethoscope", "fa-suitcase", "fa-bell-o", "fa-coffee", "fa-cutlery", "fa-file-text-o", "fa-building-o", "fa-hospital-o", "fa-ambulance", "fa-medkit", "fa-fighter-jet", "fa-beer", "fa-h-square", "fa-plus-square", "fa-angle-double-left", "fa-angle-double-right", "fa-angle-double-up", "fa-angle-double-down", "fa-angle-left", "fa-angle-right", "fa-angle-up", "fa-angle-down", "fa-desktop", "fa-laptop", "fa-tablet", "fa-mobile-phone", "fa-circle-o", "fa-quote-left", "fa-quote-right", "fa-spinner", "fa-circle", "fa-mail-reply", "fa-github-alt", "fa-folder-o", "fa-folder-open-o", "fa-smile-o", "fa-frown-o", "fa-meh-o", "fa-gamepad", "fa-keyboard-o", "fa-flag-o", "fa-flag-checkered", "fa-terminal", "fa-code", "fa-mail-reply-all", "fa-star-half-empty", "fa-location-arrow", "fa-crop", "fa-code-fork", "fa-unlink", "fa-question", "fa-info", "fa-exclamation", "fa-superscript", "fa-subscript", "fa-eraser", "fa-puzzle-piece", "fa-microphone", "fa-microphone-slash", "fa-shield", "fa-calendar-o", "fa-fire-extinguisher", "fa-rocket", "fa-maxcdn", "fa-chevron-circle-left", "fa-chevron-circle-right", "fa-chevron-circle-up", "fa-chevron-circle-down", "fa-html5", "fa-css3", "fa-anchor", "fa-unlock-alt", "fa-bullseye", "fa-ellipsis-h", "fa-ellipsis-v", "fa-rss-square", "fa-play-circle", "fa-ticket", "fa-minus-square", "fa-minus-square-o", "fa-level-up", "fa-level-down", "fa-check-square", "fa-pencil-square", "fa-external-link-square", "fa-share-square", "fa-compass", "fa-toggle-down", "fa-toggle-up", "fa-toggle-right", "fa-euro", "fa-gbp", "fa-dollar", "fa-rupee", "fa-cny", "fa-ruble", "fa-won", "fa-bitcoin", "fa-file", "fa-file-text", "fa-sort-alpha-asc", "fa-sort-alpha-desc", "fa-sort-amount-asc", "fa-sort-amount-desc", "fa-sort-numeric-asc", "fa-sort-numeric-desc", "fa-thumbs-up", "fa-thumbs-down", "fa-youtube-square", "fa-youtube", "fa-xing", "fa-xing-square", "fa-youtube-play", "fa-dropbox", "fa-stack-overflow", "fa-instagram", "fa-flickr", "fa-adn", "fa-bitbucket", "fa-bitbucket-square", "fa-tumblr", "fa-tumblr-square", "fa-long-arrow-down", "fa-long-arrow-up", "fa-long-arrow-left", "fa-long-arrow-right", "fa-apple", "fa-windows", "fa-android", "fa-linux", "fa-dribbble", "fa-skype", "fa-foursquare", "fa-trello", "fa-female", "fa-male", "fa-gittip", "fa-sun-o", "fa-moon-o", "fa-archive", "fa-bug", "fa-vk", "fa-weibo", "fa-renren", "fa-pagelines", "fa-stack-exchange", "fa-arrow-circle-o-right", "fa-arrow-circle-o-left", "fa-toggle-left", "fa-dot-circle-o", "fa-wheelchair", "fa-vimeo-square", "fa-turkish-lira", "fa-plus-square-o", "fa-space-shuttle", "fa-slack", "fa-envelope-square", "fa-wordpress", "fa-openid", "fa-institution", "fa-mortar-board", "fa-yahoo", "fa-google", "fa-reddit", "fa-reddit-square", "fa-stumbleupon-circle", "fa-stumbleupon", "fa-delicious", "fa-digg", "fa-pied-piper", "fa-pied-piper-alt", "fa-drupal", "fa-joomla", "fa-language", "fa-fax", "fa-building", "fa-child", "fa-paw", "fa-spoon", "fa-cube", "fa-cubes", "fa-behance", "fa-behance-square", "fa-steam", "fa-steam-square", "fa-recycle", "fa-automobile", "fa-cab", "fa-tree", "fa-spotify", "fa-deviantart", "fa-soundcloud", "fa-database", "fa-file-pdf-o", "fa-file-word-o", "fa-file-excel-o", "fa-file-powerpoint-o", "fa-file-photo-o", "fa-file-zip-o", "fa-file-sound-o", "fa-file-movie-o", "fa-file-code-o", "fa-vine", "fa-codepen", "fa-jsfiddle", "fa-life-bouy", "fa-circle-o-notch", "fa-ra", "fa-ge", "fa-git-square", "fa-git", "fa-y-combinator-square", "fa-tencent-weibo", "fa-qq", "fa-wechat", "fa-send", "fa-send-o", "fa-history", "fa-circle-thin", "fa-header", "fa-paragraph", "fa-sliders", "fa-share-alt", "fa-share-alt-square", "fa-bomb", "fa-soccer-ball-o", "fa-tty", "fa-binoculars", "fa-plug", "fa-slideshare", "fa-twitch", "fa-yelp", "fa-newspaper-o", "fa-wifi", "fa-calculator", "fa-paypal", "fa-google-wallet", "fa-cc-visa", "fa-cc-mastercard", "fa-cc-discover", "fa-cc-amex", "fa-cc-paypal", "fa-cc-stripe", "fa-bell-slash", "fa-bell-slash-o", "fa-trash", "fa-copyright", "fa-at", "fa-eyedropper", "fa-paint-brush", "fa-birthday-cake", "fa-area-chart", "fa-pie-chart", "fa-line-chart", "fa-lastfm", "fa-lastfm-square", "fa-toggle-off", "fa-toggle-on", "fa-bicycle", "fa-bus", "fa-ioxhost", "fa-angellist", "fa-cc", "fa-shekel", "fa-meanpath", "fa-buysellads", "fa-connectdevelop", "fa-dashcube", "fa-forumbee", "fa-leanpub", "fa-sellsy", "fa-shirtsinbulk", "fa-simplybuilt", "fa-skyatlas", "fa-cart-plus", "fa-cart-arrow-down", "fa-diamond", "fa-ship", "fa-user-secret", "fa-motorcycle", "fa-street-view", "fa-heartbeat", "fa-venus", "fa-mars", "fa-mercury", "fa-intersex", "fa-transgender-alt", "fa-venus-double", "fa-mars-double", "fa-venus-mars", "fa-mars-stroke", "fa-mars-stroke-v", "fa-mars-stroke-h", "fa-neuter", "fa-genderless", "fa-facebook-official", "fa-pinterest-p", "fa-whatsapp", "fa-server", "fa-user-plus", "fa-user-times", "fa-hotel", "fa-viacoin", "fa-train", "fa-subway", "fa-medium", "fa-yc", "fa-optin-monster", "fa-opencart", "fa-expeditedssl", "fa-battery-4", "fa-battery-3", "fa-battery-2", "fa-battery-1", "fa-battery-0", "fa-mouse-pointer", "fa-i-cursor", "fa-object-group", "fa-object-ungroup", "fa-sticky-note", "fa-sticky-note-o", "fa-cc-jcb", "fa-cc-diners-club", "fa-clone", "fa-balance-scale", "fa-hourglass-o", "fa-hourglass-1", "fa-hourglass-2", "fa-hourglass-3", "fa-hourglass", "fa-hand-grab-o", "fa-hand-stop-o", "fa-hand-scissors-o", "fa-hand-lizard-o", "fa-hand-spock-o", "fa-hand-pointer-o", "fa-hand-peace-o", "fa-trademark", "fa-registered", "fa-creative-commons", "fa-gg", "fa-gg-circle", "fa-tripadvisor", "fa-odnoklassniki", "fa-odnoklassniki-square", "fa-get-pocket", "fa-wikipedia-w", "fa-safari", "fa-chrome", "fa-firefox", "fa-opera", "fa-internet-explorer", "fa-tv", "fa-contao", "fa-500px", "fa-amazon", "fa-calendar-plus-o", "fa-calendar-minus-o", "fa-calendar-times-o", "fa-calendar-check-o", "fa-industry", "fa-map-pin", "fa-map-signs", "fa-map-o", "fa-map", "fa-commenting", "fa-commenting-o", "fa-houzz", "fa-vimeo", "fa-black-tie", "fa-fonticons"]
          });
        }, 300);


      };

      $scope.selectedimage = null;

      $scope.removeQuestionImages = function (question) {
        if (question.question[0].internalObject == undefined) {
          question.question[0].internalObject = {}
          return;
        }

        if (question.question[0].internalObject.image_tokens) {
          bootbox.confirm("Are you sure you want to remove all images ?", function (res) {
            if (res) {
              question.question[0].internalObject.image_tokens = [];
              DataProvider.updateQuestionData(
                question.question[0].id,
                question.question[0].internalObject
              ).success(function () {

              }).error(function (d) {
                Log.error(d);
              });
            }
          })
        }

      };

      $scope.addImageToQuestion = function (question) {
        if (question.question[0].internalObject == undefined) {
          question.question[0].internalObject = {}
        }
        if (!question.question[0].internalObject.image_tokens) {
          question.question[0].internalObject.image_tokens = [];
        }

        function fileNameChanged(event) {
          var file = event.target.files[0];
          if (!file) {
            return;
          }
          console.log(file, arguments);

          var formData = new FormData();
          formData.append('file', file, file.name);

          DataProvider.uploadImage(formData).then(function (result) {

            var errors = "";
            if (result.data.status != "ok") {
              errors = result.data.errors.join(", ");
            } else {
              question.question[0].internalObject.image_tokens.push(result.data.fid);
              DataProvider.updateQuestionData(
                question.question[0].id,
                question.question[0].internalObject
              ).success(function () {

              }).error(function (d) {
                Log.error(d);
              });


            }
          });
          event.target.value = null;


        }

        console.log("add image to question", question);
        var ctrl = angular.element("#txtUploadFile");
        ctrl.on('change', fileNameChanged);
        ctrl.click();
      };


      $scope.deleteQuestionFromQuestionnaire = function (category, questionId) {
        bootbox.confirm("Are you sure ?", function (res) {
          if (res) {
            DataProvider.deleteQuestionFromQuestionnaire(category, questionId).success(function () {
              $scope.loadQuestions();
            }).error(function (d) {
              Log.error(d);
            });
          }
        })
      };
      $scope.hasData = function (question) {
        return ['single_string_choice', 'multi_string_choice', 'single_image_choice', 'multi_image_choice', 'multi_string_choice_filter'].indexOf(question.question[0].type) > -1;
      };

      $scope.removeAnswerOption = function (question, option) {
        var c = window.confirm("Are you sure you want to delete the answer option ? \n This cannot be undone");
        if (c) {
          var answerValue = option.value;

          DataProvider.deleteAnswer(question.reference_id, answerValue, function (res) {
            console.log("delete answer response", res)
          })

          if (question.internalObject == undefined) {
            return;
          }
          if (question.internalObject.options == undefined) {
            return;
          }
          var newOptions = [];
          for (var i = 0; i < question.internalObject.options.length; i++) {
            if (question.internalObject.options[i]['$$hashKey'] == option['$$hashKey']) {
              continue;
            }
            newOptions.push({
              value: question.internalObject.options[i].value,
              label: question.internalObject.options[i].label
            });
          }

          question.internalObject.options = newOptions;
          DataProvider.updateQuestionData(question.id, question.internalObject).success(function () {

          }).error(function (d) {
            Log.error(d);
          });
        }
      };


      var answerUpdatedHandler = function (question, index) {
        return function (option) {
          var answerData = option.data;
          delete option['data'];
          Log.log("arguments", arguments);
          if (question.internalObject == undefined) {
            question.internalObject = {}
          }
          if (question.internalObject.options == undefined) {
            question.internalObject.options = [];
          }
          Log.info("index to update", index, question.internalObject.options);
          if (index > -1) {
            var key = index;
            if (key != -1) {
              delete question.internalObject.options[key];
            }
            question.internalObject.options[key] = option;
          } else {
            question.internalObject.options.push(option);
          }
          var updateAnswer = function () {
            try {
              Log.info("answer data", answerData);
              var parsed = JSON.parse(answerData);
              var method = DataProvider.edit;
              if (!option.reference_id) {
                method = DataProvider.new;
              }
              method('answer', {
                'questionnaire_id': $scope.referenceId,
                'question_id': question.reference_id,
                answer: option.value,
                reference_id: option.reference_id,
                data: answerData
              }).success(function () {
                Log.info("Updated answer functions")
              }).error(function (d) {
                Log.error("Failed to update answer functions", d)
              })
            } catch (e) {
              Log.info("failed to parse function data. not updating on server", e)
            }

          };
          DataProvider.edit('question', {
            reference_id: question.reference_id,
            data: JSON.stringify(question.internalObject)
          }).success(updateAnswer).error(updateAnswer);

        }
      };
      $scope.editAnswerOption = function (question, option) {
        var that = this;
        that.question = question;
        DataProvider.get('answer', {
          where: "questionnaire_id:" + $scope.referenceId + "," +
          "question_id:" + question.reference_id + "," + "answer:" + option.value
        }).success(function (d) {
          if (d.data.length > 0) {
            var option1 = d.data[0];
            if (option1.data.length > 0) {
              option.reference_id = option1.reference_id;
              try {
                option.data = JSON.parse(option1.data);
              } catch (e) {
                Log.error("Failed to parse function ", e);
                option.data = {};
              }
            } else {
              option.data = {};
            }
          }
          //var op = question.internalObject.options.filter(function (e) {
          //    return (e.value == option.answer)
          //});
          //if (op.length > 0) {
          //    option.value = op[0].value;
          //    option.label = op[0].label;
          //}

          var modalInstance = $modal.open({
            templateUrl: Data.partials() + 'add_answer_option.html',
            controller: AnswerOptionModalController,
            resolve: {
              Log: function () {
                return Log;
              },
              EditData: function () {
                return option
              },
              FeatureList: function () {
                return $scope.featureList;
              },
              DataProvider: function () {
                return DataProvider;
              },
              imageServerRoot: function () {
                return $scope.imageServerRoot;
              },
              question: function () {
                return that.question;
              }
            }
          });

          modalInstance.result.then(answerUpdatedHandler(question, question.internalObject.options.indexOf(option)), function () {
            Log.log('Modal dismissed at: ' + new Date());
          });
        }).error(function (d) {
          Log.error("failed to load answer data", d)
        });

      };


      $scope.setAnswersFromFeature = function (question, e) {
        console.log("set answers from feature");
        var value = $(e.target).parent().parent().find("select[name=featureListSingleSelect]").val();
        var featureName = value;
        bootbox.confirm("Set answers from feature value " + value + " ?", function (ok) {
          if (ok) {
            DataProvider.getValuesForFeatureName(value, $scope.category0.reference_id).success(function (r) {
              var list = r;
              question.internalObject = {};
              question.internalObject.options = [];
              question.internalObject.key = [featureName];
              var done = {};
              for (var i = 0; i < list.length; i++) {
                var value = list[i];
                var name = list[i];
                name = value.value;


                if (name.indexOf("/") > -1) {
                  name = name.substring(name.lastIndexOf("/") + 1)
                }
                if (done[name]) {
                  continue;
                }
                question.internalObject.options.push({value: value.value, label: name});
                done[name] = true;
              }
              DataProvider.edit('question', {
                reference_id: question.reference_id,
                data: JSON.stringify(question.internalObject)
              }).success(function () {
                $scope.loadQuestions();
              }).error(function () {
                bootbox.alert("Failed to update answers")
              });
            });
          }
        });
      };

      $scope.addAnswerOption = function (question) {
        // Log.log($modal.open, Data);
        var modalInstance = $modal.open({
          templateUrl: Data.partials() + 'add_answer_option.html',
          controller: AnswerOptionModalController,
          resolve: {
            Log: function () {
              return Log;
            },
            EditData: function () {
              return undefined;
            },
            FeatureList: function () {
              return $scope.featureList;
            },
            DataProvider: function () {
              return DataProvider;
            },
            imageServerRoot: function () {
              return $scope.imageServerRoot;
            }
          }
        });

        modalInstance.result.then(answerUpdatedHandler(question), function () {
          Log.log('Modal dismissed at: ' + new Date());
        });
      };


      $scope.addQuestion = function () {
        // Log.log($modal.open, Data);
        var modalInstance = $modal.open({
          templateUrl: Data.partials() + 'add_question.html',
          controller: QuestionModalController,
          resolve: {
            DataProvider: function () {
              return DataProvider
            },
            Log: function () {
              return Log;
            }
          }
        });

        modalInstance.result.then(function (selectedItem) {
          Log.log("selected", selectedItem);
          if (angular.isObject(selectedItem)) {
            DataProvider.new('questionnaire_question', {
              questionnaire_id: $scope.referenceId,
              question_id: selectedItem.referenceId
            }).then(function () {
              $scope.loadQuestions()
            }).error(function (d) {
              Log.error(d);
            });
          } else if (angular.isString(selectedItem)) {
            DataProvider.new("question", {question: selectedItem}).success(function (d) {
              DataProvider.new("questionnaire_question", {
                questionnaire_id: $scope.referenceId,
                question_id: d.reference_id
              }).success(function () {
                $scope.loadQuestions();
              }).error(function (d) {
                Log.error(d);
              });
            }).error(function (d) {
              Log.error(d);
            })
          }
        }, function () {
          Log.log('Modal dismissed at: ' + new Date());
        });
      };

      function xlsFileNameChanged(event) {
        var file = event.target.files[0];
        if (!file) {
          return;
        }
        console.log(file, arguments);

        var formData = new FormData();
        formData.append('file', file, file.name);
        DataProvider.uploadXls("questionnaire", $stateParams.referenceId, formData).then(function (result) {

          var errors = "";
          if (result.data.status != "ok") {
            errors = result.data.errors.join(", ");
          }

          bootbox.alert("Upload response: " + result.data.status + ", " + errors);
          $scope.loadQuestions();
        });
        event.target.value = null;


      }


      $scope.uploadXls = function () {
        console.log("upload xls", arguments);
        var ctrl = angular.element("#txtUploadFile");
        ctrl.on('change', xlsFileNameChanged);
        ctrl.click();
      };

      // $("#fupload").on("change", function () {
      //     var file = $("#fupload")[0].files[0];
      //
      //     bootbox.confirm("Are you sure you want to upload the question description from: " + file.name, function (confirm) {
      //         if (confirm) {
      //             var formData = new FormData();
      //             formData.append('file', file, file.name);
      //             // $("<input type='file' formenctype='multipart/form-data'>")
      //             // var xhr = new XMLHttpRequest();
      //             //
      //             // xhr.open('POST', DataProvider.apiRoot + "category/" + $stateParams.referenceId + "/xls", true);
      //
      //             DataProvider.uploadXls("questionnaire", $stateParams.referenceId, formData).then(function (result) {
      //
      //                 var errors = "";
      //                 if (result.data.status != "ok") {
      //                     errors = result.data.errors.join(", ");
      //                 }
      //
      //                 bootbox.alert("Upload response: " + result.data.status + ", " + errors);
      //                 $scope.loadQuestions();
      //             })
      //
      //         }
      //         $("#fupload")[0].value = null;
      //     })
      // });


    }
  ]
);
var QuestionModalController = function ($scope, $modalInstance, DataProvider, Log) {
  $scope.filter = "";
  $scope.selected = {question: ''};


  var timer = null;
  $scope.update = function (filter) {


    if (timer != null) {
      clearTimeout(timer)
    }

    timer = setTimeout(function () {
      $scope.selected = {question: ''};
      $scope.filter = filter;
      DataProvider.getAllQuestions(filter).success(function (d) {
        $scope.questions = d;
      }).error(function (d) {
        Log.error(d);
      });
    }, 400);


  };
  $scope.update("");

  $scope.ok = function () {
    Log.log($scope.selected);
    if ($scope.selected.question == "") {
      Log.log($scope.filter);
      $modalInstance.close($scope.filter);
    } else {
      $modalInstance.close($scope.selected);
    }
  };

  $scope.select = function (item) {
    $scope.selected = item;
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
};

var AnswerOptionModalController = function ($scope, $modalInstance, Log, EditData, FeatureList, DataProvider, imageServerRoot, question) {
  angular.element(document).ready(function () {
    // Example #1
    // instantiate the bloodhound suggestion engine
    var numbers = new Bloodhound({
      datumTokenizer: function (d) {
        return Bloodhound.tokenizers.whitespace(d);
      },
      queryTokenizer: Bloodhound.tokenizers.whitespace,
      local: FeatureList
    });

    // initialize the bloodhound suggestion engine
    numbers.initialize();

    // instantiate the typeahead UI
    if (App.isRTL()) {
      $('#typeahead_example_1').attr("dir", "rtl");
    }
    $('#typeahead_example_1').typeahead(null, {
      displayKey: 'num',
      hint: (App.isRTL() ? false : true),
      source: numbers.ttAdapter()
    });


    // console.log("init", $("#iupload"));
    // setTimeout(function () {
    //   $("#iupload").on("change", function (event) {
    //     console.log("uploadXls", event, event.target);
    //
    //     var file = $("#iupload")[0].files[0];
    //
    //     var formData = new FormData();
    //     formData.append('file', file, file.name);
    //
    //     DataProvider.uploadImage(formData).then(function (result) {
    //
    //       var errors = "";
    //       if (result.data.status != "ok") {
    //         errors = result.data.errors.join(", ");
    //       } else {
    //         $("#image_token").val(result.data.fid);
    //         if (!$scope.image_tokens) {
    //           $scope.image_tokens = [];
    //         }
    //         $scope.image_tokens.push(result.data.fid);
    //       }
    //     });
    //
    //   })
    // }, 300);


  });

  $scope.uploadImage = function () {

    console.log("uploadXls", event, event.target);

    var file = $("#iupload")[0].files[0];

    var formData = new FormData();
    formData.append('file', file, file.name);

    DataProvider.uploadImage(formData).then(function (result) {
      var errors = "";
      if (result.data.status != "ok") {
        errors = result.data.errors.join(", ");
      } else {
        $("#image_token").val(result.data.fid);
        if (!$scope.image_tokens) {
          $scope.image_tokens = [];
        }
        $scope.image_tokens.push(result.data.fid);
      }
    });

  };


  var parent = $scope;
  $scope.imageServerRoot = imageServerRoot;

  console.log("question of this answer model", question);
  $scope.question = question;

  $scope.select = function (item) {
    console.log("feature selected", item);
    //var item = FeatureList[i];
    if (item != undefined && $scope.data[item] == undefined) {
      Log.info("added ", item);
      $scope.data[item] = {formula: "", symbols: [], substitutions: []}
    }
    if (item != undefined) {
      $scope.feature = item;
    }
  };
  $scope.featureList = FeatureList;
  if (EditData) {
    Log.info("Edit the answer", EditData);
    $scope.value = EditData.value;
    $scope.label = EditData.label;
    $scope.image_tokens = EditData.image_tokens;
    if (EditData.data) {
      $scope.data = EditData.data;
    } else {
      $scope.data = {};
    }
    Log.info("data", $scope.data);
  } else {
    $scope.value = "";
    $scope.label = "";
    $scope.data = {};
    $scope.image_tokens = [];
  }
  $scope.removeFeature = function (feature) {
    if ($scope.data[feature]) {
      delete $scope.data[feature];
    }
  };

  $scope.updateFeature = function (feature) {
    if ($scope.data[feature]) {
      $scope.data[feature].formula = $scope.selection.formula;
      $scope.data[feature].symbols = $scope.selection.symbols.split(" ");
      var substitutions = $scope.selection.substitutions;
      if (substitutions && substitutions.trim().length > 0) {
        $scope.data[feature].substitutions = substitutions.split("\n").map(function (e) {
          var parts = e.split(" -> ");
          return {variable: parts[0].trim(), value: parts[1].trim()}
        });
      } else {
        $scope.data[feature].substitutions = [];
      }
    }
  };


  $scope.changed = function (feature) {
    if (!feature) {
      return;
    }
    var selectedFeature = $scope.data[feature];
    var selection = {};
    selection.formula = selectedFeature.formula;
    selection.symbols = selectedFeature.symbols.join(" ");
    if (selectedFeature.substitutions) {
      selection.substitutions = selectedFeature.substitutions.map(function (e) {
        return e.variable + " -> " + e.value
      }).join("\n");
    } else {
      selection.substitutions = "";
    }
    parent.selection = selection;
  };

  $scope.ok = function (value, label, data, image_tokens) {
    Log.log(value, label, data);
    var referenceId = undefined;
    if (EditData && EditData.reference_id) {
      referenceId = EditData.reference_id;
    }

    $modalInstance.close({
      value: value,
      label: label,
      image_tokens: image_tokens,
      data: JSON.stringify(data),
      reference_id: referenceId
    });
  };

  $scope.removeImage = function (image) {
    console.log(image, $scope.image_tokens);
    var index = $scope.image_tokens.indexOf(image);
    if (index > -1) {
      $scope.image_tokens.splice(index, 1);
    }
  };

  // $scope.removeImage = function(imageServerRoot, img) {
  //     DataProvider.deleteImage(imageServerRoot, img)
  // }

  $scope.enableImage = function() {
      return $scope.questionType.includes("image");
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
};

