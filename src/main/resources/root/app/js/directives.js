/***
 GLobal Directives
 ***/

MetronicApp.factory('GroupService', function ($rootScope) {
  var permissionList;
  return {
    setGroups: function (permissions) {
      permissionList = permissions;
      $rootScope.$broadcast('groups.changed');
    },
    hasGroup: function (group) {
      if (!permissionList || permissionList.length < 1) {
        return false;
      }
      return permissionList.indexOf(group) > -1;
    }
  };
});

// Route State Load Spinner(used on page or content load)
MetronicApp.directive('ngSpinnerBar', ['$rootScope',
  function ($rootScope) {
    return {
      link: function (scope, element, attrs) {
        // by defult hide the spinner bar
        element.addClass('hide'); // hide spinner bar by default

        // display the spinner bar whenever the route changes(the content part started loading)
        $rootScope.$on('$stateChangeStart', function () {
          element.removeClass('hide'); // show spinner bar
        });

        // hide the spinner bar on rounte change success(after the content loaded)
        $rootScope.$on('$stateChangeSuccess', function () {
          element.addClass('hide'); // hide spinner bar
          $('body').removeClass('page-on-load'); // remove page loading indicator
          Layout.setSidebarMenuActiveLink('match'); // activate selected link in the sidebar menu

          // auto scorll to page top
          setTimeout(function () {
            App.scrollTop(); // scroll to the top on content load
          }, $rootScope.settings.layout.pageAutoScrollOnLoad);
        });

        // handle errors
        $rootScope.$on('$stateNotFound', function () {
          element.addClass('hide'); // hide spinner bar
        });

        // handle errors
        $rootScope.$on('$stateChangeError', function () {
          element.addClass('hide'); // hide spinner bar
        });
      }
    };
  }
]);

MetronicApp.directive('hasGroup', ['$rootScope', 'GroupService',
  function ($rootScope, GroupService) {
    return {
      link: function (scope, element, attrs) {
        console.log(attrs, attrs.hasGroup)
        var value = attrs.hasGroup.toString().trim();
        var notPermissionFlag = value[0] === '!';
        if (notPermissionFlag) {
          value = value.slice(1).trim();
        }

        function toggleVisibilityBasedOnPermission() {
          var hasPermission = GroupService.hasGroup(value);
          if (hasPermission && !notPermissionFlag || !hasPermission && notPermissionFlag) {
            element[0].style.display = 'block';
          }
          else {
            element[0].style.display = 'none';
          }
        }

        toggleVisibilityBasedOnPermission();
        scope.$on('groups.changed', toggleVisibilityBasedOnPermission);
      }
    };
  }
]);

// Handle global LINK click
MetronicApp.directive('a', function () {
  return {
    restrict: 'E',
    link: function (scope, elem, attrs) {
      if (attrs.ngClick || attrs.href === '' || attrs.href === '#') {
        elem.on('click', function (e) {
          e.preventDefault(); // prevent link click for above criteria
        });
      }
    }
  };
});

// Handle Dropdown Hover Plugin Integration
MetronicApp.directive('dropdownMenuHover', function () {
  return {
    link: function (scope, elem) {
      elem.dropdownHover();
    }
  };
});