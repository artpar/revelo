#!/usr/bin/env bash

export NAMESPACE=revelo
kubectl --namespace $NAMESPACE create configmap revelo-config --from-file=config.yml