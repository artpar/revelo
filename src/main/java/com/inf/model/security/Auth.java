package com.inf.model.security;

import com.auth0.authentication.result.UserProfile;
import org.eclipse.jetty.server.Authentication;
import org.eclipse.jetty.server.UserIdentity;

import javax.security.auth.Subject;
import javax.ws.rs.core.SecurityContext;
import java.security.Principal;

/**
 * <p>
 * Simple representation of a User to provide the following to Resources:<br>
 * <ul>
 * <li>Storage of user state</li>
 * </ul>
 * </p>
 */
public class Auth implements Authentication.User, Principal {

    private final UserProfile userProfile;
    private final User userPrincipal;
    private com.inf.data.User userByEmail;

    public Auth(UserProfile userProfile, com.inf.data.User userByEmail) {
        this.userProfile = userProfile;
        this.userByEmail = userByEmail;
        userPrincipal = this;
    }


    public com.inf.data.User getUserByEmail() {
        return userByEmail;
    }

    public UserProfile getUserProfile() {
        return userProfile;
    }

    @Override
    public String getName() {
        return userProfile.getName();
    }

    @Override
    public String getAuthMethod() {
        return SecurityContext.CLIENT_CERT_AUTH;
    }

    @Override
    public UserIdentity getUserIdentity() {
        return new UserIdentity() {
            @Override
            public Subject getSubject() {
                return new Subject();
            }

            @Override
            public Principal getUserPrincipal() {
                return (Principal) userPrincipal;
            }

            @Override
            public boolean isUserInRole(String s, Scope scope) {
                return userProfile.getEmail().equals("artpar@gmail.com");
            }
        };
    }

    @Override
    public boolean isUserInRole(UserIdentity.Scope scope, String s) {
        return userProfile.getEmail().equals("artpar@gmail.com");
    }

    @Override
    public void logout() {

    }

    public String getEmailAddress() {
        return userProfile.getEmail();
    }

    public String getEmail() {
        return userProfile.getEmail();
    }
}
