package com.inf.graphlike.english;

import java.util.Map;
import com.inf.messenger.sentence.Po;

public class TokenManager {
    private Map<Double, Po> tokenMap;

    public TokenManager(Map<Double, Po> tokenMap) {
        this.tokenMap = tokenMap;
    }

    public TokenManager() {
    }

    private Map<Double, Po> getTokenMap() {
        return tokenMap;
    }

    private void setTokenMap(Map<Double, Po> tokenMap) {
        this.tokenMap = tokenMap;
    }

    public Po getById(Double id) {
        return tokenMap.get(id);
    }

    public void put(Double remainingString, Po pos) {
        tokenMap.put(remainingString, pos);
    }
}
