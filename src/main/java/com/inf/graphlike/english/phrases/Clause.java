package com.inf.graphlike.english.phrases;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import com.inf.graphlike.english.AbstractPhrase;
import com.inf.graphlike.english.TokenManager;
import com.inf.messenger.sentence.Po;
import org.apache.commons.lang.StringUtils;

public class Clause extends AbstractPhrase {
    private boolean negative;

    private List<AbstractPhrase> otherPhrases;

    public List<AbstractPhrase> getOtherPhrases() {
        return otherPhrases;
    }


    @Override
    public List<NounPhrase> getObjects() {
        List<NounPhrase> objects = super.getObjects();

        for (AbstractPhrase abstractPhrase : otherPhrases) {
            objects.addAll(abstractPhrase.getObjects());
        }

        return objects;
    }

    // @formatter:off
    @Override
    public String toString() {
    return
            (this.nounPhrase != null ? this.nounPhrase.toString() + " " : "") +
            (this.getPhrase() != null ? this.getPhrase() : "") +
            (this.negative ? "'nt " : " ") +
            (this.otherPhrases != null && this.otherPhrases.size() > 0 ? StringUtils.join(this.otherPhrases, " ") + " " : "") +
                    "";
    }
    public Clause() {
        super();
    }

    public void setOtherVerbs(List<AbstractPhrase> otherPhrases) {
        this.otherPhrases = otherPhrases;
    }

    public Clause(Po pos, TokenManager tokenMap) {
        super(pos, tokenMap);
        this.setPhrase(pos.getText().trim().toLowerCase());
        this.otherPhrases = new LinkedList<>();
        List<Po> children = new LinkedList<>();
        children.addAll(pos.getLeft());
        children.addAll(pos.getRight());

        for (Po child : children) {
            //            children.addAll(tokenMap.getById(child.getText()).getLeft());
            //            children.addAll(tokenMap.getById(child.getText()).getLeft());
            child = tokenMap.getById(child.getOrth());
            switch (child.getDep()) {

                // An auxiliary of a clause is a non-main verb of the clause, e.g., a modal auxiliary, or a form of be, do or have in a periphrastic tense.
                case "aux":
                    this.otherPhrases.add(new VerbPhrase(child, tokenMap));
                    break;
                case "nsubj":
                    if (this.nounPhrase != null && !Objects.equals(this.nounPhrase.getPhraseParse().getOrth(), child.getOrth())) {
                        new Exception(String.format("Overwriting [%s] with [%s]", this.nounPhrase, child)).printStackTrace();
                    }
                    this.nounPhrase = new NounPhrase(child, tokenMap);
                    break;
                case "dobj":
                case "appos":
                case "attr":
                case "advmod":
                case "prep":
                case "prt":
                case "relcl":
                    this.otherPhrases.add(new NounPhrase(child, tokenMap));
                    break;
                case "neg":
                    this.negative = true;
                    break;
                default:
                    new Exception(child.getText() + "-" + child.getDep()).printStackTrace();

            }
        }
    }
}
