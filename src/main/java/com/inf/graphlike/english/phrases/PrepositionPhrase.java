package com.inf.graphlike.english.phrases;

import java.util.LinkedList;
import java.util.List;
import com.inf.graphlike.english.AbstractPhrase;
import com.inf.graphlike.english.TokenManager;
import com.inf.messenger.sentence.Po;
import org.apache.commons.lang.StringUtils;

public class PrepositionPhrase extends AbstractPhrase {
    // @formatter:off
    @Override
    public String toString() {
        return (getPhrase() != null ? getPhrase() + " " : "") +
                (this.nounPhrase != null ? this.nounPhrase.toString() + " " : "") +
                (this.complement != null ? StringUtils.join(this.complement, " ") + " " : "") +
                "";
    }

    @Override
    public List<NounPhrase> getObjects() {
        List<NounPhrase> list = super.getObjects();
        return list;
    }

    public PrepositionPhrase() {
    }

    public PrepositionPhrase(Po pos, TokenManager tokenMap) {
        super(pos, tokenMap);
        this.setPhrase(pos.getText().trim().toLowerCase());

        List<Po> children = new LinkedList<>();
        children.addAll(pos.getLeft());
        children.addAll(pos.getRight());
        for (Po child : children) {
            child = tokenMap.getById(child.getOrth());
            switch (child.getDep()) {

                case "pobj":
                    this.nounPhrase = new NounPhrase(child, tokenMap);
                    break;
                case "punct":
                    break;
                case "prep":
                    this.complement.add(new NounPhrase(child, tokenMap));
                    break;
                default:
                    new Exception("Prep:" + child.getText() + "-" + child.getDep()).printStackTrace();
            }
        }
    }
}
