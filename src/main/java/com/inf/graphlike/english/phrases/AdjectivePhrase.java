package com.inf.graphlike.english.phrases;

import java.util.LinkedList;
import java.util.List;
import com.inf.graphlike.english.AbstractPhrase;
import com.inf.graphlike.english.TokenManager;
import com.inf.messenger.sentence.Po;

public class AdjectivePhrase extends AbstractPhrase {
    @Override
    public String toString() {
        return getPhrase() + (nounPhrase != null ?  " " + nounPhrase.toString() : " ");
    }

    public AdjectivePhrase() {
        super();
    }


    public AdjectivePhrase(Po pos, TokenManager tokenMap) {
        super(pos, tokenMap);

        this.setPhrase(pos.getText().toLowerCase().trim());

        List<Po> children = new LinkedList<>();
        children.addAll(pos.getLeft());
        children.addAll(pos.getRight());
        for (Po child : children) {
            child = tokenMap.getById(child.getOrth());
            switch (child.getDep()) {
                case "auxpass":
                    this.nounPhrase.getVerbs().add(new VerbPhrase(child, tokenMap));
                    break;
                case "advmod":
                    this.nounPhrase.getAdverbs().add(new AdverbPhrase(child, tokenMap));
                    break;
                default:
                    new Exception(child.getText() + "-" + child.getDep()).printStackTrace();

            }
        }


    }
}
