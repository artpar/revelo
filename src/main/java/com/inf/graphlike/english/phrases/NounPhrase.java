package com.inf.graphlike.english.phrases;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import com.inf.graphlike.english.AbstractPhrase;
import com.inf.graphlike.english.TokenManager;
import com.inf.messenger.sentence.Po;
import org.apache.commons.lang.StringUtils;

public class NounPhrase extends AbstractPhrase {
    private String object;

    private String pronoun;

    private String subject;

    private List<Clause> clauses;

    private List<AdjectivePhrase> adjectivePhrases;

    private List<NounPhrase> adjunct;

    private List<PrepositionPhrase> prepositionalPhrases;

    private List<AdverbPhrase> adverbs;

    private List<VerbPhrase> verbs;

    @Override
    public List<NounPhrase> getObjects() {
        List<NounPhrase> list = super.getObjects();

        if (adjunct != null) {
            list.addAll(adjunct);
            for (NounPhrase nounPhrase : adjunct) {
                list.addAll(nounPhrase.getObjects());
            }

        }
        for (AdjectivePhrase adjectivePhrase : adjectivePhrases) {
            list.addAll(adjectivePhrase.getObjects());
        }
        for (PrepositionPhrase prepositionPhrase : prepositionalPhrases) {
            list.addAll(prepositionPhrase.getObjects());
        }
        for (AdverbPhrase adverb : adverbs) {
            list.addAll(adverb.getObjects());
        }
        for (VerbPhrase verb : verbs) {
            list.addAll(verb.getObjects());
        }
        return list;
    }

    // @formatter:off
    @Override
    public String toString() {
        return
                (this.adjectivePhrases != null && this.adjectivePhrases.size() > 0 ? this.adjectivePhrases.toString() + " " : "") +
                (this.adjunct != null && this.adjunct.size() > 0 ? this.adjunct.toString() + " " : "") +
                (this.adverbs != null && this.adverbs.size() > 0 ? this.adverbs.toString() + " " : "") +
                (this.verbs != null && this.verbs.size() > 0 ? this.verbs.toString() + " " : "") +
//                (this.nounPhrase != null ? this.nounPhrase.toString() + " " : "") +
                (this.getPhrase() != null ? this.getPhrase() + " " : "") +
                (this.clauses != null && this.clauses.size() > 0 ?  StringUtils.join(this.clauses, " ") + " " : "") +
                (this.prepositionalPhrases != null && this.prepositionalPhrases.size() > 0 ? StringUtils.join(this.prepositionalPhrases, " ") + " " : "")
                ;

    }




    public NounPhrase(Po pos, TokenManager tokenMap) {
        super(pos, tokenMap);

        this.setPhrase(pos.getText().trim().toLowerCase());

        if (pos.getTag().equals("PRP$")) {

            String[] parts = pos.getText().split(" ");
            String possessivePronoun = parts[0];
            if (parts.length > 1) {

                String[] remaining = Arrays.copyOfRange(parts, 1, parts.length, String[].class);
                String remainingString = StringUtils.join(remaining, " ");

                this.pronoun = possessivePronoun;
                this.object = remainingString;

                switch(possessivePronoun) {

                    case "your":
                        this.subject = "second";
                    case "my":
                        this.subject = "first";
                    case "his":
                    case "her":
                        this.subject = "third";
                    case "our":
                        this.subject = "first";
                    case "their":
                        this.subject = "third";
                    case "who":
                        this.subject = "first";


                }
            }



        }

        this.adjectivePhrases = new LinkedList<>();
        this.clauses = new LinkedList<>();
        this.adjunct = new LinkedList<>();
        this.prepositionalPhrases = new LinkedList<>();
        this.adverbs = new LinkedList<>();
        this.verbs = new LinkedList<>();

        List<Po> children = new LinkedList<>();
        children.addAll(pos.getLeft());
        children.addAll(pos.getRight());

        for (Po child : children) {
            //            children.addAll(tokenMap.getById(child.getText()).getLeft());
            //            children.addAll(tokenMap.getById(child.getText()).getLeft());
            child = tokenMap.getById(child.getOrth());
            switch (child.getDep()) {
                case "acl":
                    this.adjectivePhrases.add(new AdjectivePhrase(child, tokenMap));
                    break;
                case "amod":
                    this.adjectivePhrases.add(new AdjectivePhrase(child, tokenMap));
                    break;
                case "prep":
                    this.addPreposition(new PrepositionPhrase(child, tokenMap));
                    break;
                case "appos":
                case "relcl":
                case "mark":
                case "dobj":
                case "advmod":
                case "det":
                    this.clauses.add(new Clause(child, tokenMap));
                    break;
                case "nummod":
                    this.clauses.add(new Clause(child, tokenMap));
                    break;
                case "aux":
                case "conj":
                case "cc":
                case "pobj":
                case "advcl":
                case "ccomp":
                case "pcomp":
                case "xcomp":
                    this.clauses.add(new Clause(child, tokenMap));
                    break;
                case "punct":
                    break;
                default:
                    new Exception(child.getText() + "-" + child.getDep()).printStackTrace();
                    break;

            }
        }
    }

    public NounPhrase() {
        super();
        this.adjectivePhrases = new LinkedList<>();
        this.clauses = new LinkedList<>();
        this.adjunct = new LinkedList<>();
        this.prepositionalPhrases = new LinkedList<>();
        this.adverbs = new LinkedList<>();
        this.verbs = new LinkedList<>();
    }

    public List<VerbPhrase> getVerbs() {
        return verbs;
    }

    public void setVerbs(List<VerbPhrase> verbs) {
        this.verbs = verbs;
    }

    public List<Clause> getClauses() {
        return clauses;
    }

    public void setClauses(List<Clause> clauses) {
        this.clauses = clauses;
    }

    public List<AdjectivePhrase> getAdjectivePhrases() {
        return adjectivePhrases;
    }

    public void setAdjectivePhrases(List<AdjectivePhrase> adjectivePhrases) {
        this.adjectivePhrases = adjectivePhrases;
    }

    public List<NounPhrase> getAdjunct() {
        return adjunct;
    }

    public void setAdjunct(List<NounPhrase> adjunct) {
        this.adjunct = adjunct;
    }

    public List<PrepositionPhrase> getPrepositionalPhrases() {
        return prepositionalPhrases;
    }

    public void setPrepositionalPhrases(List<PrepositionPhrase> prepositionalPhrases) {
        this.prepositionalPhrases = prepositionalPhrases;
    }

    public List<AdverbPhrase> getAdverbs() {
        return adverbs;
    }

    public void setAdverbs(List<AdverbPhrase> adverbs) {
        this.adverbs = adverbs;
    }

    public void addPreposition(PrepositionPhrase prepositionPhrase) {
        this.prepositionalPhrases.add(prepositionPhrase);
    }

    public void addAdjective(AdjectivePhrase adjectivePhrase) {
        this.adjectivePhrases.add(adjectivePhrase);
    }
}
