package com.inf.graphlike.english.phrases;

import java.util.LinkedList;
import java.util.List;
import com.inf.graphlike.english.AbstractPhrase;
import com.inf.graphlike.english.TokenManager;
import com.inf.messenger.sentence.Po;
import org.apache.commons.lang.StringUtils;

public class VerbPhrase extends AbstractPhrase {
    private boolean isNegative;

    private List<AdverbPhrase> adverbs;

    private List<NounPhrase> targetObjects;

    private String determiner;

    private List<AdjectivePhrase> adjectives;

    private List<VerbPhrase> auxiliryVerbs;

    public VerbPhrase() {
    }

    public List<NounPhrase> getObjects() {

        List<NounPhrase> objects = super.getObjects();

        for (AdverbPhrase adverb : adverbs) {
            objects.addAll(adverb.getObjects());
        }

        for (NounPhrase obj : targetObjects) {
            objects.add(obj);
            objects.addAll(obj.getObjects());
        }

        for (AdjectivePhrase adjective : adjectives) {
            objects.addAll(adjective.getObjects());
        }

        for (VerbPhrase verbPhrase : auxiliryVerbs) {
            objects.addAll(verbPhrase.getObjects());
        }
        return objects;
    }

    @Override
    public String toString() {
        // @formatter:off
        return
                (this.adverbs != null && this.adverbs.size() > 0 ? StringUtils.join(this.adverbs, " ") + " " : "") +
                (this.adjectives != null && this.adjectives.size() > 0 ? StringUtils.join(this.adjectives, " ") + " " : "") +
                (this.auxiliryVerbs != null && this.auxiliryVerbs.size() > 0 ? StringUtils.join(this.auxiliryVerbs, " ") + " " : "") +
                (this.getPhrase() != null ? this.getPhrase() + " ": "") +
                (this.targetObjects != null && this.targetObjects.size() > 0 ? StringUtils.join(this.targetObjects, " ") + " " : "") +
                (this.nounPhrase != null ? this.nounPhrase.toString() + " " : "") +
                        "";
    }

    public VerbPhrase(Po po, TokenManager tokenMap) {
        super(po, tokenMap);
        this.setPhrase(po.getLemma().trim().toLowerCase());
        this.adjectives = new LinkedList<>();
        this.auxiliryVerbs = new LinkedList<>();
        this.targetObjects = new LinkedList<>();
        this.adverbs = new LinkedList<>();
        po = tokenMap.getById(po.getOrth());
        LinkedList<Po> children = new LinkedList<>();
        children.addAll(po.getLeft());
        children.addAll(po.getRight());

        switch (po.getDep()) {
            case "advcl":
            case "attr":
                this.setPhrase(po.getText().trim().toLowerCase());
                break;

        }

        for (Po child : children) {
            child = tokenMap.getById(child.getOrth());
            switch (child.getDep()) {

                case "attr":
                    switch (child.getTag()) {
                        case "WP":
                        case "NN":
                        default:
                            this.targetObjects.add(new NounPhrase(child, tokenMap));
                            break;
                    }
                    break;

                case "neg":
                    this.isNegative = true;

                case "aux":
                case "auxpass":
                    this.auxiliryVerbs.add(new VerbPhrase(child, tokenMap));
                    break;
                case "advmod":
                    this.targetObjects.add(new NounPhrase(child, tokenMap));
                    break;
                case "acomp":
                case "prep":
                    this.targetObjects.add(new NounPhrase(child, tokenMap));

                    break;
                case "dobj":
                case "dative":
                    this.targetObjects.add(new NounPhrase(child, tokenMap));
                    break;
                case "nsubjpass":
                case "compound":
                    this.nounPhrase = new NounPhrase(child, tokenMap);
                    break;

                case "ccomp":
                case "xcomp":
                case "amod":
                case "appos":
                case "npadvmod":
                case "conj":
                case "cc":
                    this.targetObjects.add(new NounPhrase(child, tokenMap));
                    break;
                case "punct":
                    break;
                case "det":
                    this.determiner = child.getText();
                    break;
                case "advcl":
                case "nummod":
                    this.adverbs.add(new AdverbPhrase(child, tokenMap));
                    break;
                default:
                    new Exception(child.getText() + "-" + child.getDep()).printStackTrace();
                    break;

            }

        }

    }

    public List<AdverbPhrase> getAdverbs() {
        return adverbs;
    }

    public void setAdverbs(List<AdverbPhrase> adverbs) {
        this.adverbs = adverbs;
    }

    public List<AdjectivePhrase> getAdjectives() {
        return adjectives;
    }

    public void setAdjectives(List<AdjectivePhrase> adjectives) {
        this.adjectives = adjectives;
    }

    public List<VerbPhrase> getAuxiliryVerbs() {
        return auxiliryVerbs;
    }

    public void setAuxiliryVerbs(List<VerbPhrase> auxiliryVerbs) {
        this.auxiliryVerbs = auxiliryVerbs;
    }

    public NounPhrase getNounPhrase() {
        return nounPhrase;
    }

    public void setNounPhrase(NounPhrase nounPhrase) {
        this.nounPhrase = nounPhrase;
    }

    public List<NounPhrase> getTargetObjects() {
        return targetObjects;
    }

    public void setObjects(List<NounPhrase> objects) {
        this.targetObjects = objects;
    }

    public void addObject(NounPhrase nounPhrase) {

    }
}
