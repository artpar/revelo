package com.inf.graphlike.english.phrases;

import java.util.LinkedList;
import java.util.List;
import com.inf.graphlike.english.AbstractPhrase;
import com.inf.graphlike.english.TokenManager;
import com.inf.messenger.sentence.Po;

public class AdverbPhrase extends AbstractPhrase {
    private List<AbstractPhrase> classifier;

    @Override
    public String toString() {
        return (classifier.size() > 0 ? classifier.toString() : " ") + " " + getPhrase() + (this.nounPhrase != null ? " " + this.nounPhrase
                .toString() : "");
    }

    @Override
    public List<NounPhrase> getObjects() {
        List<NounPhrase> objects = super.getObjects();

        for (AbstractPhrase abstractPhrase : classifier) {
            objects.addAll(abstractPhrase.getObjects());
        }

        return objects;
    }

    public AdverbPhrase() {
        super();
    }

    public AdverbPhrase(Po pos, TokenManager tokenMap) {
        super(pos, tokenMap);
        this.setPhrase(pos.getText().trim().toLowerCase());
        this.classifier = new LinkedList<>();
        List<Po> children = new LinkedList<>();
        children.addAll(pos.getLeft());
        children.addAll(pos.getRight());

        for (Po child : children) {
            //            children.addAll(tokenMap.getById(child.getText()).getLeft());
            //            children.addAll(tokenMap.getById(child.getText()).getLeft());
            child = tokenMap.getById(child.getOrth());
            switch (child.getDep()) {
                case "det":
                    this.classifier.add(new NounPhrase(child, tokenMap));
                    break;
                case "auxpass":
                case "advmod":
                case "mark":
                case "nsubjpass":
                    this.classifier.add(new NounPhrase(child, tokenMap));
                    break;
            }
        }
    }
}
