package com.inf.graphlike.english;

import java.util.LinkedList;
import java.util.List;
import com.inf.graphlike.english.phrases.NounPhrase;
import com.inf.graphlike.english.phrases.VerbPhrase;
import com.inf.messenger.sentence.Po;
import com.inf.messenger.sentence.Sentence;

public class SentenceImpl {
    private boolean isNegative;

    private boolean isExclamatory;

    private boolean isQuestion;

    private String person;

    private String voice;

    private String sentence;

    private NounPhrase nounPhrase;

    private VerbPhrase verbPhrase;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SentenceImpl sentence = (SentenceImpl) o;

        if (isNegative != sentence.isNegative) {
            return false;
        }
        if (isExclamatory != sentence.isExclamatory) {
            return false;
        }
        if (isQuestion != sentence.isQuestion) {
            return false;
        }
        if (nounPhrase != null ? !nounPhrase.equals(sentence.nounPhrase) : sentence.nounPhrase != null) {
            return false;
        }
        return verbPhrase != null ? verbPhrase.equals(sentence.verbPhrase) : sentence.verbPhrase == null;
    }

    @Override
    public int hashCode() {
        int result = (isNegative ? 1 : 0);
        result = 31 * result + (isExclamatory ? 1 : 0);
        result = 31 * result + (isQuestion ? 1 : 0);
        result = 31 * result + (nounPhrase != null ? nounPhrase.hashCode() : 0);
        result = 31 * result + (verbPhrase != null ? verbPhrase.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return this.sentence;
    }

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }

    public String getVoice() {
        return voice;
    }

    public void setVoice(String voice) {
        this.voice = voice;
    }

    public SentenceImpl(Sentence sentence, TokenManager tokenMap) {

        Po root = sentence.getRoot();
        if (root == null) {
            this.sentence = sentence.getSentence();
            return;
        }
        this.verbPhrase = new VerbPhrase(tokenMap.getById(root.getOrth()), tokenMap);
        this.nounPhrase = this.verbPhrase.getNounPhrase();
        this.verbPhrase.setNounPhrase(null);
        this.sentence = sentence.getSentence();
        this.isNegative = sentence.getSentenceType().equals("negative");
        this.isExclamatory = sentence.getSentenceType().equals("exclamatory");
        this.isQuestion = sentence.getSentenceType().equals("interrogative");

    }

    public boolean isNegative() {
        return isNegative;
    }

    public boolean isExclamatory() {
        return isExclamatory;
    }

    public boolean isQuestion() {
        return isQuestion;
    }

    public String getSentence() {
        return sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }

    public SentenceImpl() {
    }

    public void setNegative(boolean negative) {
        isNegative = negative;
    }

    public void setExclamatory(boolean exclamatory) {
        isExclamatory = exclamatory;
    }

    public void setQuestion(boolean question) {
        isQuestion = question;
    }

    public NounPhrase getNounPhrase() {
        return nounPhrase;
    }

    public void setNounPhrase(NounPhrase nounPhrase) {
        this.nounPhrase = nounPhrase;
    }

    public VerbPhrase getVerbPhrase() {
        return verbPhrase;
    }

    public void setVerbPhrase(VerbPhrase verbPhrase) {
        this.verbPhrase = verbPhrase;
    }

    public List<NounPhrase> getObjects() {
        List<NounPhrase> list = new LinkedList<>();

        if (nounPhrase != null) {

            list.add(this.nounPhrase);
            list.addAll(this.nounPhrase.getObjects());
        }
        if (this.verbPhrase != null) {
            list.addAll(this.verbPhrase.getObjects());
        }

        return list;
    }
}
