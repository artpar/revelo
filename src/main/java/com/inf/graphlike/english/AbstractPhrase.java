package com.inf.graphlike.english;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import com.inf.graphlike.english.phrases.NounPhrase;
import com.inf.messenger.sentence.Po;

public abstract class AbstractPhrase {
    private TokenManager tokenMap;

    private String phrase;

    protected Po phraseParse;

    protected List<Po> children;

    protected NounPhrase nounPhrase;

    protected List<NounPhrase> complement;

    public AbstractPhrase() {

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AbstractPhrase that = (AbstractPhrase) o;

        if (phrase != null && !phrase.equals(that.phrase)) {
            return false;
        }
        if (phraseParse != null && !phraseParse.equals(that.phraseParse)) {
            return false;
        }
        if (children != null) {
            if (!children.equals(that.children)) {
                return false;
            }
        }
        return nounPhrase != null ? nounPhrase.equals(that.nounPhrase) : that.nounPhrase == null;
    }

    @Override
    public int hashCode() {
        int result = 0;
        if (phrase != null) {
            result = phrase.hashCode();
        }
        if (phraseParse != null) {
            result = 31 * result + phraseParse.hashCode();
        }
        if (children != null) {
            result = 31 * result + children.hashCode();
        }
        result = 31 * result + (nounPhrase != null ? nounPhrase.hashCode() : 0);
        return result;
    }

    public String getPhrase() {

        if (Objects.equals(phrase, "s")) {
            return "be";
        }

        return phrase;
    }

    public List<NounPhrase> getComplement() {
        return complement;
    }

    public void setComplement(List<NounPhrase> complement) {
        this.complement = complement;
    }

    public void setPhrase(String phrase) {
        if (phrase == null) {
            this.phrase = null;
            return;
        }
        this.phrase = phrase.toLowerCase().trim();
    }

    public NounPhrase getNounPhrase() {
        return nounPhrase;
    }

    public void setNounPhrase(NounPhrase nounPhrase) {
        this.nounPhrase = nounPhrase;
    }

    public Po getPhraseParse() {
        return phraseParse;
    }

    public void setPhraseParse(Po phraseParse) {
        this.phraseParse = phraseParse;
    }

    public List<Po> getChildren() {
        return children;
    }

    public void setChildren(List<Po> children) {
        this.children = children;
    }

    public AbstractPhrase(Po pos, TokenManager tokenMap) {
        this.phraseParse = pos;
        nounPhrase = new NounPhrase();
        complement = new LinkedList<>();
        this.children = new LinkedList<>();
        this.tokenMap = tokenMap;
        pos = tokenMap.getById(pos.getOrth());

        List<Po> children = new LinkedList<>();
        children.addAll(pos.getLeft());
        children.addAll(pos.getRight());
        for (Po child : children) {
            //            children.addAll(tokenMap.getById(child.getText()).getLeft());
            //            children.addAll(tokenMap.getById(child.getText()).getLeft());
            child = tokenMap.getById(child.getOrth());

            if (child == null) {
                new Exception("Failed to get child by orth id").printStackTrace();
                continue;
            }

            this.children.add(child);
            switch (child.getDep()) {
                case "pcomp":
                case "nsubj":
                case "dative":
                case "nsubjpass":
                    NounPhrase nounPhrase = new NounPhrase(child, tokenMap);
                    //                    this.nounPhrase.setNounPhrase(nounPhrase);
                    this.nounPhrase.setPhrase(nounPhrase.getPhrase());
                    this.nounPhrase.getAdjectivePhrases().addAll(nounPhrase.getAdjectivePhrases());
                    this.nounPhrase.getAdjunct().addAll(nounPhrase.getAdjunct());
                    this.nounPhrase.getAdverbs().addAll(nounPhrase.getAdverbs());
                    this.nounPhrase.getClauses().addAll(nounPhrase.getClauses());
                    this.nounPhrase.getPrepositionalPhrases().addAll(nounPhrase.getPrepositionalPhrases());
                    this.nounPhrase.phraseParse = nounPhrase.phraseParse;
                    break;

                //                case "prep":
                //                    this.nounPhrase.addPreposition(new PrepositionPhrase(child, tokenMap));
                //                    break;

                case "punct":
                    break;
                default:
                    //                    new Exception("[ABS]" + child.getText() + "-" + child.getDep()).printStackTrace();
                    break;

            }

        }

    }

    public List<String> getPhrases() {
        return null;
    }

    public List<NounPhrase> getObjects() {

        List<NounPhrase> objects = new LinkedList<>();

        if (nounPhrase != null && this.nounPhrase.getPhrase() != null && this.nounPhrase.getPhrase().length() > 0) {
            objects.add(this.nounPhrase);
            objects.addAll(nounPhrase.getObjects());
        }

        if (complement != null) {

            for (NounPhrase compl : complement) {
                objects.add(compl);
                objects.addAll(compl.getObjects());
            }
        }

        return objects;
    }
}
