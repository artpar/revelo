package com.inf.cache;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import java.util.Date;
import java.util.Objects;

/**
 * Created by parth on 29/4/16.
 */
public class RedisCache implements CacheInterface<String, String> {
    private final JedisPool pool;
    private final String password;
    private final String cacheName;

    public RedisCache(JedisPool pool, String password, String cacheName) {
        this.pool = pool;
        this.password = password;
        this.cacheName = cacheName;
    }

    @Override
    public String get(String key) {
        try (Jedis resource = pool.getResource()) {
            resource.auth(password);
            String s = resource.get(cacheName + "::" + key);
            return s;
        }
    }

    @Override
    public boolean isPresent(String key) {
        try (Jedis resource = pool.getResource();) {
            resource.auth(password);
            String type = resource.type(cacheName + "::" + key);
            return !Objects.equals(type, "none");
        }
    }

    @Override
    public void set(String key, String item) {
        try (Jedis resource = pool.getResource()) {
            resource.auth(password);
            resource.set(cacheName + "::" + key, item);
        }

    }
}
