package com.inf.cache;

/**
 * Created by parth on 29/4/16.
 */
public interface CacheInterface<K, T> {
    T get(K key);
    boolean isPresent(K key);
    void set(K key, T item);
}
