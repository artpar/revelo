package com.inf.messenger;

public enum MessageType {
    INCOMING, OUTGOING
}
