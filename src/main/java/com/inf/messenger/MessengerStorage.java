package com.inf.messenger;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.inf.graphlike.english.AbstractPhrase;
import com.inf.graphlike.english.SentenceImpl;
import com.inf.graphlike.english.phrases.NounPhrase;
import com.inf.graphlike.english.phrases.VerbPhrase;
import org.hypergraphdb.HGHandle;
import org.hypergraphdb.HGQuery;
import org.hypergraphdb.HGQuery.hg;
import org.hypergraphdb.HyperGraph;
import org.hypergraphdb.atom.HGRel;
import org.hypergraphdb.atom.HGRelType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

public class MessengerStorage {
    private JedisPool jedis;
    private final String jedisPassword;
    private ObjectMapper objectMapper;

    private HyperGraph graph;

    private Logger logger = LoggerFactory.getLogger(MessengerStorage.class);

    public MessengerStorage(JedisPool jedis, String jedisPassword, ObjectMapper objectMapper, HyperGraph graph) {
        this.jedis = jedis;
        this.jedisPassword = jedisPassword;
        this.objectMapper = objectMapper;
        this.graph = graph;
    }

    public ChatState getHistory(String personId) {
        List<ChatMessage> chatHistoryList = new LinkedList<>();
        String chatStorageKey = "chat_" + personId;

        try (Jedis res = jedis.getResource()) {
            res.auth(jedisPassword);
            String chatHistory = res.get(chatStorageKey);
            if (chatHistory != null && chatHistory.length() > 5) {
                ChatState chatState = objectMapper.readValue(chatHistory, ChatState.class);
                return chatState;
            }
        } catch (IOException e) {
            logger.error("Failed to read cached value", e);
        }
        return new ChatState(chatHistoryList);
    }

    public void setChatState(ChatState chatState, String personId) {
        String chatStorageKey = "chat_" + personId;
        String js = null;
        try {
            js = objectMapper.writeValueAsString(chatState);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return;
        }

        try (Jedis res = jedis.getResource()) {
            res.auth(jedisPassword);
            res.set(chatStorageKey, js);

        }
    }

    public void clearHistory(String personId) {
        String chatStorageKey = "chat_" + personId;
        try (Jedis res = jedis.getResource()) {
            res.auth(jedisPassword);
            res.set(chatStorageKey, "");
        }
    }

    //    public void setObjectAttribute(String subject1, String relation, String subject2, List<NounPhrase> objects) {
    //
    //    }

    public void storeSentence(String senderId, SentenceImpl sentenceImpl) {

        AbstractPhrase nounPhrase = sentenceImpl.getNounPhrase();
        AbstractPhrase verbPhrase = sentenceImpl.getVerbPhrase();
        List<AbstractPhrase> objects = new LinkedList<>();
        objects.add(nounPhrase);
        objects.addAll(verbPhrase.getObjects());
        //        String nounPhraseString = nounPhrase.getPhrase().toLowerCase().trim();
        //        String verbPhraseString = verbPhrase.getPhrase().toLowerCase().trim();

        HGHandle sentenceHandle = hg.assertAtom(graph, sentenceImpl);
        HGHandle nounHandle = hg.assertAtom(graph, nounPhrase);

        //        graph.add(new HGRelType("from_sentence", sentenceHandle, nounHandle));

        logger.info("Noun handle: {}", nounHandle.toString());
        logger.info("Sentence handle: {}", sentenceHandle.toString());

        long cnt = hg
                .count(graph, hg.and(hg.type(HGRelType.class), hg.eq("name", "from_sentence"), hg.incident(sentenceHandle), hg.incident(nounHandle)));

        if (cnt > 0) {
            return;
        }

        List<HGHandle> verbLinkList = hg
                .findAll(graph, hg.and(hg.type(HGRelType.class), hg.incident(nounHandle), hg.eq("name", verbPhrase.getPhrase())));

        HGHandle selectedList = null;
        if (verbLinkList.size() == 0) {

            selectedList = null;

        } else {
            selectedList = verbLinkList.get(0);
        }

        // TODO: complete implementation
        HGHandle[] targetHandles = new HGHandle[2 + objects.size()];
        targetHandles[1] = nounHandle;
        for (int i = 0; i < objects.size(); i++) {
            AbstractPhrase object = objects.get(i);

            HGHandle objectHandle = hg.assertAtom(graph, object);
            targetHandles[i + 2] = objectHandle;
            HGRelType existingLink = hg.getOne(graph,
                    hg.and(hg.type(HGRelType.class), hg.eq("name", "from_sentence"), hg.eq("name", verbPhrase.getPhrase()), hg.incident(nounHandle),
                            hg.incident(objectHandle)));
            logger.info("Object relation: {}, {}, {}", verbPhrase.getPhrase(), nounPhrase.toString(), object.toString());

        }
        //            selectedList
        targetHandles[0] = sentenceHandle;
        graph.add(new HGRelType("from_sentence", targetHandles));

        for (HGHandle hgHandle : verbLinkList) {
            graph.remove(hgHandle);
        }

        HGHandle verbHandle = hg.assertAtom(graph, verbPhrase);

        targetHandles[0] = verbHandle;
        graph.add(new HGRelType(verbPhrase.getPhrase(), targetHandles));

    }

    public List<HGHandle> getNounPhrases(String nounValue) {
        nounValue = nounValue.toLowerCase().trim();
        return graph.findAll(hg.and(hg.type(NounPhrase.class), hg.matches("phrase", ".*" + nounValue + ".*")));
    }

    public List<HGHandle> getPerfectNounPhrases(String nounValue) {
        nounValue = nounValue.toLowerCase().trim();
        return graph.findAll(hg.and(hg.type(NounPhrase.class), hg.matches("phrase", nounValue)));
    }

    public List<NounPhrase> getRelations(List<HGHandle> hgHandles, String verb) {

        List<HGRelType> links = new LinkedList<>();

        for (HGHandle hgHandle : hgHandles) {
            links.addAll(hg.getAll(graph, hg.and(hg.type(HGRelType.class), hg.incident(hgHandle), hg.eq("name", verb))));
        }

        LinkedList<NounPhrase> targets = new LinkedList<NounPhrase>();

        for (HGRelType link : links) {
            int linkArity = link.getArity();
            logger.info("Link [{}] arity: {}", link.toString(), linkArity);
            for (int i = 0; i < linkArity; i++) {
                if (hgHandles.contains(link.getTargetAt(i))) {
                    continue;
                }
                targets.add(graph.get(link.getTargetAt(i)));
            }
        }
        return targets;
    }

    public List<SentenceImpl> resolveSentencesFromObjects(List<HGHandle> knownObjects, String verb) {

        List<HGHandle> targets = new LinkedList<>();

        for (HGHandle knownObject : knownObjects) {

            List<HGRelType> results;
            if (verb.equals("be")) {
                results = hg.getAll(graph, hg.and(hg.type(HGRelType.class), hg.incident(knownObject)));
            } else {
                results = hg.getAll(graph, hg.and(hg.type(HGRelType.class), hg.incident(knownObject), hg.eq("name", verb)));
            }

            if (results.size() > 0) {

                for (HGRelType result : results) {
                    if (result.getName().equals("from_sentence")) {
                        continue;
                    }
                    int pointsToNodeCount = result.getArity();
                    for (int i = 0; i < pointsToNodeCount; i++) {
                        targets.add(result.getTargetAt(i));
                    }
                }
            }
        }

        List<HGRelType> links = new LinkedList<>();
        for (HGHandle target : targets) {
            links.addAll(hg.getAll(graph, hg.and(hg.type(HGRelType.class), hg.eq("name", "from_sentence"), hg.incident(target))));
        }

        List<SentenceImpl> res = new LinkedList<>();
        for (HGRelType link : links) {

            SentenceImpl e = graph.get(link.getTargetAt(0));
            if (res.contains(e)) {
                continue;
            }
            res.add(e);
        }
        return res;

    }

    public List<NounPhrase> resolveSubjectFromObjects(List<HGHandle> knownObjects, String verb) {

        List<NounPhrase> answers = new LinkedList<>();

        for (HGHandle knownObject : knownObjects) {

            List<HGRelType> results;
            if (verb.equals("be")) {
                results = hg.getAll(graph, hg.and(hg.type(HGRelType.class), hg.incident(knownObject)));
            } else {
                results = hg.getAll(graph, hg.and(hg.type(HGRelType.class), hg.incident(knownObject), hg.eq("name", verb)));
            }

            if (results.size() > 0) {

                NounPhrase knownObjectInstance = graph.get(knownObject);

                for (HGRelType result : results) {
                    if (result.getName().equals("from_sentence")) {
                        continue;
                    }
                    int pointsToNodeCount = result.getArity();
                    for (int i = 0; i < pointsToNodeCount; i++) {
                        NounPhrase targetAtI = graph.get(result.getTargetAt(i));

                        if (!knownObjectInstance.getPhrase().equals(targetAtI.getPhrase())) {
                            answers.add(targetAtI);
                        }

                    }

                }

            }
        }

        return answers;

    }

    public List<NounPhrase> resolveVerbFromObjects(List<HGHandle> knownObjects) {

        List<NounPhrase> answers = new LinkedList<>();

        for (HGHandle knownObject : knownObjects) {
            List<HGRelType> results = hg.getAll(graph, hg.and(hg.type(HGRelType.class), hg.incident(knownObject)));
            if (results.size() > 0) {

                NounPhrase knownObjectInstance = graph.get(knownObject);

                for (HGRelType result : results) {

                    int pointsToNodeCount = result.getArity();
                    for (int i = 0; i < pointsToNodeCount; i++) {
                        NounPhrase targetAtI = graph.get(result.getTargetAt(i));

                        if (!knownObjectInstance.getPhrase().equals(targetAtI.getPhrase())) {
                            answers.add(targetAtI);
                        }

                    }

                }

            }
        }

        return answers;

    }

    public <T> List<T> getHandles(List<HGHandle> subjectHandle) {
        List<T> retList = new LinkedList<>();
        for (HGHandle hgHandle : subjectHandle) {
            retList.add(graph.get(hgHandle));
        }

        return retList;
    }

    public List<NounPhrase> resolveSubjectFromObjects(List<HGHandle> subjectHandle) {
        return resolveSubjectFromObjects(subjectHandle, "be");
    }

    public List<SentenceImpl> resolveSentencesFromObjects(List<HGHandle> subjectHandle) {
        return resolveSentencesFromObjects(subjectHandle, "be");
    }

    public List<String> getVerbRelations(HGHandle topHandle, NounPhrase answerNoun) {

        List<HGRelType> verbs = hg.getAll(graph, hg.and(hg.type(HGRelType.class), hg.incident(topHandle)));

        return verbs.stream().filter(e -> {
            return e.getTargetAt(0).getPersistent().toString().contains(answerNoun.getPhrase()) || e.getTargetAt(1).getPersistent().toString()
                                                                                                    .contains(answerNoun.getPhrase());
        }).map(HGRelType::getName).collect(Collectors.toList());

    }

    public void removeHandles(List<HGHandle> nounHandle) {

        for (HGHandle hgHandle : nounHandle) {
            graph.remove(hgHandle);
        }

    }
}
