package com.inf.messenger;

import com.inf.messenger.sentence.SentenceParse;
import com.inf.messenger.worddefinition.WordDefinitionResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class NlpService {
    private final String parseEndpoint;

    private final String spellcheckEndpoint;

    private final String definitionEndpoint;

    private RestTemplate restTemplate;

    public NlpService(String serverEndpoint, RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
        this.parseEndpoint = serverEndpoint + "/nlp/parse";
        this.definitionEndpoint = serverEndpoint + "/nlp/word";
        this.spellcheckEndpoint = serverEndpoint + "/nlp/spellcheck";
    }

    public SpellCheckResult spellCheck(String sentence) {

        ResponseEntity<SpellCheckResult> sentenceParse = restTemplate
                .getForEntity(this.spellcheckEndpoint + "?sentence=" + sentence, SpellCheckResult.class);

        return sentenceParse.getBody();

    }

    public WordDefinitionResponse wordDefinition(String word, String pos) {

        ResponseEntity<WordDefinitionResponse> sentenceParse = restTemplate
                .getForEntity(this.definitionEndpoint + "?word=" + word + "&pos=" + pos, WordDefinitionResponse.class);

        return sentenceParse.getBody();

    }

    public SentenceParse parseSentence(String sentence) {

        ResponseEntity<SentenceParse> sentenceParse = restTemplate.getForEntity(this.parseEndpoint + "?sentence=" + sentence, SentenceParse.class);

        return sentenceParse.getBody();

    }
    //
    //    public WordLookup lookupWord(String word) {
    //
    //    }

}
