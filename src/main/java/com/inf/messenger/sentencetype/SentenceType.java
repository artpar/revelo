package com.inf.messenger.sentencetype;

public enum SentenceType {
    assertive, negative, interrogative, imperative, exclamatory
}
