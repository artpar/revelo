package com.inf.messenger.sentencetype;

public class SentenceHandler {
    SentenceType type;

    public static void handleAssertiveSentence() {

    }

    public static String generateSentence(String originalSubject, String subject, String object, String action) {

        if (subject == null) {
            switch (originalSubject) {
                case "my":
                    subject = "Your";
                    break;
                case "i":
                    subject = "You";
                case "me":
                    subject = "You";
                case "you":
                    subject = "Me";

            }

        }

        return subject + " " + action + " " + object;

    }
}
