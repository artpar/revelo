
package com.inf.messenger.sentence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "relations",
    "sentences",
    "entities"
})
public class SentenceParse {

    @JsonProperty("relations")
    private List<Relation> relations = null;
    @JsonProperty("sentences")
    private List<Sentence> sentences = null;
    @JsonProperty("entities")
    private List<Entity> entities = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("relations")
    public List<Relation> getRelations() {
        return relations;
    }

    @JsonProperty("relations")
    public void setRelations(List<Relation> relations) {
        this.relations = relations;
    }

    @JsonProperty("sentences")
    public List<Sentence> getSentences() {
        return sentences;
    }

    @JsonProperty("sentences")
    public void setSentences(List<Sentence> sentences) {
        this.sentences = sentences;
    }

    @JsonProperty("entities")
    public List<Entity> getEntities() {
        return entities;
    }

    @JsonProperty("entities")
    public void setEntities(List<Entity> entities) {
        this.entities = entities;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("relations", relations).append("sentences", sentences).append("entities", entities).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(relations).append(additionalProperties).append(sentences).append(entities).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof SentenceParse) == false) {
            return false;
        }
        SentenceParse rhs = ((SentenceParse) other);
        return new EqualsBuilder().append(relations, rhs.relations).append(additionalProperties, rhs.additionalProperties).append(sentences, rhs.sentences).append(entities, rhs.entities).isEquals();
    }

}
