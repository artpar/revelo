package com.inf.messenger.sentence;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.inf.messenger.worddefinition.HasLemmaNames;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude (JsonInclude.Include.NON_NULL)
@JsonPropertyOrder ({ "text", "lemma", "pos", "tag", "dep", "shape", "left", "right", "is_alpha", "orth", "is_stop" })
public class Po implements HasLemmaNames {
  @JsonProperty ("text")
  private String text;

  @JsonProperty ("lemma")
  private String lemma;

  @JsonProperty ("pos")
  private String pos;

  @JsonProperty ("tag")
  private String tag;

  @JsonProperty ("dep")
  private String dep;

  @JsonProperty ("shape")
  private String shape;

  @JsonProperty ("orth")
  private Double orth;

  @JsonProperty ("left")
  private List<Po> left = null;

  @JsonProperty ("right")
  private List<Po> right = null;

  @JsonProperty ("is_alpha")
  private Boolean isAlpha;

  @JsonProperty ("is_stop")
  private Boolean isStop;

  @JsonIgnore
  private Map<String, Object> additionalProperties = new HashMap<String, Object>();

  @JsonProperty ("text")
  public String getText() {
    return text;
  }

  @JsonProperty ("text")
  public void setText(String text) {
    this.text = text;
  }

  @JsonProperty ("orth")
  public Double getOrth() {
    return orth;
  }

  @JsonProperty ("orth")
  public void setOrth(Double orth) {
    this.orth = orth;
  }

  @JsonProperty ("lemma")
  public String getLemma() {
    return lemma;
  }

  @JsonProperty ("lemma")
  public void setLemma(String lemma) {
    this.lemma = lemma;
  }

  @JsonProperty ("pos")
  public String getPos() {
    return pos;
  }

  @JsonProperty ("pos")
  public void setPos(String pos) {
    this.pos = pos;
  }

  @JsonProperty ("tag")
  public String getTag() {
    return tag;
  }

  @JsonProperty ("tag")
  public void setTag(String tag) {
    this.tag = tag;
  }

  @JsonProperty ("dep")
  public String getDep() {
    return dep;
  }

  @JsonProperty ("dep")
  public void setDep(String dep) {
    this.dep = dep;
  }

  @JsonProperty ("shape")
  public String getShape() {
    return shape;
  }

  @JsonProperty ("shape")
  public void setShape(String shape) {
    this.shape = shape;
  }

  @JsonProperty ("left")
  public List<Po> getLeft() {
    return left;
  }

  @JsonProperty ("left")
  public void setLeft(List<Po> left) {
    this.left = left;
  }

  @JsonProperty ("right")
  public List<Po> getRight() {
    return right;
  }

  @JsonProperty ("right")
  public void setRight(List<Po> right) {
    this.right = right;
  }

  public Po() {
  }

  public Po(String text) {
    this.text = text;
  }

  @JsonProperty ("is_alpha")
  public Boolean getIsAlpha() {
    return isAlpha;
  }

  @JsonProperty ("is_alpha")
  public void setIsAlpha(Boolean isAlpha) {
    this.isAlpha = isAlpha;
  }

  @JsonProperty ("is_stop")
  public Boolean getIsStop() {
    return isStop;
  }

  @JsonProperty ("is_stop")
  public void setIsStop(Boolean isStop) {
    this.isStop = isStop;
  }

  @JsonAnyGetter
  public Map<String, Object> getAdditionalProperties() {
    return this.additionalProperties;
  }

  @JsonAnySetter
  public void setAdditionalProperty(String name, Object value) {
    this.additionalProperties.put(name, value);
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this).append("text", text).append("lemma", lemma).append("pos", pos).append("tag", tag).append("dep", dep)
                                    .append("shape", shape).append("left", left).append("right", right).append("isAlpha", isAlpha)
                                    .append("isStop", isStop).append("additionalProperties", additionalProperties).toString();
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder().append(lemma).append(text).append(additionalProperties).append(isAlpha).append(tag).append(isStop).append(left)
                                .append(shape).append(right).append(dep).append(pos).toHashCode();
  }

  @Override
  public boolean equals(Object other) {
    if (other == this) {
      return true;
    }
    if ((other instanceof Po) == false) {
      return false;
    }
    Po rhs = ((Po) other);
    return new EqualsBuilder().append(lemma, rhs.lemma).append(text, rhs.text).append(additionalProperties, rhs.additionalProperties)
                              .append(isAlpha, rhs.isAlpha).append(tag, rhs.tag).append(isStop, rhs.isStop).append(left, rhs.left)
                              .append(shape, rhs.shape).append(right, rhs.right).append(dep, rhs.dep).append(pos, rhs.pos).isEquals();
  }

  @Override
  @JsonIgnore
  public List<String> getLemmaNames() {
    return Collections.singletonList(lemma == null ? text : lemma);
  }
}
