
package com.inf.messenger.sentence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "sentence",
    "sentence_type",
    "root",
    "pos"
})
public class Sentence {

    @JsonProperty("sentence")
    private String sentence;
    @JsonProperty("root")
    private Po root;
    @JsonProperty("sentence_type")
    private String sentenceType;
    @JsonProperty("pos")
    private List<Po> pos = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("sentence_type")
    public String getSentenceType() {
        return sentenceType;
    }

    @JsonProperty("sentence_type")
    public void setSentenceType(String sentenceType) {
        this.sentenceType = sentenceType;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @JsonProperty("sentence")
    public String getSentence() {
        return sentence;
    }

    @JsonProperty("sentence")
    public void setSentence(String sentence) {
        this.sentence = sentence;
    }

    @JsonProperty("root")
    public Po getRoot() {
        return root;
    }

    @JsonProperty("root")
    public void setRoot(Po root) {
        this.root = root;
    }

    @JsonProperty("pos")
    public List<Po> getPos() {
        return pos;
    }

    @JsonProperty("pos")
    public void setPos(List<Po> pos) {
        this.pos = pos;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("sentence", sentence).append("root", root).append("pos", pos).append("sentence_type", sentenceType).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(root).append(additionalProperties).append(sentence).append(sentenceType).append(pos).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Sentence) == false) {
            return false;
        }
        Sentence rhs = ((Sentence) other);
        return new EqualsBuilder().append(root, rhs.root).append(additionalProperties, rhs.additionalProperties).append(sentence, rhs.sentence).append(pos, rhs.pos).append(sentenceType, rhs.sentenceType).isEquals();
    }

}
