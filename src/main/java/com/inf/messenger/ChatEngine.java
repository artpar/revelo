package com.inf.messenger;

import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import com.inf.data.Suggestion;
import com.inf.graphlike.english.SentenceImpl;
import com.inf.graphlike.english.TokenManager;
import com.inf.graphlike.english.phrases.NounPhrase;
import com.inf.graphlike.english.phrases.VerbPhrase;
import com.inf.messenger.sentence.Po;
import com.inf.messenger.sentence.Sentence;
import com.inf.messenger.sentence.SentenceParse;
import com.inf.messenger.worddefinition.HasLemmaNames;
import com.inf.messenger.worddefinition.Lemma;
import com.inf.messenger.worddefinition.WordDefinition;
import com.inf.messenger.worddefinition.WordDefinitionResponse;
import com.inf.survey.SurveyManager;
import com.inf.survey.data.Question;
import com.inf.survey.data.Survey;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.hypergraphdb.HGHandle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StreamUtils;

public class ChatEngine {
  private Logger logger = LoggerFactory.getLogger(ChatEngine.class);

  private MessengerStorage messengerStorage;

  List<String> questionWords = Arrays.asList("what", "where", "who", "when", "why");

  private SurveyManager surveyManager;

  public ChatEngine(NlpService nlpService, MessengerStorage messengerStorage, SurveyManager surveyManager) {
    this.nlpService = nlpService;
    this.messengerStorage = messengerStorage;
    this.surveyManager = surveyManager;
  }

  Random random = new SecureRandom();

  private NlpService nlpService;

  public SentenceImpl parseSentenceFromString(Sentence sentence, TokenManager tokenManager) {

    SentenceImpl sentence1 = new SentenceImpl(sentence, tokenManager);
    logger.info("Parsed [{}]", sentence1.toString());
    return sentence1;

  }

  public LinkedList<ChatMessage> applySentence(String senderId, String text, ChatState chatState) throws Exception {

    LinkedList<ChatMessage> payloads = new LinkedList<>();

    SentenceParse sentenceParse = this.nlpService.parseSentence(text);

    List<Sentence> sentences = sentenceParse.getSentences();

    Map<Double, Po> tokenMap1 = new HashMap<>();

    for (Sentence sentence : sentences) {

      for (Po po : sentence.getPos()) {
        po.setText(po.getText().trim().toLowerCase());
        tokenMap1.put(po.getOrth(), po);
      }
    }

    TokenManager tokenManager = new TokenManager(tokenMap1);

    for (Sentence sentence : sentences) {

      SentenceImpl sentenceImpl = parseSentenceFromString(sentence, tokenManager);
      ChatMessage chatMessage = new ChatMessage();
      chatMessage.setPersonId(senderId);
      chatMessage.setSentence(sentenceImpl);
      chatMessage.setAuthor(senderId);
      chatMessage.setMessageType(MessageType.INCOMING);
      chatState.addMessage(chatMessage);
      //            payloads.add(textMessage(sentenceImpl.toString(), senderId));

      VerbPhrase verbPhrase = sentenceImpl.getVerbPhrase();
      List<NounPhrase> objects = verbPhrase.getTargetObjects();

      NounPhrase nounPhrase = sentenceImpl.getNounPhrase();

      String location, time, person, thing;
      NounPhrase object = null;
      if (verbPhrase.getTargetObjects().size() > 0) {
        object = verbPhrase.getTargetObjects().get(0);
      }

      // links: verbs, prepositions

      String nounPhraseString;
      if (nounPhrase.getPhrase() != null) {

        nounPhraseString = nounPhrase.getPhrase().trim();
      } else {
        nounPhraseString = "i";
      }

      WordDefinitionResponse verbDefinition = nlpService.wordDefinition(verbPhrase.getPhrase(), "v");

      String targetSubjectPossessive = OppositePossessivePronoun(nounPhraseString);
      List<String> frameStrings = new LinkedList<>();

      for (WordDefinition wordDefinition : verbDefinition.getWordDefinitions()) {
        for (Lemma lemma : wordDefinition.getLemmas()) {
          if (lemma.getName().equals(verbPhrase.getPhrase())) {
            frameStrings.addAll(lemma.getFrameStrings());
          }
        }
      }

      // person/thing
      // position
      // time
      // color
      // taste
      // sound
      // temperature
      //

            /*
            // identify
            request
            demand
            need
            require

             */

      switch (sentence.getSentenceType()) {

        case "negative":
        case "assertive":

          logger.info("Interrogative Subject [{}] Verb [{}] Object [{}]", nounPhraseString, verbPhrase.toString(), objects.toString());

          //                    this.messengerStorage.setObjectAttribute(nounPhrase.toString(), verbPhrase.getPhrase(), objects);

          switch (verbPhrase.getPhrase()) {
            case "be":
            case "have":
            default:

              List<HGHandle> nounHandle = messengerStorage.getNounPhrases(nounPhraseString);
              if (nounHandle != null || nounHandle.size() > 0) {
                logger.info("New information about: {}", nounPhrase);

                boolean foundExisting = false;
                List<NounPhrase> relations = messengerStorage.getRelations(nounHandle, verbPhrase.getPhrase());

                List<NounPhrase> answerNouns = new LinkedList<>();
                for (NounPhrase relation : relations) {
                  if (!relation.getPhrase().equals(nounPhraseString)) {
                    answerNouns.add(relation);
                  }
                }
                if (answerNouns.size() > 0) {
                  logger.info("Found answer: {}", answerNouns);
                  foundExisting = true;
                  payloads.add(messageFromSelf("Are you sure it's not " + StringUtils.join(answerNouns, ", ") + "?", senderId));
                }

                messengerStorage.storeSentence(senderId, sentenceImpl);
              } else {
                messengerStorage.storeSentence(senderId, sentenceImpl);
              }

          }

          if (object != null) {
//            payloads.add(messageFromSelf("Oh, " + object, senderId));
          } else {
//            payloads.add(messageFromSelf("Oh, " + verbPhrase, senderId));
          }
          break;

        case "exclamatory":
          switch (verbPhrase.getPhraseParse().getLemma()) {
            case "hello":
              payloads.add(textMessage(verbPhrase.getPhraseParse().getLemma(), senderId));
              break;
          }
          break;

        case "interrogative":

          if (nounPhraseString.isEmpty() || nounPhraseString.equalsIgnoreCase("who")) {

            // this is a search for subject

            List<NounPhrase> givenObjects = sentenceImpl.getObjects();
            List<HGHandle> knownObjects = new LinkedList<>();

            for (NounPhrase givenObject : givenObjects) {
              if (questionWords.indexOf(givenObject.getPhrase()) == -1) {
                knownObjects.addAll(messengerStorage.getNounPhrases(givenObject.getPhrase()));
              }
            }

            if (knownObjects.size() == 0) {
              payloads.add(messageFromSelf("How can I even know this ?", senderId));
            } else {

              List<NounPhrase> answers = messengerStorage.resolveSubjectFromObjects(knownObjects, verbPhrase.getPhrase());

              if (answers.size() > 1) {

                for (int i = 0; i < answers.size(); i++) {
                  NounPhrase answer = answers.get(i);

                  if (i + 1 == answers.size()) {
                    payloads.add(messageFromSelf("Or ...", senderId));
                  }

                  payloads.add(messageFromSelf("Is it " + answer.getPhrase() + "?", senderId));

                }
              } else if (answers.size() == 1) {
                payloads.add(messageFromSelf(answers.get(0).getPhrase() + "?", senderId));

              }

            }

          } else if (nounPhrase.getPhrase().equals("what") && Arrays.asList("happen", "be").indexOf(verbPhrase.getPhrase()) > -1) {

            List<HGHandle> knownObjects = new LinkedList<>();

            List<NounPhrase> givenObjects = verbPhrase.getObjects();

            for (NounPhrase givenObject : givenObjects) {
              if (questionWords.indexOf(givenObject.getPhrase()) == -1) {
                knownObjects.addAll(messengerStorage.getNounPhrases(givenObject.getPhrase()));
              }
            }

            List<NounPhrase> answers = messengerStorage.resolveVerbFromObjects(knownObjects);

            for (NounPhrase answer : answers) {
              payloads.add(messageFromSelf(answer.toString(), senderId));
            }

          } else {

            logger.info("Question was: {}", sentenceImpl);
            List<HGHandle> subjectHandle = messengerStorage.getNounPhrases(nounPhraseString.trim().toLowerCase());
            if (subjectHandle == null || subjectHandle.size() == 0) {
              payloads.add(
                      messageFromSelf("I don't know what " + OppositePronoun(nounPhraseString) + " can ask me about " + targetSubjectPossessive + ".",
                              senderId));
            } else {

              //                            HGHandle topHandle = subjectHandle.get(0);
              //
              //                            List<NounPhrase> subjectNames = messengerStorage.getHandles(subjectHandle);
              //
              //                            NounPhrase topHandleName = subjectNames.get(0);
              //
              List<SentenceImpl> sentences1 = messengerStorage.resolveSentencesFromObjects(subjectHandle, verbPhrase.getPhrase());

              if (sentences.size() == 0) {
                payloads.add(messageFromSelf("I don't know this about " + targetSubjectPossessive, senderId));
              } else {

                for (SentenceImpl answerNoun : sentences1) {
                  payloads.add(messageFromSelf(answerNoun.getSentence(), senderId));

                }

              }

            }
          }

          logger.info("Interrogative Subject [{}] Verb [{}] Object [{}] ", nounPhraseString, verbPhrase.toString(), objects.toString());

          break;

        case "imperative":

          switch (verbPhrase.getPhrase()) {
            case "tell":
              break;
            case "forget":
            case "delete":
              List<NounPhrase> objectsToForget = verbPhrase.getObjects();

              for (NounPhrase phrase : objectsToForget) {
                List<HGHandle> nounHandle = messengerStorage.getPerfectNounPhrases(phrase.getPhrase());
                payloads.add(messageFromSelf("Forgot about " + phrase.getPhrase(), senderId));
                messengerStorage.removeHandles(nounHandle);
              }
              break;

          }

//          payloads.add(messageFromSelf("I will not " + verbPhrase.getPhrase() + ".", senderId));
          break;

      }
    }

    return payloads;
  }

  public String getSurveyId() {
    if (true) {
      return "f08f7577-377a-47f1-88a8-ada2fc43fa2e";
    }
    List<NounPhrase> nounPhrases = messengerStorage.resolveSubjectFromObjects(messengerStorage.getNounPhrases("the survey"), "be");
    if (nounPhrases == null || nounPhrases.size() == 0) {
      return null;
    }
    return nounPhrases.get(0).getPhrase().trim();

  }

  public ChatMessage messageFromSelf(String message, String senderId) {

    SentenceParse questionParse = this.nlpService.parseSentence(message);

    Sentence questionSentence = questionParse.getSentences().get(0);
    Map<Double, Po> tokenMap = new HashMap<>();
    for (Po po : questionSentence.getPos()) {
      tokenMap.put(po.getOrth(), po);
    }

    SentenceImpl sentence = new SentenceImpl(questionSentence, new TokenManager(tokenMap));
    ChatMessage chatMessage = new ChatMessage();
    chatMessage.setPersonId(senderId);
    chatMessage.setSentence(sentence);
    chatMessage.setAuthor("self");
    return chatMessage;
  }

  public ChatMessage textMessage(String message, String senderId) {
    Sentence sentence1 = new Sentence();
    sentence1.setSentence(message);
    SentenceImpl sentence = new SentenceImpl(sentence1, new TokenManager(new HashMap<Double, Po>() {}));
    ChatMessage chatMessage = new ChatMessage();
    chatMessage.setPersonId(senderId);
    chatMessage.setSentence(sentence);
    chatMessage.setAuthor("self");
    return chatMessage;
  }

  private String Plural(String attribute) {
    return attribute;
  }

  private Boolean CheckIsIrregularVerb(String rootVerbLemma) {
    switch (rootVerbLemma) {

      case "am":
      case "is":
      case "has":
      case "does":
      case "be":
      case "being":
      case "having":
      case "are":
      case "was":
      case "were":
      case "been":
      case "have":
      case "had":
      case "could":
      case "should":
      case "would":
      case "may":
      case "might":
      case "must":
      case "shall":
      case "can":
      case "will":
      case "do":
      case "did":
        return false;

    }
    return true;

  }

  private String OppositePronoun(String s) {
    switch (s) {
      case "me":
        return "you";
      case "you":
        return "i";
      case "i":
        return "you";
      case "your":
        return "me";
      case "my":
        return "your";
      default:
        return "you";
    }
  }

  private String OppositePossessivePronoun(String s) {
    if (s == null) {
      return "";
    }
    switch (s) {
      case "me":
        return "your";
      case "you":
        return "i";
      case "i":
        return "your";
      case "your":
        return "my";
      case "my":
        return "your";
      default:
        return s;
    }
  }

  private Map<String, String> getAnswerMap(JSONObject answers) {
    Map<String, String> answersList = new HashMap<>();

    //        JSONObject answers = questions.get(lastQuestionId).getInternalObject();
    JSONArray options = answers.getJSONArray("options");

    Object[] array = options.toArray();
    for (int i = 0; i < options.size(); i++) {
      JSONObject option = options.getJSONObject(i);
      String label = option.getString("label");
      String value = option.getString("value");
      answersList.put(label.trim().toLowerCase(), value);
      //            answersList.add(value);

    }
    return answersList;
  }

  private List<String> getAnswerList(JSONObject answers) {
    List<String> answersList = new LinkedList<>();

    //        JSONObject answers = questions.get(lastQuestionId).getInternalObject();
    JSONArray options = answers.getJSONArray("options");

    Object[] array = options.toArray();
    for (int i = 0; i < options.size(); i++) {
      JSONObject option = options.getJSONObject(i);
      String label = option.getString("label");
      //            String value = option.getMessageType("value");
      answersList.add(label);
      //            answersList.add(value);

    }
    return answersList;
  }

  public static String findClosestMatch(Collection<String> collection, Object target) {
    int distance = Integer.MAX_VALUE;
    String closest = null;
    for (String compareObject : collection) {
      int currentDistance = org.apache.commons.lang3.StringUtils.getLevenshteinDistance(compareObject.toString(), target.toString());
      if (currentDistance < distance) {
        distance = currentDistance;
        closest = compareObject;
      }
    }
    return closest;
  }

  public List<ChatMessage> applyQuestionnaire(String senderId, ChatState chatState, String text, String questionnaireId) {

    List<ChatMessage> payloads = new LinkedList<>();
    try {

      String state = chatState.getState();
      if (state == null || state.equals("") || text.trim().toLowerCase().equals("restart survey")) {

        if (questionnaireId != null) {

          Survey survey = surveyManager.getNewSurveyByIdentifier(questionnaireId, "artpar@gmail.com");

          chatState.setSurveyId(survey.getId());

          List<Question> questions = surveyManager.getQuestionsBySurveyId(survey.getId());

          payloads.add(messageFromSelf(String.format("I have %d questions for you", questions.size()), senderId));
          payloads.add(messageFromSelf(questions.get(0).getQuestion(), senderId));
          chatState.setQuestions(questions);
          chatState.setLastQuestionIndex(0);
          chatState.setSuggestionList(null);
          chatState.setState("expect_reply");
        }
      } else {

        SentenceParse sentenceParse = this.nlpService.parseSentence(text);

        Sentence mainSentence = sentenceParse.getSentences().get(0);

        String mainVerb = mainSentence.getRoot().getLemma();
        List<HasLemmaNames> mainVerbLemmas = new LinkedList<>();
        mainVerbLemmas.add(mainSentence.getRoot());

        if (mainVerb != null) {

          WordDefinitionResponse mainVerbDefinition = nlpService.wordDefinition(mainVerb, "v");

          mainVerbDefinition.getWordDefinitions().forEach(e -> {
            mainVerbLemmas.addAll(e.getHypernyms());
            mainVerbLemmas.addAll(e.getHyponyms());
          });
        }

        int lastQuestionId = chatState.getLastQuestionIndex();
        List<Question> question = chatState.getQuestions();
        Question lastQuestion = question.get(lastQuestionId);

        List<Question> questions = chatState.getQuestions();
        Integer sessionSurveyId = chatState.getSurveyId();

        List<String> answers = getAnswerList(lastQuestion.getInternalObject());
        Map<String, String> answerMap = getAnswerMap(lastQuestion.getInternalObject());
        String confirmAnswer = chatState.getConfirmAnswer();

        String nextState = null;
        switch (state) {

          case "confirm_next_survey":

            if (Arrays.asList("yes", "ok").indexOf(text.trim().toLowerCase()) > -1) {

              chatState.setSuggestionList(Collections.emptyList());
              chatState.setConfirmAnswer("");
              Survey survey = surveyManager.getNewSurveyByIdentifier(questionnaireId, "artpar@gmail.com");
              chatState.setSurveyId(survey.getId());

              List<Question> questionsNew = surveyManager.getQuestionsBySurveyId(survey.getId());

              payloads.add(messageFromSelf(String.format("I have %d questions for you", questionsNew.size()), senderId));
              payloads.add(messageFromSelf(questionsNew.get(0).getQuestion(), senderId));
              chatState.setQuestions(questionsNew);
              chatState.setLastQuestionIndex(0);
              chatState.setState("expect_reply");

            }
            break;

          case "completed_survey":

            payloads.add(messageFromSelf("Do you want to find another kind of mobile?", senderId));
            chatState.setState("confirm_next_survey");
            nextState = "confirm_next_survey";
            break;

          case "confirm_answer":

          case "expect_reply":

            String closestMatch = findClosestMatch(answers, text);

            if (text.trim().equalsIgnoreCase(closestMatch.trim())) {
              payloads.add(textMessage("Okay", senderId));

              surveyManager.updateAnswer(sessionSurveyId, questions.get(chatState.getLastQuestionIndex()).getId().intValue(),
                      answerMap.get(closestMatch.toLowerCase().trim()));
              Collection<Suggestion> productScores = surveyManager.getProductScores(sessionSurveyId);

              Collection<Suggestion> previousList = chatState.getSuggestionList();

              chatState.setSuggestionList(productScores);
              if (productScores.iterator().hasNext()) {
                Suggestion currentTop = productScores.iterator().next();
                if (previousList != null && previousList.iterator().hasNext()) {
                  Suggestion lastTop = previousList.iterator().next();

                  if (!currentTop.getProduct().getName().equals(lastTop.getProduct().getName())) {
                    payloads.add(textMessage(
                            "Remember what I said about " + currentTop.getProduct().getCompanyName() + " " + lastTop.getProduct().getName() + " ?",
                            senderId));
                    payloads.add(textMessage("Here is a product with all of those features", senderId));
                    payloads.add(textMessage("but even better, since you said " + closestMatch, senderId));
                    payloads.add(textMessage(currentTop.getProduct().getCompanyName() + " " + currentTop.getProduct().getName(), senderId));
                  }

                } else {
                  payloads.add(textMessage("I think " + currentTop.getProduct().getCompanyName() + " " + currentTop.getProduct()
                                                                                                                   .getName() + " would be a good fit for you",
                          senderId));
                  payloads.add(textMessage("I actually have couple of other suggestions too..", senderId));
                }
              }

              nextState = "ask_next";
            } else if (Arrays.asList("yes", "yeah", "right", "yep").indexOf(text.trim().toLowerCase()) > -1) {

              surveyManager.updateAnswer(sessionSurveyId, questions.get(chatState.getLastQuestionIndex()).getId().intValue(),
                      answerMap.get(confirmAnswer.trim().toLowerCase()));
              Collection<Suggestion> productScores = surveyManager.getProductScores(sessionSurveyId);

              Collection<Suggestion> previousList = chatState.getSuggestionList();

              chatState.setSuggestionList(productScores);
              if (productScores.iterator().hasNext()) {
                Suggestion currentTop = productScores.iterator().next();
                if (previousList != null && previousList.iterator().hasNext()) {
                  Suggestion lastTop = previousList.iterator().next();

                  if (!currentTop.getProduct().getName().equals(lastTop.getProduct().getName())) {
                    payloads.add(textMessage(
                            "Remember what I said about " + lastTop.getProduct().getCompanyName() + " " + lastTop.getProduct().getName() + " ?",
                            senderId));
                    payloads.add(textMessage("Here is a product with all of those features", senderId));
                    payloads.add(textMessage("but even better, since you said " + confirmAnswer, senderId));
                    payloads.add(textMessage(currentTop.getProduct().getCompanyName() + " " + currentTop.getProduct().getName(), senderId));
                  }

                } else {
                  payloads.add(textMessage(
                          "I think " + currentTop.getProduct().getCompanyName() + currentTop.getProduct().getName() + " would be a good fit for you",
                          senderId));
                  payloads.add(textMessage("I actually have couple of other suggestions too..", senderId));

                }
              }

              nextState = "ask_next";

            } else {

              String otherAnswer = OtherThen(answers, confirmAnswer);

              if (confirmAnswer != null && confirmAnswer.equals(closestMatch)) {
                chatState.setConfirmAnswer(otherAnswer);
                payloads.add(textMessage("Do you mean " + otherAnswer, senderId));
              } else {
                chatState.setConfirmAnswer(closestMatch);
                payloads.add(textMessage("Do you mean " + closestMatch, senderId));
              }

              if (Math.random() < 0.3) {
//                payloads.add(textMessage(lastQuestion.getQuestion(), senderId));
              }

              nextState = "expect_reply";
              //                            chatState.setState("expect_reply");

            }

          default:

            break;
        }

        chatState.setState(nextState);

        if (nextState != null) {

          switch (nextState) {

            case "ask_next":
              int nextQuestionId = lastQuestionId + 1;
              if (nextQuestionId == questions.size()) {

                payloads.add(messageFromSelf("That's all.", senderId));
                Collection<Suggestion> previousList = chatState.getSuggestionList();
                Suggestion previousTop = previousList.iterator().next();
                payloads.add(messageFromSelf(previousTop.getProduct().getName() + " is your best bet.", senderId));
                chatState.setState("completed_survey");
                break;
              }
              Question nextQuestion = questions.get(nextQuestionId);
              payloads.add(messageFromSelf(nextQuestion.getQuestion(), senderId));
              chatState.setState("expect_reply");

              chatState.setLastQuestionIndex(nextQuestionId);

              chatState.setConfirmAnswer(null);
              break;

            case "confirm_answer":

              break;

          }
        } else {

        }

      }

    } catch (Exception e) {
      logger.error("Failed to parse sentence", e);
      payloads.add(messageFromSelf("What just happened ? I don't remember it.", senderId));
    }

    return payloads;
  }

  private String ChooseOne(List<String> strings) {
    return strings.get(random.nextInt(strings.size()));
  }

  private String OtherThen(List<String> answers, String confirmAnswer) {
    Collections.shuffle(answers);

    for (String answer : answers) {
      if (!answer.equalsIgnoreCase(confirmAnswer)) {
        return answer;
      }
    }
    return confirmAnswer;
  }

  //    private ChatResponse buildTextMessage(String message, String messageType, String senderId) {
  //
  //        switch (messageType) {
  //            case "ask":
  //                new ChatResponse(message, "interrogative", senderId);
  //            case "tell":
  //                new ChatResponse(message, "assertive", senderId);
  //            case "confirm":
  //                new ChatResponse(message, "interrogative", senderId);
  //            case "request":
  //                new ChatResponse(message, "imperative", senderId);
  //            case "greet":
  //                new ChatResponse(message, "exclamatory", senderId);
  //        }
  //
  //        return new ChatResponse(message, "assertive", senderId);
  //    }

  //    private ChatResponse askQuestion(String question, String senderId) {
  //        return buildTextMessage(question, "ask", senderId);
  //    }

}
