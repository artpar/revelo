package com.inf.messenger.worddefinition;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;

public interface HasLemmaNames {
  @JsonProperty ("lemma_names")
  List<String> getLemmaNames();
}
