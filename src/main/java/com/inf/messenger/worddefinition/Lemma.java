package com.inf.messenger.worddefinition;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude (JsonInclude.Include.NON_NULL)
@JsonPropertyOrder ({ "frame_strings", "name", "similar_words", "frame_ids", "hypernyms", "hyponyms" })
public class Lemma implements HasLemmaNames {
  @JsonProperty ("frame_strings")
  private List<String> frameStrings = null;

  @JsonProperty ("name")
  private String name;

  @JsonProperty ("similar_words")
  private List<SimilarWord> similarWords = null;

  @JsonProperty ("frame_ids")
  private List<Integer> frameIds = null;

  @JsonProperty ("hypernyms")
  private List<Object> hypernyms = null;

  @JsonProperty ("hyponyms")
  private List<Object> hyponyms = null;

  @JsonIgnore
  private Map<String, Object> additionalProperties = new HashMap<String, Object>();

  @JsonProperty ("frame_strings")
  public List<String> getFrameStrings() {
    return frameStrings;
  }

  @JsonProperty ("frame_strings")
  public void setFrameStrings(List<String> frameStrings) {
    this.frameStrings = frameStrings;
  }

  @JsonProperty ("name")
  public String getName() {
    return name;
  }

  @JsonProperty ("name")
  public void setName(String name) {
    this.name = name;
  }

  @JsonProperty ("similar_words")
  public List<SimilarWord> getSimilarWords() {
    return similarWords;
  }

  @JsonProperty ("similar_words")
  public void setSimilarWords(List<SimilarWord> similarWords) {
    this.similarWords = similarWords;
  }

  @JsonProperty ("frame_ids")
  public List<Integer> getFrameIds() {
    return frameIds;
  }

  @JsonProperty ("frame_ids")
  public void setFrameIds(List<Integer> frameIds) {
    this.frameIds = frameIds;
  }

  @JsonProperty ("hypernyms")
  public List<Object> getHypernyms() {
    return hypernyms;
  }

  @JsonProperty ("hypernyms")
  public void setHypernyms(List<Object> hypernyms) {
    this.hypernyms = hypernyms;
  }

  @JsonProperty ("hyponyms")
  public List<Object> getHyponyms() {
    return hyponyms;
  }

  @JsonProperty ("hyponyms")
  public void setHyponyms(List<Object> hyponyms) {
    this.hyponyms = hyponyms;
  }

  @JsonAnyGetter
  public Map<String, Object> getAdditionalProperties() {
    return this.additionalProperties;
  }

  @JsonAnySetter
  public void setAdditionalProperty(String name, Object value) {
    this.additionalProperties.put(name, value);
  }

  @Override
  @JsonIgnore
  public List<String> getLemmaNames() {
    return Collections.singletonList(this.name);
  }

}
