
package com.inf.messenger.worddefinition;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "definition",
    "examples",
    "lemma_names"
})
public class Hypernym implements HasLemmaNames {

    @JsonProperty("definition")
    private String definition;
    @JsonProperty("examples")
    private List<String> examples;
    @JsonProperty("lemma_names")
    private List<String> lemmaNames;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("definition")
    public String getDefinition() {
        return definition;
    }

    @JsonProperty("definition")
    public void setDefinition(String definition) {
        this.definition = definition;
    }

    @JsonProperty("examples")
    public List<String> getExamples() {
        return examples;
    }

    @JsonProperty("examples")
    public void setExamples(List<String> examples) {
        this.examples = examples;
    }

    @JsonProperty("lemma_names")
    public List<String> getLemmaNames() {
        return lemmaNames;
    }

    @JsonProperty("lemma_names")
    public void setLemmaNames(List<String> lemmaNames) {
        this.lemmaNames = lemmaNames;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
