package com.inf.messenger.worddefinition;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import com.inf.graphlike.english.SentenceImpl;

public class SomeFrameString {
    private final List<String> frameParts;

    private List<String> partsLeft;

    private List<String> partsRight;

    private List<String> partType;

    public SomeFrameString(String frameString) {
        frameParts = Arrays.asList(frameString.split(" "));
    }

    public boolean parseString(SentenceImpl sentenceImpl) {

        partType = new LinkedList<>();

        int verbIndex = frameParts.indexOf(sentenceImpl.getVerbPhrase().getPhrase());
        if (verbIndex > -1) {

            partsLeft = frameParts.subList(0, verbIndex);

            for (String s : partsLeft) {

                if (Arrays.asList("something", "somebody").indexOf(s) == 1) {
                    partType.add(s);
                }

            }
            partType.add(frameParts.get(verbIndex));

            if (frameParts.size() > verbIndex + 1) {
                partsRight = frameParts.subList(verbIndex + 1, frameParts.size());

                for (String s : partsRight) {
                    if (Arrays.asList("something", "somebody").indexOf(s) == 1) {
                        partType.add(s);
                    }
                }

            }

            return true;
        }

        return false;
    }
}
