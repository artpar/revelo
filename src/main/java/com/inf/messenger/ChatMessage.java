package com.inf.messenger;

import com.inf.graphlike.english.SentenceImpl;
import com.inf.messenger.sentence.Sentence;

public class ChatMessage {
    private MessageType messageType;
    private String personId;
    private SentenceImpl sentence;
    private String author;

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public MessageType getMessageType() {
        return messageType;
    }

    public void setMessageType(MessageType string) {
        this.messageType = string;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public SentenceImpl getSentence() {
        return sentence;
    }

    public void setSentence(SentenceImpl sentence) {
        this.sentence = sentence;
    }
}
