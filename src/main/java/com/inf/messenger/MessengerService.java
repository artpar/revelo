package com.inf.messenger;

import static com.github.messenger4j.send.message.richmedia.RichMediaAsset.Type.IMAGE;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import com.github.messenger4j.Messenger;
import com.github.messenger4j.common.WebviewHeightRatio;
import com.github.messenger4j.exception.MessengerApiException;
import com.github.messenger4j.exception.MessengerIOException;
import com.github.messenger4j.exception.MessengerVerificationException;
import com.github.messenger4j.send.MessagePayload;
import com.github.messenger4j.send.MessageResponse;
import com.github.messenger4j.send.SenderActionPayload;
import com.github.messenger4j.send.message.RichMediaMessage;
import com.github.messenger4j.send.message.TemplateMessage;
import com.github.messenger4j.send.message.TextMessage;
import com.github.messenger4j.send.message.quickreply.LocationQuickReply;
import com.github.messenger4j.send.message.quickreply.QuickReply;
import com.github.messenger4j.send.message.quickreply.TextQuickReply;
import com.github.messenger4j.send.message.richmedia.UrlRichMediaAsset;
import com.github.messenger4j.send.message.template.ButtonTemplate;
import com.github.messenger4j.send.message.template.ReceiptTemplate;
import com.github.messenger4j.send.message.template.button.Button;
import com.github.messenger4j.send.message.template.button.PostbackButton;
import com.github.messenger4j.send.message.template.button.UrlButton;
import com.github.messenger4j.send.message.template.receipt.Address;
import com.github.messenger4j.send.message.template.receipt.Adjustment;
import com.github.messenger4j.send.message.template.receipt.Item;
import com.github.messenger4j.send.message.template.receipt.Summary;
import com.github.messenger4j.send.senderaction.SenderAction;
import com.github.messenger4j.webhook.event.attachment.Attachment;
import com.inf.data.Suggestion;
import com.inf.drop.MessengerConfig;
import com.inf.survey.SurveyManager;
import com.inf.survey.data.Question;
import com.inf.survey.data.Survey;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.codec.digest.HmacAlgorithms;
import org.apache.commons.codec.digest.HmacUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MessengerService {
  private final String accessToken;

  private final String appSecret;

  private final String verifyToken;

  private ChatEngine chatEngine;

  private final Messenger messenger;

  private Logger logger = LoggerFactory.getLogger(MessengerService.class);

  private MessengerStorage messengerStorage;

  private SurveyManager surveyManager;

  private Random random = new Random(Date.from(Instant.now()).getTime());

  public MessengerService(MessengerConfig messengerConfig, MessengerStorage messengerStorage, ChatEngine engine, SurveyManager surveyManager) {

    this.accessToken = messengerConfig.getPageAccessToken();
    this.appSecret = messengerConfig.getAppSecret();
    this.verifyToken = messengerConfig.getVerifyToken();
    this.messengerStorage = messengerStorage;
    this.surveyManager = surveyManager;
    //        this.messengerStorage = messengerStorage;
    this.messenger = Messenger.create(accessToken, appSecret, verifyToken);
    //        this.nlpService = nlpService;
    this.chatEngine = engine;

  }

  public Messenger getMessenger() {
    return messenger;
  }

  public List<ChatMessage> handleEvent(String body, String signature, String questionnaireId) throws MessengerVerificationException {

    List<ChatMessage> responsePayloads = new LinkedList<>();
    try {

      // give or take
      // graph

      //            Mac mac = HmacUtils.getInitializedMac(HmacAlgorithms.HMAC_SHA_1, signature.getBytes());
      //            mac.update(body.getBytes());
      String signature1 = new HmacUtils(HmacAlgorithms.HMAC_SHA_1, appSecret).hmacHex(body);
      logger.info("Calclated: {} Actual: {}", signature1, signature);
      messenger.onReceiveEvents(body, of(signature), event -> {
        logger.info("Received TextMessageEvent: {}", event);

        final String senderId = event.senderId();
        final Instant timestamp = event.timestamp();
        ChatState chatState = messengerStorage.getHistory(senderId);

        if (event.isAttachmentMessageEvent()) {

          final List<Attachment> attachments = event.asAttachmentMessageEvent().attachments();

          logger.info("Received message with attachments from user '{}' at '{}':", senderId, timestamp);

          attachments.forEach(attachment -> {

            String payloadAsString = null;
            String attachmentType = "";
            if (attachment.isLocationAttachment()) {
              attachmentType = "location";
              payloadAsString = attachment.asLocationAttachment().toString();
            } else if (attachment.isFallbackAttachment()) {
              attachmentType = "fallback";
              payloadAsString = attachment.asFallbackAttachment().json();
              attachmentType = "richmedia";
            } else if (attachment.isRichMediaAttachment()) {
              payloadAsString = attachment.asRichMediaAttachment().url().toString();
            }

            logger.info("Attachment of type '{}' with payload '{}'", attachmentType, payloadAsString);
          });

        } else if (event.isQuickReplyMessageEvent()) {

        } else if (event.isTextMessageEvent()) {
          String text = event.asTextMessageEvent().text();

          logger.info("Received message with text '{}' from user '{}' at '{}'", text, senderId, timestamp);

          try {
            switch (text.toLowerCase()) {
              case "image":
                sendImageMessage(senderId);
                break;

              case "gif":
                sendGifMessage(senderId);
                break;

              case "button":
                sendButtonMessage(senderId);
                break;

              case "generic":
                sendGenericMessage(senderId);
                break;

              case "receipt":
                sendReceiptMessage(senderId);
                break;

              case "quick reply":
                sendQuickReply(senderId);
                break;

              case "read receipt":
                sendReadReceipt(senderId);
                break;
              default:
                try {
                  LinkedList<ChatMessage> responses = replyTextMessage(senderId, text, chatState, questionnaireId);

                  for (ChatMessage respons : responses) {
                    chatState.addMessage(respons);
                  }

                  messengerStorage.setChatState(chatState, senderId);
                  logger.info("Got {} replies", responses.size());
                  responsePayloads.addAll(responses);
                } catch (Exception e) {

                }
            }
          } catch (MessengerApiException | MessengerIOException e) {
            handleSendException(e);
          } catch (MalformedURLException e) {
            e.printStackTrace();
          }
        } else {
          logger.info("Don't know how to handle: {}", event.toString());
        }

      });
    } catch (Exception e) {
      logger.error("Failed", e);
    }
    return responsePayloads;
  }

  private void sendImageMessage(String recipientId) throws MessengerApiException, MessengerIOException, MalformedURLException {

    final UrlRichMediaAsset richMediaAsset = UrlRichMediaAsset.create(IMAGE, new URL("https://media.giphy.com/media/11sBLVxNs7v6WA/giphy.gif"));
    final RichMediaMessage richMediaMessage = RichMediaMessage.create(richMediaAsset);
    final MessagePayload payload = MessagePayload.create(recipientId, richMediaMessage);

    //        sendMessageTo(payload, recipientId);
  }

  private void sendGifMessage(String recipientId) throws MessengerApiException, MessengerIOException, MalformedURLException {
    final UrlRichMediaAsset richMediaAsset = UrlRichMediaAsset.create(IMAGE, new URL("https://media.giphy.com/media/11sBLVxNs7v6WA/giphy.gif"));
    final RichMediaMessage richMediaMessage = RichMediaMessage.create(richMediaAsset);
    final MessagePayload payload = MessagePayload.create(recipientId, richMediaMessage);
    //        sendMessageTo(payload, recipientId);
  }

  private void sendButtonMessage(String recipientId) throws MessengerApiException, MessengerIOException, MalformedURLException {

    final UrlButton buttonA = UrlButton.create("Show Website", new URL("https://revlo.club"));
    final PostbackButton buttonB = PostbackButton.create("Start Chatting", "USER_DEFINED_PAYLOAD");
    final UrlButton buttonC = UrlButton
            .create("Show Website", new URL("https://revlo.club"), of(WebviewHeightRatio.FULL), of(true), of(new URL("https://revlo.club")));

    final List<Button> buttons = Arrays.asList(buttonA, buttonB, buttonC);
    final ButtonTemplate buttonTemplate = ButtonTemplate.create("What do you want to do next?", buttons);

    final TemplateMessage templateMessage = TemplateMessage.create(buttonTemplate);
    final MessagePayload payload = MessagePayload.create(recipientId, templateMessage);

    //        sendMessageTo(payload, recipientId);

  }

  private void sendGenericMessage(String recipientId) throws MessengerApiException, MessengerIOException, MalformedURLException {

    final String text = "Here is a quick reply!";

    final TextQuickReply quickReplyA = TextQuickReply.create("Search", "<POSTBACK_PAYLOAD>", of(new URL("http://example.com/img/red.png")));
    final LocationQuickReply quickReplyB = LocationQuickReply.create();
    final TextQuickReply quickReplyC = TextQuickReply.create("Something Else", "<POSTBACK_PAYLOAD>");

    final List<QuickReply> quickReplies = Arrays.asList(quickReplyA, quickReplyB, quickReplyC);

    final TextMessage message = TextMessage.create(text, of(quickReplies), empty());
    final MessagePayload payload = MessagePayload.create(recipientId, message);

    //        sendMessageTo(payload, recipientId);
  }

  private void sendReceiptMessage(String recipientId) throws MessengerApiException, MessengerIOException, MalformedURLException {

    final Adjustment adjustment1 = Adjustment.create("New Customer Discount", 20.00F);
    final Adjustment adjustment2 = Adjustment.create("$10 Off Coupon", 10.00F);

    final Item item1 = Item.create("Classic White T-Shirt", 50F, of("100% Soft and Luxurious Cotton"), of(2), of("USD"),
            of(new URL("https://revlo.club/img/whiteshirt.png")));

    final Item item2 = Item.create("Classic Gray T-Shirt", 25F, of("100% Soft and Luxurious Cotton"), of(1), of("USD"),
            of(new URL("https://revlo.club/img/grayshirt.png")));

    final Address address = Address.create("1 Hacker Way", of(""), "Menlo Park", "94025", "CA", "US");
    final Summary summary = Summary.create(56.14F, of(75.00F), of(6.19F), of(4.95F));

    final ReceiptTemplate receiptTemplate = ReceiptTemplate
            .create("Stephane Crozatier", "12345678902", "Visa 2345", "USD", summary, of(address), of(Arrays.asList(item1, item2)),
                    of(Arrays.asList(adjustment1, adjustment2)), empty(), of(new URL("https://revlo.club/order?order_id=123456")), empty(),
                    of(ZonedDateTime.of(2015, 4, 7, 22, 14, 12, 0, ZoneOffset.UTC).toInstant()));

    final MessagePayload payload = MessagePayload.create(recipientId, TemplateMessage.create(receiptTemplate));

    //        sendMessageTo(payload, recipientId);

    //        messenger.sendTemplate(recipientId, receiptTemplate);
  }

  private void sendQuickReply(String recipientId) throws MessengerApiException, MessengerIOException, MalformedURLException {

    final String text = "Here is a quick reply!";

    final TextQuickReply quickReplyA = TextQuickReply.create("Search", "<POSTBACK_PAYLOAD>", of(new URL("http://example.com/img/red.png")));
    final LocationQuickReply quickReplyB = LocationQuickReply.create();
    final TextQuickReply quickReplyC = TextQuickReply.create("Something Else", "<POSTBACK_PAYLOAD>");

    final List<QuickReply> quickReplies = Arrays.asList(quickReplyA, quickReplyB, quickReplyC);

    final TextMessage message = TextMessage.create(text, of(quickReplies), empty());
    final MessagePayload payload = MessagePayload.create(recipientId, message);

    //        sendMessageTo(payload, recipientId);

  }

  private void sendReadReceipt(String recipientId) throws MessengerApiException, MessengerIOException {

    final SenderAction senderAction = SenderAction.MARK_SEEN;

    final SenderActionPayload payload = SenderActionPayload.create(recipientId, senderAction);
    messenger.send(payload);
  }

  public LinkedList<ChatMessage> replyTextMessage(String senderId, String text, ChatState chatState, String questionnaireId)
          throws MessengerApiException, MessengerIOException {
    LinkedList<ChatMessage> payloads = new LinkedList<>();

    try {
      payloads.addAll(chatEngine.applySentence(senderId, text, chatState));
    } catch (Exception failedToApply) {
      logger.error("Failed to parse: [%s]", text, failedToApply);
    }

    try {
      payloads.addAll(chatEngine.applyQuestionnaire(senderId, chatState, text, questionnaireId));
    } catch (Exception failedToApply) {
      logger.error("Failed to apply questionnaire", text, failedToApply);
    }

    return payloads;
  }

  public MessageResponse sendMessageTo(ChatMessage chatResponse) {
    logger.info("Sending message to {}", chatResponse);
    try {

      final TextMessage message = TextMessage.create(chatResponse.getSentence().getSentence(), empty(), empty());
      final MessagePayload payload = MessagePayload.create(chatResponse.getPersonId(), message);
      return messenger.send(payload);
    } catch (MessengerApiException | MessengerIOException e) {
      logger.error("Failed to send messenger message", e);
      //            e.printStackTrace();
    }
    return null;
  }

  private void handleSendException(Exception e) {
    logger.error("Message could not be sent. An unexpected error occurred.", e);
  }

}
