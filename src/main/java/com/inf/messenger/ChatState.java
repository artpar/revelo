package com.inf.messenger;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import com.inf.data.Suggestion;
import com.inf.survey.data.Question;

public class ChatState {
    private List<ChatMessage> history;

    private String state;

    private String personId;

    /**
     * Opening,feedforward
     */
    private String conversationState;

    private PersonState personState;

    private String sentenceParse;

    private Integer surveyId;

    private List<Question> questions;

    private int lastQuestionIndex;

    private String confirmAnswer;

    private Collection<Suggestion> suggestionList;

    public PersonState getPersonState() {
        return personState;
    }

    public void setPersonState(PersonState personState) {
        this.personState = personState;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public ChatState(List<ChatMessage> history) {

        this.history = history;
    }

    public String getSentenceParse() {
        return sentenceParse;
    }

    public void setSentenceParse(String sentenceParse) {
        this.sentenceParse = sentenceParse;
    }

    public ChatState() {
        this.history = new LinkedList<ChatMessage>();
    }

    public List<ChatMessage> getHistory() {
        return history;
    }

    public void setHistory(List<ChatMessage> history) {
        if (history == null) {
            throw new NullPointerException("history is null");
        }
        this.history = history;
    }

    public void addMessage(ChatMessage chatMessage) {
        this.history.add(chatMessage);
    }

    public boolean currentStateIs(Object o) {
        if (this.state == null && o == null) {
            return true;
        }
        if (this.state == null || o == null) {
            return false;
        }
        return this.state.equals(o);
    }

    public void setSurveyId(Integer surveyId) {
        this.surveyId = surveyId;
    }

    public Integer getSurveyId() {
        return surveyId;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setLastQuestionIndex(int lastQuestionIndex) {
        this.lastQuestionIndex = lastQuestionIndex;
    }

    public int getLastQuestionIndex() {
        return lastQuestionIndex;
    }

    public void setConfirmAnswer(String confirmAnswer) {
        this.confirmAnswer = confirmAnswer;
    }

    public String getConfirmAnswer() {
        return confirmAnswer;
    }

    public void setSuggestionList(Collection<Suggestion> suggestionList) {
        this.suggestionList = suggestionList;
    }

    public Collection<Suggestion> getSuggestionList() {
        return suggestionList;
    }
}
