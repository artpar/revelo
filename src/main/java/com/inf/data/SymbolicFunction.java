package com.inf.data;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * author parth.mudgal on 23/09/14.
 */
public class SymbolicFunction {
    private Collection<String> symbols;

    private String formula;

    private List<Substitution> substitutions;

    private Double weight;

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public SymbolicFunction() {
        this.substitutions = new LinkedList<>();
    }

    public SymbolicFunction(String formula, String weightStr) {
        this.formula = formula;
        this.weight = Double.valueOf(weightStr);
    }

    public SymbolicFunction(String s) {
        String[] parts = s.split(";");
        if (parts.length == 2) {
            weight = Double.valueOf(parts[1].trim());
            formula = parts[0].trim();
        } else {
            this.weight = 1.0;
            this.formula = s;
        }
    }

    public Collection<String> getSymbols() {
        return symbols;
    }

    public void setSymbols(Collection<String> symbols) {
        this.symbols = symbols;
    }

    public String getFormula() {
        return formula.trim();
    }



    public void setFormula(String formula) {
        this.formula = formula;
    }

    public List<Substitution> getSubstitutions() {
        if (substitutions == null) {
            return new LinkedList<>();
        }
        return substitutions;
    }

    public void setSubstitutions(List<Substitution> substitutions) {
        if (substitutions == null) {
            this.substitutions = new LinkedList<>();
        } else {
            this.substitutions = substitutions;
        }
    }

    public SymbolicFunction add(SymbolicFunction f) {
        SymbolicFunction newFunction = new SymbolicFunction("");
        newFunction.setFormula(this.getFormula() + " + " + f.getFormula());

        List<String> finalSymbols = new LinkedList<String>(this.symbols);
        Collection<String> otherSymbols = f.getSymbols();
        for (String sym : otherSymbols) {
            if (!finalSymbols.contains(sym)) {
                finalSymbols.add(sym);
            }
        }
        newFunction.setSymbols(finalSymbols);

        List<Substitution> finalSubstitutions = new LinkedList<Substitution>();
        for (Substitution substitution : this.getSubstitutions()) {
            Substitution temp = new Substitution();
            temp.setValue(substitution.value);
            temp.setVariable(substitution.variable);
            finalSubstitutions.add(temp);
        }

        for (Substitution substitution : f.getSubstitutions()) {
            Substitution temp = new Substitution();
            temp.setValue(substitution.value);
            temp.setVariable(substitution.variable);
            finalSubstitutions.add(temp);
        }

        newFunction.setSubstitutions(finalSubstitutions);
        return newFunction;
    }

    public SymbolicFunction clone() {
        SymbolicFunction newFunction = new SymbolicFunction("");
        newFunction.setFormula(this.getFormula());

        if (this.symbols != null) {

            List<String> finalSymbols = new LinkedList<String>(this.symbols);
            newFunction.setSymbols(finalSymbols);
        } else {
            newFunction.setSymbols(new LinkedList<>());
        }

        if (this.getSubstitutions() != null) {
            List<Substitution> finalSubstitutions = new LinkedList<Substitution>();
            for (Substitution substitution : this.getSubstitutions()) {
                Substitution temp = new Substitution();
                temp.setValue(substitution.value);
                temp.setVariable(substitution.variable);
                finalSubstitutions.add(temp);
            }
            newFunction.setSubstitutions(finalSubstitutions);
        } else {
            newFunction.setSubstitutions(new LinkedList<>());
        }

        return newFunction;
    }

    @Override
    public String toString() {
        return "SymbolicFunction{" + "symbols=" + symbols + ", formula='" + formula + '\'' + ", weight=" + weight +", substitutions=" + substitutions + '}';
    }
}
