package com.inf.data.page;

/**
 * author parth.mudgal on 19/03/14.
 */
public class PageInfo
{
	private int id;
	private String website;
	private String pageSelector;
	private String resultType;

	public String getResultType()
	{
		return resultType;
	}

	public void setResultType(String resultType)
	{
		this.resultType = resultType;
	}

	public String getPageSelector()
	{
		return pageSelector;
	}

	public void setPageSelector(String pageSelector)
	{
		this.pageSelector = pageSelector;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getWebsite()
	{
		return website;
	}

	public void setWebsite(String website)
	{
		this.website = website;
	}

}
