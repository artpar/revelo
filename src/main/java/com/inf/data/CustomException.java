package com.inf.data;

/**
 * author parth.mudgal on 11/08/14.
 */
public class CustomException extends Exception {
  public CustomException(String s, Object other) {
    super(s + " - " + other);
  }

}
