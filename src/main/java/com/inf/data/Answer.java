package com.inf.data;

import net.sf.json.JSONObject;

/**
 * author parth.mudgal on 12/07/14.
 */
public class Answer {
    Integer id;
    Integer categoryId;
    Integer questionId;
    QuestionType questionType;
    String answer;
    String answerString;
    String reasonString;
    JSONObject data;
    String internalObject;
    String status;
    String referenceId;

    public QuestionType getQuestionType() {
        return questionType;
    }

    public void setQuestionType(QuestionType questionType) {
        this.questionType = questionType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public void setInternalObject(String internalObject) {
        this.internalObject = internalObject;
        data = JSONObject.fromObject(internalObject);
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Integer questionId) {
        this.questionId = questionId;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getAnswerString() {
        return answerString;
    }

    public void setAnswerString(String answerString) {
        this.answerString = answerString;
    }

    public String getReasonString() {
        return reasonString;
    }

    public void setReasonString(String reasonString) {
        if (reasonString == null) {
            this.reasonString = "";
        } else {
            this.reasonString = reasonString.trim();
        }
    }

    public JSONObject getData() {
        return data;
    }

    public void setData(JSONObject data) {
        this.data = data;
    }

}
