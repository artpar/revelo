package com.inf.data;

import java.util.Objects;

/**
 * author parth.mudgal on 23/09/14.
 */
public class Substitution {
    String variable;
    String value;

    public String getVariable() {
        return variable;
    }

    public void setVariable(String variable) {
        this.variable = variable;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Substitution that = (Substitution) o;
        return Objects.equals(getVariable(), that.getVariable()) &&
                Objects.equals(getValue(), that.getValue());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getVariable(), getValue());
    }

    @Override
    public String toString() {
        return "Substitution{" +
                "variable='" + variable + '\'' +
                ", value='" + value + '\'' +
                '}';
    }

    public String getValue()

    {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
