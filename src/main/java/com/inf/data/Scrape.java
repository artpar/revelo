package com.inf.data;

/**
 * author parth.mudgal on 19/03/14.
 */
public class Scrape
{
	int id;

	Integer productId;
	Integer scrapeWebsiteId;
	private String html;
	private String link;

	public String getLink()
	{
		return link;
	}

	public void setLink(String link)
	{
		this.link = link;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public Integer getProductId()
	{
		return productId;
	}

	public void setProductId(Integer productId)
	{
		this.productId = productId;
	}

	public Integer getScrapeWebsiteId()
	{
		return scrapeWebsiteId;
	}

	public void setScrapeWebsiteId(Integer scrapeWebsiteId)
	{
		this.scrapeWebsiteId = scrapeWebsiteId;
	}

	public void setHtml(String html)
	{
		this.html = html;
	}

	public String getHtml()
	{
		return html;
	}
}
