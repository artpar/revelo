package com.inf.data;

import com.inf.survey.FeatureData;
import com.inf.survey.SurveyManagerImpl;

import java.util.HashMap;


/**
 * author parth.mudgal on 12/07/14.
 */
public class Suggestion implements SurveyManagerImpl.ValueGetter {
    protected Product product;
    protected double score;
    protected double maxPossibleScore;
    protected long normalizedScore;
    protected HashMap<String, FeatureData> categoryProperties;

    //protected Map<String, String []> featureToUsageMapping;

    public Suggestion(Product product, double score) {
        this.product = product;
        this.score = score;
    }

    public Suggestion(Product product, double score, double maxPossibleScore) {
        this.product = product;
        this.score = score;
        this.maxPossibleScore = maxPossibleScore;
    }

    public Suggestion() {
    }

    @Override
    public String toString() {
        return "{" +
                "product=" + product +
                ", score=" + score +
                ", maxPossibleScore=" + maxPossibleScore +
                '}';
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public double getMaxPossibleScore() {
        return maxPossibleScore;
    }

    public void setMaxPossibleScore(Double maxPossibleScore) {
        this.maxPossibleScore = maxPossibleScore;
    }

    public HashMap<String, FeatureData> getCategoryProperties() {
      return categoryProperties;
    }

    public void setCategoryProperties(HashMap<String,FeatureData> categoryProperties) {
      this.categoryProperties = categoryProperties;
    }

    @Override
    public Number GetValue() {
        return score;
    }

    public void computeNormalizedScore() {
        this.normalizedScore = Math.round(score*100/maxPossibleScore);
    }
}
