package com.inf.data;

import java.util.Date;

public class Questionnaire {
  private Integer id;
  private Integer userId;
  private Integer categoryId;
  private String categoryName;
  private String name;
  private String referenceId;
  private String status;
  private Date createdAt;

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getCategoryName() {
    return categoryName;
  }

  public void setCategoryName(String categoryName) {
    this.categoryName = categoryName;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Integer getUserId() {
    return userId;
  }

  public void setUserId(Integer userId) {
    this.userId = userId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getReferenceId() {
    return referenceId;
  }

  public void setReferenceId(String referenceId) {
    this.referenceId = referenceId;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public Integer getCategoryId() {
    return categoryId;
  }

  public void setCategoryId(Integer categoryId) {
    this.categoryId = categoryId;
  }

  @Override
  public String toString() {
    return "Questionnaire{" +
        "id=" + id +
        ", userId=" + userId +
        ", categoryId=" + categoryId +
        ", categoryName='" + categoryName + '\'' +
        ", name='" + name + '\'' +
        ", referenceId='" + referenceId + '\'' +
        ", status='" + status + '\'' +
        ", createdAt=" + createdAt +
        '}';
  }
}
