package com.inf.data;

/**
 * author parth.mudgal on 22/09/14.
 */
public enum QuestionType {
    single_string_choice, multi_string_choice, single_integer, single_string, single_image_choice, multi_image_choice, multi_string_choice_filter;
}
