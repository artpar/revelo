package com.inf.data;

/**
 * author parth.mudgal on 19/03/14.
 */
public class Product {
  private String id;
  private String name;
  private String companyName;
  private Integer categoryId;
  private String numericValue;


  public Product() {
  }

  @Override
  public String toString() {
    return "Product{" +
        "id='" + id + '\'' +
        ", name='" + name + '\'' +
        ", companyName='" + companyName + '\'' +
        ", categoryId=" + categoryId +
        ", numericValue='" + numericValue + '\'' +
        '}';
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCompanyName() {
    return companyName;
  }

  public void setCompanyName(String companyName) {
    this.companyName = companyName;
  }

  public Integer getCategoryId() {
    return categoryId;
  }

  public void setCategoryId(Integer categoryId) {
    this.categoryId = categoryId;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getNumericValue() {
    return numericValue;
  }

  public void setNumericValue(String numericValue) {
    this.numericValue = numericValue;
  }
}
