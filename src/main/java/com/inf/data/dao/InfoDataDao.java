package com.inf.data.dao;

import java.io.IOException;
import java.util.List;
import com.inf.data.Category;
import com.inf.data.Product;
import com.inf.data.ProductFeature;
import com.inf.data.dao.impl.FeatureValue;
import com.inf.mvc.CategoryUpdateRequest;

/**
 * Created by parth on 22/4/16.
 */
public interface InfoDataDao {
  void updateCategoryByReferenceId(CategoryUpdateRequest categoryUpdateRequest);

  List<Product> getProductsByQuestionnaireId(Integer questionnaireId);

  List<Product> getProductsByCategoryId(Integer categoryId) throws IOException;

  List<Product> getProductsByCategoryReferenceId(String categoryId);

  Category getCategoryById(Integer id);

  List<ProductFeature> getProductFeatures(Product product);

  ProductFeature getProductSingleFeatureValue(Product product, String featureName);

  List<ProductFeature> getFeatureWeights(Integer categoryId);

  List<String> getCategoryFeatures(Integer categoryId) throws IOException;

  List<ProductFeature> getProductFeaturesNumericValue(Product product);

  List<String> getFeatureList(Integer categoryId) throws IOException;

  void deleteResultFromCategory(Integer id, String groupName, String name);

  List<FeatureValue> getCategoryFeatureValues(Integer id, String featureName);

  List<FeatureValue> getCategoryFeatureDistinctValues(Integer id, String featureName);
}
