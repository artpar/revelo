package com.inf.data.dao;

import com.inf.data.Product;
import com.inf.data.ProductFeature;
import com.inf.exceptions.InvalidCategoryException;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Created by parth on 22/4/16.
 */
public interface ProductDao {
    void updateOrInsertProductInfo(Integer categoryId, String company, String product, String key, String value,
                                   String numericValue, String sourceLink);

    void setTransformedValue(Integer category, String company, String product, String key, Double value);

    Map<String, String> getProductFeaturesWithNullFeatures(Product product);

    List<Product> getProductsByCategoryId(Integer categoryId) throws InvalidCategoryException, IOException;

    List<ProductFeature> getProductFeatures(Product product) throws InvalidCategoryException;

    List<String> getCategoryFeatures(Integer categoryId) throws InvalidCategoryException, IOException;

    List<ProductFeature> getProductFeaturesNumericValue(Product product) throws InvalidCategoryException;

    List<String> getFeatureList(Integer categoryId) throws InvalidCategoryException, IOException;

    void deleteResultFromCategory(Integer id, String groupName, String name) throws InvalidCategoryException;
}
