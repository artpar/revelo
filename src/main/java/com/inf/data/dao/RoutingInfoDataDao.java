package com.inf.data.dao;

import java.util.LinkedList;
import java.util.List;
import com.inf.data.Category;
import com.inf.data.Product;
import com.inf.data.ProductFeature;
import com.inf.data.dao.impl.FeatureValue;
import com.inf.data.dao.impl.InfoDataDaoFactory;
import com.inf.exceptions.InvalidCategoryException;
import com.inf.mvc.CategoryUpdateRequest;

/**
 * Created by parth on 22/4/16.
 */
public class RoutingInfoDataDao implements InfoDataDao {
  private final InfoDataDao databaseInfoDataDao;

  InfoDataDaoFactory infoDataDaoFactory;

  public RoutingInfoDataDao(InfoDataDaoFactory infoDataDaoFactory) throws InvalidCategoryException {
    this.infoDataDaoFactory = infoDataDaoFactory;
    this.databaseInfoDataDao = infoDataDaoFactory.getInfoDataDao("database");
  }

  @Override
  public void updateCategoryByReferenceId(CategoryUpdateRequest categoryUpdateRequest) {
  }

  @Override
  public List<Product> getProductsByQuestionnaireId(Integer questionnaireId) {

    return null;
  }

  @Override
  public List<Product> getProductsByCategoryId(Integer categoryId) {
    return null;
  }

  @Override
  public List<Product> getProductsByCategoryReferenceId(String categoryId) {
    return null;
  }

  @Override
  public Category getCategoryById(Integer id) {
    return databaseInfoDataDao.getCategoryById(id);
  }

  @Override
  public List<ProductFeature> getProductFeatures(Product product) {
    return null;
  }

  @Override
  public ProductFeature getProductSingleFeatureValue(Product product, String featureName) {
    List<ProductFeature> features = getProductFeatures(product);
    if (features.size() > 0) {
      return features.get(0);
    } else {
      return new ProductFeature();
    }
  }

  @Override
  public List<ProductFeature> getFeatureWeights(Integer categoryId) {
    return new LinkedList<>();
  }

  @Override
  public List<String> getCategoryFeatures(Integer categoryId) {
    return null;
  }

  @Override
  public List<ProductFeature> getProductFeaturesNumericValue(Product product) {
    return null;
  }

  @Override
  public List<String> getFeatureList(Integer categoryId) {
    return null;
  }

  @Override
  public void deleteResultFromCategory(Integer id, String groupName, String name) {

  }

  @Override
  public List<FeatureValue> getCategoryFeatureValues(Integer id, String featureName) {
    return null;
  }

  @Override
  public List<FeatureValue> getCategoryFeatureDistinctValues(Integer id, String featureName) {
    return null;
  }
}
