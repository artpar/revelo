package com.inf.data.dao.impl;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.inf.cache.CacheInterface;
import com.inf.data.Category;
import com.inf.data.Product;
import com.inf.data.ProductFeature;
import com.inf.data.dao.InfoDataDao;
import com.inf.mvc.CategoryUpdateRequest;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.client.RestTemplate;

/**
 * Created by parth on 25/4/16.
 */
public class DbpediaInfoDataDao extends AbstractWebApiInfoDataDao {
  public DbpediaInfoDataDao(ObjectMapper objectMapper, CacheInterface<String, String> cache, RestTemplate restTemplate, InfoDataDao databaseDao) {
    super("Dbpedia", objectMapper, cache, restTemplate, databaseDao);
  }

  @Override
  public void updateCategoryByReferenceId(CategoryUpdateRequest categoryUpdateRequest) {
    databaseDao.updateCategoryByReferenceId(categoryUpdateRequest);
  }

  @Override
  public List<Product> getProductsByQuestionnaireId(Integer questionnaireId) {
    return null;
  }

  @Override
  public List<Product> getProductsByCategoryId(Integer categoryId) throws IOException {
    Category cat = c(categoryId);
    String[] parts = cat.getDatasource().split("::");
    if (parts.length < 3) {
      return new LinkedList<>();
    }
    String query = getSparqlHeader() + "SELECT DISTINCT ?name  ?companyName\n" + "WHERE {\n" + "   ?object rdf:type " + parts[1] + ".\n" + "   ?object rdfs:label ?name.\n" + "   ?object " + parts[2] + " ?country.\n" + "   ?country rdfs:label ?companyName.\n" + "   FILTER ( lang(?companyName) = 'en' and lang(?name) = 'en')\n" + " }";
    logger.info("Query: \n" + query);

    String response = sparql(query, false);
    SparqlQueryResult result;
    try {

      result = objectMapper.readValue(response, SparqlQueryResult.class);
      if (result.results.bindings.size() < 5) {
        response = sparql(query, true);
        result = objectMapper.readValue(response, SparqlQueryResult.class);
      }
    } catch (Exception e) {
      response = sparql(query, true);
      result = objectMapper.readValue(response, SparqlQueryResult.class);

    }
    List<Product> fi = new LinkedList<>();
    for (Map<String, Object> binding : result.results.bindings) {
      Product p = new Product();
      p.setName((String) ((Map) binding.get("name")).get("value"));
      p.setCompanyName((String) ((Map) binding.get("companyName")).get("value"));
      p.setCategoryId(categoryId);
      fi.add(p);
    }

    return fi;
  }

  @Override
  public List<Product> getProductsByCategoryReferenceId(String categoryId) {
    return null;
  }

  @Override
  public Category getCategoryById(Integer id) {
    return null;
  }

  @Override
  public List<ProductFeature> getProductFeatures(Product product) {
    final Category category = c(product.getCategoryId());
    String[] parts = category.getDatasource().split("::");
    String query = getSparqlHeader() + "SELECT DISTINCT ?name  ?key ?companyName ?value\n" + "WHERE {\n" + "   ?object rdf:type " + parts[1] + ".\n" + "   ?object rdfs:label ?name.\n" + "   ?object " + parts[2] + " ?country.\n" + "   ?country rdfs:label ?companyName.\n" + "   ?object ?key ?value .\n" + "   FILTER ( lang(?companyName) = 'en' and lang(?name) = 'en') .\n" + "  filter(regex(?name,  '^" + StringUtils
            .replace(product.getName(), "'", "\'") + "') ) .\n" + "} ";
    return SparqlToPojo(query, ProductFeature.class);
  }

  @Override
  public ProductFeature getProductSingleFeatureValue(Product product, String featureName) {
    return null;
  }

  @Override
  public List<ProductFeature> getFeatureWeights(Integer categoryId) {
    return new LinkedList<>();
  }

  @Override
  public List<String> getCategoryFeatures(Integer categoryId) throws IOException {

    final Category category = c(categoryId);
    String[] parts = category.getDatasource().split("::");

    String query = getSparqlHeader() + "SELECT DISTINCT ?name \n" + "WHERE {\n" + "   ?object rdf:type " + parts[1] + ".\n" + "   ?object ?key ?value .\n" + "   ?key rdfs:label ?name .\n" + "   FILTER ( lang(?name) = 'en')\n" + "}";
    logger.info("Query: \n" + query);
    String response = sparql(query, false);
    if (response == null) {
      return new LinkedList<>();
    }

    SparqlQueryResult result = objectMapper.readValue(response, SparqlQueryResult.class);
    List<String> fi = new LinkedList<>();
    for (Map<String, Object> binding : result.results.bindings) {
      //            ProductFeature p = new ProductFeature();
      String key = (String) ((Map) binding.get("name")).get("value");
      //            p.setKey(key);
      fi.add(key);
    }

    return fi;
  }

  private String sparql(String query, boolean skipCache) {
    logger.info("Sparql: \n" + query);
    Map<String, String> keys = new HashMap<>();
    keys.put("query", query);
    keys.put("uri", "http://dbpedia.org");
    String url = "http://dbpedia.org/sparql?default-graph-uri={uri}&query={query}&format=json&timeout=60000";
    return query(keys, url, skipCache);
  }

  @Override
  public List<ProductFeature> getProductFeaturesNumericValue(Product product) {
    final Category c = c(product.getCategoryId());
    String[] parts = c.getDatasource().split("::");
    String query = getSparqlHeader() + "SELECT DISTINCT ?name  ?key ?value\n" + "WHERE {\n" + "   ?object rdf:type " + parts[1] + ".\n" + "   ?object rdfs:label ?name.\n" + "   ?object " + parts[2] + " ?country.\n" + "   ?object ?key ?value .\n" + "   FILTER ( lang(?companyName) = 'en' and lang(?name) = 'en')\n" + "  filter(regex(?name,  '^" + StringUtils
            .replace(product.getName(), "'", "\'") + "') ) .\n" + "} ";
    return SparqlToPojo(query, ProductFeature.class);
  }

  @Override
  public List<String> getFeatureList(Integer categoryId) throws IOException {
    return getCategoryFeatures(categoryId);
  }

  @Override
  public void deleteResultFromCategory(Integer id, String groupName, String name) {

  }

  @Override
  public List<FeatureValue> getCategoryFeatureDistinctValues(Integer id, String featureName) {
    Category cat = c(id);
    String[] parts = cat.getDatasource().split("::");

    String query = getSparqlHeader() + "SELECT DISTINCT ?name ?value\n" + "WHERE {\n" + "   ?object rdf:type " + parts[1] + ".\n" + "   ?object rdfs:label ?name.\n" + "   ?object " + parts[2] + " ?country.\n" + "   ?country rdfs:label ?companyName.\n" + "   ?object ?prop ?value .\n" + "   ?prop rdfs:label ?key .\n" + "   FILTER ( lang(?companyName) = 'en' and lang(?name) = 'en')\n" + "  filter(regex(?key,  '^" + featureName
            .replaceAll("([\\(\\)])", "\\\\\\\\$1").replaceAll("'", "\\\\'") + "') ) .\n" + "} ";

    return SparqlToPojo(query, FeatureValue.class);

  }

  @Override
  public List<FeatureValue> getCategoryFeatureValues(Integer id, String featureName) {
    Category cat = c(id);
    String[] parts = cat.getDatasource().split("::");

    String query = getSparqlHeader() + "SELECT DISTINCT ?name  ?key ?companyName ?value\n" + "WHERE {\n" + "   ?object rdf:type " + parts[1] + " .\n" + "   ?object rdfs:label ?name.\n" + "   ?object " + parts[2] + " ?country.\n" + "   ?country rdfs:label ?companyName.\n" + "   ?object ?prop ?value .\n" + "   ?prop rdfs:label ?key .\n" + "   FILTER ( lang(?companyName) = 'en' and lang(?name) = 'en') .\n" + "  filter(regex(?key,  '^" + featureName
            .replaceAll("([\\(\\)])", "\\\\\\\\$1").replaceAll("'", "\\\\'") + "') ) .\n" + "} ";

    return SparqlToPojo(query, FeatureValue.class);

  }

  private String getSparqlHeader() {
    return "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> \n" + "PREFIX  dbpedia-owl:  <http://dbpedia.org/ontology/>\n" + "PREFIX type: <http://dbpedia.org/ontology/> \n" + "PREFIX prop: <http://dbpedia.org/property/> \n";
  }

  private <T> List<T> SparqlToPojo(String query, Class<T> classType) {
    List<String> skip = new LinkedList<>();

    SparqlQueryResult result;
    String response = sparql(query, false);
    if (response == null) {
      return new LinkedList<T>();
    }
    try {
      result = objectMapper.readValue(response, SparqlQueryResult.class);
      List<T> featureValues = bindingsToPojo(result, classType);
      if (classType.equals(FeatureValue.class)) {

        for (T featureValue : featureValues) {
          FeatureValue o = (FeatureValue) featureValue;
          if (skip.contains(o.getKey())) {
            continue;
          }
          try {
            Double numberValue = Double.parseDouble(o.getValue());
            o.setNormalisedNumberValue(numberValue);
          } catch (NumberFormatException nfe) {
            //                        logger.error("Added [" + o.getKey() + "] to skip list because of the value [" + o.getValue() + "] for [" + o.getName() + "]");
            //                        skip.add(o.getKey());
            o.setNormalisedNumberValue(null);
          }
        }
      }

      if (featureValues.size() < 3) {
        sparql(query, true);
      }

      return featureValues;

    } catch (IOException | IntrospectionException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
      logger.error("failed to read as json \n" + response, e);
      sparql(query, true);
      return new LinkedList<>();
    }
  }

  private <T> List<T> bindingsToPojo(SparqlQueryResult result, Class<T> valueType)
          throws IntrospectionException, IllegalAccessException, InvocationTargetException, InstantiationException {
    SparqlHead head = result.getHead();

    BeanInfo beanInfo = Introspector.getBeanInfo(valueType);
    Constructor<?>[] constructors = valueType.getConstructors();
    Constructor<?> selectedConstructor = null;
    for (Constructor<?> constructor : constructors) {
      if (constructor.getParameterCount() == 0) {
        logger.info("No parameter constructor: " + constructor.getName());
        selectedConstructor = constructor;
        break;
      }
      logger.info("Constructor has " + constructor.getParameterCount() + " parameters :" + constructor.getName());
    }

    Map<String, Method> writer = new HashMap<>();
    for (String s : head.getVars()) {
      writer.put(s, null);
    }

    for (PropertyDescriptor propertyDescriptor : beanInfo.getPropertyDescriptors()) {
      if (writer.containsKey(propertyDescriptor.getName())) {
        writer.put(propertyDescriptor.getName(), propertyDescriptor.getWriteMethod());
      }
    }

    List<T> finalList = new LinkedList<>();

    for (Map<String, Object> binding : result.results.bindings) {
      assert selectedConstructor != null;
      T instance = (T) selectedConstructor.newInstance();
      for (Map.Entry<String, Method> stringMethodEntry : writer.entrySet()) {
        if (stringMethodEntry.getValue() == null) {
          logger.warn("Unable to set value of " + stringMethodEntry.getKey() + " in class : [" + valueType.getName() + "] value is null");
          continue;
        }
        stringMethodEntry.getValue().invoke(instance, (String) ((Map) binding.get(stringMethodEntry.getKey())).get("value"));
      }
      finalList.add(instance);

    }
    return finalList;

  }

  public static class SparqlHead {
    List<String> link;

    List<String> vars;

    public List<String> getLink() {
      return link;
    }

    public void setLink(List<String> link) {
      this.link = link;
    }

    public List<String> getVars() {
      return vars;
    }

    public void setVars(List<String> vars) {
      this.vars = vars;
    }
  }

  public static class SparqlResult {
    Boolean distinct;

    Boolean ordered;

    List<Map<String, Object>> bindings;

    public Boolean getDistinct() {
      return distinct;
    }

    public void setDistinct(Boolean distinct) {
      this.distinct = distinct;
    }

    public Boolean getOrdered() {
      return ordered;
    }

    public void setOrdered(Boolean ordered) {
      this.ordered = ordered;
    }

    public List<Map<String, Object>> getBindings() {
      return bindings;
    }

    public void setBindings(List<Map<String, Object>> bindings) {
      this.bindings = bindings;
    }
  }

  public static class SparqlQueryResult {
    SparqlHead head;

    SparqlResult results;

    public SparqlHead getHead() {
      return head;
    }

    public void setHead(SparqlHead head) {
      this.head = head;
    }

    public SparqlResult getResults() {
      return results;
    }

    public void setResults(SparqlResult results) {
      this.results = results;
    }
  }
}
