package com.inf.data.dao.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.inf.cache.CacheInterface;
import com.inf.data.Category;
import com.inf.data.dao.InfoDataDao;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by parth on 25/4/16.
 */
public abstract class AbstractWebApiInfoDataDao implements InfoDataDao {
    protected final RestTemplate restTemplate;
    protected final CacheInterface<String, String> cacheInterface;
    protected InfoDataDao databaseDao;
    protected ObjectMapper objectMapper;
    protected Logger logger = LoggerFactory.getLogger(AbstractWebApiInfoDataDao.class);
    private final String NAME;

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    public AbstractWebApiInfoDataDao(String name, ObjectMapper objectMapper, CacheInterface<String, String> cacheInterface, RestTemplate restTemplate, InfoDataDao databaseDao) {
        NAME = name;
        this.objectMapper = objectMapper;
        this.cacheInterface = cacheInterface;
        this.restTemplate = restTemplate;

        this.databaseDao = databaseDao;
    }

    String makeUrlQuery(Map<String, Object> data) {
        String v = "";
        for (Map.Entry<String, Object> stringStringEntry : data.entrySet()) {
            v = v + stringStringEntry.getKey() + "=" + String.valueOf(stringStringEntry.getValue()) + "&";
        }
        return v;

    }

    public String query(Map keys, String url, boolean skipCache) {

        final String key;
        String query;
        try {
            query = objectMapper.writeValueAsString(keys);
        } catch (JsonProcessingException e) {
            logger.error("Failed to write as string ", keys);
            return "no-result";
        }
        key = NAME + "::" + query;

        if (!skipCache) {

            final String value = cacheInterface.get(key);
            if (value != null && value.length() > 0) {
                logger.info("[CacheOk] Fetching query [" + query + "]");
                return value;
            }
        }
        for (Object o : keys.keySet()) {
            try {
                url = url.replaceAll("\\{" + String.valueOf(o) + "\\}", URLEncoder.encode( String.valueOf(keys.get(o)), "UTF-8" ));
            } catch (UnsupportedEncodingException e) {
                logger.error("Should not have happened: ", e);
            }
        }

        logger.info("[CacheMiss] Value for query [" + query + "] not present in cache: skip cache: " + String.valueOf(skipCache));
        logger.info("Final Url: " + url);
        URLConnection connection = null;
        try {

            connection = new URL(url).openConnection();
            connection.setConnectTimeout(5000);
            connection.setReadTimeout(300000);
            connection.connect();
            InputStream body = (InputStream) connection.getContent();
            final String result = IOUtils.toString(body, Charset.defaultCharset());
            cacheInterface.set(key, result);
            cacheInterface.set(key + "_settime", String.valueOf(new Date().getTime()));
            return result;
        } catch (Exception ex) {
            if (ex instanceof HttpClientErrorException) {

                logger.error("Response body: " + ((HttpClientErrorException) ex).getResponseBodyAsString() );
            }
            logger.error("Failed to query " +  NAME + ": ", ex);
        }
        return null;
    }

    protected Category c(Integer cat) {
        return databaseDao.getCategoryById(cat);
    }

}
