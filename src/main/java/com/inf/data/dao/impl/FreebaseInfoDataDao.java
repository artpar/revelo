package com.inf.data.dao.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.inf.cache.CacheInterface;
import com.inf.data.Category;
import com.inf.data.Product;
import com.inf.data.ProductFeature;
import com.inf.data.dao.InfoDataDao;
import com.inf.mvc.CategoryUpdateRequest;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.*;

/**
 * Created by parth on 22/4/16.
 */
public class FreebaseInfoDataDao extends AbstractWebApiInfoDataDao {


    public static final String ApiKey = "AIzaSyB1QSxjjl5tjgit-TFOQpQf6MAGgQ4s7ms";

    public FreebaseInfoDataDao(RestTemplate restTemplate, CacheInterface<String, String> jedisPool, InfoDataDao databaseDao, ObjectMapper objectMapper) {
        super("Freebase", objectMapper, jedisPool, restTemplate, databaseDao);

    }


    @Override
    public void updateCategoryByReferenceId(CategoryUpdateRequest categoryUpdateRequest) {
        databaseDao.updateCategoryByReferenceId(categoryUpdateRequest);
    }

    @Override
    public List<Product> getProductsByQuestionnaireId(Integer questionnaireId) {
        return databaseDao.getProductsByQuestionnaireId(questionnaireId);
    }


    @Override
    public List<Product> getProductsByCategoryId(Integer categoryId) throws IOException {
        Category x = c(categoryId);
        String query = "[{\"id\":null,\"name\":null,\"type\":\"/"+x.getName()+"\"}]";

        Map<String, String> keys = new HashMap<>();
        keys.put("query", query);
        keys.put("key", ApiKey);
        String res = query(keys, "https://www.googleapis.com/freebase/v1/mqlread?query={query}&key={key}", false);
        TypeResultResponse list = objectMapper.readValue(res, TypeResultResponse.class);

        List<Product> r = new LinkedList<>();
        for (TypeResult typeResult : list.getResult()) {
            Product p = new Product();
            p.setCompanyName(typeResult.getType());
            p.setName(typeResult.getName());
            p.setId(typeResult.getId());
            r.add(p);
        }


        return r;
    }

    static public class TypeResultResponse {
        List<TypeResult> result;

        public List<TypeResult> getResult() {
            return result;
        }

        public void setResult(List<TypeResult> result) {
            this.result = result;
        }
    }

    static public class TypeResult {
        String name;
        String id;
        String type;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

    @Override
    public List<Product> getProductsByCategoryReferenceId(String categoryId) {
        return null;
    }

    @Override
    public Category getCategoryById(Integer id) {
        return databaseDao.getCategoryById(id);
    }

    @Override
    public List<ProductFeature> getProductFeatures(Product product) {

        Category x = c(product.getCategoryId());
        String query = "[{\"id\":null,\"name\":null,\"type\":\"/type/property\",\"schema\":{\"id\":\"/"+x.getName()+"\"}}]";
        Map<String, String> keys = new HashMap<>();
        keys.put("query", query);
        keys.put("key", ApiKey);
        String res = query(keys, "https://www.googleapis.com/freebase/v1/mqlread?query={query}&key={key}", false);
        logger.info("Result product feature: " + res);
        return null;
    }

  @Override
  public ProductFeature getProductSingleFeatureValue(Product product, String featureName) {
    return null;
    }

  @Override
    public List<ProductFeature> getFeatureWeights(Integer categoryId) {
        return new LinkedList<>();
    }

    static public class ResultResponse {
        List<ProductFeature> result;
    }

    @Override
    public List<String> getCategoryFeatures(Integer categoryId) {
        return null;
    }

    @Override
    public List<ProductFeature> getProductFeaturesNumericValue(Product product) {
        return null;
    }

    @Override
    public List<String> getFeatureList(Integer categoryId) {
        return null;
    }

    @Override
    public void deleteResultFromCategory(Integer id, String groupName, String name) {

    }

    @Override
    public List<FeatureValue> getCategoryFeatureValues(Integer id, String featureName) {
        return null;
    }

    @Override
    public List<FeatureValue> getCategoryFeatureDistinctValues(Integer id, String featureName) {
        return null;
    }
}
