package com.inf.data.dao.impl;

/**
 * Created by parth on 26/4/16.
 */
public class FeatureValue {
  private String name;
  private String companyName;
  private String key;
  private String value;
  private Double normalisedNumberValue;
  private Double numberValue;
  private Double featureMaxValue;
  private Double featureMinValue;

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCompanyName() {
    return companyName;
  }

  public void setCompanyName(String companyName) {
    this.companyName = companyName;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public Double getNormalisedNumberValue() {
    return normalisedNumberValue;
  }

  public void setNormalisedNumberValue(Double normalisedNumberValue) {
    this.normalisedNumberValue = normalisedNumberValue;
  }

  public Double getNumberValue() {
    return numberValue;
  }

  public void setNumberValue(Double numberValue) {
    this.numberValue = numberValue;
  }

  public Double getFeatureMaxValue() {
    return featureMaxValue;
  }

  public void setFeatureMaxValue(Double featureMaxValue) {
    this.featureMaxValue = featureMaxValue;
  }

  public Double getFeatureMinValue() {
    return featureMinValue;
  }

  public void setFeatureMinValue(Double featureMinValue) {
    this.featureMinValue = featureMinValue;
  }
}
