package com.inf.data.dao.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.inf.cache.CacheInterface;
import com.inf.cache.RedisCache;
import com.inf.data.dao.InfoDataDao;
import com.inf.exceptions.InvalidCategoryException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.client.RestTemplate;
import redis.clients.jedis.JedisPool;

/**
 * Created by parth on 22/4/16.
 */
public class InfoDataDaoFactory {

    private final DbpediaInfoDataDao dbpediaInfoDataDao;
    private AbstractWebApiInfoDataDao abstractWebApiInfoDataDao;
    private DatabaseInfoDataDao databaseInfoDataDao;

    public InfoDataDaoFactory(JedisPool jedisPool, String jedisPassword, JdbcTemplate jdbcTemplate, RestTemplate restTemplate) {
//        this.jedisPool = jedisPool;
//        this.jdbcTemplate = jdbcTemplate;
//        this.restTemplate = restTemplate;
        databaseInfoDataDao = new DatabaseInfoDataDao(jdbcTemplate);
        ObjectMapper objectMapper = new ObjectMapper();
        abstractWebApiInfoDataDao = new FreebaseInfoDataDao(restTemplate, new RedisCache(jedisPool, jedisPassword, "freebase"), databaseInfoDataDao, objectMapper);
        dbpediaInfoDataDao = new DbpediaInfoDataDao(objectMapper, new RedisCache(jedisPool, jedisPassword, "dbpedia"), restTemplate, databaseInfoDataDao);
    }

    public InfoDataDao getInfoDataDao(String type) throws InvalidCategoryException {
        int loc = type.indexOf("::");

        String part1;
        if (loc < 0) {
            part1 = type;
        } else {

            part1 = type.substring(0, loc);
        }

        switch (part1.trim().toLowerCase()) {
            case "database":
                return databaseInfoDataDao;
            case "freebase":
                return abstractWebApiInfoDataDao;
            case "dbpedia":
                return dbpediaInfoDataDao;
        }

        return null;
    }
}
