package com.inf.data.dao.impl;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.inf.data.Category;
import com.inf.data.CustomException;
import com.inf.data.Product;
import com.inf.data.ProductFeature;
import com.inf.data.QuestionType;
import com.inf.data.Questionnaire;
import com.inf.data.Scrape;
import com.inf.data.Substitution;
import com.inf.data.SymbolicFunction;
import com.inf.data.User;
import com.inf.data.dao.ProductDao;
import com.inf.data.page.PageInfo;
import com.inf.discovery.scraper.AbstractScrapeActivity;
import com.inf.discovery.scraper.ScrapeManager;
import com.inf.exceptions.InvalidCategoryException;
import com.inf.exceptions.InvalidQuestionException;
import com.inf.exceptions.InvalidQuestionnaireException;
import com.inf.model.security.Authority;
import com.inf.mvc.CategoryUpdateRequest;
import com.inf.mvc.QuestionnaireStats;
import com.inf.survey.data.Question;
import com.inf.survey.data.Transformation;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.web.client.RestTemplate;
import redis.clients.jedis.JedisPool;

/**
 * author parth.mudgal on 19/03/14.
 */
public class DataDao {
  private final Map<String, Constructor<?>> constructorMap = new HashMap<>();

  private final DatabaseProductDao productDao = new DatabaseProductDao();

  final private ObjectMapper objectMapper;

  private final JedisPool jedisPool;

  private final RestTemplate restTemplate;

  final private JdbcTemplate jdbcTemplate;

  private final InfoDataDaoFactory infoDataDao;

  public UserDao userDao;

  private String functionEvaluateUrl;

  private Logger logger = LoggerFactory.getLogger(DataDao.class);

  public DataDao(JdbcTemplate jdbcTemplate, ObjectMapper objectMapper, JedisPool jedisPool, String jedisPassword, RestTemplate restTemplate, UserDao userDao) {
    this.jdbcTemplate = jdbcTemplate;
    this.objectMapper = objectMapper;
    this.jedisPool = jedisPool;
    this.restTemplate = restTemplate;
    this.userDao = userDao;
    infoDataDao = new InfoDataDaoFactory(jedisPool, jedisPassword, jdbcTemplate, restTemplate);
  }

  public void setFunctionEvaluateUrl(String functionEvaluateUrl) {
    this.functionEvaluateUrl = functionEvaluateUrl + "evaluate?query={query}";
  }

  public List<Integer> getWebsiteForScrape(Product product, ScrapeManager scrapeManager) {
    return jdbcTemplate.queryForList(
            "SELECT s.scrape_website_id FROM scrapemap s WHERE (s.company=? OR s.company='*')" + " AND (s.category=? OR s.category='*') AND (s.product=? OR s.product='*')",
            new Object[] { product.getCompanyName(), product.getCategoryId(), product.getName() }, Integer.class);
  }

  public void addNewSearchScrapeActivity(Integer productId, Integer scrapeSiteId) {
    Integer res = jdbcTemplate.queryForObject("SELECT count(*) FROM queue_searchscrape WHERE product_id=? AND scrapewebsite_id=? AND done=0",
            new Object[] { productId, scrapeSiteId }, Integer.class);
    if (res > 0) {
      return;
    }
    jdbcTemplate.update("INSERT INTO queue_searchscrape (product_id, scrapewebsite_id) VALUE (?, ?)", productId, scrapeSiteId);
  }

  public synchronized AbstractScrapeActivity getScrapeActivity(String scrapeType, String scrapeWebsiteId)
          throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
    scrapeType = scrapeType.toLowerCase();

    Scrape scrapeItem = jdbcTemplate.queryForObject(
            "select id, scrape_website_id, link, html from queue_" + scrapeType + "scrape where done = 0 and scrape_website_id=? order by creation_time limit 1",
            new BeanPropertyRowMapper<Scrape>(Scrape.class), scrapeWebsiteId);

    jdbcTemplate.update("update queue_" + scrapeType + "scrape set done = -1 where id = ?", scrapeItem.getId());

    Constructor<?> constructor = null;
    if (!constructorMap.containsKey(scrapeType)) {
      Class<?> clazz = Class.forName("com.inf.discovery.scraper." + StringUtils.capitalize(scrapeType) + "ScrapeActivity");
      constructor = clazz.getConstructor(Scrape.class, DataDao.class);
      constructorMap.put(scrapeType, constructor);
    } else {
      constructor = constructorMap.get(scrapeType);
    }

    return (AbstractScrapeActivity) constructor.newInstance(scrapeItem, this);
  }

  public List<PageInfo> getPageInfo(Integer scrapeWebsiteId, String scrapeType) {
    return jdbcTemplate
            .query("select psf.id, website, url_template, xpath as page_selector, result_type from scrapewebsite sw join pagescrapeformat_" + scrapeType + " psf on sw.id=psf.scrapewebsite_id  where sw.id=?",
                    new Object[] { scrapeWebsiteId }, new BeanPropertyRowMapper<PageInfo>(PageInfo.class));
  }

  public void addNewScrapeActivity(int scrapeSiteId, String link, String type) {
    Integer result = jdbcTemplate
            .queryForObject("select count(*) from queue_" + type + "scrape where scrape_website_id=? and link=?", new Object[] { scrapeSiteId, link },
                    Integer.class);
    if (result > 0) {
      logger.info("Already Exists - {}", link);
      return;
    }
    jdbcTemplate.update("insert into queue_" + type + "scrape (scrape_website_id, link) value (?, ?)", scrapeSiteId, link);
  }

  public void updateHtml(int scrapeId, String html, String type) {
    jdbcTemplate.update("update queue_" + type + "scrape set html=? where id=?", html, scrapeId);
  }

  public void markDone(int id, String scrapeType) {
    jdbcTemplate.update("update queue_" + scrapeType + "scrape set done=1 where id=?", id);
  }

  public Questionnaire getQuestionnaireById(Integer id) {
    try {
      return jdbcTemplate.queryForObject("SELECT id, name, user_id, created_at, reference_id, status, category_id FROM questionnaire WHERE id = ?",
              new BeanPropertyRowMapper<>(Questionnaire.class), id);
    } catch (EmptyResultDataAccessException e) {
      return null;
    }
  }

  public Questionnaire getQuestionnaireByReferenceId(String referenceId) {
    try {
      return jdbcTemplate.queryForObject(
              "SELECT q.id, q.name, q.user_id, q.created_at, q.category_id, q.reference_id, c.name AS category_name, q.status FROM questionnaire q JOIN category c ON c.id = q.category_id WHERE q.reference_id = ?",
              new BeanPropertyRowMapper<>(Questionnaire.class), referenceId);
    } catch (EmptyResultDataAccessException e) {
      return null;
    }
  }

  public Category getCategoryByName(String categoryName) {
    try {

      return jdbcTemplate.queryForObject("SELECT id, name, reference_id, status , datasource FROM category WHERE name=?",
              new BeanPropertyRowMapper<Category>(Category.class), categoryName);
    } catch (EmptyResultDataAccessException e) {
      return null;
    }
  }

  public Category getCategoryByReferenceId(String referenceId) {
    try {

      return jdbcTemplate.queryForObject("SELECT id, name, reference_id, status, datasource FROM category WHERE reference_id=?",
              new BeanPropertyRowMapper<Category>(Category.class), referenceId);
    } catch (EmptyResultDataAccessException e) {
      return null;
    }
  }

  public List<Question> getQuestionnaireQuestions(String referenceId) {
    return jdbcTemplate
            .query("SELECT q.id, q.question, q.type, q.data, cq.status AS status, q.reference_id, q.question_group FROM questionnaire qq JOIN questionnaire_question cq ON cq.questionnaire_id=qq.id JOIN question q ON q.id=cq.question_id WHERE qq.reference_id = ?",
                    new BeanPropertyRowMapper<Question>(Question.class), referenceId);
  }

  public List<Question> getQuestions(String filter) {
    return jdbcTemplate
            .query("SELECT q.id, q.question, q.type, q.data, q.reference_id, q.question_group FROM question q WHERE q.question LIKE ? LIMIT 10",
                    new BeanPropertyRowMapper<Question>(Question.class), "%" + filter + "%");
  }

  public Question addQuestion(String question, Integer userId, Integer userGroupId) {
    String referenceId = UUID.randomUUID().toString();
    jdbcTemplate
            .update("INSERT INTO question (id, question, type, data, reference_id, user_id, usergroup_id) VALUES (NULL, ?, 'single_string', NULL, ?, ?, ?)",
                    question, referenceId, userId, userGroupId);
    return getQuestionByReferenceId(referenceId);
  }

  private Question getQuestionByReferenceId(String referenceId) {
    return jdbcTemplate.queryForObject("SELECT id, question, type, data, question_group FROM question WHERE reference_id=?",
            new BeanPropertyRowMapper<Question>(Question.class), referenceId);
  }

  private Question getQuestionById(Integer id) {
    try {

      return jdbcTemplate.queryForObject("SELECT id, question, type, data, reference_id, question_group FROM question WHERE id=?",
              new BeanPropertyRowMapper<Question>(Question.class), id);
    } catch (EmptyResultDataAccessException e) {
      return null;
    }
  }

  public void addQuestionToQuestionnaire(String questionnaireReferenceId, Integer questionId, Integer userId)
          throws CustomException, InvalidCategoryException, InvalidQuestionException, InvalidQuestionnaireException {
    Questionnaire c = getQuestionnaireByReferenceId(questionnaireReferenceId);
    if (c == null) {
      throw new InvalidQuestionnaireException(questionnaireReferenceId);
    }
    Question q = getQuestionById(questionId);
    if (q == null) {
      throw new InvalidQuestionException(questionId);
    }

    Integer count = jdbcTemplate
            .queryForObject("SELECT count(*) AS count FROM questionnaire_question cq WHERE cq.questionnaire_id=? AND cq.question_id=?", Integer.class,
                    c.getId(), q.getId());
    if (count == 0) {
      jdbcTemplate
              .update("INSERT INTO questionnaire_question (questionnaire_id, `order`, question_id, state, reference_id, user_id, usergroup_id, status) VALUE (?, 10, ?, 'active', UUID(), ?, 1, 'active')",
                      c.getId(), q.getId(), userId);
    }
  }

  public void deleteQuestionFromQuestionnaire(String questionnaireReferenceId, Integer questionId)
          throws CustomException, InvalidCategoryException, InvalidQuestionException, InvalidQuestionnaireException {
    Questionnaire c = getQuestionnaireByReferenceId(questionnaireReferenceId);
    if (c == null) {
      throw new InvalidQuestionnaireException(questionnaireReferenceId);
    }
    Question q = getQuestionById(questionId);
    if (q == null) {
      throw new InvalidQuestionException(questionId);
    }

    Integer count = jdbcTemplate
            .queryForObject("SELECT count(*) AS count FROM questionnaire_question cq WHERE cq.questionnaire_id=? AND cq.question_id=?", Integer.class,
                    c.getId(), q.getId());
    if (count > 0) {
      jdbcTemplate.update("DELETE FROM questionnaire_question WHERE questionnaire_id=? AND question_id =?", c.getId(), q.getId());
    }
  }

  public List<QuestionType> getAllQuestionTypes() {
    return jdbcTemplate.query("SELECT `name` FROM question_type", new RowMapper<QuestionType>() {
      @Override
      public QuestionType mapRow(ResultSet resultSet, int i) throws SQLException {
        return QuestionType.valueOf(resultSet.getString(1));
      }
    });
  }

  public void updateQuestionType(Long questionId, String type) {
    jdbcTemplate.update("UPDATE question SET type=? WHERE id = ?", type, questionId);
  }

  public void updateQuestionGroup(Long questionId, String group) {
    jdbcTemplate.update("UPDATE question SET question_group=? WHERE id = ?", group, questionId);
  }

  public void updateQuestionData(Long questionId, String data) {
    jdbcTemplate.update("UPDATE question SET data=? WHERE id = ?", data, questionId);
  }

  public void updateAnswerFunctionMappingData(Long questionId, Integer questionnaireId, String answer, String answerString, String reasonString,
          String data, Integer userId, Integer userGroupId) {
    Integer count = jdbcTemplate
            .queryForObject("SELECT count(*) FROM answer WHERE questionnaire_id=? AND question_id=? AND answer = ?", Integer.class, questionnaireId,
                    questionId, answer);
    if (count > 0) {
      jdbcTemplate.update("UPDATE answer SET answer_string=?, reason_string = ?, data=? WHERE questionnaire_id=? AND question_id=? AND answer = ?",
              answerString, reasonString, data, questionnaireId, questionId, answer);
    } else {
      jdbcTemplate
              .update("INSERT INTO answer (id, questionnaire_id, question_id, answer, answer_string, reason_string, data, reference_id, user_id, usergroup_id) VALUES (NULL, ?, ?, ?, ?, ?, ?, UUID(), ?, ?)",
                      questionnaireId, questionId, answer, answerString, reasonString, data, userId, userGroupId);
    }
  }

  public SymbolicFunction getSymbolicFunctionFromJsonObject(JSONObject jsonObject) {
    SymbolicFunction f;
    String weight = "1";
    if (jsonObject.has("weight")) {
      weight = jsonObject.getString("weight");
    }
    f = new SymbolicFunction(jsonObject.getString("formula"), weight);

    f.setSymbols(new LinkedList<>(jsonObject.getJSONArray("symbols")));

    List<Substitution> substitutionList = new LinkedList<Substitution>();

    if (jsonObject.has("substitutions") && jsonObject.get("substitutions") != null) {

      try {
        JSONObject subs = jsonObject.getJSONObject("substitutions");
        if (subs.isArray()) {

          JSONArray substitutions = jsonObject.getJSONArray("substitutions");
          for (int i = 0; i < substitutions.size(); i++) {
            JSONObject obj = substitutions.getJSONObject(i);
            Substitution s = new Substitution();
            s.setVariable(obj.getString("variable"));
            s.setValue(obj.getString("value"));
            substitutionList.add(s);
          }
        }

      } catch (Exception e) {

      }

    }
    f.setSubstitutions(substitutionList);
    return f;
  }

  public Map<String, SymbolicFunction> getAnswerData(Integer questionnaireId, Integer questionId, String answer) {
    try {

      String data = jdbcTemplate
              .queryForObject("SELECT `data` FROM answer WHERE questionnaire_id=? AND question_id=? AND answer=?", String.class, questionnaireId,
                      questionId, answer);
      Map<String, SymbolicFunction> functionMap = new HashMap<>();
      Map<String, SymbolicFunction> res = addAnswerDataToMap(JSONObject.fromObject(data));
      functionMap.putAll(res);
      return functionMap;
    } catch (EmptyResultDataAccessException e) {
      return new HashMap<>();
    }

  }

  public Map<String, SymbolicFunction> addAnswerDataToMap(JSONObject data) {
    Map<String, SymbolicFunction> functionMultiMap = new HashMap<>();
    for (Iterator keys = data.keys(); keys.hasNext(); ) {
      String key = (String) keys.next();
      SymbolicFunction function = getSymbolicFunctionFromJsonObject(data.getJSONObject(key));
      functionMultiMap.put(key, function);
    }
    return functionMultiMap;
  }

  public List<Transformation> getValueTransformers() {
    return jdbcTemplate
            .query("SELECT `id`, `regex`, `replace` FROM value_transforms ORDER BY length(regex)", new BeanPropertyRowMapper<>(Transformation.class));
  }

  public List<Questionnaire> getQuestionnaireList(String email) {
    return jdbcTemplate
            .query("SELECT c.id, c.name, c.user_id, c.reference_id, c.status, c.created_at, q.name AS category_name FROM questionnaire c JOIN user u ON c.user_id = u.id JOIN category q ON q.id = c.category_id WHERE u.email = ?",
                    new BeanPropertyRowMapper<>(Questionnaire.class), email);
  }

  public List<Questionnaire> getQuestionnaireListByReferenceId(String referenceId) {
    return jdbcTemplate
            .query("SELECT c.id, c.name, c.user_id, c.reference_id FROM questionnaire c JOIN category cc ON cc.id=c.category_id WHERE c.reference_id = ?",
                    new BeanPropertyRowMapper<>(Questionnaire.class), referenceId);
  }

  public Questionnaire newQuestionnaire(String name, Integer categoryId, Integer userId) {

    String referenceId = UUID.randomUUID().toString();
    int count = jdbcTemplate
            .update("INSERT INTO questionnaire (name, reference_id, user_id, category_id) VALUE (?,?,?,?)", name, referenceId, userId, categoryId);
    if (count < 1) {
      logger.error("Failed to insert row into questionnaire ");
    }
    return getQuestionnaireByReferenceId(referenceId);
  }

  public Category newCategory(String categoryName) {
    int ret = jdbcTemplate.update("INSERT INTO category (name, reference_id) VALUE (?, UUID())", categoryName);
    if (ret < 1) {
      logger.error("Failed to make category: " + categoryName);
    }
    return getCategoryByName(categoryName);
  }

  public List<FeatureValue> getOriginalFeatureValues(Integer categoryId, String featureName) throws InvalidCategoryException, IOException {

    Category c = getCategoryById(categoryId);
    return infoDataDao.getInfoDataDao(c.getDatasource()).getCategoryFeatureValues(c.getId(), featureName);
  }

  public List<FeatureValue> getOriginalFeatureDistinctValues(Integer categoryId, String featureName) throws InvalidCategoryException, IOException {

    Category c = getCategoryById(categoryId);
    return infoDataDao.getInfoDataDao(c.getDatasource()).getCategoryFeatureDistinctValues(c.getId(), featureName);
  }

  public List<FeatureValue> getFeatureValues(Integer categoryId, String featureName) throws InvalidCategoryException, IOException {
    Category c = getCategoryById(categoryId);
    final List<FeatureValue> categoryFeatureValues = infoDataDao.getInfoDataDao(c.getDatasource()).getCategoryFeatureValues(c.getId(), featureName);
    if (categoryFeatureValues.size() < 1) {
      return new LinkedList<>();
    }
    Double minScore = null; //categoryFeatureValues.get(0).getNumberValue();
    Double maxScore = null; //categoryFeatureValues.get(0).getNumberValue();
    Double total = 0.0;
    Integer count = 0;
    for (FeatureValue categoryFeatureValue : categoryFeatureValues) {
      final Double numberValue = categoryFeatureValue.getNumberValue();
      if (numberValue == null) { //|| numberValue == 0
        continue;
      }
      if (minScore == null) {
        minScore = numberValue;
        maxScore = numberValue;
      } else if (minScore > numberValue) {
        minScore = numberValue;
      } else if (maxScore < numberValue) {
        maxScore = numberValue;
      }

      total += numberValue;
      count += 1;
    }

    if (minScore == null || maxScore == null) {
      return categoryFeatureValues;
    }

    double mean = total / count;

    final List<FeatureValue> categoryFeatureValuesFinal = new LinkedList<>();
    Rescale rescale = new Rescale(minScore, maxScore, -10000, 10000);
    for (FeatureValue categoryFeatureValue : categoryFeatureValues) {
      Double numberValue = categoryFeatureValue.getNumberValue();
      if (numberValue == null) {
        numberValue = 0.0;
      }
      categoryFeatureValue.setNumberValue(numberValue);
      final double rescaledValue = rescale.rescale(numberValue);
      categoryFeatureValue.setNormalisedNumberValue(rescaledValue);
      categoryFeatureValue.setFeatureMinValue(minScore);
      categoryFeatureValue.setFeatureMaxValue(maxScore);
      categoryFeatureValuesFinal.add(categoryFeatureValue);
    }

    return categoryFeatureValuesFinal;
  }

  public void updateNumberValuesByCategory(Integer id, String oldValue, String number) {
    jdbcTemplate.update("UPDATE data SET number_value=? WHERE category_id=? AND value=?", Double.valueOf(number), id, oldValue);
  }

  public List<String> getUserRoles(String userId) {
    return userDao.getUserRoles(userId);
  }

  public Set<Authority> getUserAuthorities(String emailAddress) {
    return userDao.getUserAuthorities(emailAddress);
  }

  public User getUserByEmail(String email) {
    return userDao.getUserByEmail(email);
  }

  public List<Product> getProductsByQuestionnaireId(Integer questionnaireId) throws InvalidCategoryException, IOException {
    Integer categoryId = getQuestionnaireById(questionnaireId).getCategoryId();
    //    Category category = infoDataDao.getInfoDataDao("database").getCategoryById(categoryId);
    return getProductsByCategoryId(categoryId);
  }

  public List<Product> getProductsByCategoryReferenceId(String categoryId) throws InvalidCategoryException {
    Category category = getCategoryByReferenceId(categoryId);
    return infoDataDao.getInfoDataDao(category.getDatasource()).getProductsByCategoryReferenceId(categoryId);
  }

  public double evaluateSymbolicFunction(final SymbolicFunction function) throws IOException {
    String target = null;
    String object = null;
    String value = null;
    List<Substitution> subs = function.getSubstitutions();
    for (Substitution sub : subs) {
      if (sub.getVariable().equals("a")) {
         target = sub.getValue();
      } else if (sub.getVariable().equals("s")) {
        object = sub.getValue();
      } else if (sub.getVariable().equals("x")) {
        value = sub.getValue();
      }
    }

    if (function.getFormula().startsWith("equal")) {

      if (target == null || object == null || object.equals("null") || target.equals("null")) {
        return 0;
      }

      List<String> answers = Arrays.stream(target.split(",")).collect(Collectors.toList());
      if (target.equals(object) || answers.contains(object)) {
        return 10000;
      } else {
        return -10000;
      }

    }

    target = null;

    if (function.getFormula().startsWith("contains")) {
      String formula = function.getFormula();
      String formulaOptions = formula.substring("contains".length() + 1, formula.length() - 1);
      List<String> allowedValues = Arrays.stream(formulaOptions.split(",")).map(String::trim).map(String::toLowerCase).collect(Collectors.toList());

      if (allowedValues.contains(object.toLowerCase())) {
        return 10000;
      } else {
        return -10000;
      }
    }

    if (value == null || value.equals("null")) {
      throw new IOException("Target value is null");
    }

    String response = restTemplate.getForObject(functionEvaluateUrl, String.class, new HashMap<String, String>() {
      {
        put("query", objectMapper.writeValueAsString(function));
      }
    });
    return Math.round(Double.parseDouble(response));
  }

  public void updateProductInfo(Integer categoryId, String subCategoryName, String product, String key, String value, String numericValue,
          String source) {
    productDao.updateOrInsertProductInfo(categoryId, subCategoryName, product, key, value, numericValue, source);
  }

  public void setTransformedValue(Integer category, String company, String product, String key, Double value) {
    productDao.setTransformedValue(category, company, product, key, value);
  }

  public Map<String, String> getProductFeaturesWithNullFeatures(Product product) {
    return productDao.getProductFeaturesWithNullFeatures(product);
  }

  public List<Product> getProductsByCategoryId(Integer categoryId) throws InvalidCategoryException, IOException {
    return productDao.getProductsByCategoryId(categoryId);
  }

  public List<ProductFeature> getProductFeatures(Product product) throws InvalidCategoryException {
    return infoDataDao.getInfoDataDao(getCategoryById((product.getCategoryId())).getDatasource()).getProductFeatures(product);
    //        return productDao.getProductFeatures(product);
  }

  public ProductFeature getProductSingleFeatureValue(Product product, String featureName) throws InvalidCategoryException {
    Category categoryById = getCategoryById((product.getCategoryId()));
    return infoDataDao.getInfoDataDao(categoryById.getDatasource()).getProductSingleFeatureValue(product, featureName);
    //      return productDao.getProductFeatures(product);
  }

  public List<ProductFeature> getFeatureWeights(Integer categoryId) throws InvalidCategoryException {
    return infoDataDao.getInfoDataDao(getCategoryById((categoryId)).getDatasource()).getFeatureWeights(categoryId);
    //        return productDao.getProductFeatures(product);
  }

  public List<String> getCategoryFeatures(Integer categoryId) throws InvalidCategoryException, IOException {
    return productDao.getCategoryFeatures(categoryId);
  }

  public List<ProductFeature> getProductFeaturesNumericValue(Product product) throws InvalidCategoryException {
    return productDao.getProductFeaturesNumericValue(product);
  }

  public List<String> getFeatureList(Integer categoryId) throws InvalidCategoryException, IOException {

    return infoDataDao.getInfoDataDao(getCategoryById(categoryId).getDatasource()).getFeatureList(categoryId);
  }

  public void deleteResultFromCategory(Integer id, String groupName, String name) throws InvalidCategoryException {
    productDao.deleteResultFromCategory(id, groupName, name);
  }

  public Category getCategoryById(Integer categoryId) throws InvalidCategoryException {
    return infoDataDao.getInfoDataDao("database").getCategoryById(categoryId);
  }

  public QuestionnaireStats getQuestionnaireStats(String referenceId) {
    Questionnaire questionnaire = getQuestionnaireByReferenceId(referenceId);
    Integer id = questionnaire.getId();
    Integer questionCount = jdbcTemplate.queryForObject("SELECT count(*) FROM questionnaire_question WHERE questionnaire_id=?", Integer.class, id);
    Integer attempts = jdbcTemplate.queryForObject("SELECT count(*) FROM survey WHERE questionnaire_id=?", Integer.class, id);
    List<Map<String, Object>> counts = jdbcTemplate.queryForList(
            "SELECT q.question, answer, count(*) AS count FROM answer a JOIN questionnaire_question qq ON a.questionnaire_id = qq.questionnaire_id AND a.question_id = qq.question_id JOIN question q ON qq.question_id = q.id WHERE qq.questionnaire_id = ? GROUP BY qq.question_id, answer",
            id);
    QuestionnaireStats s = new QuestionnaireStats();
    s.setQuestionCount(questionCount);
    s.setAttemptCount(attempts);
    s.setAnswerSplit(counts);
    return s;
  }

  public void updateCategoryByReferenceId(CategoryUpdateRequest categoryUpdateRequest) throws InvalidCategoryException {
    infoDataDao.getInfoDataDao("database").updateCategoryByReferenceId(categoryUpdateRequest);
  }

  public class Rescale {
    private final double range0, range1, domain0, domain1;

    public Rescale(double domain0, double domain1, double range0, double range1) {
      this.range0 = range0;
      this.range1 = range1;
      this.domain0 = domain0;
      this.domain1 = domain1;
    }

    private double interpolate(double x) {
      return range0 * (1 - x) + range1 * x;
    }

    private double uninterpolate(double x) {
      double b = (domain1 - domain0) != 0 ? domain1 - domain0 : 1 / domain1;
      return (x - domain0) / b;
    }

    public double rescale(double x) {
      return interpolate(uninterpolate(x));
    }
  }

  private class DatabaseProductDao implements ProductDao {
    @Override
    public void updateOrInsertProductInfo(Integer categoryId, String subCategoryId, String product, String key, String value, String numericValue,
            String sourceLink) {
      int count = jdbcTemplate.queryForObject("SELECT count(*) FROM data WHERE category_id=? AND company=? AND product=? AND `key`=?",
              new Object[] { categoryId, subCategoryId, product, key }, Integer.class);
      if (count > 0) {
        jdbcTemplate.update("UPDATE data SET value=?, number_value=?, source=? WHERE category_id=? AND company=? AND product=? AND `key`=?", value,
                numericValue, sourceLink, categoryId, subCategoryId, product, key);
      } else {
        jdbcTemplate.update("INSERT INTO data (category_id, company, product, `key`, `value`, `source`, `number_value`) VALUE (?,?,?,?,?,?,?)",
                categoryId, subCategoryId, product, key, value, sourceLink, numericValue);
      }
    }

    @Override
    public void setTransformedValue(Integer category, String company, String product, String key, Double value) {
      jdbcTemplate
              .update("UPDATE data SET number_value=? WHERE category_id=? AND company=? AND product=? AND `key`=?", value, category, company, product,
                      key);
    }

    @Override
    public Map<String, String> getProductFeaturesWithNullFeatures(Product product) {
      List<Map<String, Object>> list = jdbcTemplate
              .queryForList("SELECT `key`, `value` FROM `data` d WHERE d.category=? AND d.company=? AND d.product=? AND number_value IS NULL",
                      product.getCategoryId(), product.getCompanyName(), product.getName());
      Map<String, String> response = new HashMap<String, String>();
      for (Map<String, Object> item : list) {
        response.put((String) item.get("key"), item.get("value").toString());
      }

      return response;
    }

    @Override
    public List<Product> getProductsByCategoryId(Integer categoryId) throws InvalidCategoryException, IOException {
      Category category = infoDataDao.getInfoDataDao("database").getCategoryById(categoryId);
      return infoDataDao.getInfoDataDao(category.getDatasource()).getProductsByCategoryId(categoryId);
    }

    @Override
    public List<ProductFeature> getProductFeatures(Product product) throws InvalidCategoryException {
      Category category = infoDataDao.getInfoDataDao("database").getCategoryById(product.getCategoryId());
      return infoDataDao.getInfoDataDao(category.getDatasource()).getProductFeatures(product);
    }

    @Override
    public List<String> getCategoryFeatures(Integer categoryId) throws InvalidCategoryException, IOException {
      Category category = infoDataDao.getInfoDataDao("database").getCategoryById(categoryId);
      return infoDataDao.getInfoDataDao(category.getDatasource()).getCategoryFeatures(categoryId);
    }

    @Override
    public List<ProductFeature> getProductFeaturesNumericValue(Product product) throws InvalidCategoryException {
      Category category = infoDataDao.getInfoDataDao("database").getCategoryById(product.getCategoryId());
      return infoDataDao.getInfoDataDao(category.getDatasource()).getProductFeaturesNumericValue(product);
    }

    @Override
    public List<String> getFeatureList(Integer categoryId) throws InvalidCategoryException, IOException {
      Category category = infoDataDao.getInfoDataDao("database").getCategoryById(categoryId);
      return infoDataDao.getInfoDataDao(category.getDatasource()).getFeatureList(categoryId);
    }

    @Override
    public void deleteResultFromCategory(Integer catgoryId, String groupName, String name) throws InvalidCategoryException {
      Category category = infoDataDao.getInfoDataDao("database").getCategoryById(catgoryId);
      infoDataDao.getInfoDataDao(category.getDatasource()).deleteResultFromCategory(catgoryId, groupName, name);
    }
  }
}


