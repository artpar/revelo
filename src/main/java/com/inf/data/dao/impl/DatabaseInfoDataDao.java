package com.inf.data.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import com.inf.data.Category;
import com.inf.data.Product;
import com.inf.data.ProductFeature;
import com.inf.data.dao.InfoDataDao;
import com.inf.mvc.CategoryUpdateRequest;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * Created by parth on 22/4/16.
 */
class DatabaseInfoDataDao implements InfoDataDao {
  private JdbcTemplate jdbcTemplate;

  private Map<Integer, Category> categoryMap = new HashMap<>();

  public DatabaseInfoDataDao(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

  @Override
  public void updateCategoryByReferenceId(CategoryUpdateRequest categoryUpdateRequest) {

    switch (categoryUpdateRequest.getKey()) {
      case "datasource":
        jdbcTemplate.update("UPDATE category SET datasource=? WHERE reference_id=?", categoryUpdateRequest.getValue(),
                categoryUpdateRequest.getReferenceId());
        break;
      case "status":
        jdbcTemplate.update("UPDATE category SET status=? WHERE reference_id=?", categoryUpdateRequest.getValue(),
                categoryUpdateRequest.getReferenceId());
        break;
    }
    categoryMap.clear();

  }

  @Override
  public List<Product> getProductsByQuestionnaireId(Integer questionnaireId) {
    return jdbcTemplate
            .query("SELECT 1 AS id, product AS name, company AS company_name, d.category_id FROM data d" + " JOIN questionnaire q ON q.category_id = d.category_id  WHERE q.id = ? GROUP BY product, company;",
                    new Object[] { questionnaireId }, new BeanPropertyRowMapper<Product>(Product.class));

  }

  @Override
  public List<Product> getProductsByCategoryId(Integer categoryId) {
    return jdbcTemplate
            .query("SELECT 1 AS id, product AS name, company AS company_name, category_id FROM data WHERE category_id=? and product not like '\\_\\_%' GROUP BY product, company;",
                    new Object[] { categoryId }, new BeanPropertyRowMapper<Product>(Product.class));

  }

  @Override
  public List<Product> getProductsByCategoryReferenceId(String categoryId) {
    return jdbcTemplate
            .query("SELECT 1 AS id, product AS name, company AS company_name, category_id AS category_name FROM data j JOIN category c ON c.id=j.category_id WHERE c.reference_id=? GROUP BY product, company;",
                    new Object[] { categoryId }, new BeanPropertyRowMapper<Product>(Product.class));

  }

  public Category getCategoryById(Integer id) {
    if (categoryMap.containsKey(id)) {
      return categoryMap.get(id);
    }
    try {

      Category category = jdbcTemplate.queryForObject("SELECT id, name, reference_id, status, datasource FROM category WHERE id=?",
              new BeanPropertyRowMapper<Category>(Category.class), id);
      categoryMap.put(id, category);
      return category;
    } catch (EmptyResultDataAccessException e) {
      return null;
    }
  }

  @Override
  public List<ProductFeature> getProductFeatures(Product product) {
    return jdbcTemplate.query("SELECT `key`, `value` FROM `data` d WHERE d.category_id=? AND d.company=? AND d.product=?",
            new Object[] { product.getCategoryId(), product.getCompanyName(), product.getName() },
            new BeanPropertyRowMapper<ProductFeature>(ProductFeature.class));
  }

  @Override
  public ProductFeature getProductSingleFeatureValue(Product product, String featureName) {
    List<ProductFeature> items = jdbcTemplate.query("SELECT `key`, `value` FROM `data` d WHERE d.company=? AND d.product=? and `key`=? limit 1",
            new Object[] { product.getCompanyName(), product.getName(), featureName },
            new BeanPropertyRowMapper<ProductFeature>(ProductFeature.class));
    if (items.size() > 0) {
      return items.get(0);
    }
    return null;
  }

  @Override
  public List<ProductFeature> getFeatureWeights(Integer categoryId) {
    Product product = new Product();
    product.setCompanyName("");
    product.setName("__weight");
    product.setCategoryId(categoryId);
    return getProductFeatures(product);
    //        return jdbcTemplate.query("SELECT `key`, `value` FROM `data` d WHERE d.category_id=? AND d.company=? AND d.product=?",
    //                new Object[] { product.getCategoryId(), product.getCompanyName(), product.getName() },
    //                new BeanPropertyRowMapper<ProductFeature>(ProductFeature.class));
  }

  @Override
  public List<String> getCategoryFeatures(Integer categoryId) {
    return jdbcTemplate.query("SELECT DISTINCT(`key`) FROM `data` d WHERE d.category_id=?", new Object[] { categoryId },
            new BeanPropertyRowMapper<>(ProductFeature.class)).stream().map(ProductFeature::getKey).collect(Collectors.toList());

  }

  @Override
  public List<ProductFeature> getProductFeaturesNumericValue(Product product) {
    return jdbcTemplate
            .query("SELECT `key`, `number_value` AS value FROM `data` d WHERE d.category_id=? AND d.company=? AND d.product=? AND number_value IS NOT NULL",
                    new Object[] { product.getCategoryId(), product.getCompanyName(), product.getName() },
                    new BeanPropertyRowMapper<ProductFeature>(ProductFeature.class));
  }

  @Override
  public List<String> getFeatureList(Integer categoryId) {
    return jdbcTemplate.query("SELECT DISTINCT(`key`) `key`, 'nan' AS value FROM `data` WHERE category_id=? ORDER BY `key`",
            new BeanPropertyRowMapper<ProductFeature>(ProductFeature.class), categoryId).stream().map(ProductFeature::getKey)
                       .collect(Collectors.toList());
  }

  @Override
  public void deleteResultFromCategory(Integer categoryId, String groupName, String name) {
    jdbcTemplate.update("DELETE FROM data WHERE category_id = ? AND company = ? AND product = ?", categoryId, groupName, name);
  }

  @Override
  public List<FeatureValue> getCategoryFeatureValues(Integer id, String featureName) {
    return jdbcTemplate
            .query("SELECT product AS name, company AS company_name, `value` as value, `key` as `key`, ifnull(number_value,0) as number_value FROM data d JOIN category c ON d.category_id = c.id WHERE c.id=? AND `key` = ?",
                    new BeanPropertyRowMapper<FeatureValue>(FeatureValue.class), id, featureName);
  }

  @Override
  public List<FeatureValue> getCategoryFeatureDistinctValues(Integer id, String featureName) {
    return jdbcTemplate
            .query("SELECT product AS name, company AS company_name, value, ifnull(number_value,0), `value` FROM data d JOIN category c ON d.category_id = c.id WHERE c.id=? AND `key` = ?",
                    new BeanPropertyRowMapper<FeatureValue>(FeatureValue.class), id, featureName);
  }

}
