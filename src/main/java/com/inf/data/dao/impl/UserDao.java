package com.inf.data.dao.impl;

import com.auth0.authentication.result.UserProfile;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.inf.data.User;
import com.inf.data.UserGroup;
import com.inf.model.security.Authority;
import com.inf.mvc.UserStats;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Created by parth on 22/4/16.
 */
public class UserDao implements com.inf.drop.UserDao {
  private JdbcTemplate jdbcTemplate;
  private ObjectMapper objectMapper;
  private Logger logger = LoggerFactory.getLogger(UserDao.class);

  public UserDao(JdbcTemplate jdbcTemplate, ObjectMapper objectMapper) {
    this.jdbcTemplate = jdbcTemplate;
    this.objectMapper = objectMapper;
  }


  public UserGroup createUserGroup(String name, Integer userId) {
    String uuid = UUID.randomUUID().toString();
    try {
      jdbcTemplate.update("INSERT INTO usergroup (name, reference_id, user_id) VALUES (?,?,?)", name, uuid, userId);
      UserGroup userGroup = getUserGroupByReferenceId(uuid);
      jdbcTemplate.update("UPDATE usergroup SET usergroup_id = ? WHERE id = ?", userGroup.getId(), userGroup.getId());
      return userGroup;
    } catch (DataAccessException e) {
      logger.error("Failed to insert user group " + name, e);
      return null;
    }
  }


  public void deleteUserGroup(String referenceId) {
    jdbcTemplate.update("UPDATE usergroup SET status = 'deleted' WHERE reference_id = ?", referenceId);
  }


  public void addUserToUserGroup(Integer userId, Integer userGroupId) {
    try {

      jdbcTemplate.update("INSERT INTO user_usergroup (user_id, usergroup_id) VALUES (?,?)", userId, userGroupId);
    } catch (Exception e) {
      logger.error("User[%s] insert to usergroup[%s] failed", userId, userGroupId, e);
    }
  }


  public void deleteUserFromUserGroup(Integer userId, Integer userGroupId) {
    jdbcTemplate.update("UPDATE user_usergroup SET status = 'deleted'  WHERE user_id = ? AND usergroup_id = ?", userId, userGroupId);
  }


  public List<User> getUsersByUserGroupId(Integer userGroupId, String status) {
    return jdbcTemplate.query("SELECT u.id, u.email, u.status, u.name, u.picture_url, u.reference_id FROM user u JOIN user_usergroup ug ON ug.user_id = u.id WHERE ug.usergroup_id = ? AND u.status != 'deleted' AND ug.status != 'deleted'",
        new BeanPropertyRowMapper<User>(User.class), userGroupId);
  }


  public List<UserGroup> getGroupsOfUser(Integer userId) {
    return jdbcTemplate.query("SELECT ug.id, ug.reference_id, ug.user_id, ug.name, ug.created_at, ug.status FROM usergroup ug JOIN user_usergroup uug ON ug.id=uug.usergroup_id WHERE uug.user_id = ? AND ug.status != 'deleted'",
        new BeanPropertyRowMapper<UserGroup>(UserGroup.class), userId);
  }

  public UserGroup getUserGroupByReferenceId(String referenceId) {
    try {

      return jdbcTemplate.queryForObject("SELECT id, user_id, reference_id, name, status, created_at FROM usergroup WHERE reference_id = ? AND status != 'deleted'",
          new BeanPropertyRowMapper<UserGroup>(UserGroup.class), referenceId);
    } catch (EmptyResultDataAccessException e) {
      return null;
    }
  }

  public List<String> getUserRoles(String userId) {
    return jdbcTemplate.queryForList("SELECT role FROM user u JOIN scope s ON s.scope=u.scope WHERE u.user_id=?",
        String.class, userId);
  }

  @Override
  public Set<Authority> getUserAuthorities(String emailAddress) {
    List<String> roles =
        jdbcTemplate.queryForList("SELECT role FROM user u JOIN scope s ON s.scope=u.scope WHERE u.user_id=?",
            String.class, emailAddress);


    roles.add("ROLE_PUBLIC");
    return roles.stream().map(Authority::valueOf).collect(Collectors.toSet());
  }

  @Override
  public User getUserByEmail(String email) {
    try {
      List<User> list = jdbcTemplate.query("SELECT id, email, created_at, status, reference_id, name, nickname, picture_url FROM user WHERE email = ? AND `status` != 'deleted'", new Object[]{email}, new BeanPropertyRowMapper<User>(User.class));
      if (list.size() == 0) {
        return null;
      }
      return list.get(0);
    } catch (DataAccessException e) {
      return null;
    }
  }

  @Override
  public UserStats getCounts(Integer id) {
    Integer surveyCount = jdbcTemplate.queryForObject("SELECT count(*) FROM  survey WHERE user_id = ?", Integer.class, id);
    UserStats n = new UserStats();
    n.setSurveyCount(surveyCount);
    return n;
  }

  @Override
  public User getUserByReferenceId(String referenceId) {
    return jdbcTemplate.queryForObject("SELECT id, name, email, nickname, picture_url, reference_id, status FROM user WHERE reference_id = ? AND status != 'deleted'", new BeanPropertyRowMapper<User>(User.class), referenceId);
  }

  @Override
  public List<String> getGroupIdsOfUser(Integer userId) {
    return jdbcTemplate.queryForList("SELECT ug.reference_id FROM user_usergroup uug JOIN usergroup ug ON ug.id = uug.usergroup_id WHERE uug.user_id = ? AND ug.status != 'deleted'", String.class, userId);
  }


  @Override
  public UserGroup getHomeGroupByUserId(Integer userId) {
    List<UserGroup> res = jdbcTemplate.query("SELECT ug.id, ug.user_id, ug.reference_id, ug.created_at, ug.status, ug.name, ug.usergroup_id FROM user_usergroup uug JOIN usergroup ug ON ug.id = uug.usergroup_id WHERE uug.user_id = ? AND ug.usergroup_id = ug.id AND ug.user_id = ?  AND ug.status != 'deleted'", new Object[]{userId, userId}, new BeanPropertyRowMapper<UserGroup>(UserGroup.class));

    if (res.size() < 1) {
      return null;
    }

    return res.get(0);
  }


  public User createUser(UserProfile profile) {
    String referenceId = null;

    synchronized (this) {
      try {
        Integer count = jdbcTemplate.queryForObject("SELECT count(*) FROM user WHERE email = ? AND status != 'deleted'", Integer.class, profile.getEmail());
        if (count > 0) {
          logger.info("User[{}] already exists", profile.getEmail());
          return getUserByEmail(profile.getEmail());
        }
        referenceId = UUID.randomUUID().toString();


        int res = jdbcTemplate.update("INSERT INTO user (email, name, nickname, picture_url, extra_info, identities, reference_id) VALUE (?,?,?,?,?,?,?)",
            profile.getEmail(), profile.getName(), profile.getNickname(),
            profile.getPictureURL(), objectMapper.writeValueAsString(profile.getExtraInfo()), objectMapper.writeValueAsString(profile.getIdentities()), referenceId);
      } catch (java.io.IOException e) {
        logger.error("Failed to write json: ", e);
//        int res = jdbcTemplate.update("INSERT INTO user (email, name, nickname, picture_url, extra_info, identities, reference_id) VALUE (?,?,?,?,?,?,?)",
//            profile.getEmail(), profile.getName(), profile.getNickname(),
//            profile.getPictureURL(), "", "", referenceId);

      }
    }
    User user = getUserByEmail(profile.getEmail());
    Integer userId = user.getId();
    UserGroup userGroup = createUserGroup("home group of user " + user.getName(), userId);
    Integer userGroupId = userGroup.getId();

    addUserToUserGroup(userId, userGroupId);


    int res = jdbcTemplate.update("UPDATE user SET user_id =?, usergroup_id=? WHERE id  = ?", userId, userGroupId, userId);
    if (res != 1) {
      logger.error("Expected to update 1 row, but updated {} rows", res);
    }


    return user;
  }
}
