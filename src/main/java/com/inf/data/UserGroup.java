package com.inf.data;

import java.util.Date;

/**
 * Created by parth on 29/4/16.
 */
public class UserGroup {
  private Integer id;
  private Integer userId;
  private Integer userGroupId;
  private String referenceId;
  private String status;
  private String name;
  private Date createdAt;


  public Integer getUserGroupId() {
    return userGroupId;
  }

  public void setUserGroupId(Integer userGroupId) {
    this.userGroupId = userGroupId;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Integer getUserId() {
    return userId;
  }

  public void setUserId(Integer userId) {
    this.userId = userId;
  }

  public String getReferenceId() {
    return referenceId;
  }

  public void setReferenceId(String referenceId) {
    this.referenceId = referenceId;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }
}
