package com.inf.mvc;

import java.util.List;

public class ResponseBody< T> {
    List<T> data;

    public ResponseBody(List<T> response) {
        this.data = response;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }
}
