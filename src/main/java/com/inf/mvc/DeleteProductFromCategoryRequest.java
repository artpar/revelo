package com.inf.mvc;

import javax.ws.rs.FormParam;

/**
 * Created by artpar on 5/8/17.
 */
public class DeleteProductFromCategoryRequest {
  private String name;
  private String referenceId;
  private String companyName;


  public String getCompanyName() {
    return companyName;
  }

  public void setCompanyName(String companyName) {
    this.companyName = companyName;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getGroupName() {
    return companyName;
  }

  public void setGroupName(String groupName) {
    this.companyName = groupName;
  }

  public String getReferenceId() {
    return referenceId;
  }

  public void setReferenceId(String referenceId) {
    this.referenceId = referenceId;
  }
}
