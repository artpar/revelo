package com.inf.mvc;

import com.inf.data.User;
import com.inf.data.dao.impl.UserDao;
import io.dropwizard.auth.Auth;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 * Created by parth on 29/4/16.
 */
@Path("user1")
@Produces("application/json")
public class UserController {

    UserDao userDao;

    public UserController(UserDao userDao) {
        this.userDao = userDao;
    }

    @GET
    public User getUser(@Auth com.inf.model.security.Auth auth) {
        return userDao.getUserByEmail(auth.getEmail());
    }


    @GET
    @Path("{referenceId}")
    public User getUserByReferenceId(@PathParam("referenceId") String referenceId){
        return userDao.getUserByReferenceId(referenceId);
    }


    @GET
    @Path("stats")
    public UserStats getUserStats(@Auth com.inf.model.security.Auth auth) {
        return userDao.getCounts(auth.getUserByEmail().getId());
    }


    @GET
    @Path("{referenceId}/stats")
    public UserStats getUserStats(@PathParam("referenceId") String referenceId) {
        return userDao.getCounts(userDao.getUserByReferenceId(referenceId).getId());
    }
}
