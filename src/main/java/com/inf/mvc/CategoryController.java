package com.inf.mvc;

// import com.fasterxml.jackson.databind.ObjectMapper;
import com.inf.data.*;
import com.inf.data.dao.impl.DataDao;
import com.inf.data.ProductFeature;
import com.inf.data.dao.impl.FeatureValue;
import com.inf.drop.InfConfiguration;
import com.inf.exceptions.InvalidCategoryException;
import com.inf.exceptions.InvalidQuestionException;
import com.inf.exceptions.InvalidQuestionnaireException;
import com.inf.services.XlsFileService;
import io.dropwizard.auth.Auth;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
// import org.slf4j.Logger;
// import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * author parth.mudgal on 27/05/14.
 */
@Path("category")
@Produces(MediaType.APPLICATION_JSON)
public class CategoryController {

  // private ObjectMapper objectMapper;

  private DataDao dataDao;
  // private Logger logger = LoggerFactory.getLogger(CategoryController.class);
  private XlsFileService xlsFileService;

  public void setDataDao(DataDao dataDao) {
    this.dataDao = dataDao;
  }

  public void setObjectMapper(com.fasterxml.jackson.databind.ObjectMapper objectMapper) {
    // this.objectMapper = objectMapper;
  }


  @Path("{referenceId}/stats")
  @GET
  public CategoryStats getCategoryStats(@PathParam("referenceId") String referenceId) {
    return new CategoryStats();
  }

  @Path("{referenceId}/originalFeatureValues")
  @GET
  public List<FeatureValue> getOriginalFeatureValueList(@PathParam("referenceId") String referenceId, @QueryParam("featureName") String featureName) throws InvalidCategoryException, IOException {
    List<FeatureValue> featureValues = dataDao.getOriginalFeatureDistinctValues(dataDao.getCategoryByReferenceId(referenceId).getId(), featureName);
//        if (featureValues.size() > 1000) {
//            featureValues = featureValues.subList(0, 1000);
//        }
//        for (FeatureValue featureValue : featureValues) {
//            if (!values.contains(featureValue.getValue())) {
//                values.add(featureValue.getValue());
//            }
//        }

    return featureValues;

//        return featureValues.stream().map(FeatureValue::getValue).limit(1000).collect(Collectors.toList());
  }

  @Path("{referenceId}/featureValues")
  @GET
  public List<FeatureValue> getFeatureValueList(@PathParam("referenceId") String referenceId, @QueryParam("featureName") String featureName) throws InvalidCategoryException, IOException {
    List<FeatureValue> featureValues = dataDao.getFeatureValues(dataDao.getCategoryByReferenceId(referenceId).getId(), featureName);
    if (featureValues.size() > 500) {
      featureValues = featureValues.subList(0, 500);
    }
    return featureValues;
  }

  @GET
  @Path("{referenceId}/questionnaire")
  public List<Questionnaire> getQestionnaireByCategoryReferenceId(@PathParam("referenceId") String referenceId) {
    return dataDao.getQuestionnaireListByReferenceId(referenceId);
  }

  @POST
  @Path("{referenceId}/xls")
  @Consumes(MediaType.MULTIPART_FORM_DATA)
  public HashMap<String, Object> uploadXls(@PathParam("referenceId") String referenceId, @FormDataParam("file") final InputStream fileInputStream, @FormDataParam("file") final FormDataContentDisposition contentDispositionHeader) throws IOException {
    XSSFWorkbook workbook = new XSSFWorkbook(fileInputStream);
    String fileName = contentDispositionHeader.getFileName();

    Category category = dataDao.getCategoryByReferenceId(referenceId);
    HashMap<String, Object> response = xlsFileService.updateCategoryFromXls(workbook, fileName, category);

    return response;
  }

  @GET
  @Path("{referenceId}/export")
  @Produces(MediaType.APPLICATION_OCTET_STREAM)
  public Response exportCategoryData(@PathParam("referenceId") String referenceId) throws IOException, InvalidCategoryException {

    Category category = dataDao.getCategoryByReferenceId(referenceId);

    List<String> categoryFeatures = dataDao.getCategoryFeatures(category.getId());

    List<Product> products = dataDao.getProductsByCategoryId(category.getId());


    XSSFWorkbook workbook = new XSSFWorkbook();


    int featureSize = categoryFeatures.size();

    int sheetLimit = 250;

    for (int offset = 0; offset < (featureSize / sheetLimit) + 1; offset++) {

      XSSFSheet sheet = workbook.createSheet(category.getName() + "_" + String.valueOf(offset + 1));
      XSSFRow firstRow = sheet.createRow(0);


      XSSFCell cell0 = firstRow.createCell(0, Cell.CELL_TYPE_STRING);
      cell0.setCellValue("Name");
      XSSFCell cell1 = firstRow.createCell(1, Cell.CELL_TYPE_STRING);
      cell1.setCellValue("Subcategory Name");

      Map<String, Integer> featureToColumnMapping = new HashMap<>();

      for (int i = 0; i < featureSize && i < sheetLimit; i++) {
        XSSFCell cell = firstRow.createCell(i + 2, Cell.CELL_TYPE_STRING);

        String featureName = categoryFeatures.get(i + (offset * sheetLimit));
        cell.setCellValue(featureName);
        featureToColumnMapping.put(featureName, i + 2);
      }


      int currentRow = 0;
      for (Product product : products) {
        List<ProductFeature> features = dataDao.getProductFeatures(product);

        currentRow += 1;
        XSSFRow row = sheet.createRow(currentRow);

        cell1 = row.createCell(0, Cell.CELL_TYPE_STRING);
        cell1.setCellValue(product.getName());

        cell1 = row.createCell(1, Cell.CELL_TYPE_STRING);
        cell1.setCellValue(product.getCompanyName());


        for (ProductFeature feature : features) {
          if (!featureToColumnMapping.containsKey(feature.getKey())) {
            continue;
          }
          Integer columnNumber = featureToColumnMapping.get(feature.getKey());


          XSSFCell cell = row.createCell(columnNumber, Cell.CELL_TYPE_STRING);
          cell.setCellValue(feature.getValue());
        }


      }

    }


//    HashMap<String, Object> response = xlsFileService.updateCategoryFromXls(workbook, fileName, category);

//    ServletOutputStream output = response.getOutputStream();


//    workbook.write(output);

    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
    workbook.write(byteArrayOutputStream);
    return Response.ok(byteArrayOutputStream.toByteArray())
        .header(HttpHeaders.CONTENT_DISPOSITION,
            String.format("attachment; filename=\"%s.xlsx\"", category.getName()))
        .header(HttpHeaders.CONTENT_TYPE, "application/octet-stream")
        .build();
  }


  @POST
  @Path("result/delete")
  public List<Product> deleteResult(DeleteProductFromCategoryRequest deleteProductFromCategoryRequest) throws InvalidCategoryException {
    Category category = dataDao.getCategoryByReferenceId(deleteProductFromCategoryRequest.getReferenceId());
    Integer categoryId = category.getId();
    dataDao.deleteResultFromCategory(categoryId, deleteProductFromCategoryRequest.getGroupName(), deleteProductFromCategoryRequest.getName());
    return dataDao.getProductsByCategoryReferenceId(deleteProductFromCategoryRequest.getReferenceId());
  }

  @POST
  @Path("product/edit")
  public List<ProductFeature> setProductFeature(
      @FormParam("name") String productName,
      @FormParam("companyName") String companyName,
      @FormParam("referenceId") String categoryReferenceId,
      @FormParam("key") String key,
      @FormParam("value") String value,
      @FormParam("regex") String regex, @Auth com.inf.model.security.Auth auth) throws InvalidCategoryException {
    Integer categoryId = dataDao.getCategoryByReferenceId(categoryReferenceId).getId();
    java.lang.String numericValue = String.valueOf(xlsFileService.transformValueToNumber(value));
    dataDao.updateProductInfo(categoryId, companyName, productName, key, value, numericValue, "usersubmit[" + auth.getEmail() + "]");
    Product product = new Product();
    product.setCategoryId(categoryId);
    product.setName(productName);
    product.setCompanyName(companyName);
    product.setNumericValue(numericValue);
    return dataDao.getProductFeatures(product);
  }

  @POST
  @Path("{referenceId}/value/edit")
  public String updateNumberValue(@PathParam("referenceId") String referenceId, @QueryParam("value") String oldValue, @QueryParam("newNumberValue") String number) {
    dataDao.updateNumberValuesByCategory(dataDao.getCategoryByReferenceId(referenceId).getId(), oldValue, number);
    return InfConfiguration.OK;
  }


  @GET
  @Path("product")
  public List<ProductFeature> getProductFeatures(@QueryParam("name") String productName, @QueryParam("companyName") String companyName, @QueryParam("referenceId") String categoryReferenceId) throws InvalidCategoryException {
    Product product = new Product();
    product.setCategoryId(dataDao.getCategoryByReferenceId(categoryReferenceId).getId());
    product.setName(productName);
    product.setCompanyName(companyName);
    return dataDao.getProductFeatures(product);
  }


  @POST
  @Path("result")
  public List<Product> addProduct(AddProductRequest categoryUpdateRequest, @FormParam("referenceId") String categoryReferenceId,
                                  @FormParam("name") String resultName,
                                  @Auth com.inf.model.security.Auth auth,
                                  @FormParam("companyName") String companyName) throws InvalidCategoryException, IOException {
    if (companyName == null || companyName.length() < 2) {
      throw new InvalidCategoryException("invalid company name " + companyName);
    }
    Category category = dataDao.getCategoryByReferenceId(categoryReferenceId);
    dataDao.updateProductInfo(category.getId(), companyName, resultName, "status", "active", "creation", "usersubmit[" + auth.getEmail() + "]");
    return dataDao.getProductsByCategoryId(category.getId());
  }


  @GET
  @Path("{referenceId}/result")
  public Response getProductsByCategoryRefenceId(@PathParam("referenceId") String referenceId, @QueryParam("matrix") String matrix) throws InvalidCategoryException, IOException {


    Category category = dataDao.getCategoryByReferenceId(referenceId);
    List<Product> data = dataDao.getProductsByCategoryId(category.getId());

    if (matrix != null && Objects.equals(matrix.toLowerCase(), "true")) {
      Map<String, Map<String, Object>> result = new HashMap<>();


      List<String> categoryFeatures = dataDao.getCategoryFeatures(category.getId());
      for (Product product : data) {

        List<ProductFeature> features = dataDao.getProductFeatures(product);
        String key = cleanKey(product.getCompanyName() + " " + product.getName());
        if (!result.containsKey(key)) {
          HashMap<String, Object> newMap = new HashMap<>();
          newMap.put("id", key);
          result.put(cleanKey(key), newMap);
        }


        for (ProductFeature feature : features) {
          try {
            float v = Float.parseFloat(feature.getValue());
            result.get(key).put(cleanKey(feature.getKey()), v);
          } catch (Exception e) {

            result.get(key).put(cleanKey(feature.getKey()), feature.getValue());
          }

        }

        for (String categoryFeature : categoryFeatures) {
          if (!result.get(key).containsKey(categoryFeature)) {
            result.get(key).put(cleanKey(categoryFeature), "");
          }
        }


      }

      return Response.ok(result.values())
          .header("Access-Control-Allow-Origin", "http://voyager.revelo.authme.host:30405")
          .build();

    }

    return Response.ok(data)
        .header("Access-Control-Allow-Origin", "http://voyager.revelo.authme.host:30405")
        .build();
  }

  private String cleanKey(String key) {
    return key.replaceAll(" ", "_");
  }

//  @GET
//  @Path("list")
//  public List<Category> getCategoryList() {
//    return dataDao.getCategoryList();
//  }

  @Path(value = "answerData")
  @GET
  public Map<String, SymbolicFunction> getAnswerData(@QueryParam("referenceId") String referenceId,
                                                     @QueryParam("questionId") Integer questionId, @QueryParam("answer") String answer) {
    Questionnaire questionnaire = dataDao.getQuestionnaireByReferenceId(referenceId);
    return dataDao.getAnswerData(questionnaire.getId(), questionId, answer);
  }

  @GET
  @Path(value = "{category}/features")
  public List<String> getFeatureList(@PathParam("category") String category) throws InvalidCategoryException, IOException {
    return dataDao.getFeatureList(dataDao.getCategoryByReferenceId(category).getId());
  }


  @POST
  @Path("update")
  public String updateCategory(CategoryUpdateRequest categoryUpdateRequest) throws InvalidCategoryException {
    dataDao.updateCategoryByReferenceId(categoryUpdateRequest);
    return InfConfiguration.OK;
  }


  @GET
  @Path("reference/{referenceId}")
  public Category getCategoryByReferenceId(@PathParam("referenceId") String referenceId) {
    return dataDao.getCategoryByReferenceId(referenceId);
  }

  @Path("question/delete")
  @DELETE
  public String deleteQuestionFromQuestionnaire(@FormParam("referenceId") String referenceId,
                                                @FormParam("questionId") Integer questionId) throws InvalidQuestionException, CustomException,
      InvalidCategoryException, InvalidQuestionnaireException {
    dataDao.deleteQuestionFromQuestionnaire(referenceId, questionId);
    return InfConfiguration.OK;
  }

  @Path("question/add")
  @POST
  public String addQuestionToQuestionnaire(@FormParam("referenceId") String referenceId,
                                           @FormParam("questionId") Integer questionId,
                                           @Auth com.inf.model.security.Auth auth) throws InvalidQuestionException, CustomException,
      InvalidCategoryException, InvalidQuestionnaireException {
    dataDao.addQuestionToQuestionnaire(referenceId, questionId, auth.getUserByEmail().getId());
    return InfConfiguration.OK;
  }

  public XlsFileService getXlsFileService() {
    return xlsFileService;
  }

  public void setXlsFileService(XlsFileService xlsFileService) {
    this.xlsFileService = xlsFileService;
  }
}
