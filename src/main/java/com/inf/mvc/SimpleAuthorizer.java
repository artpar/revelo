package com.inf.mvc;

import com.inf.model.security.Auth;

import java.util.Objects;

/**
 * Created by parth on 19/4/16.
 */
public class SimpleAuthorizer implements io.dropwizard.auth.Authorizer<Auth> {
  @Override
  public boolean authorize(Auth auth, String s) {
    if (Objects.equals(s, "ROLE_USER")) {
      return true;
    } else {
      if (auth == null) {
        return false;
      }
      if (auth.getUserProfile().getEmail().equals("artpar@gmail.com")) {
        return true;
      }
    }
    return false;
  }
}
