package com.inf.mvc;

import java.util.Map;

/**
 * Created by parth on 29/4/16.
 */
public class UserStats {
    Integer questionCount;
    Integer answerCreatedCount;
    Integer answersGivenCount;
    Integer surveyCount;
    Integer questionnaireCount;

    public Integer getQuestionCount() {
        return questionCount;
    }

    public void setQuestionCount(Integer questionCount) {
        this.questionCount = questionCount;
    }

    public Integer getAnswerCreatedCount() {
        return answerCreatedCount;
    }

    public void setAnswerCreatedCount(Integer answerCreatedCount) {
        this.answerCreatedCount = answerCreatedCount;
    }

    public Integer getAnswersGivenCount() {
        return answersGivenCount;
    }

    public void setAnswersGivenCount(Integer answersGivenCount) {
        this.answersGivenCount = answersGivenCount;
    }

    public Integer getSurveyCount() {
        return surveyCount;
    }

    public void setSurveyCount(Integer surveyCount) {
        this.surveyCount = surveyCount;
    }

    public Integer getQuestionnaireCount() {
        return questionnaireCount;
    }

    public void setQuestionnaireCount(Integer questionnaireCount) {
        this.questionnaireCount = questionnaireCount;
    }
}
