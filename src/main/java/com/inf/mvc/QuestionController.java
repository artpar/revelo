package com.inf.mvc;

import com.inf.data.QuestionType;
import com.inf.data.Questionnaire;
import com.inf.data.User;
import com.inf.data.UserGroup;
import com.inf.data.dao.impl.DataDao;
import com.inf.drop.InfConfiguration;
import com.inf.drop.UserDao;
import com.inf.survey.data.Question;
import io.dropwizard.auth.Auth;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.Set;

/**
 * author parth.mudgal on 11/08/14.
 */
@Path("question")
@Produces(MediaType.APPLICATION_JSON)
public class QuestionController {

  private DataDao dataDao;
  private UserDao userDao;
  private JedisPool jedisPool;
  private String jedisPassword;

  public void setJedisPool(JedisPool jedisPool) {
    this.jedisPool = jedisPool;
  }

  public void setJedisPassword(String jedisPassword) {this.jedisPassword = jedisPassword;}

  public void setUserDao(UserDao userDao) {
    this.userDao = userDao;
  }

  public void setDataDao(DataDao dataDao) {
    this.dataDao = dataDao;
  }


  @Path("search")
  @GET
  public List<Question> getAllQuestions(@QueryParam("question") String question) {
    return dataDao.getQuestions(question);
  }

  @Path(value = "type")
  @POST
  public String updateQuestionType(@FormParam("questionId") String questionId, @FormParam("type") String type) {
    dataDao.updateQuestionType(Long.valueOf(questionId), type);
    return InfConfiguration.OK;
  }

  @Path(value = "answerFunctions")
  @POST
  public String updateAnswerData(@FormParam("questionId") String questionId,
                                 @FormParam("referenceId") String referenceId,
                                 @FormParam("answer") String answer,
                                 @FormParam("data") String data,
                                 @Auth com.inf.model.security.Auth auth) {
    Questionnaire questionnaire = dataDao.getQuestionnaireByReferenceId(referenceId);
    data = data.trim();
    answer = answer.trim();
    User userByEmail = auth.getUserByEmail();
    UserGroup userHomeGroup = userDao.getHomeGroupByUserId(userByEmail.getId());
    //TODO: get answerString and reasonString from ui also.
    dataDao.updateAnswerFunctionMappingData(Long.valueOf(questionId), questionnaire.getId(), answer, "", "" , data, userByEmail.getId(), userHomeGroup.getId());
    return InfConfiguration.OK;
  }

  @Path(value = "data")
  @POST
  public String updateQuestionData(QuestionUpdateRequest questionUpdateRequest) {

    dataDao.updateQuestionData(questionUpdateRequest.getQuestionId(), questionUpdateRequest.getData());

    try (Jedis res = jedisPool.getResource()) {
      res.auth(jedisPassword);
      final String cacheKey = "question." + questionUpdateRequest.getQuestionId() + ".*";
      Set<String> keys = res.keys(cacheKey);
      for (String key : keys) {
        res.del(key);
      }
    }
    return InfConfiguration.OK;
  }

  @Path("questions")
  @GET
  public List<Question> getAllQuestions() {
    return dataDao.getQuestions("");
  }

  @Path("add")
  @POST
  public Question addQuestion(@FormParam("question") String question, @Auth com.inf.model.security.Auth auth) {

    User user = dataDao.getUserByEmail(auth.getEmail());
    UserGroup userGroup = userDao.getHomeGroupByUserId(user.getId());
    return dataDao.addQuestion(question, auth.getUserByEmail().getId(), userGroup.getId());
  }

  @Path("types")
  @GET
  public List<QuestionType> getAllQuestionTypes() {
    return dataDao.getAllQuestionTypes();
  }

  public static class QuestionUpdateRequest {
    String data;
    Long questionId;

    @Override
    public String toString() {
      return "QuestionUpdateRequest{" +
          "data='" + data + '\'' +
          ", questionId='" + questionId + '\'' +
          '}';
    }

    public String getData() {
      return data;
    }

    public void setData(String data) {
      this.data = data;
    }

    public Long getQuestionId() {
      return questionId;
    }

    public void setQuestionId(Long questionId) {
      this.questionId = questionId;
    }
  }

}
