package com.inf.mvc;

import static com.github.messenger4j.Messenger.CHALLENGE_REQUEST_PARAM_NAME;
import static com.github.messenger4j.Messenger.MODE_REQUEST_PARAM_NAME;
import static com.github.messenger4j.Messenger.SIGNATURE_HEADER_NAME;
import static com.github.messenger4j.Messenger.VERIFY_TOKEN_REQUEST_PARAM_NAME;
import java.util.LinkedList;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.messenger4j.exception.MessengerVerificationException;
import com.inf.messenger.ChatMessage;
import com.inf.messenger.MessengerPlatformCallbackHandler;
import com.inf.messenger.MessengerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path ("messenger")
public class MessengerController {
    private static final Logger logger = LoggerFactory.getLogger(MessengerPlatformCallbackHandler.class);

    private final MessengerService messengerService;

    private ObjectMapper objectMapper;

    /**
     * Constructs the {@code MessengerPlatformCallbackHandler} and initializes the {@code MessengerReceiveClient}.
     */
    public MessengerController(MessengerService messengerService, ObjectMapper objectMapper) {

        this.messengerService = messengerService;
        this.objectMapper = objectMapper;
    }

    /**
     * Webhook verification endpoint.
     * <p>
     * The passed verification token (as query parameter) must match the configured verification token.
     * In case this is true, the passed challenge string must be returned by this endpoint.
     */
    @GET
    public Response verifyWebhook(@QueryParam (MODE_REQUEST_PARAM_NAME) final String mode,
            @QueryParam (VERIFY_TOKEN_REQUEST_PARAM_NAME) final String verifyToken, @QueryParam (CHALLENGE_REQUEST_PARAM_NAME) final String challenge)
            throws MessengerVerificationException {

        logger.info("Received Webhook verification request - mode: {} | verifyToken: {} | challenge: {}", mode, verifyToken, challenge);
        // messengerService.getMessenger().verifyWebhook(mode, verifyToken);
        return Response.ok(challenge).build();
    }

    /**
     * Callback endpoint responsible for processing the inbound messages and events.
     */
    @POST
    @Path("/{questionnaireId}")
    public Response handleCallback(String body, @HeaderParam (SIGNATURE_HEADER_NAME) final String signature, @PathParam("questionnaireId") String questionnaireId)
            throws MessengerVerificationException, JsonProcessingException {

        logger.info("Received Messenger Platform callback - payload: {} | signature: {}", body, signature);

        logger.info("Processed callback payload successfully");
        //            return ResponseEntity.status(HttpStatus.OK).build();
        List<ChatMessage> responsePayloads = this.messengerService.handleEvent(body, signature, questionnaireId);

        for (ChatMessage responsePayload : responsePayloads) {
            if (responsePayload.getPersonId().startsWith("r-")) {
                continue;
            }
            this.messengerService.sendMessageTo(responsePayload);
        }

        List<TextResponse> response = new LinkedList<>();

        for (ChatMessage responsePayload : responsePayloads) {
            response.add(new TextResponse(responsePayload.getSentence().getSentence()));
        }

        ResponseBody body1 = new ResponseBody<TextResponse>(response);

        return Response.ok().entity(objectMapper.writeValueAsString(response)).build();
    }

}
