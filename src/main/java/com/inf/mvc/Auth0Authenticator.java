package com.inf.mvc;

import com.auth0.Auth0;
import com.auth0.Auth0Exception;
import com.auth0.authentication.AuthenticationAPIClient;
import com.auth0.authentication.result.Delegation;
import com.auth0.authentication.result.UserProfile;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.JWTVerifyException;
import com.auth0.request.Request;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Optional;
import com.inf.data.User;
import com.inf.data.dao.impl.DataDao;
import com.inf.model.security.Auth;
import io.dropwizard.auth.AuthenticationException;
import io.dropwizard.auth.Authenticator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.Base64;
import java.util.List;
import java.util.Map;


public class Auth0Authenticator implements Authenticator<String, Auth> {
  private final Auth0 auth0 = new Auth0("Ag9rX5XhV6YqyF11jnSyj2JqmAqmluxT", "cjHLSM2mNYHsJR0HIRFvCGvCa9itdTKXo7HBnZae7qCoL71OawaxQyl312Oe7ahu", "revelo.auth0.com");
  private final AuthenticationAPIClient client = auth0.newAuthenticationAPIClient();
  private JWTVerifier jwtVerifier = new JWTVerifier(
      Base64.getDecoder().decode("cjHLSM2mNYHsJR0HIRFvCGvCa9itdTKXo7HBnZae7qCoL71OawaxQyl312Oe7ahu"),
      "Ag9rX5XhV6YqyF11jnSyj2JqmAqmluxT");
  private Logger logger = LoggerFactory.getLogger(Auth0Authenticator.class);
  private DataDao dataDao;
  private JedisPool build;
  private String jedisPaasword;
  private ObjectMapper objectMapper = new ObjectMapper();

  public Auth0Authenticator(DataDao dataDao) {
    this.dataDao = dataDao;
  }

  public Auth0Authenticator(DataDao dataDao, JedisPool build, String password) {

    this.dataDao = dataDao;
    this.build = build;
    this.jedisPaasword = password;
  }

  public Jedis getJedis() {
    logger.debug("Getting jedis Resource:" + build.getNumActive() +","+ build.getNumIdle());
    Jedis jedis = build.getResource();
    jedis.auth(jedisPaasword);
    return jedis;
  }

  @Override
  public Optional<Auth> authenticate(String credentials) throws AuthenticationException {
    try {
      Map<String, Object> re = jwtVerifier.verify(credentials);
      UserProfile userProfile = getUserProfile(credentials);
      if (userProfile == null) {
        logger.error("Failed to get profile from credentials ");
        return Optional.absent();
      } else {
        User user = dataDao.getUserByEmail(userProfile.getEmail());
        if (user == null) {
          logger.info("user entry does not exist, creating for " + userProfile.getEmail());
          dataDao.userDao.createUser(userProfile);
          user = dataDao.getUserByEmail(userProfile.getEmail());
        }
        return Optional.of(new Auth(userProfile, user));

      }


    } catch (NoSuchAlgorithmException | InvalidKeyException | IOException | SignatureException | JWTVerifyException e) {
//            throw new AuthenticationException(e);
    }
    return Optional.absent();
  }

  private UserProfile getUserProfile(String credentials) {
    UserProfile userProfile;

    try (Jedis jedis = getJedis()) {
      if (jedis == null) {
        throw new Auth0Exception("Redis is not set");
      }

      String userProfileJson = jedis.get("auth_" + credentials);
      if (userProfileJson != null) {
        try {
          Map values = objectMapper.readValue(userProfileJson, Map.class);
          values.put("user_id", values.get("id"));
          List<Map<String, Object>> identities = (List<Map<String, Object>>) values.get("identities");
          for (Map<String, Object> identity : identities) {
            identity.put("isSocial", true);
          }

          userProfile = new UserProfile(values);
          logger.info("User profile served from redis: " + userProfile.getEmail());
          return userProfile;
        } catch (Exception e) {
          throw new Auth0Exception("Failed to read value as user profile [" + userProfileJson + "]", e);
        }
      }


      Delegation d = client.delegationWithIdToken(credentials).execute();
      Request<UserProfile> p = client.tokenInfo(d.getIdToken());
      userProfile = p.execute();
      try {
        logger.info("Added to cache for " + userProfile.getEmail());
        jedis.set("auth_" + credentials, objectMapper.writeValueAsString(userProfile));
      } catch (JsonProcessingException e) {
        logger.error("Failed to write value as json + " + d.getIdToken(), e);
      }
    }

    return userProfile;
  }
}