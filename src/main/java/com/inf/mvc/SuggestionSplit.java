package com.inf.mvc;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.inf.data.Suggestion;
import com.inf.survey.AnswerScore;
import com.inf.survey.Score;
import java.util.List;
import java.util.Map;

/**
 * Created by parth on 4/5/16.
 */
@JsonAutoDetect (fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class SuggestionSplit {
    private Suggestion suggestion;
    private Map<String, Score> featureScoreMap;
    private List<AnswerScore> positiveAnswerFeaturewiseScoreList;
    private List<AnswerScore> negativeAnswerFeaturewiseScoreList;

    private SuggestionSplit() {
    }

    public SuggestionSplit(Suggestion suggestion, Map<String, Score> scoreMap,
                           List<AnswerScore> positiveAnswerFeaturewiseScoreList, List<AnswerScore> negativeAnswerFeaturewiseScoreList) {
        this.suggestion = suggestion;
        this.featureScoreMap = scoreMap;
        this.positiveAnswerFeaturewiseScoreList = positiveAnswerFeaturewiseScoreList;
        this.negativeAnswerFeaturewiseScoreList = negativeAnswerFeaturewiseScoreList;
    }


    private void setSuggestion(Suggestion suggestion) {
        this.suggestion = suggestion;
    }

    private void setScoreMap(Map<String, Score> scoreMap) {
        this.featureScoreMap = scoreMap;
    }

    private Suggestion getSuggestion() {
        return suggestion;
    }

    private Map<String, Score> getFeatureScoreMap() {
        return featureScoreMap;
    }

    private List<AnswerScore> getPostiveAnswerFeaturewiseScoreList() {
        return positiveAnswerFeaturewiseScoreList;
    }

    private List<AnswerScore> getNegativeAnswerFeaturewiseScoreList() {
        return negativeAnswerFeaturewiseScoreList;
    }

}
