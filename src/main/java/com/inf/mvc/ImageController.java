package com.inf.mvc;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import com.inf.drop.InfConfiguration;
import com.inf.imageclient.getty.ImageNotFound;
import com.inf.interfaces.ImageManager;
import com.inf.interfaces.ViewImage;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by parth on 29/4/16.
 */
@Path ("image")
public class ImageController {
  private ImageManager client;

  private InfConfiguration configuration;

  private Logger logger = LoggerFactory.getLogger(ImageController.class);

  public ImageController(InfConfiguration configuration, ImageManager client) {
    this.configuration = configuration;
    this.client = client;
  }

  @GET
  @Path ("/{referenceId}")
  @Produces ("image/jpg")
  public Response getImage(@PathParam ("referenceId") String referenceId) throws ImageNotFound {

    byte[] bytes;
    try {
      bytes = client.getImageByReferenceId(referenceId);
    } catch (ImageNotFound e) {

      return Response.status(404).build();
    }

    return Response.ok(new ByteArrayInputStream(bytes)).build();

  }

  @POST
  @Path ("upload")
  @Consumes (MediaType.MULTIPART_FORM_DATA)
  @Produces ("application/json")
  public Response upload(@FormDataParam ("file") final InputStream fileInputStream,
          @FormDataParam ("file") final FormDataContentDisposition contentDispositionHeader) throws IOException {
    String imageIdentifier = client.uploadImage(fileInputStream);
    Map<String, String> response = new HashMap<>();
    response.put("fid", imageIdentifier);
    response.put("status", "ok");
    return Response.ok().entity(response).build();
  }

  @GET
  @Path ("search")
  @Produces ("application/json")
  public List<ViewImage> search(@QueryParam ("query") String token) throws ImageNotFound, Exception {
    if (configuration.getAppConfig().getShowImages().booleanValue()) {
      return client.getImagesForQuery(token);
    } else {
      return new LinkedList<>();
    }

  }

  @GET
  @Path ("lookup")
  @Produces ("application/json")
  public List<ViewImage> lookup(@QueryParam ("category") Integer category, @QueryParam ("company") String company,
          @QueryParam ("product") String product, @QueryParam ("query") String query) throws ImageNotFound, Exception {
    if (configuration.getAppConfig().getShowImages().booleanValue()) {
      if (category != null && company != null && product != null && company.length() > 0 && product.length() > 0) {
        try {
          return client.getImagesForQuery(category, company, product);
        } catch (Exception e) {
          e.printStackTrace();
        }
      } else {
        return client.getImagesForQuery(query);
      }
    } else {
      return new LinkedList<>();
    }
    return new LinkedList<>();

  }
}
