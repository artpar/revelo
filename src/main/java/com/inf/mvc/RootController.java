package com.inf.mvc;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.inf.drop.InfConfiguration;
import com.inf.exceptions.InvalidCategoryException;
import com.inf.exceptions.InvalidQuestionnaireException;
import com.inf.survey.SurveyManager;
import com.inf.survey.data.Survey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by parth on 22/4/16.
 */
@Path ("/")
public class RootController {
  protected Logger logger = LoggerFactory.getLogger(RootController.class);

  SurveyManager surveyManager;

  ObjectMapper objectMapper;

  public void setObjectMapper(ObjectMapper objectMapper) {
    this.objectMapper = objectMapper;
  }

  public void setSurveyManager(SurveyManager surveyManager) {
    this.surveyManager = surveyManager;
  }

  @GET
  public String index(@Context HttpServletResponse response) throws IOException {
    response.sendRedirect("/home/");
    return InfConfiguration.OK;
  }

  @GET
  @Path ("home")
  public HomeContents getHomeContents() {
    return null;
  }

  @Path (value = "start/{aName}/{referenceId}")
  @GET
  @ResponseBody
  public String startSurveyGet(@PathParam ("referenceId") String referenceId, @PathParam ("aName") String name, @Context HttpServletResponse response,
          @Context HttpServletRequest request) throws IOException, InvalidCategoryException, InvalidQuestionnaireException {
    logger.info("Start a survey for {}", "dummy: artpar@gmail.com");
    Survey newSurveyByIdentifier = surveyManager.getNewSurveyByIdentifier(referenceId, "artpar@gmail.com");
    response.sendRedirect("http://" + request.getHeader("Host") + "/root/app/#/survey/" + newSurveyByIdentifier.getId() + "/" + name);
    return "";
  }
}
