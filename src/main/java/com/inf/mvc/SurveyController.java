package com.inf.mvc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.inf.data.Product;
import com.inf.data.Suggestion;
import com.inf.data.SymbolicFunction;
import com.inf.data.dao.impl.FeatureValue;
import com.inf.drop.InfConfiguration;
import com.inf.exceptions.InvalidCategoryException;
import com.inf.exceptions.InvalidQuestionnaireException;
import com.inf.model.security.Auth;
import com.inf.survey.SurveyManager;
import com.inf.survey.data.Question;
import com.inf.survey.data.Survey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.*;

/**
 * author parth.mudgal on 27/04/14.
 */
@Path("survey")
@Produces(MediaType.APPLICATION_JSON)
public class SurveyController {

  protected Logger logger = LoggerFactory.getLogger(SurveyController.class);

  SurveyManager surveyManager;

  ObjectMapper objectMapper;
  
  public void setObjectMapper(ObjectMapper objectMapper) {
    this.objectMapper = objectMapper;
  }

  public void setSurveyManager(SurveyManager surveyManager) {
    this.surveyManager = surveyManager;
  }

  @Path(value = "start/{referenceId}")
  @POST
  public Survey startSurvey(@PathParam("referenceId") String referenceId, @io.dropwizard.auth.Auth Auth auth)
      throws IOException, InvalidCategoryException, InvalidQuestionnaireException {
    logger.info("Start a survey for {}", auth.getEmailAddress());
    return surveyManager.getNewSurveyByIdentifier(referenceId, auth.getEmail());
  }


  @Path("list")
  @GET
  @RolesAllowed({"ROLE_PUBLIC"})
  public List<Survey> getSurveyList(@io.dropwizard.auth.Auth Auth auth) {
    return surveyManager.getUserSurveys(auth);
  }

  @Path("details")
  @GET
  public List<Question> getSurveyDetails(@QueryParam("surveyId") Integer surveyId) {
    return surveyManager.getQuestionsBySurveyId(surveyId);
  }

  @Path("getSuggestions")
  @GET
  public Collection<Suggestion>
  getSuggestions(@QueryParam("surveyId") Integer surveyId)
      throws InvalidCategoryException, IOException {
    return surveyManager.getProductScores(surveyId);
  }


  @Path("getSplit")
  @GET
  public SuggestionSplit getSplit(@QueryParam("surveyId") Integer surveyId, @QueryParam("productName") String productName, @QueryParam("subcategory") String subcategoryName) throws InvalidCategoryException, IOException {
    return surveyManager.getProductSplit(surveyId, productName, subcategoryName);
  }

  @Path(value = "updateAnswer")
  @POST
  public String updateAnswer(@QueryParam("surveyId") Integer surveyId,
                             @QueryParam("questionId") Integer questionId, @Context HttpServletRequest request) throws JsonProcessingException {
    String answer = request.getParameter("answer");
    String[] allAnswer = request.getParameterValues("answer");
    if (allAnswer != null && allAnswer.length > 1) {
      answer = String.join(",", allAnswer);
    }
    surveyManager.updateAnswer(surveyId, questionId, answer);
    return InfConfiguration.OK;
  }
}
