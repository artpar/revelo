package com.inf.mvc;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.IOException;
import java.util.Map;

public class DateNotSpecifiedServletFilter implements javax.servlet.Filter {
    private FilterConfig filterConfig;
    // Other methods in interface omitted for brevity

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

        this.filterConfig = filterConfig;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (request instanceof HttpServletRequest) {
            HttpServletRequest newRequest = new HttpServletRequestWrapper((HttpServletRequest) request) {
                @Override
                public String getParameter(String name) {

                    String parameter = super.getParameter(name);
                    if (parameter == null) {
                        return null;
                    }
                    return parameter.toLowerCase();
                }

                @Override
                public Map<String, String[]> getParameterMap() {
                    Map<String, String[]> map = super.getParameterMap();
                    for (String key : map.keySet()) {
                        String[] arr = map.get(key);
                        arrayLowerCase(arr);
                        map.put(key, arr);
                    }
                    return map;
                }

                @Override
                public String[] getParameterValues(String name) {
                    String[] parameterValues = super.getParameterValues(name);
                    arrayLowerCase(parameterValues);
                    return parameterValues;
                }

            };

            chain.doFilter(request, response); // This signals that the request should pass this filter
        }
    }

    private void arrayLowerCase(String[] arr) {
        for (int i = 0, arrLength = arr.length; i < arrLength; i++) {
            if (arr[i] == null) {
                continue;
            }
            arr[i] = arr[i].toLowerCase();

        }
    }

    @Override
    public void destroy() {

    }
}