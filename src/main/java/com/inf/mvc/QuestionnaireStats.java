package com.inf.mvc;

import java.util.List;
import java.util.Map;

/**
 * Created by parth on 28/4/16.
 */
public class QuestionnaireStats {
    private Integer questionCount;
    private Integer attemptCount;
    private List<Map<String, Object>> answerSplit;

    public void setQuestionCount(Integer questionCount) {
        this.questionCount = questionCount;
    }

    public Integer getQuestionCount() {
        return questionCount;
    }


    public void setAttemptCount(Integer attemptCount) {
        this.attemptCount = attemptCount;
    }

    public Integer getAttemptCount() {
        return attemptCount;
    }

    public void setAnswerSplit(List<Map<String, Object>> answerSplit) {
        this.answerSplit = answerSplit;
    }

    public List<Map<String, Object>> getAnswerSplit() {
        return answerSplit;
    }
}
