package com.inf.mvc;

import com.inf.discovery.scraper.ScrapeManager;
import com.inf.drop.InfConfiguration;
import com.inf.exceptions.InvalidCategoryException;
import io.dropwizard.auth.Auth;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;

/**
 * author parth.mudgal on 19/03/14.
 */
@Path("scrape")
public class ScrapeController {

  private ScrapeManager scrapeManager;

  public void setScrapeManager(ScrapeManager scrapeManager) {
    this.scrapeManager = scrapeManager;
  }

  @Path("run/{type}")
  @RolesAllowed("ROLE_ADMIN")
  @GET
  public String runSearchQueue(@PathParam("type") String scrapeType, @QueryParam("scrapeWebsiteId") String scrapeWebsiteId, @Auth com.inf.model.security.Auth auth)
      throws ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException,
      InvocationTargetException {
    if (scrapeWebsiteId == null || scrapeWebsiteId.length() < 0) {
      scrapeWebsiteId = "%";
    }
    scrapeManager.run(scrapeType, scrapeWebsiteId);
    return InfConfiguration.OK;
  }

  @Path("transform/{id}")
  @RolesAllowed("ROLE_ADMIN")
  @GET
  public String runTransform(@PathParam("id") Integer categoryId, @Auth com.inf.model.security.Auth auth) throws InvalidCategoryException, IOException {
    scrapeManager.transformValues(categoryId);
    return InfConfiguration.OK;
  }


}
