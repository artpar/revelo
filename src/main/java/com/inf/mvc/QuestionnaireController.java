package com.inf.mvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.inf.data.Category;
import com.inf.data.CustomException;
import com.inf.data.Questionnaire;
import com.inf.data.User;
import com.inf.data.dao.impl.DataDao;
import com.inf.exceptions.InvalidCategoryException;
import com.inf.exceptions.InvalidQuestionException;
import com.inf.exceptions.InvalidQuestionnaireException;
import com.inf.services.XlsFileService;
import com.inf.survey.data.Question;
import io.dropwizard.auth.Auth;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by parth on 21/4/16.
 */
@Path("questionnaire")
public class QuestionnaireController {
  private DataDao dataDao;
  private ObjectMapper objectMapper;
  private XlsFileService xlsFileService;

  public void setXlsFileService(XlsFileService xlsFileService) {
    this.xlsFileService = xlsFileService;
  }

  public void setDataDao(DataDao dataDao) {
    this.dataDao = dataDao;
  }

  @GET
  @Path("list")
  public List<Questionnaire> getQuestionnaireList(@Auth com.inf.model.security.Auth auth) {
    return dataDao.getQuestionnaireList(auth.getEmail());
  }

  @GET
  @Path("{referenceId}/features")
  public List<String> getQuestionaireFeatures(@PathParam("referenceId") String referenceId) throws InvalidCategoryException, IOException {
    return dataDao.getFeatureList(dataDao.getQuestionnaireByReferenceId(referenceId).getCategoryId());
  }

  @DELETE
  @Path("{questionnaireReferenceId}/question/{questionId}")
  public Map<String, Object> deleteQuestionFromQuestionnaire(@PathParam("questionnaireReferenceId") String questionnaireReferenceId, @PathParam("questionId") Integer questionId) {
    Map<String, Object> response = new HashMap<>();
    try {
      dataDao.deleteQuestionFromQuestionnaire(questionnaireReferenceId, questionId);
      response.put("status", "ok");
    } catch (CustomException | InvalidCategoryException | InvalidQuestionnaireException | InvalidQuestionException e) {
      response.put("status", "failed");
    }

    return response;
  }

  @POST
  @Path("{referenceId}/xls")
  @Consumes(MediaType.MULTIPART_FORM_DATA)
  @Produces(MediaType.APPLICATION_JSON)
  public HashMap<String, Object> uploadXls(@PathParam("referenceId") String referenceId, @FormDataParam("file") final InputStream fileInputStream, @FormDataParam("file") final FormDataContentDisposition contentDispositionHeader, @Auth com.inf.model.security.Auth auth) throws IOException {
    XSSFWorkbook workbook = new XSSFWorkbook(fileInputStream);
    String fileName = contentDispositionHeader.getFileName();

    Questionnaire questionnaire = dataDao.getQuestionnaireByReferenceId(referenceId);
    User user = dataDao.getUserByEmail(auth.getEmail());
    HashMap<String, Object> response = xlsFileService.updateQuestionnaireFromXls(workbook, fileName, questionnaire, user.getId());

    return response;
  }


  @GET
  @Path("{referenceId}/stats")
  public QuestionnaireStats getStats(@PathParam("referenceId") String referenceId) {
    return dataDao.getQuestionnaireStats(referenceId);
  }


  @GET
  @Path("{referenceId}/questions")
  public List<Question> getQuestions(@PathParam("referenceId") String referenceId) {
    return dataDao.getQuestionnaireQuestions(referenceId);
  }

  @GET
  @Path("{referenceId}/category")
  public Category getCategory(@PathParam("referenceId") String referenceId) throws InvalidCategoryException {
    return dataDao.getCategoryById(dataDao.getQuestionnaireByReferenceId(referenceId).getCategoryId());
  }

  @GET
  @Path("{referenceId}")
  public Questionnaire getQuestionnaire(@PathParam("referenceId") String referenceId) {
    return dataDao.getQuestionnaireByReferenceId(referenceId);
  }

  public ObjectMapper getObjectMapper() {
    return objectMapper;
  }

  public void setObjectMapper(ObjectMapper objectMapper) {
    this.objectMapper = objectMapper;
  }
}
