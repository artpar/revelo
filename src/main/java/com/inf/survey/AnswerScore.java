package com.inf.survey;

import java.util.HashMap;
import java.util.Map;

public class AnswerScore {
  protected String targetString;

  protected Long answerScore;

  protected Map<String, Score> featureWiseScore = new HashMap<>();

  private AnswerScore() {
  }

  public AnswerScore(String targetString, Map<String, Score> featureToScoreMap) {
    this.targetString = targetString;
    featureWiseScore.putAll(featureToScoreMap);
    Double scoreForAnswer = featureWiseScore.values().parallelStream().map(Score::getWeightedScore).mapToDouble(Double::valueOf).sum();
    Double maxPossibleScoreForAnswer = featureWiseScore.values().parallelStream().map(Score::getMaxPossibleScore).mapToDouble(Double::valueOf).sum();
    answerScore = Math.round(scoreForAnswer * 100 / maxPossibleScoreForAnswer);
  }

  public String getTargetString() {
    return targetString;
  }

  private void setTargetString(String targetString) {
    this.targetString = targetString;
  }

  public Long getAnswerScore() {
    return answerScore;
  }

  private void setAnswerScore(Long answerScore) {
    this.answerScore = answerScore;
  }

  public Map<String, Score> getFeatureWiseScore() {
    return featureWiseScore;
  }

  private void setFeatureWiseScore(Map<String, Score> featureWiseScore) {
    this.featureWiseScore = featureWiseScore;
  }

}
