package com.inf.survey;

import java.util.List;
import java.util.regex.Matcher;

import com.inf.data.dao.impl.DataDao;
import com.inf.survey.data.Transformation;

/**
 * author parth.mudgal on 12/10/14.
 */
public class RegexValueTransformer implements ValueTransformer {
  List<Transformation> transformationList;

  @Override
  public void setTransformationList(List<Transformation> transformationList) {
    this.transformationList = transformationList;
  }


  @Override
  public Double transform(String value) {
    try {
      return Double.parseDouble(value);
    } catch (NumberFormatException nfe) {
      for (Transformation transformation : transformationList) {
        final Matcher matcher = transformation.matches(value.toLowerCase());
        if (matcher.matches()) {
          try {
            return Double.parseDouble(matcher.replaceAll(transformation.getReplace()).replaceAll(",", ""));
          } catch (NumberFormatException nfe2) {
            return null;
          }
        }
      }
      return null;
    }
  }

  @Override
  public String toString() {
    return "RegexValueTransformer{" + "transformationList=" + transformationList + '}';
  }
}
