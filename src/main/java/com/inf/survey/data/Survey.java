package com.inf.survey.data;

import java.util.Date;

/**
 * author parth.mudgal on 28/05/14.
 */
public class Survey {
    Integer id;
    Integer userId;
    Integer questionnaireId;
    String questionnaireName;
    String categoryName;
    SurveyStatus status;
    String referenceId;
    Date stampCreation;
    Date stampUpdated;

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public String getQuestionnaireName() {
        return questionnaireName;
    }

    public void setQuestionnaireName(String questionnaireName) {
        this.questionnaireName = questionnaireName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Integer getQuestionnaireId() {
        return questionnaireId;
    }

    public void setQuestionnaireId(Integer questionnaireId) {
        this.questionnaireId = questionnaireId;
    }

    public SurveyStatus getStatus() {
        return status;
    }

    public void setStatus(SurveyStatus status) {
        this.status = status;
    }

    public Date getStampCreation() {
        return stampCreation;
    }

    public void setStampCreation(Date stampCreation) {
        this.stampCreation = stampCreation;
    }

    public Date getStampUpdated() {
        return stampUpdated;
    }

    public void setStampUpdated(Date stampUpdated) {
        this.stampUpdated = stampUpdated;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "Survey{" +
                "id=" + id +
                ", userId=" + userId +
                ", questionnaireId=" + questionnaireId +
                ", questionnaireName='" + questionnaireName + '\'' +
                ", categoryName='" + categoryName + '\'' +
                ", status=" + status +
                ", referenceId='" + referenceId + '\'' +
                ", stampCreation=" + stampCreation +
                ", stampUpdated=" + stampUpdated +
                '}';
    }
}
