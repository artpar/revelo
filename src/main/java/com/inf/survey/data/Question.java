package com.inf.survey.data;

import com.inf.data.QuestionType;
import net.sf.json.JSONObject;

/**
 * author parth.mudgal on 28/05/14.
 */
public class Question {
  private Long id;
  private String question;
  private String answer;
  private QuestionType type;
  private String status;
  private String referenceId;
  private String questionGroup;
  private JSONObject internalObject;

  public String getQuestionGroup() {
    return questionGroup;
  }

  public void setQuestionGroup(String questionGroup) {
    this.questionGroup = questionGroup;
  }

  public String getReferenceId() {
    return referenceId;
  }

  public void setReferenceId(String referenceId) {
    this.referenceId = referenceId;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getType() {
    return type.toString();
  }

  public void setType(QuestionType type) {
    this.type = type;
  }

  public JSONObject getInternalObject() {
    return internalObject;
  }

  public void setData(String data) {
    internalObject = JSONObject.fromObject(data);
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getAnswer() {
    return answer;
  }

  public void setAnswer(String answer) {
    this.answer = answer;
  }

  public String getQuestion() {
    return question;
  }

  public void setQuestion(String question) {
    this.question = question;
  }

}
