package com.inf.survey.data;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * author parth.mudgal on 12/10/14.
 */
public class Transformation
{
	String id;
	String regex;
	String replace;
	Pattern pattern;

	@Override
	public String toString()
	{
		return "Transformation{" + "id='" + id + '\'' + ", regex='" + regex + '\'' + ", replace='" + replace + '\''
		        + '}';
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public void setRegex(String regex)
	{
		this.regex = regex;
		this.pattern = Pattern.compile(regex);
	}

	public void setReplace(String replace)
	{
		this.replace = replace;
	}

	public String getRegex()
	{
		return regex;
	}

	public String getReplace()
	{
		return replace;
	}

	public Pattern getPattern()
	{
		return pattern;
	}

	public Matcher matches(String value)
	{
		return pattern.matcher(value);
	}

}
