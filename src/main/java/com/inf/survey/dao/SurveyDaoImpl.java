package com.inf.survey.dao;

import com.inf.data.Answer;
import com.inf.data.Questionnaire;
import com.inf.data.dao.impl.DataDao;
import com.inf.exceptions.InvalidQuestionnaireException;
import com.inf.model.security.Auth;
import com.inf.survey.data.Question;
import com.inf.survey.data.Survey;
import com.inf.survey.data.SurveyStatus;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;
import java.util.UUID;

/**
 * author parth.mudgal on 27/04/14.
 */
public class SurveyDaoImpl implements SurveyDao {
  private static Logger logger = LoggerFactory.getLogger(SurveyDaoImpl.class);
  private final BeanPropertyRowMapper<Answer> answerRowMapper = new BeanPropertyRowMapper<>(Answer.class);
  private final BeanPropertyRowMapper<Question> questionRowMapper = new BeanPropertyRowMapper<>(Question.class);
  private final BeanPropertyRowMapper<Survey> surveyRowMapper = new BeanPropertyRowMapper<>(Survey.class);
  private JdbcTemplate jdbcTemplate;
  private DataDao dataDao;

  public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

  public void setDataDao(DataDao dataDao) {
    this.dataDao = dataDao;
  }

  @Override
  public Survey newSurvey(String referenceId, Integer userId) throws InvalidQuestionnaireException {

    Questionnaire questionnaire = dataDao.getQuestionnaireByReferenceId(referenceId);
    if (questionnaire == null) {
      throw new InvalidQuestionnaireException(referenceId);
    }
    Survey newSurvey = new Survey();
    newSurvey.setReferenceId(UUID.randomUUID().toString());
    newSurvey.setUserId(userId);
    newSurvey.setStatus(SurveyStatus.active);
    newSurvey.setQuestionnaireId(questionnaire.getId());
    jdbcTemplate.update(
        "INSERT INTO survey (user_id, questionnaire_id, `status`, `stamp_creation`, reference_id) VALUE (?, ?, ?, now(), ?)",
        newSurvey.getUserId(), questionnaire.getId(), SurveyStatus.active.toString(), newSurvey.getReferenceId());

    Integer id =
        jdbcTemplate.queryForObject(
            "SELECT id FROM survey WHERE user_id=? AND questionnaire_id=? ORDER BY stamp_creation DESC LIMIT 1",
            Integer.class, newSurvey.getUserId(), questionnaire.getId());
    newSurvey = getSurveyByReferenceId(newSurvey.getReferenceId());

    int count =
        jdbcTemplate.update("INSERT INTO survey_questions "
                + "(survey_id, question, question_id, question_type, data, answer, question_group) "
                + "(SELECT ?, question,id, type, data, NULL, question.question_group FROM question WHERE id IN "
                + "(SELECT question_id FROM questionnaire_question WHERE questionnaire_id=? AND status='active')" + ")",
            id, questionnaire.getId());
    logger.info("{} questions insert for new survey {} about {}", count, id, questionnaire.toString());
    newSurvey.setCategoryName(questionnaire.getCategoryName());
    return newSurvey;
  }

  @Override
  public List<Survey> getUserSurveys(Auth auth) {
    return jdbcTemplate
        .query("SELECT s.id, u.email, q.name AS name, cat.name AS category_name, s.status, s.stamp_creation, s.stamp_updated, q.id AS questionnaire_id, q.name AS questionnaire_name FROM survey s JOIN questionnaire q ON s.questionnaire_id=q.id JOIN category cat ON cat.id=q.category_id JOIN user u ON s.user_id = u.id WHERE u.email=?",
            new Object[]{auth.getEmail()}, surveyRowMapper);
  }

  @Override
  public List<Question> getQuestionsBySurveyId(Integer surveyId) {
    return jdbcTemplate
        .query("SELECT question_id AS id, question, question_type, answer, data, question_type AS type, question_group FROM survey_questions WHERE survey_id=?",
            questionRowMapper, surveyId);
  }

  @Override
  public List<Answer> getAnswerById(Integer id) {

    return jdbcTemplate
        .query("SELECT a.id, a.question_id, a.answer, a.data AS internal_object, a.reason_string, q.type AS question_type FROM answer a JOIN question q ON a.question_id = q.id WHERE a.id = ?",
            answerRowMapper, id);
  }

  @Override
  public JSONObject getQuestionData(Integer questionId) {
    return JSONObject.fromObject(jdbcTemplate.queryForObject("SELECT data FROM question WHERE id = ?", String.class, questionId));
  }

  @Override
  public List<Answer> getAnswers(Integer surveyId) {
    Survey survey = getSurveyById(surveyId);

    final List<Answer> answerList = jdbcTemplate
        .query("SELECT a.id, a.question_id, a.answer, a.answer_string, a.reason_string, a.data AS internal_object, q.type AS question_type " +
                "FROM answer a " +
                "JOIN survey_questions sq ON sq.question_id=a.question_id AND FIND_IN_SET(a.answer, sq.answer) " +
                "JOIN question q ON sq.question_id=q.id " +
                "WHERE sq.survey_id=? AND sq.answer IS NOT NULL AND a.questionnaire_id=?",
            answerRowMapper, surveyId, survey.getQuestionnaireId());
    final List<Answer> filterAnswers = jdbcTemplate.query("SELECT NULL AS id, sq.question_id, sq.answer, q.data AS internal_object, question_type " +
        "FROM\n" +
        "    survey_questions sq JOIN question q ON q.id = sq.question_id \n" +
        "    WHERE sq.survey_id = ? \n" +
        "    AND question_type LIKE '%filter' \n" +
        "ORDER BY id DESC", answerRowMapper, surveyId);
    answerList.addAll(filterAnswers);
    return answerList;
  }

  @Override
  public void updateAnswer(Integer surveyId, Integer questionId, String answer) {
    jdbcTemplate.update("UPDATE survey_questions SET answer=? WHERE survey_id=? AND question_id=?", answer,
        surveyId, questionId);
  }

  @Override
  public Survey getSurveyByReferenceId(String referenceId) {
    return jdbcTemplate
        .queryForObject(
            "SELECT s.id, u.email, c.name AS category, c.id AS questionnaire_id, s.status, s.stamp_creation, s.stamp_updated, s.reference_id " +
                "FROM survey s " +
                "JOIN questionnaire c ON s.questionnaire_id=c.id " +
                "JOIN user u ON u.id=s.user_id " +
                "WHERE s.reference_id=?",
            new Object[]{referenceId}, surveyRowMapper);
  }

  @Override
  public Survey getSurveyById(Integer surveyId) {
    return jdbcTemplate
        .queryForObject(
            "SELECT s.id, u.email, c.name AS category, q.id AS questionnaire_id, s.status, s.stamp_creation, s.stamp_updated " +
                "FROM survey s " +
                "JOIN questionnaire q ON s.questionnaire_id = q.id " +
                "JOIN category c ON q.category_id=c.id " +
                "JOIN user u ON u.id=s.user_id " +
                "WHERE s.id=?",
            new Object[]{surveyId}, surveyRowMapper);
  }
}
