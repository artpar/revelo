package com.inf.survey.dao;

import java.util.List;

import com.inf.data.Answer;
import com.inf.exceptions.InvalidCategoryException;
import com.inf.exceptions.InvalidQuestionnaireException;
import com.inf.model.security.Auth;
import com.inf.survey.data.Question;
import com.inf.survey.data.Survey;
import net.sf.json.JSONObject;

/**
 * author parth.mudgal on 27/04/14.
 */
public interface SurveyDao
{
	Survey newSurvey(String referenceId, Integer userId) throws InvalidCategoryException, InvalidQuestionnaireException;

	List<Survey> getUserSurveys(Auth auth);

	List<Question> getQuestionsBySurveyId(Integer surveyId);

	List<Answer> getAnswers(Integer surveyId);

	void updateAnswer(Integer surveyId, Integer questionId, String answer);

	Survey getSurveyByReferenceId(String surveyId);

	Survey getSurveyById(Integer surveyId);

	List<Answer> getAnswerById(Integer id);

	JSONObject getQuestionData(Integer questionId);
}
