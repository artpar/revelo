package com.inf.survey;

public class FeatureData{
	private String feature;
	private Double featureValue;
	private String unit;
	private String icon="";
	
	FeatureData(String feature, Double value, String unit){
		this.setFeature(feature);
		this.setFeatureValue(value);
		this.setUnit(unit);
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public Double getFeatureValue() {
		return featureValue;
	}

	public void setFeatureValue(Double featureValue) {
		this.featureValue = featureValue;
	}

	public String getFeature() {
		return feature;
	}

	public void setFeature(String feature) {
		this.feature = feature;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}
}