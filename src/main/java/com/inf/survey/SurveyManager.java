package com.inf.survey;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.inf.data.Suggestion;
import com.inf.data.SymbolicFunction;
import com.inf.exceptions.InvalidCategoryException;
import com.inf.exceptions.InvalidQuestionnaireException;
import com.inf.model.security.Auth;
import com.inf.mvc.SuggestionSplit;
import com.inf.survey.data.Question;
import com.inf.survey.data.Survey;

/**
 * author parth.mudgal on 27/04/14.
 */
public interface SurveyManager {

  Survey getNewSurveyByIdentifier(String referenceId, String email) throws InvalidCategoryException, InvalidQuestionnaireException;

  List<Survey> getUserSurveys(Auth auth);

  List<Question> getQuestionsBySurveyId(Integer surveyId);

  SuggestionSplit getProductSplit(Integer surveyId, String productName, String companyName) throws InvalidCategoryException, IOException;

  Collection<Suggestion> getProductScores(Integer surveyId) throws InvalidCategoryException, IOException;

  java.util.Map getProductFunctionsAsString(Integer surveyId);

  void updateAnswer(Integer surveyId, Integer questionId, String answer);

}
