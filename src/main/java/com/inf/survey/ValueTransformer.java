package com.inf.survey;

import com.inf.survey.data.Transformation;

import java.util.List;

/**
 * author parth.mudgal on 12/10/14.
 */
public interface ValueTransformer {
  Double transform(String value);

  void setTransformationList(List<Transformation> transformationList);
}
