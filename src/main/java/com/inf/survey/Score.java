package com.inf.survey;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by parth on 10/5/16.
 */
public class Score {
    Double score;
    Double weightedScore;
    Double maxPossibleScore;
    Long normalizedScore;
    String value;
    String reason;
    String weight;

    List<String> reasonList = new ArrayList<String>();


    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public Double getWeightedScore() {
        return weightedScore;
    }

    public void setWeightedScore(Double weightedScore) {
        this.weightedScore = weightedScore;
    }

    public Double getMaxPossibleScore() {
        return maxPossibleScore;
    }

    public void setMaxPossibleScore(Double maxPossibleScore) {
        this.maxPossibleScore = maxPossibleScore;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getReason() {
        return this.reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public void computeNormalizedScore() {
        this.normalizedScore = Math.round(weightedScore*100/maxPossibleScore);
    }

    public void appendReason(String reason) {

        if (this.reason == null) {
            this.reason = reason;
            return;
        }
        this.reason = this.reason.concat(", " + reason);
    }
}
