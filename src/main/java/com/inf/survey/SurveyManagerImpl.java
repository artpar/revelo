package com.inf.survey;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;

import org.apache.commons.lang.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.inf.data.Answer;
import com.inf.data.Category;
import com.inf.data.Product;
import com.inf.data.ProductFeature;
import com.inf.data.QuestionType;
import com.inf.data.Questionnaire;
import com.inf.data.Substitution;
import com.inf.data.Suggestion;
import com.inf.data.SymbolicFunction;
import com.inf.data.dao.impl.DataDao;
import com.inf.data.dao.impl.FeatureValue;
import com.inf.drop.InfConfiguration;
import com.inf.exceptions.InvalidCategoryException;
import com.inf.exceptions.InvalidQuestionnaireException;
import com.inf.model.security.Auth;
import com.inf.mvc.SuggestionSplit;
import com.inf.survey.dao.SurveyDao;
import com.inf.survey.data.Question;
import com.inf.survey.data.Survey;

import net.sf.json.JSONObject;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

/**
 * author parth.mudgal on 27/04/14.
 */
public class SurveyManagerImpl implements SurveyManager {
    private SurveyDao surveyDao;

    private DataDao dataDao;

	  private InfConfiguration configuration;

    private Logger logger = LoggerFactory.getLogger(SurveyManagerImpl.class.getName());

    private JedisPool jedisPool;

    private String jedisPassword;

    private ObjectMapper objectMapper = new ObjectMapper();

    public void setValueTransformer(ValueTransformer valueTransformer) {
    }

    public void setJedisPool(JedisPool jedisPool) {
        this.jedisPool = jedisPool;
    }

    public void setJedisPassword(String jedisPassword) {
        this.jedisPassword = jedisPassword;
    }

    public InfConfiguration getConfiguration() {
        return configuration;
    }

    public void setConfiguration(InfConfiguration configuration) {
        this.configuration = configuration;
    }

    public void setDataDao(DataDao dataDao) {
        this.dataDao = dataDao;
    }

    public void setSurveyDao(SurveyDao surveyDao) {
        this.surveyDao = surveyDao;
    }

    private static long ANSWER_SCORE_THRESHOLD = 50;

    @Override
    public Survey getNewSurveyByIdentifier(String referenceId, String email) throws InvalidCategoryException, InvalidQuestionnaireException {
        return surveyDao.newSurvey(referenceId, dataDao.getUserByEmail(email).getId());
    }

    @Override
    public List<Survey> getUserSurveys(Auth auth) {
        return surveyDao.getUserSurveys(auth);
    }

    @Override
    public List<Question> getQuestionsBySurveyId(Integer surveyId) {
        return surveyDao.getQuestionsBySurveyId(surveyId);
    }

    @Override
    public SuggestionSplit getProductSplit(Integer surveyId, String productName, String companyName) throws InvalidCategoryException, IOException {
        Survey survey = surveyDao.getSurveyById(surveyId);
        Questionnaire questionnaire = dataDao.getQuestionnaireById(survey.getQuestionnaireId());
        List<Answer> answers = surveyDao.getAnswers(surveyId);

        List<String> answerKey = answers.stream().map(a -> a.getAnswer()).collect(Collectors.toList());
        String hashKey = "split_" + productName + "." + String.join(",", answerKey);
        try (Jedis res = jedisPool.getResource()) {

            res.auth(jedisPassword);

            String cachedString = res.get(hashKey);
            if (cachedString != null && cachedString.length() > 5) {
                try {

                    SuggestionSplit suggestionSplit = objectMapper.readValue(cachedString, SuggestionSplit.class);
                    return suggestionSplit;

                }catch (Exception e) {
                  logger.error("Failed to read cached value: ", e);
                  res.del(hashKey);
                }
            }
        } catch (Exception e) {

            logger.error("Failed to read cached value: ", e);
        }

        final Product product = new Product();
        product.setName(productName);
        product.setCompanyName(companyName);
        if (answers.size() == 0) {
            return new SuggestionSplit(new Suggestion(product, (double) 0), new HashMap<>(), new LinkedList<>(), new LinkedList<>());
        }
        product.setCategoryId(questionnaire.getCategoryId());

        Map<String, Score> answerScoreMap = new HashMap<>();
        Map<String, Score> consolidatedFeatureScoreMap = new HashMap<>();
        List<AnswerScore> positiveAnswerFeaturewiseScoreList = new LinkedList<>();
        List<AnswerScore> negativeAnswerFeaturewiseScoreList = new LinkedList<>();
        //Map<String, AnswerScore> answerFeaturewiseScoreMap = new HashMap<>();

        Map<String, Map<String, FeatureValue>> featureToProductScoreMapping = new HashMap<>();
        List<String> keys = new LinkedList<>();

        List<ProductFeature> featureWeights = dataDao.getFeatureWeights(questionnaire.getCategoryId());
        Map<String, Double> featureWeightMap = new HashMap<>();
        for (ProductFeature featureWeight : featureWeights) {
            featureWeightMap.put(featureWeight.getKey(), Double.valueOf(featureWeight.getValue()));
        }

        for (Answer answer : answers) {
            MultivaluedMap<String, SymbolicFunction> functionMapOfTheAttemptedAnswer = getFunctionMapForSurveyAnswer(answer.getId(),
                    answer.getQuestionId(), answer.getQuestionType());

            Map<String, Score> featureToScoreMap = getFeatureToScoreMap(featureToProductScoreMapping, functionMapOfTheAttemptedAnswer, product,
                    answer.getAnswer(), featureWeightMap);
            //      scores.putAll(productScoreMap);

            for (Score score : featureToScoreMap.values()) {
                score.setReason(answer.getReasonString());
            }

            consolidatedFeatureScoreMap = updateFeatureScoreMap(consolidatedFeatureScoreMap, featureToScoreMap);

            //String targetString = answer.getAnswerString() + ":" + answer.getReasonString();
            String targetString = answer.getReasonString().trim();
            if(!targetString.isEmpty() && featureToScoreMap.size()>0) {
                targetString = targetString.substring(0, 1).toUpperCase() + targetString.substring(1);

                AnswerScore currAnswerScore = new AnswerScore(targetString, featureToScoreMap);

                int index=0;
                if(currAnswerScore.getAnswerScore()>ANSWER_SCORE_THRESHOLD) {
                    while (index < positiveAnswerFeaturewiseScoreList.size()
                            && currAnswerScore.getAnswerScore() <= positiveAnswerFeaturewiseScoreList.get(index).getAnswerScore()) {
                        index++;
                    }
                    positiveAnswerFeaturewiseScoreList.add(index, currAnswerScore);
                }
                else {
                    while (index < negativeAnswerFeaturewiseScoreList.size()
                            && currAnswerScore.getAnswerScore() <= negativeAnswerFeaturewiseScoreList.get(index).getAnswerScore()) {
                        index++;
                    }
                    negativeAnswerFeaturewiseScoreList.add(index, currAnswerScore);
                }
            }

            for (String s : functionMapOfTheAttemptedAnswer.keySet()) {
                if (!keys.contains(s)) {
                    keys.add(s);
                }
            }
        }

        //Update normalised score
        for(Score score : consolidatedFeatureScoreMap.values()) {
            score.computeNormalizedScore();
        }

        final double weightedScore = consolidatedFeatureScoreMap.values().parallelStream().map(Score::getWeightedScore).mapToDouble(Double::valueOf).sum();
        final double maxPossibleScore = consolidatedFeatureScoreMap.values().parallelStream().map(Score::getMaxPossibleScore).mapToDouble(Double::valueOf).sum();

        /// /        logger.info("Product " + product.getName() + " got score " + score);
        answerScoreMap.putAll(consolidatedFeatureScoreMap);

        Suggestion suggestion = new Suggestion(product, weightedScore, maxPossibleScore);
        suggestion.computeNormalizedScore();
        SuggestionSplit suggestionSplit = new SuggestionSplit(suggestion, answerScoreMap, positiveAnswerFeaturewiseScoreList, negativeAnswerFeaturewiseScoreList );

        try (Jedis res = jedisPool.getResource()) {
            res.auth(jedisPassword);
            res.set(hashKey, objectMapper.writeValueAsString(suggestionSplit));
        } catch (Exception e) {
            logger.error("Failed to cache value for " + hashKey, e);
        }

        return suggestionSplit;
    }

    private Map<String,Score> updateFeatureScoreMap(Map<String, Score> consolidatedFeatureScoreMap, Map<String, Score> featureToScoreMap)
    {
        for (String proKey : featureToScoreMap.keySet()) {
            Score v = featureToScoreMap.get(proKey);
            if (v.getReason() != null && !v.getReason().trim().isEmpty()) {
                if (v.getScore() > 6000) {
                    v.setReason("*perfect for " + v.getReason());
                } else if (v.getScore() > 0) {
                    v.setReason("+suitable for " + v.getReason());
                } else if (v.getScore() < 0) {
                    v.setReason("-not suitable for " + v.getReason());
                }
            }

            if (consolidatedFeatureScoreMap.containsKey(proKey)) {
                Score s = consolidatedFeatureScoreMap.get(proKey);
                s.setScore(s.getScore() + v.getScore());
                s.setWeightedScore(s.getWeightedScore() + v.getWeightedScore());
                s.setMaxPossibleScore(s.getMaxPossibleScore() + v.getMaxPossibleScore());
                s.appendReason(v.getReason());
            } else {
                if (v.getReason() != null && !v.getReason().trim().isEmpty()) {
					FeatureUnit featureUnit = extractFeatureNameAndUnitFromKey(proKey);
					String featureValueStr = "";
					if(featureUnit != null) {
					featureValueStr = writeASentence(v.getValue(), featureUnit.unit, featureUnit.feature,
							v.getReason().substring(1));
					}else {
						featureValueStr = writeASentence(v.getValue(), "", proKey,
								v.getReason().substring(1));
					}
                    String symbol = v.getReason().substring(0, 1);
                    String newString = symbol + featureValueStr;
                    v.setReason(newString);
                }
                consolidatedFeatureScoreMap.put(proKey, v);
            }
        }
        return consolidatedFeatureScoreMap;
    }
    
    private class FeatureUnit{
    	String feature;
    	String unit;
    	
    	FeatureUnit(String feature, String unit){
    		this.feature = feature;
    		this.unit = unit;
    	}
    };

	private FeatureUnit extractFeatureNameAndUnitFromKey(String proKey) {
		if (proKey.contains("_")) {
		String[] proKeyParts = proKey.split("_");
		String featureName = proKeyParts[0];
		String unit = "";
		if (proKeyParts.length > 1) {
			unit = " " + proKeyParts[1];
		}
			return new FeatureUnit(featureName, unit);
		}
		return new FeatureUnit(proKey, "");
	}

    private String writeASentence(String featureValue, String featureUnit, String featureName, String featureSuitability) {

        return featureValue + featureUnit + " of " + featureName + " is " + featureSuitability;
    }

    @Override
    public Collection<Suggestion> getProductScores(Integer surveyId) throws InvalidCategoryException, IOException {

        Survey survey = surveyDao.getSurveyById(surveyId);
        logger.info("Survey: " + survey);
        Questionnaire questionnaire = dataDao.getQuestionnaireById(survey.getQuestionnaireId());
        logger.info("Questionnaire: " + questionnaire);
        Category category = dataDao.getCategoryById(questionnaire.getCategoryId());
        logger.info("Category: " + category);
        List<Answer> answers = surveyDao.getAnswers(surveyId);
        if (answers.size() == 0) {
            return Collections.emptyList();
        }

        List<Product> products = dataDao.getProductsByQuestionnaireId(survey.getQuestionnaireId());
        if (products == null) {
            logger.error("Zero products for category: " + category.getId() + " -> " + category.getName());
            return null;
        }

        Map<String, Suggestion> scoreMap = evaluateAnswerForProducts(products, answers);

        Collection<Suggestion> topN = getTopN(scoreMap.values(), 9);

        Iterator<Suggestion> iterator = topN.iterator();
        while(iterator.hasNext()) {
			Suggestion suggestion = iterator.next();
			suggestion.computeNormalizedScore();
			getConfiguredProductFeaturesForSuggestion(suggestion);
        }

        return topN;
    }

	private void getConfiguredProductFeaturesForSuggestion(Suggestion suggestion) throws InvalidCategoryException {
		Iterator<Map.Entry<Integer, String>> configIter = configuration.getCategoryConfig().getDisplayProperties()
				.entrySet().iterator();
		HashMap<String, FeatureData> categoryProperties = new HashMap<String, FeatureData>();
		while (configIter.hasNext()) {
			Entry<Integer, String> entry = configIter.next();
			String catProps = entry.getValue();
			String[] catProperties = catProps.split(";");
			ProductFeature prodFeature = dataDao.getProductSingleFeatureValue(suggestion.getProduct(),
					catProperties[0]);
			if (prodFeature != null) {
				String key = prodFeature.getKey();
				FeatureUnit featureUnit = extractFeatureNameAndUnitFromKey(key);
				FeatureData featureData = new FeatureData(featureUnit.feature, new Double(prodFeature.getValue()),featureUnit.unit);
				featureData.setIcon(catProperties[1]);
				categoryProperties.put(featureUnit.feature, featureData);
			}
		}
		suggestion.setCategoryProperties(categoryProperties);
	}

    public <T extends ValueGetter> List<T> getTopN(Collection<T> inputs, int TopCount) {

        //Let's sort first. We doing insertion sort here.
        List<T> top = new LinkedList<>();
        for (T input : inputs) {
            int i = 0;
            while (i < top.size() && input.GetValue().doubleValue() <= top.get(i).GetValue().doubleValue()) {
                i++;
            }
            top.add(i, input);
        }

        // Returning topN from here
        if (top.size() > TopCount) {
            return top.subList(0, TopCount);
        } else {
            return top;
        }
    }

    private Map<String, Suggestion> evaluateAnswerForProducts(List<Product> products, List<Answer> answers)
            throws IOException, InvalidCategoryException {
        Map<String, Map<String, FeatureValue>> values = new HashMap<>();
        Map<String, Suggestion> scoreMap = new HashMap<>();
        boolean first = true;
        List<ProductFeature> featureWeights = dataDao.getFeatureWeights(products.get(0).getCategoryId());
        Map<String, Double> featureWeightMap = new HashMap<>();

        for (ProductFeature featureWeight : featureWeights) {
            featureWeightMap.put(featureWeight.getKey(), Double.valueOf(featureWeight.getValue()));
        }

        for (Answer answer : answers) {
            List<Suggestion> answerScoreMap = evaluateAnswerOnProductList(products, values, answer, featureWeightMap);

            if (answerScoreMap.size() < 1) {
                logger.error("Zero results after filtering from answer: {}", answer);
                continue;
            }

            List<String> found = new LinkedList<>();
            for (Suggestion suggestion : answerScoreMap) {
                final String identifier = String.valueOf(suggestion.getProduct().getCompanyName()) + ":" + suggestion.getProduct().getName();
                found.add(identifier);
                if (first) {
                    scoreMap.put(identifier, suggestion);
                } else {
                    if (scoreMap.containsKey(identifier)) {
                        final Suggestion existingSuggestion = scoreMap.get(identifier);
                        Double current = existingSuggestion.getScore();
                        existingSuggestion.setScore(current + suggestion.getScore());
                        existingSuggestion.setMaxPossibleScore(existingSuggestion.getMaxPossibleScore()+suggestion.getMaxPossibleScore());
                    }
                }
            }
            List<String> toDelete = new LinkedList<>();
            for (String s : scoreMap.keySet()) {
                if (!found.contains(s)) {
                    toDelete.add(s);
                }
            }
            for (String key : toDelete) {
                logger.info("Deleting product: " + key);
                scoreMap.remove(key);
            }

            first = false;
        }

        return scoreMap;
    }

    private List<Suggestion> evaluateAnswerOnProductList(List<Product> products, Map<String, Map<String, FeatureValue>> values,
                         Answer answer, Map<String, Double> featureWeights) throws IOException, InvalidCategoryException {

        final String cacheKey = "question." + answer.getQuestionId() + "." + answer.getAnswer();
        logger.info("Evaluate answer on products {}", cacheKey);
        List<Suggestion> answerScoreMap = new LinkedList<>();

        try (Jedis res = jedisPool.getResource()) {
            res.auth(jedisPassword);
            final String cachedResult;
            cachedResult = res.get(cacheKey);
            if (cachedResult != null) {
                answerScoreMap = objectMapper.readValue(cachedResult, objectMapper.getTypeFactory()
                                             .constructCollectionType(LinkedList.class, Suggestion.class));
                if (answerScoreMap.size() > 3) {
                    return answerScoreMap;
                } else {
                    logger.info("Result found in cache but answer set is too small: {}", cacheKey);
                }
            } else {
                logger.info("Result not found in cache: {}", cacheKey);
            }
        }

        MultivaluedMap<String, SymbolicFunction> functionMap = getFunctionMapForSurveyAnswer(answer.getId(), answer.getQuestionId(),
                answer.getQuestionType());

        for (Product product : products) {
            Map<String, Score> productFeatureScoreMap = getFeatureToScoreMap(values, functionMap, product, answer.getAnswer(), featureWeights);

            if (productFeatureScoreMap.size() > 0) {

                if (productFeatureScoreMap.values().stream().anyMatch(ps -> ps.getScore() <= -10000)) {
                    continue;
                }

                //if (productFeatureScoreMap.containsKey("price_INR") && productFeatureScoreMap.get("price_INR").getScore() <= -10000) {
                //    continue;
                //}

                final double score = productFeatureScoreMap.values().parallelStream().map(Score::getWeightedScore).mapToDouble(Double::valueOf).sum();
                //                logger.info("Product " + product.getName() + " got score " + score);
                final double maxPossibleScore = productFeatureScoreMap.values().parallelStream().map(Score::getMaxPossibleScore).mapToDouble(Double::valueOf).sum();

                Suggestion suggestion = new Suggestion(product, score, maxPossibleScore);
                answerScoreMap.add(suggestion);
            }
        }

        try (Jedis res = jedisPool.getResource()) {
            res.auth(jedisPassword);
            res.set(cacheKey, objectMapper.writeValueAsString(answerScoreMap));
            logger.info("Cached result for {}", cacheKey);
        }

        return answerScoreMap;
    }

    private Map<String, Score> getFeatureToScoreMap(Map<String, Map<String, FeatureValue>> featureToProductMapping,
            MultivaluedMap<String, SymbolicFunction> functionMap, Product product, String answer,
            Map<String, Double> featureWeights) throws InvalidCategoryException, IOException {

        Set<String> featureNames = functionMap.keySet();

        for (String featureName : featureNames) {
            if (!featureToProductMapping.containsKey(featureName)) {
                List<FeatureValue> val = dataDao.getFeatureValues(product.getCategoryId(), featureName);
                Map<String, FeatureValue> iVal = new HashMap<>();
                for (FeatureValue featureValue : val) {
                    iVal.put(featureValue.getCompanyName() + ":" + featureValue.getName(), featureValue);
                }
                featureToProductMapping.put(featureName, iVal);
            }
        }

        final String identifier = product.getCompanyName() + ":" + product.getName();
        final Map<String, Score> stringScoreByFeatureMap = calculateScore(featureToProductMapping, functionMap, identifier, answer,
                featureWeights);

        //    if (stringScoreMap.size() != featureNames.size()) {
        //      logger.info("Product  " + product.getName() + " is missing some featureNames, so not added in calculation");
        //      return new HashMap<>();
        //    }

        return stringScoreByFeatureMap;
    }

    private List<Object> getAnswerSet(List<Answer> answers) {
        List<Object> answerList = new LinkedList<>();
        for (Answer answer : answers) {
            answerList.add(getAnswerCacheKey(answer));
        }
        return answerList;
    }

    private String getAnswerCacheKey(Answer answer) {
        return answer.getQuestionId() + ":" + answer.getAnswer();
    }

    private Map<String, Score> calculateScore(Map<String, Map<String, FeatureValue>> featureToProductMapping,
            MultivaluedMap<String, SymbolicFunction> functionMap, String identifier, String answer,
            Map<String, Double> featureWeights) throws InvalidCategoryException, IOException {
		logger.info(identifier + ": Evaluating score for {} products", featureToProductMapping.keySet().size());
        Map<String, Score> scoreMap = new HashMap<>();

        Set<String> featureNameKeySet = functionMap.keySet();
        for (String featureName : featureNameKeySet) {
            Double featureWeight = featureWeights.get(featureName);
            if (featureWeight == null) {
                featureWeight = 1.0;
            }

            //Extract the featureValue for currentProduct->CurrentFeature
            Map<String, FeatureValue> productMap = featureToProductMapping.get(featureName);
            FeatureValue currFeatureValue = productMap.get(identifier);
            if (currFeatureValue == null || currFeatureValue.getValue() == null || currFeatureValue.getValue().trim().length() < 1) {
                continue;
            }

            Score calculatedFeatureScore = new Score();
            try {
                List<SymbolicFunction> functions = functionMap.get(featureName);
                double keyScore = 0;
                double keyScoreWeighted = 0;
                double maxScorePossible = 0;
                for (SymbolicFunction function : functions) {
                    double currKeyScore = evaluateSymbolicFunction(function, currFeatureValue.getNormalisedNumberValue(),
                            currFeatureValue.getNumberValue(), currFeatureValue.getValue(), answer);
                    keyScore += currKeyScore;
                    keyScoreWeighted += Math.round(currKeyScore * function.getWeight());
                    maxScorePossible += Math.round(10000 * function.getWeight()); //10000 to be pushed in config
                }
                calculatedFeatureScore.setScore(keyScore);
                calculatedFeatureScore.setWeightedScore(keyScoreWeighted);
                calculatedFeatureScore.setMaxPossibleScore(maxScorePossible);
                calculatedFeatureScore.setValue(currFeatureValue.getValue());

                String[] featureNameParts = featureName.split("_");

                featureName = featureNameParts[0];
                String unit = "";
                if (featureNameParts.length > 1) {
                    unit = " " + featureNameParts[1];
                    calculatedFeatureScore.setValue(calculatedFeatureScore.getValue() + " " + unit);
                }

                scoreMap.put(featureName, calculatedFeatureScore);
            } catch (Exception e) {
                logger.error(featureName + " cannot be evaluated for [" + identifier + "] value ["
                        + currFeatureValue.getNormalisedNumberValue() + "] - [" + currFeatureValue.getNumberValue() + "]", e);
            }
        }
        return scoreMap;
    }

    private double evaluateSymbolicFunction(SymbolicFunction function, Double normalizedNumberVal, Double numberValue, String stringVal, String answer)
            throws IOException {
        SymbolicFunction f = function.clone();

        Substitution s = new Substitution();
        s.setVariable("x");
        s.setValue(String.valueOf(normalizedNumberVal));
        f.getSubstitutions().add(s);

        Substitution s1 = new Substitution();
        s1.setVariable("a");
        s1.setValue(answer);
        f.getSubstitutions().add(s1);

        Substitution s2 = new Substitution();
        s2.setVariable("s");
        s2.setValue(stringVal);
        f.getSubstitutions().add(s2);

        Substitution s3 = new Substitution();
        s3.setVariable("y");
        s3.setValue(String.valueOf(numberValue));
        f.getSubstitutions().add(s3);

        String cachedValue = null;
        try (Jedis res = jedisPool.getResource()) {
            res.auth(jedisPassword);
            cachedValue = res.get(f.toString());
        }
        if (cachedValue != null && cachedValue.length() > 0) {
            try {
                return Double.valueOf(cachedValue);
            } catch (NumberFormatException e) {
                logger.error("Failed to parse value [" + cachedValue + "] as double.", e);
            }
        }
        final double freshValue = dataDao.evaluateSymbolicFunction(f);
        try (Jedis res = jedisPool.getResource()) {
            res.auth(jedisPassword);
            //            res.set(f.toString(), String.valueOf(freshValue), "NX", "PX", new Date().getTime() + (7 * 24 * 60 * 60 * 1000));
            res.set(f.toString(), String.valueOf(freshValue));
        } catch (Exception e) {
            logger.error("Failed to write value to cache : " + freshValue, e);
        }

        return freshValue;
    }

    @Override
    public Map getProductFunctionsAsString(Integer surveyId) {
        throw new NotImplementedException("get product functions as string ?");
    }

    public MultivaluedMap<String, SymbolicFunction> getFunctionMapForSurveyAnswer(Integer answerId, Integer questionId, QuestionType questionType) {

        String cacheKey = "functionmap." + questionId + "." + answerId;
        try (Jedis res = this.jedisPool.getResource()) {
            res.auth(jedisPassword);
            String cached = res.get(cacheKey);
            if (cached != null && cached.length() > 3) {
                MultivaluedMap<String, SymbolicFunction> val = objectMapper
                        .readValue(cacheKey, new TypeReference<MultivaluedMap<String, SymbolicFunction>>() {});
                return val;
            }
        } catch (IOException e) {
        }

        // TODO: add caching
        if (QuestionType.multi_string_choice_filter.equals(questionType)) {

            JSONObject object = surveyDao.getQuestionData(questionId);
            MultivaluedMap<String, SymbolicFunction> functionMultivaluedMap = new MultivaluedHashMap<>();
            functionMultivaluedMap.putSingle(object.getJSONArray("key").getString(0), new SymbolicFunction("equal"));
            return functionMultivaluedMap;
        } else {

            List<Answer> answers = surveyDao.getAnswerById(answerId);
            List<Answer> selectedAnswer = new LinkedList<>();
            for (Answer answer : answers) {
                if (Objects.equals(answer.getId(), answerId)) {
                    selectedAnswer.add(answer);
                    break;
                }
            }

            MultivaluedMap<String, SymbolicFunction> stringSymbolicFunctionMultivaluedMap = answerToFunctionMap(selectedAnswer);

            try (Jedis res = this.jedisPool.getResource()) {
                res.auth(jedisPassword);
                res.set(cacheKey, objectMapper.writeValueAsString(stringSymbolicFunctionMultivaluedMap));
            } catch (JsonProcessingException e) {
                logger.error("Failed to serialize multivalue map {}", e);
            }

            return stringSymbolicFunctionMultivaluedMap;
        }
    }

    private MultivaluedMap<String, SymbolicFunction> answerToFunctionMap(List<Answer> answers) {
        MultivaluedMap<String, SymbolicFunction> functionMap = new MultivaluedHashMap<>();

        for (Answer answer : answers) {
            JSONObject data = answer.getData();
            Map<String, SymbolicFunction> res = dataDao.addAnswerDataToMap(data);
            for (Map.Entry<String, SymbolicFunction> stringSymbolicFunctionEntry : res.entrySet()) {
                functionMap.add(stringSymbolicFunctionEntry.getKey(), stringSymbolicFunctionEntry.getValue());
            }

        }
        return functionMap;
    }

    @Override
    public void updateAnswer(Integer surveyId, Integer questionId, String answer) {
        surveyDao.updateAnswer(surveyId, questionId, answer);
    }


    public static interface ValueGetter {
        Number GetValue();
    }

}
