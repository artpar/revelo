package com.inf.exceptions;

/**
 * author parth.mudgal on 28/05/14.
 */
public class InvalidQuestionException extends Exception
{

	public InvalidQuestionException(Object question)
	{
		super(String.valueOf(question));
	}
}
