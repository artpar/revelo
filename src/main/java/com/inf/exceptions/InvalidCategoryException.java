package com.inf.exceptions;

/**
 * author parth.mudgal on 28/05/14.
 */
public class InvalidCategoryException extends Exception
{

	public InvalidCategoryException(String category)
	{
		super(category);
	}
}
