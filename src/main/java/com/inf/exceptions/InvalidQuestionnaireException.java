package com.inf.exceptions;

/**
 * author parth.mudgal on 28/05/14.
 */
public class InvalidQuestionnaireException extends Exception {

    public InvalidQuestionnaireException(String quid) {
        super(String.valueOf(quid));
    }
}
