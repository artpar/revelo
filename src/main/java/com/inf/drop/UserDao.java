package com.inf.drop;

import java.util.List;
import java.util.Set;

import com.inf.data.User;
import com.inf.data.UserGroup;
import com.inf.model.security.Authority;
import com.inf.mvc.UserStats;

/**
 * author parth.mudgal on 27/06/15.
 */
public interface UserDao {
  public List<String> getUserRoles(String userId);

  Set<Authority> getUserAuthorities(String emailAddress);

  User getUserByEmail(String email);

  UserStats getCounts(Integer id);

  User getUserByReferenceId(String referenceId);

  List<String> getGroupIdsOfUser(Integer userId);

  UserGroup getHomeGroupByUserId(Integer userId);
}
