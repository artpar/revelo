/**
 * 
 */
package com.inf.drop;

import java.util.HashMap;

/**
 * @author Gayatri Deshpande
 *
 */
public class CategoryConfig {

	private HashMap<Integer,String> displayProperties = new HashMap<Integer,String>();

	public HashMap<Integer,String> getDisplayProperties() {
		return displayProperties;
	}

	public void setDisplayProperties(HashMap<Integer,String> displayProperties) {
		this.displayProperties = displayProperties;
	}

}
