package com.inf.drop;

/**
 * author parth.mudgal on 22/06/15.
 */
public class SympyConfiguration
{
	private String server;

	public String getServer()
	{
		return server;
	}

	public void setServer(String server)
	{
		this.server = server;
	}
}
