package com.inf.drop;

/**
 * author parth.mudgal on 18/06/15.
 */
public class DatabaseConfig
{
	private String connection;
	private String lastAcquisitionFailureDefaultUser;
	private String lastCheckinFailureDefaultUser;
	private String lastCheckoutFailureDefaultUser;
	private String lastConnectionTestFailureDefaultUser;
	private String lastIdleTestFailureDefaultUser;
	private String logWriter;
	private Integer loginTimeout;
	private Integer numBusyConnections;
	private Integer numBusyConnectionsAllUsers;
	private Integer numBusyConnectionsDefaultUser;
	private Integer numConnections;
	private Integer numConnectionsAllUsers;
	private Integer numConnectionsDefaultUser;
	private Integer numFailedCheckinsDefaultUser;
	private Integer numFailedCheckoutsDefaultUser;
	private Integer numFailedIdleTestsDefaultUser;
	private Integer numIdleConnections;
	private Integer numIdleConnectionsAllUsers;
	private Integer numIdleConnectionsDefaultUser;
	private Integer numUnclosedOrphanedConnections;
	private Integer numUnclosedOrphanedConnectionsAllUsers;
	private Integer numUnclosedOrphanedConnectionsDefaultUser;
	private Integer numUserPools;
	private String effectivePropertyCycleDefaultUser;
	private Integer startTimeMillisDefaultUser;
	private Integer statementCacheNumCheckedOutDefaultUser;
	private Integer statementCacheNumCheckedOutStatementsAllUsers;
	private Integer statementCacheNumConnectionsWithCachedStatementsAllUsers;
	private Integer statementCacheNumConnectionsWithCachedStatementsDefaultUser;
	private Integer statementCacheNumStatementsAllUsers;
	private Integer statementCacheNumStatementsDefaultUser;
	private Integer threadPoolSize;
	private Integer threadPoolNumActiveThreads;
	private Integer threadPoolNumIdleThreads;
	private Integer threadPoolNumTasksPending;
	private String threadPoolStackTraces;
	private String threadPoolStatus;
	private String overrideDefaultUser;
	private String overrideDefaultPassword;
	private String password;
	private String reference;
	private Integer upTimeMillisDefaultUser;
	private String user;
	private String userOverridesAsString;
	private String allUsers;
	private String connectionPoolDataSource;
	private String jdbcUrl;

	public String getConnection()
	{
		return connection;
	}

	public void setConnection(String connection)
	{
		this.connection = connection;
	}

	public String getLastAcquisitionFailureDefaultUser()
	{
		return lastAcquisitionFailureDefaultUser;
	}

	public void setLastAcquisitionFailureDefaultUser(String lastAcquisitionFailureDefaultUser)
	{
		this.lastAcquisitionFailureDefaultUser = lastAcquisitionFailureDefaultUser;
	}

	public String getLastCheckinFailureDefaultUser()
	{
		return lastCheckinFailureDefaultUser;
	}

	public void setLastCheckinFailureDefaultUser(String lastCheckinFailureDefaultUser)
	{
		this.lastCheckinFailureDefaultUser = lastCheckinFailureDefaultUser;
	}

	public String getLastCheckoutFailureDefaultUser()
	{
		return lastCheckoutFailureDefaultUser;
	}

	public void setLastCheckoutFailureDefaultUser(String lastCheckoutFailureDefaultUser)
	{
		this.lastCheckoutFailureDefaultUser = lastCheckoutFailureDefaultUser;
	}

	public String getLastConnectionTestFailureDefaultUser()
	{
		return lastConnectionTestFailureDefaultUser;
	}

	public void setLastConnectionTestFailureDefaultUser(String lastConnectionTestFailureDefaultUser)
	{
		this.lastConnectionTestFailureDefaultUser = lastConnectionTestFailureDefaultUser;
	}

	public String getLastIdleTestFailureDefaultUser()
	{
		return lastIdleTestFailureDefaultUser;
	}

	public void setLastIdleTestFailureDefaultUser(String lastIdleTestFailureDefaultUser)
	{
		this.lastIdleTestFailureDefaultUser = lastIdleTestFailureDefaultUser;
	}

	public String getLogWriter()
	{
		return logWriter;
	}

	public void setLogWriter(String logWriter)
	{
		this.logWriter = logWriter;
	}

	public Integer getLoginTimeout()
	{
		return loginTimeout;
	}

	public void setLoginTimeout(Integer loginTimeout)
	{
		this.loginTimeout = loginTimeout;
	}

	public Integer getNumBusyConnections()
	{
		return numBusyConnections;
	}

	public void setNumBusyConnections(Integer numBusyConnections)
	{
		this.numBusyConnections = numBusyConnections;
	}

	public Integer getNumBusyConnectionsAllUsers()
	{
		return numBusyConnectionsAllUsers;
	}

	public void setNumBusyConnectionsAllUsers(Integer numBusyConnectionsAllUsers)
	{
		this.numBusyConnectionsAllUsers = numBusyConnectionsAllUsers;
	}

	public Integer getNumBusyConnectionsDefaultUser()
	{
		return numBusyConnectionsDefaultUser;
	}

	public void setNumBusyConnectionsDefaultUser(Integer numBusyConnectionsDefaultUser)
	{
		this.numBusyConnectionsDefaultUser = numBusyConnectionsDefaultUser;
	}

	public Integer getNumConnections()
	{
		return numConnections;
	}

	public void setNumConnections(Integer numConnections)
	{
		this.numConnections = numConnections;
	}

	public Integer getNumConnectionsAllUsers()
	{
		return numConnectionsAllUsers;
	}

	public void setNumConnectionsAllUsers(Integer numConnectionsAllUsers)
	{
		this.numConnectionsAllUsers = numConnectionsAllUsers;
	}

	public Integer getNumConnectionsDefaultUser()
	{
		return numConnectionsDefaultUser;
	}

	public void setNumConnectionsDefaultUser(Integer numConnectionsDefaultUser)
	{
		this.numConnectionsDefaultUser = numConnectionsDefaultUser;
	}

	public Integer getNumFailedCheckinsDefaultUser()
	{
		return numFailedCheckinsDefaultUser;
	}

	public void setNumFailedCheckinsDefaultUser(Integer numFailedCheckinsDefaultUser)
	{
		this.numFailedCheckinsDefaultUser = numFailedCheckinsDefaultUser;
	}

	public Integer getNumFailedCheckoutsDefaultUser()
	{
		return numFailedCheckoutsDefaultUser;
	}

	public void setNumFailedCheckoutsDefaultUser(Integer numFailedCheckoutsDefaultUser)
	{
		this.numFailedCheckoutsDefaultUser = numFailedCheckoutsDefaultUser;
	}

	public Integer getNumFailedIdleTestsDefaultUser()
	{
		return numFailedIdleTestsDefaultUser;
	}

	public void setNumFailedIdleTestsDefaultUser(Integer numFailedIdleTestsDefaultUser)
	{
		this.numFailedIdleTestsDefaultUser = numFailedIdleTestsDefaultUser;
	}

	public Integer getNumIdleConnections()
	{
		return numIdleConnections;
	}

	public void setNumIdleConnections(Integer numIdleConnections)
	{
		this.numIdleConnections = numIdleConnections;
	}

	public Integer getNumIdleConnectionsAllUsers()
	{
		return numIdleConnectionsAllUsers;
	}

	public void setNumIdleConnectionsAllUsers(Integer numIdleConnectionsAllUsers)
	{
		this.numIdleConnectionsAllUsers = numIdleConnectionsAllUsers;
	}

	public Integer getNumIdleConnectionsDefaultUser()
	{
		return numIdleConnectionsDefaultUser;
	}

	public void setNumIdleConnectionsDefaultUser(Integer numIdleConnectionsDefaultUser)
	{
		this.numIdleConnectionsDefaultUser = numIdleConnectionsDefaultUser;
	}

	public Integer getNumUnclosedOrphanedConnections()
	{
		return numUnclosedOrphanedConnections;
	}

	public void setNumUnclosedOrphanedConnections(Integer numUnclosedOrphanedConnections)
	{
		this.numUnclosedOrphanedConnections = numUnclosedOrphanedConnections;
	}

	public Integer getNumUnclosedOrphanedConnectionsAllUsers()
	{
		return numUnclosedOrphanedConnectionsAllUsers;
	}

	public void setNumUnclosedOrphanedConnectionsAllUsers(Integer numUnclosedOrphanedConnectionsAllUsers)
	{
		this.numUnclosedOrphanedConnectionsAllUsers = numUnclosedOrphanedConnectionsAllUsers;
	}

	public Integer getNumUnclosedOrphanedConnectionsDefaultUser()
	{
		return numUnclosedOrphanedConnectionsDefaultUser;
	}

	public void setNumUnclosedOrphanedConnectionsDefaultUser(Integer numUnclosedOrphanedConnectionsDefaultUser)
	{
		this.numUnclosedOrphanedConnectionsDefaultUser = numUnclosedOrphanedConnectionsDefaultUser;
	}

	public Integer getNumUserPools()
	{
		return numUserPools;
	}

	public void setNumUserPools(Integer numUserPools)
	{
		this.numUserPools = numUserPools;
	}

	public String getEffectivePropertyCycleDefaultUser()
	{
		return effectivePropertyCycleDefaultUser;
	}

	public void setEffectivePropertyCycleDefaultUser(String effectivePropertyCycleDefaultUser)
	{
		this.effectivePropertyCycleDefaultUser = effectivePropertyCycleDefaultUser;
	}

	public Integer getStartTimeMillisDefaultUser()
	{
		return startTimeMillisDefaultUser;
	}

	public void setStartTimeMillisDefaultUser(Integer startTimeMillisDefaultUser)
	{
		this.startTimeMillisDefaultUser = startTimeMillisDefaultUser;
	}

	public Integer getStatementCacheNumCheckedOutDefaultUser()
	{
		return statementCacheNumCheckedOutDefaultUser;
	}

	public void setStatementCacheNumCheckedOutDefaultUser(Integer statementCacheNumCheckedOutDefaultUser)
	{
		this.statementCacheNumCheckedOutDefaultUser = statementCacheNumCheckedOutDefaultUser;
	}

	public Integer getStatementCacheNumCheckedOutStatementsAllUsers()
	{
		return statementCacheNumCheckedOutStatementsAllUsers;
	}

	public void setStatementCacheNumCheckedOutStatementsAllUsers(Integer statementCacheNumCheckedOutStatementsAllUsers)
	{
		this.statementCacheNumCheckedOutStatementsAllUsers = statementCacheNumCheckedOutStatementsAllUsers;
	}

	public Integer getStatementCacheNumConnectionsWithCachedStatementsAllUsers()
	{
		return statementCacheNumConnectionsWithCachedStatementsAllUsers;
	}

	public void setStatementCacheNumConnectionsWithCachedStatementsAllUsers(
	        Integer statementCacheNumConnectionsWithCachedStatementsAllUsers)
	{
		this.statementCacheNumConnectionsWithCachedStatementsAllUsers =
		        statementCacheNumConnectionsWithCachedStatementsAllUsers;
	}

	public Integer getStatementCacheNumConnectionsWithCachedStatementsDefaultUser()
	{
		return statementCacheNumConnectionsWithCachedStatementsDefaultUser;
	}

	public void setStatementCacheNumConnectionsWithCachedStatementsDefaultUser(
	        Integer statementCacheNumConnectionsWithCachedStatementsDefaultUser)
	{
		this.statementCacheNumConnectionsWithCachedStatementsDefaultUser =
		        statementCacheNumConnectionsWithCachedStatementsDefaultUser;
	}

	public Integer getStatementCacheNumStatementsAllUsers()
	{
		return statementCacheNumStatementsAllUsers;
	}

	public void setStatementCacheNumStatementsAllUsers(Integer statementCacheNumStatementsAllUsers)
	{
		this.statementCacheNumStatementsAllUsers = statementCacheNumStatementsAllUsers;
	}

	public Integer getStatementCacheNumStatementsDefaultUser()
	{
		return statementCacheNumStatementsDefaultUser;
	}

	public void setStatementCacheNumStatementsDefaultUser(Integer statementCacheNumStatementsDefaultUser)
	{
		this.statementCacheNumStatementsDefaultUser = statementCacheNumStatementsDefaultUser;
	}

	public Integer getThreadPoolSize()
	{
		return threadPoolSize;
	}

	public void setThreadPoolSize(Integer threadPoolSize)
	{
		this.threadPoolSize = threadPoolSize;
	}

	public Integer getThreadPoolNumActiveThreads()
	{
		return threadPoolNumActiveThreads;
	}

	public void setThreadPoolNumActiveThreads(Integer threadPoolNumActiveThreads)
	{
		this.threadPoolNumActiveThreads = threadPoolNumActiveThreads;
	}

	public Integer getThreadPoolNumIdleThreads()
	{
		return threadPoolNumIdleThreads;
	}

	public void setThreadPoolNumIdleThreads(Integer threadPoolNumIdleThreads)
	{
		this.threadPoolNumIdleThreads = threadPoolNumIdleThreads;
	}

	public Integer getThreadPoolNumTasksPending()
	{
		return threadPoolNumTasksPending;
	}

	public void setThreadPoolNumTasksPending(Integer threadPoolNumTasksPending)
	{
		this.threadPoolNumTasksPending = threadPoolNumTasksPending;
	}

	public String getThreadPoolStackTraces()
	{
		return threadPoolStackTraces;
	}

	public void setThreadPoolStackTraces(String threadPoolStackTraces)
	{
		this.threadPoolStackTraces = threadPoolStackTraces;
	}

	public String getThreadPoolStatus()
	{
		return threadPoolStatus;
	}

	public void setThreadPoolStatus(String threadPoolStatus)
	{
		this.threadPoolStatus = threadPoolStatus;
	}

	public String getOverrideDefaultUser()
	{
		return overrideDefaultUser;
	}

	public void setOverrideDefaultUser(String overrideDefaultUser)
	{
		this.overrideDefaultUser = overrideDefaultUser;
	}

	public String getOverrideDefaultPassword()
	{
		return overrideDefaultPassword;
	}

	public void setOverrideDefaultPassword(String overrideDefaultPassword)
	{
		this.overrideDefaultPassword = overrideDefaultPassword;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	public String getReference()
	{
		return reference;
	}

	public void setReference(String reference)
	{
		this.reference = reference;
	}

	public Integer getUpTimeMillisDefaultUser()
	{
		return upTimeMillisDefaultUser;
	}

	public void setUpTimeMillisDefaultUser(Integer upTimeMillisDefaultUser)
	{
		this.upTimeMillisDefaultUser = upTimeMillisDefaultUser;
	}

	public String getUser()
	{
		return user;
	}

	public void setUser(String user)
	{
		this.user = user;
	}

	public String getUserOverridesAsString()
	{
		return userOverridesAsString;
	}

	public void setUserOverridesAsString(String userOverridesAsString)
	{
		this.userOverridesAsString = userOverridesAsString;
	}

	public String getAllUsers()
	{
		return allUsers;
	}

	public void setAllUsers(String allUsers)
	{
		this.allUsers = allUsers;
	}

	public String getConnectionPoolDataSource()
	{
		return connectionPoolDataSource;
	}

	public void setConnectionPoolDataSource(String connectionPoolDataSource)
	{
		this.connectionPoolDataSource = connectionPoolDataSource;
	}

	public String getJdbcUrl()
	{
		return jdbcUrl;
	}

	public void setJdbcUrl(String jdbcUrl)
	{
		this.jdbcUrl = jdbcUrl;
	}
}
