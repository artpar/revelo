package com.inf.drop;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import javax.validation.constraints.NotNull;
import com.bendb.dropwizard.redis.JedisFactory;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.ImmutableMap;
import com.google.common.net.HostAndPort;
import io.dropwizard.Configuration;
import org.hibernate.validator.constraints.NotEmpty;

public class InfConfiguration extends Configuration {

  public static final String SESSION_TOKEN_NAME = "msid";
  public static final String OK = "{\"status\": \"ok\"}";
//	@JsonDeserialize (contentAs = OAuthCfgClass.class)
//	private List<OAuthCfgClass> oauthCfg;

  @JsonProperty
  private HashMap<String, String> oauthCustomCfg = null;

  private SympyConfiguration sympy;

  @NotEmpty
  private String template;

  @NotEmpty
  private String defaultName = "Stranger";

  private DatabaseConfig databaseConfig;

  private GraphDatabaseConfig graphDatabaseConfig;

  private AppConfig appConfig;
  
  private CategoryConfig categoryConfig;


  @NotNull
  private Map<String, Map<String, String>> viewRendererConfiguration = Collections.emptyMap();

  @JsonProperty
  private String oauthSuccessUrl = "";

  @JsonProperty
  private HashMap<String, String> adminUsers = null;
  private JedisFactory jedisFactory;
  private WeedFsConfig weedFsConfig;
  private RedisConfig redisConfig;

  private MessengerConfig messengerConfig;

  private String defaultSurveyId;



  public GraphDatabaseConfig getGraphDatabaseConfig() {
    return graphDatabaseConfig;
  }

  public void setGraphDatabaseConfig(GraphDatabaseConfig graphDatabaseConfig) {
    this.graphDatabaseConfig = graphDatabaseConfig;
  }
  public AppConfig getAppConfig() { return appConfig; }

  public void setAppConfig(AppConfig appConfig) { this.appConfig = appConfig; }

  public RedisConfig getRedisConfig() {
    return redisConfig;
  }

  public void setRedisConfig(RedisConfig redisConfig) {
    this.redisConfig = redisConfig;
  }

  public Map<String, String> getAdminUsers() {
    return adminUsers;
  }


  public HashMap<String, String> getOauthCustomCfg() {
    return oauthCustomCfg;
  }

  public void setOauthCustomCfg(HashMap<String, String> oauthCustomCfg) {
    this.oauthCustomCfg = oauthCustomCfg;
  }

  public SympyConfiguration getSympy() {
    return sympy;
  }

  public void setSympy(SympyConfiguration sympy) {
    this.sympy = sympy;
  }

  public DatabaseConfig getDatabaseConfig() {
    return databaseConfig;
  }

  public void setDatabaseConfig(DatabaseConfig databaseConfig) {
    this.databaseConfig = databaseConfig;
  }

  @JsonProperty
  public String getTemplate() {
    return template;
  }

  @JsonProperty
  public void setTemplate(String template) {
    this.template = template;
  }

  @JsonProperty
  public String getDefaultName() {
    return defaultName;
  }

  @JsonProperty
  public void setDefaultName(String defaultName) {
    this.defaultName = defaultName;
  }

  @JsonProperty("viewRendererConfiguration")
  public Map<String, Map<String, String>> getViewRendererConfiguration() {
    return viewRendererConfiguration;
  }

  @JsonProperty("viewRendererConfiguration")
  public void setViewRendererConfiguration(Map<String, Map<String, String>> viewRendererConfiguration) {
    ImmutableMap.Builder<String, Map<String, String>> builder = ImmutableMap.builder();
    for (Map.Entry<String, Map<String, String>> entry : viewRendererConfiguration.entrySet()) {
      builder.put(entry.getKey(), ImmutableMap.copyOf(entry.getValue()));
    }
    this.viewRendererConfiguration = builder.build();
  }

  public JedisFactory getJedisFactory() {
    jedisFactory = new JedisFactory();
    jedisFactory.setEndpoint(HostAndPort.fromParts(redisConfig.getHostname(), redisConfig.getPort()));
    jedisFactory.setMaxIdle(redisConfig.getMaxIdle());
    jedisFactory.setMaxTotal(redisConfig.getMaxTotal());
    jedisFactory.setMinIdle(redisConfig.getMinIdle());
    if (redisConfig.getPassword() != null && redisConfig.getPassword().length() > 0){
      jedisFactory.setPassword(redisConfig.getPassword());
    }
    return jedisFactory;
  }

  public WeedFsConfig getWeedFsConfig() {
    return weedFsConfig;
  }

  public void setWeedFsConfig(WeedFsConfig weedFsConfig) {
    this.weedFsConfig = weedFsConfig;
  }

  public MessengerConfig getMessengerConfig() {
    return messengerConfig;
  }

  private void setMessengerConfig(MessengerConfig messengerConfig) {
    this.messengerConfig = messengerConfig;
  }

  public CategoryConfig getCategoryConfig() {
	  return categoryConfig;
  }
  
  public void setCategoryConfig(CategoryConfig categoryConfig) {
	  this.categoryConfig = categoryConfig;
  }

  public String getDefaultSurveyId() {
    return defaultSurveyId;
  }

  public void setDefaultSurveyId(String defaultSurveyId) {
    this.defaultSurveyId = defaultSurveyId;
  }
}
