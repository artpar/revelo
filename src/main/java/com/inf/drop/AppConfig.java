package com.inf.drop;

public class AppConfig {
    private Boolean showImages;

    public Boolean getShowImages() {
        return showImages;
    }

    public void setShowImages(String showImages) {
        this.showImages = Boolean.valueOf(showImages);
    }
}
