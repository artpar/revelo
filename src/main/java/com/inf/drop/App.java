package com.inf.drop;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Properties;
import com.bendb.dropwizard.redis.JedisBundle;
import com.bendb.dropwizard.redis.JedisFactory;
import com.codahale.metrics.MetricRegistry;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.inf.auth.OAuthCredentialAuthFilter;
import com.inf.cache.RedisCache;
import com.inf.data.dao.impl.DataDao;
import com.inf.data.dao.impl.UserDao;
import com.inf.discovery.scraper.ScrapeManager;
import com.inf.imageclient.bing.BingClient;
import com.inf.imageclient.weedfs.WeedFsClient;
import com.inf.interfaces.DatabaseImageClient;
import com.inf.interfaces.Oauth2Util;
import com.inf.messenger.ChatEngine;
import com.inf.messenger.MessengerService;
import com.inf.messenger.MessengerStorage;
import com.inf.messenger.NlpService;
import com.inf.model.security.Auth;
import com.inf.mvc.Auth0Authenticator;
import com.inf.mvc.CategoryController;
import com.inf.mvc.ImageController;
import com.inf.mvc.MessengerController;
import com.inf.mvc.QuestionController;
import com.inf.mvc.QuestionnaireController;
import com.inf.mvc.RootController;
import com.inf.mvc.ScrapeController;
import com.inf.mvc.SimpleAuthorizer;
import com.inf.mvc.SurveyController;
import com.inf.mvc.UserController;
import com.inf.services.XlsFileService;
import com.inf.survey.RegexValueTransformer;
import com.inf.survey.SurveyManager;
import com.inf.survey.SurveyManagerImpl;
import com.inf.survey.dao.SurveyDaoImpl;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import io.artpar.curd.SchemaController;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.auth.AuthDynamicFeature;
import io.dropwizard.auth.AuthValueFactoryProvider;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.dropwizard.views.ViewBundle;
import io.dropwizard.views.ViewMessageBodyWriter;
import io.dropwizard.views.ViewRenderer;
import org.eclipse.jetty.server.session.SessionHandler;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;
import org.hypergraphdb.HyperGraph;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.client.RestTemplate;
import redis.clients.jedis.JedisPool;

/**
 * author parth.mudgal on 18/06/15.
 */
public class App extends Application<InfConfiguration> {
  private static InfConfiguration configuration;

  private Logger logger = LoggerFactory.getLogger(App.class);

  private Auth0Authenticator authenticator;

  public static void main(String[] args) throws Exception {
    new App().run(args);
  }

  public static InfConfiguration getConfig() {
    return configuration;
  }

  @Override
  public String getName() {
    return "hello-world";
  }

  @Override
  public void initialize(Bootstrap<InfConfiguration> bootstrap) {
    bootstrap.addBundle(new AssetsBundle("/html", "/web", "index.html", "html"));
    bootstrap.addBundle(new AssetsBundle("/html", "/adminweb", "admin.html", "admin"));
    bootstrap.addBundle(new AssetsBundle("/static", "/adminweb/resources", "", "adminresoruces"));
    bootstrap.addBundle(new AssetsBundle("/static", "/web/resources", "", "webresources"));
    bootstrap.addBundle(new AssetsBundle("/assets/jquery", "/jquery", "", "jquery"));
    bootstrap.addBundle(new AssetsBundle("/assets/images", "/images", "", "images"));
    bootstrap.addBundle(new AssetsBundle("/root", "/root", "index.html", "root"));
    bootstrap.addBundle(new AssetsBundle("/homepage", "/home", "index.html", "homepage"));
    bootstrap.addBundle(new AssetsBundle("/material", "/material", "index.html", "material"));
    bootstrap.addBundle(new AssetsBundle("/messenger", "/talk", "index.html", "bot"));
    bootstrap.addBundle(new ViewBundle<>());
    // nothing to do yet

    bootstrap.addBundle(new JedisBundle<InfConfiguration>() {
      @Override
      public JedisFactory getJedisFactory(InfConfiguration configuration) {
        return configuration.getJedisFactory();
      }
    });
  }

  @Override
  public void run(InfConfiguration configuration, Environment environment)
          throws InvocationTargetException, IllegalAccessException, NoSuchMethodException, SQLException {
    App.configuration = configuration;

    ObjectMapper objectMapper = environment.getObjectMapper();
    Oauth2Util.setObjectMapper(objectMapper);
    JdbcTemplate jdbc = initDb(configuration);
    JedisPool jedisPool = null;
    String jedisPassword = configuration.getRedisConfig().getPassword();
    try {
      jedisPool = configuration.getJedisFactory().build(environment);
    }
    catch(Exception E) {
      logger.error(E.getMessage());
    }

    RestTemplate restTemplate = new RestTemplate();

    RedisCache cacher = new RedisCache(jedisPool, jedisPassword, "bing");
    BingClient bingClient = new BingClient(cacher, objectMapper);

    //    GettyClient gettyClient = new GettyClient("hrfbp3xq3ven7x6rhwv7r65v", "nynxBwNEJm4ncp64r5Ym3B3Wwz3qSn5RHFQmXty86FxX8", new RedisCache(jedisPool, "getty"), objectMapper);

    UserDao userDao = new UserDao(jdbc, objectMapper);
    final DataDao dataDao = new DataDao(jdbc, objectMapper, jedisPool, jedisPassword, restTemplate, userDao);
    DatabaseImageClient databaseImageClient = new DatabaseImageClient(cacher, objectMapper, dataDao);
    dataDao.setFunctionEvaluateUrl(configuration.getSympy().getServer());
    WeedFsClient weedFsClient = new WeedFsClient(configuration.getWeedFsConfig(), bingClient, cacher, objectMapper, databaseImageClient);
    weedFsClient.setRestTemplate(restTemplate);

    //        configuration.register(new RolesAllowedRequestFilter(ra.value()));
    //        environment.jersey().register(DateNotSpecifiedServletFilter.class);
    objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
    objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
    SchemaController schemaController = new SchemaController("/", "api/v2", jdbc.getDataSource(), objectMapper);
    environment.jersey().getResourceConfig().registerResources(schemaController.getRootResource().build());

    authenticator = new Auth0Authenticator(dataDao, jedisPool, jedisPassword);
    environment.jersey().register(MultiPartFeature.class);

    //        environment.jersey().property("jersey.config.server.tracing.type", "ALL");
    //        environment.jersey().property("jersey.config.server.tracing.threshold", "TRACE");
    ResourceConfig config = environment.jersey().getResourceConfig();
    //    config.property("jersey.config.server.tracing", "ALL");
    //        config.property("jersey.config.server.tracing.threshold", "TRACE");
    //        environment.jersey().register(new LoggingFilter(
    //                java.util.logging.Logger.getLogger(LoggingFilter.class.getName()),
    //                true)
    //        );

    final AuthDynamicFeature bearer = new AuthDynamicFeature(
            new OAuthCredentialAuthFilter.Builder<Auth>().setUserDao(userDao).setAuthenticator(authenticator).setAuthorizer(new SimpleAuthorizer())
                                                         .setRealm("SUPER SECRET STUFF").setPrefix("Bearer").buildAuthFilter());
    environment.jersey().register(bearer);
    environment.jersey().register(RolesAllowedDynamicFeature.class);
    //If you want to use @Auth to inject a custom Principal type into your resource
    environment.jersey().register(new AuthValueFactoryProvider.Binder<>(Auth.class));

    UserController userController = new UserController(userDao);
    environment.jersey().register(userController);

    // If you want to use @Auth to inject a custom Principal type into your resource

    environment.getApplicationContext().setSessionHandler(new SessionHandler());
    environment.jersey().register(new ViewMessageBodyWriter(new MetricRegistry(), new LinkedList<ViewRenderer>()));
    //        environment.jersey().register(DoCheckAuthFeature.class);
    environment.jersey().packages("com.inf.loginresource");

    // nothing to do yet
    final RegexValueTransformer valueTransformer = new RegexValueTransformer();

    XlsFileService xlsFileService = new XlsFileService(valueTransformer, dataDao, objectMapper);
    xlsFileService.setUserDao(userDao);

    CategoryController categoryController = new CategoryController();
    categoryController.setDataDao(dataDao);
    categoryController.setObjectMapper(objectMapper);
    categoryController.setXlsFileService(xlsFileService);
    environment.jersey().register(categoryController);

    final Map<String, String> params = new HashMap<>();
    params.put("Access-Control-Allow-Origin", "/*");
    params.put("Access-Control-Allow-Credentials", "true");
    params.put("Access-Control-Expose-Headers", "true");
    params.put("Access-Control-Allow-Headers", "Content-Type, X-Requested-With");
    params.put("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
    environment.servlets().addFilter("cors", CrossOriginFilter.class).setInitParameters(params);

    //    CrossOriginFilter component = new CrossOriginFilter();
    //    environment.servlets().addFilter("cors", component).setInitParameters(params);

    QuestionnaireController ques = new QuestionnaireController();
    ques.setDataDao(dataDao);
    ques.setXlsFileService(xlsFileService);
    ques.setObjectMapper(objectMapper);
    environment.jersey().register(ques);

    environment.jersey().register(categoryController);
    QuestionController questionController = new QuestionController();
    questionController.setDataDao(dataDao);
    questionController.setUserDao(userDao);
    questionController.setJedisPool(jedisPool);
    questionController.setJedisPassword(jedisPassword);
    environment.jersey().register(questionController);

    //
    //    if (!new File("/tmp/getty").exists()) {
    //      boolean res = new File("/tmp/getty").mkdir();
    //      if (!res) {
    //        logger.error("Failed to make temp dir for images");
    //        throw new RuntimeException("Failed to make temp dir for images");
    //      }
    //    }
    ImageController imageController = new ImageController(configuration, weedFsClient);
    environment.jersey().register(imageController);

    ScrapeManager scrapeManager = new ScrapeManager();
    scrapeManager.setDataDao(dataDao);
    environment.jersey().register(scrapeManager);

    ScrapeController scrapeController = new ScrapeController();
    scrapeManager.setDataDao(dataDao);
    scrapeManager.setValueTransformer(valueTransformer);
    scrapeController.setScrapeManager(scrapeManager);

    environment.jersey().register(scrapeController);

    SurveyManagerImpl surveyManager = new SurveyManagerImpl();
    surveyManager.setDataDao(dataDao);
    surveyManager.setJedisPool(jedisPool);
    surveyManager.setJedisPassword(jedisPassword);
    surveyManager.setConfiguration(configuration);

    RootController rootController = new RootController();
    rootController.setSurveyManager(surveyManager);

    environment.jersey().register(rootController);
    SurveyController surveyController = new SurveyController();

    SurveyDaoImpl surveyDao = new SurveyDaoImpl();
    surveyDao.setDataDao(dataDao);
    surveyDao.setJdbcTemplate(jdbc);

    surveyManager.setSurveyDao(surveyDao);
    surveyManager.setDataDao(dataDao);
    valueTransformer.setTransformationList(dataDao.getValueTransformers());
    surveyManager.setValueTransformer(valueTransformer);
    surveyController.setSurveyManager(surveyManager);
    surveyController.setObjectMapper(objectMapper);
    environment.jersey().register(surveyController);

    HyperGraph graph = new HyperGraph(configuration.getGraphDatabaseConfig().getPath());

    NlpService nlpService = new NlpService(configuration.getSympy().getServer(), restTemplate);

    MessengerStorage messengerStorage = new MessengerStorage(jedisPool, jedisPassword, objectMapper, graph);

    ChatEngine chatEngine = new ChatEngine(nlpService, messengerStorage, surveyManager);
    MessengerService messengerService = new MessengerService(configuration.getMessengerConfig(), messengerStorage, chatEngine, surveyManager);

    MessengerController messengerController = new MessengerController(messengerService, objectMapper);

    environment.jersey().register(messengerController);

    environment.jersey().getResourceConfig().logComponents();

  }

  private JdbcTemplate initDb(InfConfiguration configuration) throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {
    final HikariConfig hikariConfig = new HikariConfig();
    DatabaseConfig dbConfig = configuration.getDatabaseConfig();
    hikariConfig.setJdbcUrl(dbConfig.getJdbcUrl());
    hikariConfig.setUsername(dbConfig.getUser());
    hikariConfig.setPassword(dbConfig.getPassword());
    hikariConfig.setConnectionTestQuery("select 1 from dual");
    hikariConfig.setValidationTimeout(250);
    Properties healthCheckProperties = new Properties();
    healthCheckProperties.put("testOnBorrow", true);
    hikariConfig.setHealthCheckProperties(healthCheckProperties);
    hikariConfig.setConnectionTimeout(250);
    HikariDataSource dataSource1 = new HikariDataSource(hikariConfig);
    return new JdbcTemplate(dataSource1);
  }
}
