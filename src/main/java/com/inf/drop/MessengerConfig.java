package com.inf.drop;

public class MessengerConfig {

    private String appSecret;

    private String verifyToken;

    private String pageAccessToken;

    public MessengerConfig(String appSecret, String verifyToken, String pageAccessToken) {

        this.appSecret = appSecret;
        this.verifyToken = verifyToken;
        this.pageAccessToken = pageAccessToken;
    }
    public MessengerConfig() {

        this.appSecret = "";
        this.verifyToken = "";
        this.pageAccessToken = "";
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }

    public String getVerifyToken() {
        return verifyToken;
    }

    public void setVerifyToken(String verifyToken) {
        this.verifyToken = verifyToken;
    }

    public String getPageAccessToken() {
        return pageAccessToken;
    }

    public void setPageAccessToken(String pageAccessToken) {
        this.pageAccessToken = pageAccessToken;
    }
}
