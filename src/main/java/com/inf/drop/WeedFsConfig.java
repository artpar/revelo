package com.inf.drop;

/**
 * Created by artpar on 5/24/17.
 */
public class WeedFsConfig {
  private String endpoint;

  public String getEndpoint() {
    return endpoint;
  }

  public void setEndpoint(String endpoint) {
    this.endpoint = endpoint;
  }
}
