package com.inf.discovery.scraper;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.jsoup.nodes.Document;

import com.inf.data.Scrape;
import com.inf.data.dao.impl.DataDao;
import com.inf.data.page.PageInfo;

/**
 * author parth.mudgal on 18/03/14.
 */
public class ProductScrapeActivity extends AbstractScrapeActivity
{
	protected Map<PageInfo, List<String>> resultMap;
	private Map<String, String> valueMap;

	public ProductScrapeActivity(Scrape scrape, DataDao dataDao)
	{
		super(scrape, dataDao, "product");
	}

	@Override
	public void scrape() throws Exception
	{
		resultMap = new HashMap<PageInfo, List<String>>();
		valueMap = new HashMap<String, String>();
		for (PageInfo pageInfo : pageInfoList)
		{
			final LinkedList<String> resultList = new LinkedList<String>();
			Document document = getDocument(pageInfo);

			final String template = pageInfo.getPageSelector();
			VelocityContext context = new VelocityContext();

			context.put("html", document);
			context.put("value", valueMap);

			final StringWriter out = new StringWriter();
			Velocity.evaluate(context, out, "Product", template);
			logger.info("Value map - {}", valueMap.toString());
		}
	}

	@Override
	public void store() throws Exception
	{
		String company = valueMap.get("company");
		String category = valueMap.get("category");
		Integer catId = dataDao.getCategoryByName(category).getId();
		String product = valueMap.get("product");
		valueMap.remove("company");
		valueMap.remove("category");
		valueMap.remove("product");
		for (String key : valueMap.keySet())
		{
			String value = valueMap.get(key);
			dataDao.updateProductInfo(catId, company, product, key, value, scrape.getLink(), "scarpe");
		}
		dataDao.markDone(scrape.getId(), SCRAPE_TYPE);
	}
}
