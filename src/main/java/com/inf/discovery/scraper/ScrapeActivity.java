package com.inf.discovery.scraper;

/**
 * author parth.mudgal on 18/03/14.
 */
interface ScrapeActivity {
  void scrape() throws Exception;

  void store() throws Exception;
}
