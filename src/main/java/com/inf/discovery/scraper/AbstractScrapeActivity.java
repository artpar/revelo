package com.inf.discovery.scraper;

import com.inf.data.Product;
import com.inf.data.Scrape;
import com.inf.data.dao.impl.DataDao;
import com.inf.data.page.PageInfo;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * author parth.mudgal on 18/03/14.
 */
public abstract class AbstractScrapeActivity implements ScrapeActivity, Runnable {
  final protected DataDao dataDao;
  final protected Product product;
  final List<PageInfo> pageInfoList;
  final Scrape scrape;
  final String SCRAPE_TYPE;
  protected Logger logger = LoggerFactory.getLogger(AbstractScrapeActivity.class);
  private Map<PageInfo, List<String>> resultMap;

  AbstractScrapeActivity(Scrape scrape, DataDao dataDao, String scrapeType) {
    this.pageInfoList = dataDao.getPageInfo(scrape.getScrapeWebsiteId(), scrapeType);
    this.scrape = scrape;
    this.dataDao = dataDao;
    this.SCRAPE_TYPE = scrapeType;

    product = new Product();
    product.setName("");
    product.setCompanyName("");
    product.setId("0");
  }

  private void updateHtml(String html) {
    logger.info("Updating page html for {} ", scrape.getLink());
    dataDao.updateHtml(scrape.getId(), html, this.SCRAPE_TYPE);
    scrape.setHtml(html);
  }

  @Override
  public void run() {
    logger.info("{} Crawl run", SCRAPE_TYPE);
    try {
      scrape();
      store();
    } catch (Exception e) {
      logger.error("Exception while running scrape activity", e);
    }
  }

  Document getDocument(PageInfo p) throws IOException {
    final String searchUrlTemplate = scrape.getLink();
    Document document;
    if (scrape.getHtml() == null || scrape.getHtml().length() < 10) {
      logger.info("Making call to website - {}", searchUrlTemplate);
      document =
          Jsoup.connect(searchUrlTemplate)
              .header("User-Agent",
                  "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.131 Safari/537.36")
              .header("Accept-Encoding", "gzip,deflate,sdch").get();
      updateHtml(document.outerHtml());
    } else {
      logger.info("Used cached html for - {}", scrape.getLink());
      document = Jsoup.parse(scrape.getHtml());
      document.setBaseUri(searchUrlTemplate);
    }
    return document;
  }

  @Override
  public void scrape() throws Exception {
    resultMap = new HashMap<PageInfo, List<String>>();
    for (PageInfo pageInfo : pageInfoList) {
      final LinkedList<String> resultList = new LinkedList<String>();
      Document document = getDocument(pageInfo);

      Elements elements = document.select(pageInfo.getPageSelector());
      if (elements.size() < 1) {
        logger.error(String.format("Element not found - %s when scraping %s looking for %s on %s",
            pageInfo.getPageSelector(), scrape.getLink(), pageInfo.getResultType(), SCRAPE_TYPE));
        continue;
      }
      for (Element element : elements) {
        String href = element.attr("href");
        if ("#".equals(href)) {
          continue;
        }
        resultList.add(element.attr("abs:href"));
      }
      resultMap.put(pageInfo, resultList);
    }
  }

  @Override
  public void store() throws Exception {
    if (resultMap.keySet().size() < 1) {
      throw new Exception("Should not call store before scrape");

    }
    for (PageInfo pageInfo : resultMap.keySet()) {
      List<String> result = resultMap.get(pageInfo);

      if (result == null || result.size() < 1) {
        continue;
      }
      for (String link : result) {
        dataDao.addNewScrapeActivity(scrape.getScrapeWebsiteId(), link, pageInfo.getResultType());
      }
    }
    dataDao.markDone(scrape.getId(), SCRAPE_TYPE);
  }
}
