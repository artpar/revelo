package com.inf.discovery.scraper;

import java.util.List;
import java.util.Map;

import com.inf.data.dao.impl.DataDao;
import com.inf.data.Scrape;
import com.inf.data.page.PageInfo;

/**
 * author parth.mudgal on 18/03/14.
 */
public class CategoryScrapeActivity extends AbstractScrapeActivity
{
	protected Map<PageInfo, List<String>> resultMap;

	public CategoryScrapeActivity(Scrape scrape, DataDao dataDao)
	{
		super(scrape, dataDao, "category");
	}

}
