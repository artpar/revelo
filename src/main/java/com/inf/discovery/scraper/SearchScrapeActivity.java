package com.inf.discovery.scraper;

import com.inf.data.dao.impl.DataDao;
import com.inf.data.Scrape;

/**
 * author parth.mudgal on 18/03/14.
 */
public class SearchScrapeActivity extends AbstractScrapeActivity
{

	public SearchScrapeActivity(Scrape scrape, DataDao dataDao)
	{
		super(scrape, dataDao, "search");
	}

}
