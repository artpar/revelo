package com.inf.discovery.scraper;

import com.inf.data.Product;
import com.inf.data.dao.impl.DataDao;
import com.inf.data.dao.impl.FeatureValue;
import com.inf.exceptions.InvalidCategoryException;
import com.inf.survey.RegexValueTransformer;
import com.inf.survey.ValueTransformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;

/**
 * author parth.mudgal on 19/03/14.
 */
public class ScrapeManager {
  private DataDao dataDao;

  private ValueTransformer valueTransformer;
  private Logger logger = LoggerFactory.getLogger(ScrapeManager.class);

  public void setDataDao(DataDao dataDao) {
    this.dataDao = dataDao;
  }

  public void run(String scrapeType, String scrapeWebsiteId) throws ClassNotFoundException, NoSuchMethodException,
      InvocationTargetException, InstantiationException, IllegalAccessException {
    AbstractScrapeActivity scrapeActivity = dataDao.getScrapeActivity(scrapeType, scrapeWebsiteId);
    new Thread(scrapeActivity).start();
  }

  public void transformValues(Integer categoryId) throws InvalidCategoryException, IOException {
    valueTransformer.setTransformationList(dataDao.getValueTransformers());
    List<Product> products = dataDao.getProductsByCategoryId(categoryId);
    for (Product product : products) {
      logger.info("For {}", product.getName());
      Map<String, String> features = dataDao.getProductFeaturesWithNullFeatures(product);
      for (String feature : features.keySet()) {
        final String originalFeatureValue = features.get(feature);
        Double transformedValue = valueTransformer.transform(originalFeatureValue);
        if (transformedValue != null) {
          logger.info("Transformed {} to {} for {}", originalFeatureValue, transformedValue, feature);
          dataDao.setTransformedValue(product.getCategoryId(), product.getCompanyName(), product.getName(),
              feature, transformedValue);
        } else {
          logger.info("Unable to transform {} - {}", feature, originalFeatureValue);
        }
      }
    }
  }

  public void setValueTransformer(RegexValueTransformer valueTransformer) {
    this.valueTransformer = valueTransformer;
  }

  public List<FeatureValue> getFeatureValues(String referenceId, String featureName) throws InvalidCategoryException, IOException {
    return dataDao.getFeatureValues(dataDao.getCategoryByReferenceId(referenceId).getId(), featureName);
  }
}
