package com.inf.auth;

import com.google.common.base.Optional;
import com.inf.data.dao.impl.UserDao;
import com.inf.model.security.Auth;
import io.artpar.curd.User;
import io.dropwizard.auth.AuthFilter;
import io.dropwizard.auth.AuthenticationException;
import io.dropwizard.auth.PrincipalImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Priority;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.SecurityContext;
import java.io.IOException;
import java.security.Principal;
import java.util.LinkedList;

@Priority(999)
public class OAuthCredentialAuthFilter<P extends Principal> extends AuthFilter<String, P> {
  private static final Logger LOGGER = LoggerFactory.getLogger(OAuthCredentialAuthFilter.class);
  private UserDao userDao;

  private OAuthCredentialAuthFilter(UserDao userDao) {
    this.userDao = userDao;
  }

  public void filter(final ContainerRequestContext requestContext) throws IOException {
    String header = (String) requestContext.getHeaders().getFirst("Authorization");
    if (header != null) {
      try {
        int e = header.indexOf(32);
        if (e > 0) {
          String method = header.substring(0, e);
          if (this.prefix.equalsIgnoreCase(method)) {
            String credentials = header.substring(e + 1);
            final Optional principal = this.authenticator.authenticate(credentials);
            if (principal.isPresent()) {
              requestContext.setSecurityContext(new SecurityContext() {
                public Principal getUserPrincipal() {
                  return (Principal) principal.get();
                }

                public boolean isUserInRole(String role) {
                  return OAuthCredentialAuthFilter.this.authorizer.authorize((P) principal.get(), role);
                }

                public boolean isSecure() {
                  return requestContext.getSecurityContext().isSecure();
                }

                public String getAuthenticationScheme() {
                  return "BASIC";
                }
              });

              final User user = new User(((Auth) principal.get()).getUserByEmail().getReferenceId(), userDao.getGroupIdsOfUser(((Auth) principal.get()).getUserByEmail().getId()));
              requestContext.setProperty("user", user);
              return;
            }
          }
        }
      } catch (AuthenticationException var7) {
        LOGGER.warn("Error authenticating credentials", var7);
        throw new InternalServerErrorException();
      }
    }

    requestContext.setSecurityContext(new SecurityContext() {
      public Principal getUserPrincipal() {
        return new PrincipalImpl("guest");
      }

      public boolean isUserInRole(String role) {
        return OAuthCredentialAuthFilter.this.authorizer.authorize(null, role);
      }

      public boolean isSecure() {
        return false;
      }

      public String getAuthenticationScheme() {
        return "NONE";
      }
    });

    requestContext.setProperty("user", new User("0", new LinkedList<>()));

//    throw new WebApplicationException(this.unauthorizedHandler.buildResponse(this.prefix, this.realm));
  }

  public static class Builder<P extends Principal> extends AuthFilterBuilder<String, P, OAuthCredentialAuthFilter<P>> {
    private UserDao userDao;

    public Builder() {
    }

    public Builder<P> setUserDao(UserDao userDao) {
      this.userDao = userDao;
      return this;
    }

    protected OAuthCredentialAuthFilter<P> newInstance() {
      return new OAuthCredentialAuthFilter(this.userDao);
    }
  }
}
