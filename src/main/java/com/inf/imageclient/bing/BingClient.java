package com.inf.imageclient.bing;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.LinkedList;
import java.util.List;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.inf.cache.CacheInterface;
import com.inf.interfaces.AbstractImageCacherClient;
import com.inf.interfaces.ViewImage;
import org.apache.commons.lang.NotImplementedException;
import org.glassfish.jersey.client.JerseyClientBuilder;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by artpar
 */
public class BingClient extends AbstractImageCacherClient {
  private final Client client;

  // ?q=laptop&format=htmlraw&first=10
  String BING_URL = "http://www.bing.com/images/async";

  private ObjectMapper objectMapper;

  private Logger logger = LoggerFactory.getLogger(BingClient.class);

  public BingClient(CacheInterface<String, String> cache, ObjectMapper objectMapper) {
    super(cache, objectMapper);
    this.client = new JerseyClientBuilder().build();
    ;
    this.objectMapper = objectMapper;
  }

  @Override
  public List<ViewImage> getImagesForQuery(String token) {

    CheckCache<List<ViewImage>> checkCache = new CheckCache<List<ViewImage>>(AbstractImageCacherClient.CACHE_PREFIX_IMAGE_SET + token).invoke();
    if (checkCache.is()) {
      return checkCache.getCachedObject();
    }

    List<ViewImage> resultsFromBing = queryBing(token);

    PutCache(AbstractImageCacherClient.CACHE_PREFIX_IMAGE_SET + token, resultsFromBing);

    return resultsFromBing;
  }

  @Override
  public String uploadImage(InputStream inputStream) {
    throw new NotImplementedException();
  }

  @Override
  public List<ViewImage> getImagesForQuery(Integer category, String company, String product) {
    return getImagesForQuery(category + " " + company + " " + product);
  }

  private List<ViewImage> queryBing(String token) {

    WebTarget webTarget = null;
    try {
      String uri = BING_URL + String.format("?q=%s&format=htmlraw&first=10", URLEncoder.encode(token, "UTF-8"));
      logger.info("Final bing url for images: {}", uri);
      webTarget = client.target(uri);
    } catch (UnsupportedEncodingException e) {
      logger.error("Failed to encode string ", e);
      return new LinkedList<>();
    }
    Invocation.Builder invocationBuilder = webTarget.request(MediaType.TEXT_HTML_TYPE);
    Response response = invocationBuilder.get();
    String responseHtml = response.readEntity(String.class);

    logger.info("Response for [{}]\n{}", token, responseHtml);
    Document doc = Jsoup.parse(responseHtml);
    List<ViewImage> list = new LinkedList<>();

    Elements as = doc.select("a");

    for (Element element : as) {
      if (!element.hasAttr("m")) {
        continue;
      }
      String attrM = element.attr("m");
      try {
        BingImageResult bingImageResult = objectMapper.readValue(attrM, BingImageResult.class);
        ViewImage viewImage = new ViewImage();

        viewImage.setUrl(bingImageResult.getMurl());

        viewImage.setToken(bingImageResult.getMd5());
        PutCache(AbstractImageCacherClient.CACHE_PREFIX_IMAGE_REF + viewImage.getToken(), viewImage.getUrl());
        list.add(viewImage);
      } catch (IOException e) {
        logger.error("Failed query for {}", e);
      }
    }

    for (ViewImage viewImage : list) {
      logger.info("Result for {} => {}", token, viewImage.getUrl());
    }

    return list;
  }

}
