package com.inf.imageclient.bing.result;

public class Thumbnail {
  private String thumbnailUrl;

  public String getThumbnailUrl() {
    return thumbnailUrl;
  }

  public void setThumbnailUrl(String thumbnailUrl) {
    this.thumbnailUrl = thumbnailUrl;
  }

  @Override
  public String toString() {
    return "ClassPojo [thumbnailUrl = " + thumbnailUrl + "]";
  }
}

			