package com.inf.imageclient.bing.result;

public class InsightsMetadata {
  private String recipeSourcesCount;

  public String getRecipeSourcesCount() {
    return recipeSourcesCount;
  }

  public void setRecipeSourcesCount(String recipeSourcesCount) {
    this.recipeSourcesCount = recipeSourcesCount;
  }

  @Override
  public String toString() {
    return "ClassPojo [recipeSourcesCount = " + recipeSourcesCount + "]";
  }
}

			