package com.inf.imageclient.bing.result;

public class Value {
  private String contentSize;

  private String contentUrl;

  private String datePublished;

  private String width;

  private String encodingFormat;

  private InsightsMetadata insightsMetadata;

  private String accentColor;

  private String hostPageUrl;

  private String hostPageDisplayUrl;

  private String imageInsightsToken;

  private String webSearchUrl;

  private String imageId;

  private String height;

  private Thumbnail thumbnail;

  private String name;

  private String thumbnailUrl;

  public String getContentSize() {
    return contentSize;
  }

  public void setContentSize(String contentSize) {
    this.contentSize = contentSize;
  }

  public String getContentUrl() {
    return contentUrl;
  }

  public void setContentUrl(String contentUrl) {
    this.contentUrl = contentUrl;
  }

  public String getDatePublished() {
    return datePublished;
  }

  public void setDatePublished(String datePublished) {
    this.datePublished = datePublished;
  }

  public String getWidth() {
    return width;
  }

  public void setWidth(String width) {
    this.width = width;
  }

  public String getEncodingFormat() {
    return encodingFormat;
  }

  public void setEncodingFormat(String encodingFormat) {
    this.encodingFormat = encodingFormat;
  }

  public InsightsMetadata getInsightsMetadata() {
    return insightsMetadata;
  }

  public void setInsightsMetadata(InsightsMetadata insightsMetadata) {
    this.insightsMetadata = insightsMetadata;
  }

  public String getAccentColor() {
    return accentColor;
  }

  public void setAccentColor(String accentColor) {
    this.accentColor = accentColor;
  }

  public String getHostPageUrl() {
    return hostPageUrl;
  }

  public void setHostPageUrl(String hostPageUrl) {
    this.hostPageUrl = hostPageUrl;
  }

  public String getHostPageDisplayUrl() {
    return hostPageDisplayUrl;
  }

  public void setHostPageDisplayUrl(String hostPageDisplayUrl) {
    this.hostPageDisplayUrl = hostPageDisplayUrl;
  }

  public String getImageInsightsToken() {
    return imageInsightsToken;
  }

  public void setImageInsightsToken(String imageInsightsToken) {
    this.imageInsightsToken = imageInsightsToken;
  }

  public String getWebSearchUrl() {
    return webSearchUrl;
  }

  public void setWebSearchUrl(String webSearchUrl) {
    this.webSearchUrl = webSearchUrl;
  }

  public String getImageId() {
    return imageId;
  }

  public void setImageId(String imageId) {
    this.imageId = imageId;
  }

  public String getHeight() {
    return height;
  }

  public void setHeight(String height) {
    this.height = height;
  }

  public Thumbnail getThumbnail() {
    return thumbnail;
  }

  public void setThumbnail(Thumbnail thumbnail) {
    this.thumbnail = thumbnail;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getThumbnailUrl() {
    return thumbnailUrl;
  }

  public void setThumbnailUrl(String thumbnailUrl) {
    this.thumbnailUrl = thumbnailUrl;
  }

  @Override
  public String toString() {
    return "ClassPojo [contentSize = " + contentSize + ", contentUrl = " + contentUrl + ", datePublished = " + datePublished + ", width = " + width + ", encodingFormat = " + encodingFormat + ", insightsMetadata = " + insightsMetadata + ", accentColor = " + accentColor + ", hostPageUrl = " + hostPageUrl + ", hostPageDisplayUrl = " + hostPageDisplayUrl + ", imageInsightsToken = " + imageInsightsToken + ", webSearchUrl = " + webSearchUrl + ", imageId = " + imageId + ", height = " + height + ", thumbnail = " + thumbnail + ", name = " + name + ", thumbnailUrl = " + thumbnailUrl + "]";
  }
}

			