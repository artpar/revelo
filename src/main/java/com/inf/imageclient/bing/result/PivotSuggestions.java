package com.inf.imageclient.bing.result;

public class PivotSuggestions
{
    private String pivot;

    private Suggestions[] suggestions;

    public String getPivot ()
    {
        return pivot;
    }

    public void setPivot (String pivot)
    {
        this.pivot = pivot;
    }

    public Suggestions[] getSuggestions ()
    {
        return suggestions;
    }

    public void setSuggestions (Suggestions[] suggestions)
    {
        this.suggestions = suggestions;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [pivot = "+pivot+", suggestions = "+suggestions+"]";
    }
}

			