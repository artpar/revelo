package com.inf.imageclient.bing.result;

public class RelatedSearches {
  private String searchLink;

  private String webSearchUrl;

  private String text;

  private Thumbnail thumbnail;

  private String displayText;

  public String getSearchLink() {
    return searchLink;
  }

  public void setSearchLink(String searchLink) {
    this.searchLink = searchLink;
  }

  public String getWebSearchUrl() {
    return webSearchUrl;
  }

  public void setWebSearchUrl(String webSearchUrl) {
    this.webSearchUrl = webSearchUrl;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public Thumbnail getThumbnail() {
    return thumbnail;
  }

  public void setThumbnail(Thumbnail thumbnail) {
    this.thumbnail = thumbnail;
  }

  public String getDisplayText() {
    return displayText;
  }

  public void setDisplayText(String displayText) {
    this.displayText = displayText;
  }

  @Override
  public String toString() {
    return "ClassPojo [searchLink = " + searchLink + ", webSearchUrl = " + webSearchUrl + ", text = " + text + ", thumbnail = " + thumbnail + ", displayText = " + displayText + "]";
  }
}
