package com.inf.imageclient.bing.result;

public class BingImageSearchResultResponse {
  private RelatedSearches[] relatedSearches;

  private String webSearchUrl;

  private String _type;

  private String totalEstimatedMatches;

  private Value[] value;

  private String nextOffset;

  private QueryExpansions[] queryExpansions;

  private String instrumentation;

  private String readLink;

  private SimilarTerms[] similarTerms;

  private PivotSuggestions[] pivotSuggestions;

  public RelatedSearches[] getRelatedSearches() {
    return relatedSearches;
  }

  public void setRelatedSearches(RelatedSearches[] relatedSearches) {
    this.relatedSearches = relatedSearches;
  }

  public String getWebSearchUrl() {
    return webSearchUrl;
  }

  public void setWebSearchUrl(String webSearchUrl) {
    this.webSearchUrl = webSearchUrl;
  }

  public String get_type() {
    return _type;
  }

  public void set_type(String _type) {
    this._type = _type;
  }

  public String getTotalEstimatedMatches() {
    return totalEstimatedMatches;
  }

  public void setTotalEstimatedMatches(String totalEstimatedMatches) {
    this.totalEstimatedMatches = totalEstimatedMatches;
  }

  public Value[] getValue() {
    return value;
  }

  public void setValue(Value[] value) {
    this.value = value;
  }

  public String getNextOffset() {
    return nextOffset;
  }

  public void setNextOffset(String nextOffset) {
    this.nextOffset = nextOffset;
  }

  public QueryExpansions[] getQueryExpansions() {
    return queryExpansions;
  }

  public void setQueryExpansions(QueryExpansions[] queryExpansions) {
    this.queryExpansions = queryExpansions;
  }

  public String getInstrumentation() {
    return instrumentation;
  }

  public void setInstrumentation(String instrumentation) {
    this.instrumentation = instrumentation;
  }

  public String getReadLink() {
    return readLink;
  }

  public void setReadLink(String readLink) {
    this.readLink = readLink;
  }

  public SimilarTerms[] getSimilarTerms() {
    return similarTerms;
  }

  public void setSimilarTerms(SimilarTerms[] similarTerms) {
    this.similarTerms = similarTerms;
  }

  public PivotSuggestions[] getPivotSuggestions() {
    return pivotSuggestions;
  }

  public void setPivotSuggestions(PivotSuggestions[] pivotSuggestions) {
    this.pivotSuggestions = pivotSuggestions;
  }

  @Override
  public String toString() {
    return "ClassPojo [relatedSearches = " + relatedSearches + ", webSearchUrl = " + webSearchUrl + ", _type = " + _type + ", totalEstimatedMatches = " + totalEstimatedMatches + ", value = " + value + ", nextOffset = " + nextOffset + ", queryExpansions = " + queryExpansions + ", instrumentation = " + instrumentation + ", readLink = " + readLink + ", similarTerms = " + similarTerms + ", pivotSuggestions = " + pivotSuggestions + "]";
  }
}