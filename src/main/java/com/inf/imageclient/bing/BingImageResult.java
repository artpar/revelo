package com.inf.imageclient.bing;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by artpar
 */
@JsonIgnoreProperties(ignoreUnknown = true)
class BingImageResult {
  private String cid;
  private String md5;
  private String purl;
  private String murl;
  private String turl;

  public String getCid() {
    return cid;
  }

  public void setCid(String cid) {
    this.cid = cid;
  }

  public String getMd5() {
    return md5;
  }

  public void setMd5(String md5) {
    this.md5 = md5;
  }

  public String getPurl() {
    return purl;
  }

  public void setPurl(String purl) {
    this.purl = purl;
  }

  public String getMurl() {
    return murl;
  }

  public void setMurl(String murl) {
    this.murl = murl;
  }

  public String getTurl() {
    return turl;
  }

  public void setTurl(String turl) {
    this.turl = turl;
  }
}
