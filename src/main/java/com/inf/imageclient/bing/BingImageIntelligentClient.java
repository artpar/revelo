package com.inf.imageclient.bing;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.LinkedList;
import java.util.List;
import javax.net.ssl.HttpsURLConnection;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.inf.cache.CacheInterface;
import com.inf.imageclient.bing.result.BingImageSearchResultResponse;
import com.inf.imageclient.bing.result.Value;
import com.inf.imageclient.getty.ImageNotFound;
import com.inf.interfaces.AbstractImageCacherClient;
import com.inf.interfaces.ViewImage;
import org.springframework.util.StreamUtils;

/*
 * Gson: https://github.com/google/gson
 * Maven info:
 *     groupId: com.google.code.gson
 *     artifactId: gson
 *     version: 2.8.1
 *
 * Once you have compiled or downloaded gson-2.8.1.jar, assuming you have placed it in the
 * same folder as this file (BingImageIntelligentClient.java), you can compile and run this program at
 * the command line as follows.
 *
 * javac BingImageIntelligentClient.java -classpath .;gson-2.8.1.jar -encoding UTF-8
 * java -cp .;gson-2.8.1.jar BingImageIntelligentClient
 */

public class BingImageIntelligentClient extends AbstractImageCacherClient {
  // Verify the endpoint URI.  At this writing, only one endpoint is used for Bing
  // search APIs.  In the future, regional endpoints may be available.  If you
  // encounter unexpected authorization errors, double-check this value against
  // the endpoint for your Bing Web search instance in your Azure dashboard.
  static String host = "https://api.cognitive.microsoft.com";

  // ***********************************************
  // *** Update or verify the following values. ***
  // **********************************************

  static String path = "/bing/v7.0/images/search";

  // Replace the subscriptionKey string value with your valid subscription key.
  private final String subscriptionKey;

  private ObjectMapper objectMapper;

  public BingImageIntelligentClient(String subscriptionKey, ObjectMapper objectMapper, CacheInterface<String, String> cache) {
    super(cache, objectMapper);
    this.subscriptionKey = subscriptionKey;
    this.objectMapper = objectMapper;
  }

  private BingImageSearchResultResponse searchImages(String searchQuery) throws Exception {
    // construct URL of search request (endpoint + query string)
    URL url = new URL(host + path + "?q=" + URLEncoder.encode(searchQuery, "UTF-8"));
    HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
    connection.setRequestProperty("Ocp-Apim-Subscription-Key", this.subscriptionKey);

    // receive JSON body
    InputStream stream = connection.getInputStream();
    String response = StreamUtils.copyToString(stream, Charset.defaultCharset());

    // construct result object for return
    BingImageSearchResultResponse results = objectMapper.readValue(response, BingImageSearchResultResponse.class);
    stream.close();
    return results;
  }

  // pretty-printer for JSON; uses GSON parser to parse and re-serialize
  public String prettify(String json_text) {
    JsonParser parser = new JsonParser();
    JsonObject json = parser.parse(json_text).getAsJsonObject();
    Gson gson = new GsonBuilder().setPrettyPrinting().create();
    return gson.toJson(json);
  }

  @Override
  public List<ViewImage> getImagesForQuery(String token) throws ImageNotFound, Exception {
    BingImageSearchResultResponse response = this.searchImages(token);

    List<ViewImage> returnList = new LinkedList<>();

    for (Value value : response.getValue()) {
      ViewImage viewImage = new ViewImage();
      viewImage.setToken(token);
      viewImage.setUrl(value.getContentUrl());
      returnList.add(viewImage);
    }

    return returnList;
  }

  @Override
  public String uploadImage(InputStream inputStream) throws IOException {
    return null;
  }

  @Override
  public List<ViewImage> getImagesForQuery(Integer category, String company, String product) throws Exception, ImageNotFound {
    return getImagesForQuery(category + " " + company + " " + product);
  }
}
