package com.inf.imageclient.getty;

import java.io.IOException;

/**
 * Created by parth on 29/4/16.
 */
public class ImageNotFound extends Throwable {
    public ImageNotFound(String referenceId) {
        super(referenceId);
    }

    public ImageNotFound(String s, IOException e) {
        super(s, e);
    }
}
