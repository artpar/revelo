package com.inf.imageclient.getty;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * Created by parth on 29/4/16.
 */
public class InvalidTokenException extends Throwable {
    public InvalidTokenException(String token, UnsupportedEncodingException e) {
    }

    public InvalidTokenException(IOException e) {
        super(e);
    }
}
