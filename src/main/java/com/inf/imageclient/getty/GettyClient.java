package com.inf.imageclient.getty;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.inf.cache.CacheInterface;
import com.inf.interfaces.AbstractImageCacherClient;
import com.inf.interfaces.Oauth2Token;
import com.inf.interfaces.ViewImage;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by parth on 28/4/16.
 */
public class GettyClient extends AbstractImageCacherClient {
  private static Oauth2Token token;

  private static Date lastAttempt = new Date();

  private final String apiKey;

  private final String secret;

  private Logger logger = LoggerFactory.getLogger(GettyClient.class);

  private Integer DELAY = 250;

  private ObjectMapper objectMapper;

  public GettyClient(String apiKey, String secret, CacheInterface<String, String> cache, ObjectMapper objectMapper) {
    super(cache, objectMapper);
    this.apiKey = apiKey;
    this.secret = secret;
    this.objectMapper = objectMapper;
  }

  // private void checkToken() {
  //   if (token == null) {
  //     token = Oauth2Util.GetOauth2Token(apiKey, secret);
  //   }
  //   if (new Date().getTime() > token.getCreatedAt().getTime() + token.getExpiresIn() * 1000) {
  //     token = null;
  //     logger.info("Token is expired");
  //     token = Oauth2Util.GetOauth2Token(apiKey, secret);
  //   }
  // }

  @Override
  public List<ViewImage> getImagesForQuery(String token) {

    CheckCache<List<ViewImage>> checkCache = new CheckCache<List<ViewImage>>(AbstractImageCacherClient.CACHE_PREFIX_IMAGE_SET + token).invoke();
    if (checkCache.is()) {
      return checkCache.getCachedObject();
    }

    String k = checkCache.getK();
    GettyImageResult result;
    try {
      result = queryGetty(token);
    } catch (InvalidTokenException e) {
      return new LinkedList<>();
    } catch (IOException e) {
      return new LinkedList<>();
    } catch (InterruptedException e) {
      return new LinkedList<>();
    }
    List<ViewImage> images = new LinkedList<>();
    for (Image image : result.getImages()) {
      image.getDisplaySizes().stream().filter(displaySize -> !displaySize.getIsWatermarked()).forEach(displaySize -> {
        ViewImage e = new ViewImage();
        String key = "image_" + DigestUtils.shaHex(displaySize.getUri());
        e.setToken(key);
        images.add(e);
        PutCache(AbstractImageCacherClient.CACHE_PREFIX_IMAGE_REF + key, displaySize.getUri());
      });
    }

    PutCache(AbstractImageCacherClient.CACHE_PREFIX_IMAGE_SET + token, result);
    return images;
  }

  @Override
  public String uploadImage(InputStream inputStream) {
    throw new NotImplementedException();
  }

  @Override
  public List<ViewImage> getImagesForQuery(Integer category, String company, String product) throws Exception, ImageNotFound {
    return getImagesForQuery(category + " " + company + " " + product);
  }

  private GettyImageResult queryGetty(String token) throws InvalidTokenException, IOException, InterruptedException {
    token = token.replaceAll("[^a-zA-Z0-9]", " ");
    token = token.replaceAll("([a-z])([A-Z])", "$1 $2");
    long passed = new Date().getTime() - lastAttempt.getTime();
    if (passed < DELAY) {
      Thread.sleep(DELAY - passed);
    }
    String url = "https://api.gettyimages.com/v3/search/images?page_size=100&fields=detail_set,id,title,thumb,referral_destinations&sort_order=best&phrase=" + URLEncoder
            .encode(token, "UTF-8");

    CheckCache<GettyImageResult> checkCache = new CheckCache<GettyImageResult>(url).invoke();
    if (checkCache.is()) {
      return checkCache.getCachedObject();
    }

    //        checkToken();
    URLConnection connection;
    connection = new URL(url).openConnection();
    connection.setRequestProperty("Api-Key", apiKey);

    String output = IOUtils.toString(connection.getInputStream(), Charset.forName("UTF-8"));
    return objectMapper.readValue(output, GettyImageResult.class);
  }

}
