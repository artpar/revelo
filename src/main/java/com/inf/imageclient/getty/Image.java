
package com.inf.imageclient.getty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import javax.validation.Valid;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "allowed_use",
    "artist",
    "asset_family",
    "call_for_image",
    "caption",
    "collection_code",
    "collection_id",
    "collection_name",
    "copyright",
    "date_created",
    "display_sizes",
    "editorial_segments",
    "event_ids",
    "graphical_style",
    "id",
    "license_model",
    "max_dimensions",
    "orientation",
    "product_types",
    "quality_rank",
    "referral_destinations",
    "title"
})
public class Image {

    @JsonProperty("allowed_use")
    @Valid
    private AllowedUse allowedUse;
    @JsonProperty("artist")
    private String artist;
    @JsonProperty("asset_family")
    private String assetFamily;
    @JsonProperty("call_for_image")
    private Boolean callForImage;
    @JsonProperty("caption")
    private String caption;
    @JsonProperty("collection_code")
    private String collectionCode;
    @JsonProperty("collection_id")
    private Integer collectionId;
    @JsonProperty("collection_name")
    private String collectionName;
    @JsonProperty("copyright")
    private String copyright;
    @JsonProperty("date_created")
    private String dateCreated;
    @JsonProperty("display_sizes")
    @Valid
    private List<DisplaySize> displaySizes = new ArrayList<DisplaySize>();
    @JsonProperty("editorial_segments")
    @Valid
    private List<Object> editorialSegments = new ArrayList<Object>();
    @JsonProperty("event_ids")
    @Valid
    private List<Object> eventIds = new ArrayList<Object>();
    @JsonProperty("graphical_style")
    private String graphicalStyle;
    @JsonProperty("id")
    private String id;
    @JsonProperty("license_model")
    private String licenseModel;
    @JsonProperty("max_dimensions")
    @Valid
    private MaxDimensions maxDimensions;
    @JsonProperty("orientation")
    private String orientation;
    @JsonProperty("product_types")
    @Valid
    private List<Object> productTypes = new ArrayList<Object>();
    @JsonProperty("quality_rank")
    private Integer qualityRank;
    @JsonProperty("referral_destinations")
    @Valid
    private List<ReferralDestination> referralDestinations = new ArrayList<ReferralDestination>();
    @JsonProperty("title")
    private String title;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The allowedUse
     */
    @JsonProperty("allowed_use")
    public AllowedUse getAllowedUse() {
        return allowedUse;
    }

    /**
     * 
     * @param allowedUse
     *     The allowed_use
     */
    @JsonProperty("allowed_use")
    public void setAllowedUse(AllowedUse allowedUse) {
        this.allowedUse = allowedUse;
    }

    /**
     * 
     * @return
     *     The artist
     */
    @JsonProperty("artist")
    public String getArtist() {
        return artist;
    }

    /**
     * 
     * @param artist
     *     The artist
     */
    @JsonProperty("artist")
    public void setArtist(String artist) {
        this.artist = artist;
    }

    /**
     * 
     * @return
     *     The assetFamily
     */
    @JsonProperty("asset_family")
    public String getAssetFamily() {
        return assetFamily;
    }

    /**
     * 
     * @param assetFamily
     *     The asset_family
     */
    @JsonProperty("asset_family")
    public void setAssetFamily(String assetFamily) {
        this.assetFamily = assetFamily;
    }

    /**
     * 
     * @return
     *     The callForImage
     */
    @JsonProperty("call_for_image")
    public Boolean getCallForImage() {
        return callForImage;
    }

    /**
     * 
     * @param callForImage
     *     The call_for_image
     */
    @JsonProperty("call_for_image")
    public void setCallForImage(Boolean callForImage) {
        this.callForImage = callForImage;
    }

    /**
     * 
     * @return
     *     The caption
     */
    @JsonProperty("caption")
    public String getCaption() {
        return caption;
    }

    /**
     * 
     * @param caption
     *     The caption
     */
    @JsonProperty("caption")
    public void setCaption(String caption) {
        this.caption = caption;
    }

    /**
     * 
     * @return
     *     The collectionCode
     */
    @JsonProperty("collection_code")
    public String getCollectionCode() {
        return collectionCode;
    }

    /**
     * 
     * @param collectionCode
     *     The collection_code
     */
    @JsonProperty("collection_code")
    public void setCollectionCode(String collectionCode) {
        this.collectionCode = collectionCode;
    }

    /**
     * 
     * @return
     *     The collectionId
     */
    @JsonProperty("collection_id")
    public Integer getCollectionId() {
        return collectionId;
    }

    /**
     * 
     * @param collectionId
     *     The collection_id
     */
    @JsonProperty("collection_id")
    public void setCollectionId(Integer collectionId) {
        this.collectionId = collectionId;
    }

    /**
     * 
     * @return
     *     The collectionName
     */
    @JsonProperty("collection_name")
    public String getCollectionName() {
        return collectionName;
    }

    /**
     * 
     * @param collectionName
     *     The collection_name
     */
    @JsonProperty("collection_name")
    public void setCollectionName(String collectionName) {
        this.collectionName = collectionName;
    }

    /**
     * 
     * @return
     *     The copyright
     */
    @JsonProperty("copyright")
    public String getCopyright() {
        return copyright;
    }

    /**
     * 
     * @param copyright
     *     The copyright
     */
    @JsonProperty("copyright")
    public void setCopyright(String copyright) {
        this.copyright = copyright;
    }

    /**
     * 
     * @return
     *     The dateCreated
     */
    @JsonProperty("date_created")
    public String getDateCreated() {
        return dateCreated;
    }

    /**
     * 
     * @param dateCreated
     *     The date_created
     */
    @JsonProperty("date_created")
    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    /**
     * 
     * @return
     *     The displaySizes
     */
    @JsonProperty("display_sizes")
    public List<DisplaySize> getDisplaySizes() {
        return displaySizes;
    }

    /**
     * 
     * @param displaySizes
     *     The display_sizes
     */
    @JsonProperty("display_sizes")
    public void setDisplaySizes(List<DisplaySize> displaySizes) {
        this.displaySizes = displaySizes;
    }

    /**
     * 
     * @return
     *     The editorialSegments
     */
    @JsonProperty("editorial_segments")
    public List<Object> getEditorialSegments() {
        return editorialSegments;
    }

    /**
     * 
     * @param editorialSegments
     *     The editorial_segments
     */
    @JsonProperty("editorial_segments")
    public void setEditorialSegments(List<Object> editorialSegments) {
        this.editorialSegments = editorialSegments;
    }

    /**
     * 
     * @return
     *     The eventIds
     */
    @JsonProperty("event_ids")
    public List<Object> getEventIds() {
        return eventIds;
    }

    /**
     * 
     * @param eventIds
     *     The event_ids
     */
    @JsonProperty("event_ids")
    public void setEventIds(List<Object> eventIds) {
        this.eventIds = eventIds;
    }

    /**
     * 
     * @return
     *     The graphicalStyle
     */
    @JsonProperty("graphical_style")
    public String getGraphicalStyle() {
        return graphicalStyle;
    }

    /**
     * 
     * @param graphicalStyle
     *     The graphical_style
     */
    @JsonProperty("graphical_style")
    public void setGraphicalStyle(String graphicalStyle) {
        this.graphicalStyle = graphicalStyle;
    }

    /**
     * 
     * @return
     *     The id
     */
    @JsonProperty("id")
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The licenseModel
     */
    @JsonProperty("license_model")
    public String getLicenseModel() {
        return licenseModel;
    }

    /**
     * 
     * @param licenseModel
     *     The license_model
     */
    @JsonProperty("license_model")
    public void setLicenseModel(String licenseModel) {
        this.licenseModel = licenseModel;
    }

    /**
     * 
     * @return
     *     The maxDimensions
     */
    @JsonProperty("max_dimensions")
    public MaxDimensions getMaxDimensions() {
        return maxDimensions;
    }

    /**
     * 
     * @param maxDimensions
     *     The max_dimensions
     */
    @JsonProperty("max_dimensions")
    public void setMaxDimensions(MaxDimensions maxDimensions) {
        this.maxDimensions = maxDimensions;
    }

    /**
     * 
     * @return
     *     The orientation
     */
    @JsonProperty("orientation")
    public String getOrientation() {
        return orientation;
    }

    /**
     * 
     * @param orientation
     *     The orientation
     */
    @JsonProperty("orientation")
    public void setOrientation(String orientation) {
        this.orientation = orientation;
    }

    /**
     * 
     * @return
     *     The productTypes
     */
    @JsonProperty("product_types")
    public List<Object> getProductTypes() {
        return productTypes;
    }

    /**
     * 
     * @param productTypes
     *     The product_types
     */
    @JsonProperty("product_types")
    public void setProductTypes(List<Object> productTypes) {
        this.productTypes = productTypes;
    }

    /**
     * 
     * @return
     *     The qualityRank
     */
    @JsonProperty("quality_rank")
    public Integer getQualityRank() {
        return qualityRank;
    }

    /**
     * 
     * @param qualityRank
     *     The quality_rank
     */
    @JsonProperty("quality_rank")
    public void setQualityRank(Integer qualityRank) {
        this.qualityRank = qualityRank;
    }

    /**
     * 
     * @return
     *     The referralDestinations
     */
    @JsonProperty("referral_destinations")
    public List<ReferralDestination> getReferralDestinations() {
        return referralDestinations;
    }

    /**
     * 
     * @param referralDestinations
     *     The referral_destinations
     */
    @JsonProperty("referral_destinations")
    public void setReferralDestinations(List<ReferralDestination> referralDestinations) {
        this.referralDestinations = referralDestinations;
    }

    /**
     * 
     * @return
     *     The title
     */
    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The title
     */
    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(allowedUse).append(artist).append(assetFamily).append(callForImage).append(caption).append(collectionCode).append(collectionId).append(collectionName).append(copyright).append(dateCreated).append(displaySizes).append(editorialSegments).append(eventIds).append(graphicalStyle).append(id).append(licenseModel).append(maxDimensions).append(orientation).append(productTypes).append(qualityRank).append(referralDestinations).append(title).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Image) == false) {
            return false;
        }
        Image rhs = ((Image) other);
        return new EqualsBuilder().append(allowedUse, rhs.allowedUse).append(artist, rhs.artist).append(assetFamily, rhs.assetFamily).append(callForImage, rhs.callForImage).append(caption, rhs.caption).append(collectionCode, rhs.collectionCode).append(collectionId, rhs.collectionId).append(collectionName, rhs.collectionName).append(copyright, rhs.copyright).append(dateCreated, rhs.dateCreated).append(displaySizes, rhs.displaySizes).append(editorialSegments, rhs.editorialSegments).append(eventIds, rhs.eventIds).append(graphicalStyle, rhs.graphicalStyle).append(id, rhs.id).append(licenseModel, rhs.licenseModel).append(maxDimensions, rhs.maxDimensions).append(orientation, rhs.orientation).append(productTypes, rhs.productTypes).append(qualityRank, rhs.qualityRank).append(referralDestinations, rhs.referralDestinations).append(title, rhs.title).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
