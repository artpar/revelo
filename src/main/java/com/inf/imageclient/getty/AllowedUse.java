
package com.inf.imageclient.getty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import javax.validation.Valid;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "how_can_i_use_it",
    "release_info",
    "usage_restrictions"
})
public class AllowedUse {

    @JsonProperty("how_can_i_use_it")
    private String howCanIUseIt;
    @JsonProperty("release_info")
    private String releaseInfo;
    @JsonProperty("usage_restrictions")
    @Valid
    private List<Object> usageRestrictions = new ArrayList<Object>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The howCanIUseIt
     */
    @JsonProperty("how_can_i_use_it")
    public String getHowCanIUseIt() {
        return howCanIUseIt;
    }

    /**
     * 
     * @param howCanIUseIt
     *     The how_can_i_use_it
     */
    @JsonProperty("how_can_i_use_it")
    public void setHowCanIUseIt(String howCanIUseIt) {
        this.howCanIUseIt = howCanIUseIt;
    }

    /**
     * 
     * @return
     *     The releaseInfo
     */
    @JsonProperty("release_info")
    public String getReleaseInfo() {
        return releaseInfo;
    }

    /**
     * 
     * @param releaseInfo
     *     The release_info
     */
    @JsonProperty("release_info")
    public void setReleaseInfo(String releaseInfo) {
        this.releaseInfo = releaseInfo;
    }

    /**
     * 
     * @return
     *     The usageRestrictions
     */
    @JsonProperty("usage_restrictions")
    public List<Object> getUsageRestrictions() {
        return usageRestrictions;
    }

    /**
     * 
     * @param usageRestrictions
     *     The usage_restrictions
     */
    @JsonProperty("usage_restrictions")
    public void setUsageRestrictions(List<Object> usageRestrictions) {
        this.usageRestrictions = usageRestrictions;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(howCanIUseIt).append(releaseInfo).append(usageRestrictions).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof AllowedUse) == false) {
            return false;
        }
        AllowedUse rhs = ((AllowedUse) other);
        return new EqualsBuilder().append(howCanIUseIt, rhs.howCanIUseIt).append(releaseInfo, rhs.releaseInfo).append(usageRestrictions, rhs.usageRestrictions).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
