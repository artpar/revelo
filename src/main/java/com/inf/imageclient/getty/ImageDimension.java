package com.inf.imageclient.getty;

/**
 * Created by parth on 29/4/16.
 */
public class ImageDimension {
    Integer width;
    Integer height;

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }
}
