package com.inf.imageclient.getty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import javax.validation.Valid;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "images",
    "result_count"
})
public class GettyImageResult {

  @JsonProperty("images")
  @Valid
  private List<Image> images = new ArrayList<Image>();
  @JsonProperty("result_count")
  private Integer resultCount;
  @JsonIgnore
  private Map<String, Object> additionalProperties = new HashMap<String, Object>();

  /**
   * @return The images
   */
  @JsonProperty("images")
  public List<Image> getImages() {
    return images;
  }

  /**
   * @param images The images
   */
  @JsonProperty("images")
  public void setImages(List<Image> images) {
    this.images = images;
  }

  /**
   * @return The resultCount
   */
  @JsonProperty("result_count")
  public Integer getResultCount() {
    return resultCount;
  }

  /**
   * @param resultCount The result_count
   */
  @JsonProperty("result_count")
  public void setResultCount(Integer resultCount) {
    this.resultCount = resultCount;
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this);
  }

  @JsonAnyGetter
  public Map<String, Object> getAdditionalProperties() {
    return this.additionalProperties;
  }

  @JsonAnySetter
  public void setAdditionalProperty(String name, Object value) {
    this.additionalProperties.put(name, value);
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder().append(images).append(resultCount).append(additionalProperties).toHashCode();
  }

  @Override
  public boolean equals(Object other) {
    if (other == this) {
      return true;
    }
    if ((other instanceof GettyImageResult) == false) {
      return false;
    }
    GettyImageResult rhs = ((GettyImageResult) other);
    return new EqualsBuilder().append(images, rhs.images).append(resultCount, rhs.resultCount).append(additionalProperties, rhs.additionalProperties).isEquals();
  }

}
