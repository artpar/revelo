package com.inf.imageclient.weedfs;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.inf.cache.CacheInterface;
import com.inf.drop.WeedFsConfig;
import com.inf.imageclient.getty.ImageNotFound;
import com.inf.interfaces.AbstractImageCacherClient;
import com.inf.interfaces.DatabaseImageClient;
import com.inf.interfaces.ImageManager;
import com.inf.interfaces.ViewImage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.StreamUtils;
import org.springframework.web.client.RestTemplate;

/**
 * Created by artpar on 5/23/17.
 */
public class WeedFsClient extends AbstractImageCacherClient implements ImageManager {
  private static final Logger logger = LoggerFactory.getLogger(WeedFsClient.class);

  private static RestTemplate restTemplate;

  private final ImageManager localImageSearchManager;

  private ExecutorService threadPool = Executors.newFixedThreadPool(10);

  private String ASSIGN_URL;

  private ImageManager imageSearchManager;

  public WeedFsClient(WeedFsConfig weedFsMasterEndpoint, ImageManager imageSearchManager, CacheInterface<String, String> cacher,
          ObjectMapper objectMapper, DatabaseImageClient databaseImageClient) {
    super(cacher, objectMapper);
    ASSIGN_URL = weedFsMasterEndpoint.getEndpoint() + "/dir/assign";
    this.imageSearchManager = imageSearchManager;
    this.localImageSearchManager = databaseImageClient;
  }

  public void setRestTemplate(RestTemplate restTemplate) {
    WeedFsClient.restTemplate = restTemplate;
  }

  @Override
  public List<ViewImage> getImagesForQuery(Integer category, String company, String product) throws Exception, ImageNotFound {

    String token = category + " " + company + " " + product;
    AbstractImageCacherClient.CheckCache<List<ViewImage>> checkCache = new AbstractImageCacherClient.CheckCache<List<ViewImage>>(
            AbstractImageCacherClient.WEED_PREFIX_IMAGE_SET + token).invoke();
    if (checkCache.is()) {
      return checkCache.getCachedObject();
    }

    List<ViewImage> images = localImageSearchManager.getImagesForQuery(category, company, product);
    if (images.size() == 0) {
      images = imageSearchManager.getImagesForQuery(category, company, product);
      if (images.size() == 0) {
        return images;
      }
    }
    List goodImages = filterGoodImages(images, localImageSearchManager);

    PutCache(AbstractImageCacherClient.WEED_PREFIX_IMAGE_SET + token, goodImages);

    return goodImages;

  }

  private List filterGoodImages(List images, ImageManager imageManager) throws ImageNotFound, IOException {
    List goodImages = new LinkedList();
    if (images.size() > 0) {
      if (images.get(0).getClass().equals(ViewImage.class)) {
        for (Object image1 : images) {
          ViewImage image = (ViewImage) image1;
          byte[] imageData;
          try {
            imageData = imageManager.getImageByReferenceId(image.getToken());
          } catch (Exception e) {
            continue;
          }

          String reference = uploadImage(new ByteArrayInputStream(imageData));
          image.setToken(reference);
          goodImages.add(image);
        }

      } else {
        for (Object image1 : images) {
          Map image = (Map) image1;
          byte[] imageData;
          try {
            imageData = imageManager.getImageByReferenceId((String) image.get("token"));
          } catch (ImageNotFound e) {
            continue;
          }

          String reference = uploadImage(new ByteArrayInputStream(imageData));
          image.put("token", reference);
          Map<String, String> data = new HashMap<>();
          data.putAll(image);
          goodImages.add(data);
        }

      }
    }
    return goodImages;
  }

  @Override
  public List<ViewImage> getImagesForQuery(String token) throws ImageNotFound, Exception {

    AbstractImageCacherClient.CheckCache<List<ViewImage>> checkCache = new AbstractImageCacherClient.CheckCache<List<ViewImage>>(
            AbstractImageCacherClient.WEED_PREFIX_IMAGE_SET + token).invoke();
    if (checkCache.is()) {
      return checkCache.getCachedObject();
    }

    List images = imageSearchManager.getImagesForQuery(token);
    List goodImages = filterGoodImages(images, imageSearchManager);

    PutCache(AbstractImageCacherClient.WEED_PREFIX_IMAGE_SET + token, goodImages);

    return goodImages;
  }

  @Override
  public byte[] getImageByReferenceId(String referenceId) throws ImageNotFound {
    throw new ImageNotFound(referenceId);
  }

  @Override
  public String uploadImage(InputStream inputStream) throws IOException {
    ResponseEntity<AssignResponse> assignResponse = restTemplate.postForEntity(ASSIGN_URL, "", AssignResponse.class);

    AssignResponse res = assignResponse.getBody();

    if (!assignResponse.getStatusCode().is2xxSuccessful()) {
      logger.error("Failed to upload image to weedfs: {}", res);
    }

    threadPool.submit(new ImageUploadInBackground(res, inputStream, this));

    return res.getFid();
  }

  public static class ImageUploadInBackground implements Runnable {
    private final AssignResponse assignResponse;

    private final ImageManager client;

    private final byte[] imageBytes;

    ImageUploadInBackground(AssignResponse assignResponse, InputStream inputStream, ImageManager client) throws IOException {

      ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
      this.assignResponse = assignResponse;
      //            byte[] imageBytes = new byte[5000000];
      //            int len = IOUtils.read(inputStream, imageBytes);
      StreamUtils.copy(inputStream, byteArrayOutputStream);
      this.imageBytes = byteArrayOutputStream.toByteArray();
      this.client = client;
    }

    @Override
    public void run() {

      LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
      map.add("file", imageBytes);
      HttpHeaders headers = new HttpHeaders();
      headers.setContentType(MediaType.MULTIPART_FORM_DATA);

      HttpEntity<LinkedMultiValueMap<String, Object>> requestEntity = new HttpEntity<LinkedMultiValueMap<String, Object>>(map,headers);
      String imageUploadUrl = assignResponse.getPublicUrl() + "/" + assignResponse.getFid();

      try {

        logger.info("Image upload url: {}", imageUploadUrl);
        ResponseEntity<String> result = restTemplate.postForEntity(imageUploadUrl, requestEntity, String.class);

        logger.info("Upload response: {}", result);
      } catch (Throwable t) {
        logger.error("Failed to upload: {}", t);
      }

    }
  }

}
