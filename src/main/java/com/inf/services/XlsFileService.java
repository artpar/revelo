package com.inf.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.inf.data.*;
import com.inf.data.dao.impl.DataDao;
import com.inf.drop.UserDao;
import com.inf.survey.ValueTransformer;
import com.inf.survey.data.Question;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.xalan.lib.sql.ObjectArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by artpar on 5/7/17.
 */
public class XlsFileService {

  private static final Logger logger = LoggerFactory.getLogger(XlsFileService.class);
  private ValueTransformer valueTransformer;
  private DataDao dataDao;
  private UserDao userDao;
  private ObjectMapper objectMapper;


  public XlsFileService(ValueTransformer valueTransformer, DataDao dataDao, ObjectMapper objectMapper) {
    this.valueTransformer = valueTransformer;
    this.dataDao = dataDao;
    this.objectMapper = objectMapper;
  }

  public static List<String> getAllMatches(String text, String regex) {
    List<String> matches = new ArrayList<String>();
    Matcher m = Pattern.compile(regex).matcher(text);
    while (m.find()) {
      matches.add(m.group(1));
    }
    return matches;
  }

  public void setValueTransformer(ValueTransformer valueTransformer) {
    this.valueTransformer = valueTransformer;
  }

  public void setUserDao(UserDao userDao) {
    this.userDao = userDao;
  }

  public void setObjectMapper(ObjectMapper objectMapper) {
    this.objectMapper = objectMapper;
  }

  public HashMap<String, Object> updateQuestionnaireFromXls(XSSFWorkbook workbook, String fileName, Questionnaire questionnaire, Integer userId) {

    UserGroup userGroup = userDao.getHomeGroupByUserId(userId);


    HashMap<String, Object> response = new HashMap<String, Object>();
    List<String> errors = new LinkedList<>();
    XSSFSheet productSheet = workbook.getSheet(questionnaire.getName());

    if (productSheet == null) {
      errors.add(String.format("No sheet named [%s] found", questionnaire.getName()));
      response.put("status", "failed");
      response.put("errors", errors);
      return response;
    }

    XSSFRow headingRow = productSheet.getRow(0);
    Map<String, Integer> columnNameToColumnIndexMapping = new HashMap<>();

    Map<String, Boolean> expectedKeys = new HashMap<>();
    expectedKeys.put("question", false);
    expectedKeys.put("questiontype", false);
    expectedKeys.put("answer", false);
    expectedKeys.put("reason", false);
    expectedKeys.put("answervalue", false);
    expectedKeys.put("answer-image", false);
    expectedKeys.put("questiongroup", false);
    Map<String, Integer> featureMap = new HashMap<>();

    boolean okToContinue = true;

    for (int i = 0; i < headingRow.getLastCellNum(); i++) {
      XSSFCell cell = headingRow.getCell(i);
      String val = cell.getStringCellValue();
      if (val == null || val.trim().length() < 1) {
        break;
      }

      String columnName = val.trim().toLowerCase();

      if (expectedKeys.containsKey(columnName)) {
        expectedKeys.put(columnName, true);
      } else {
        featureMap.put(val.trim(), i);
      }


      if (columnNameToColumnIndexMapping.containsKey(columnName)) {
        errors.add(String.format("Column [%s] already added at column number [%d]", columnName, columnNameToColumnIndexMapping.get(columnName) + 1));
        continue;
      }
      columnNameToColumnIndexMapping.put(columnName, i);
    }

    for (String s : expectedKeys.keySet()) {
      if (!expectedKeys.get(s)) {
        errors.add(String.format("Expected column [%s] not found", s));
        okToContinue = false;
      }
    }


    if (!okToContinue) {
      response.put("errors", errors);
      response.put("status", "failed");
      return response;
    }

    int featureCount = featureMap.keySet().size();
    if (featureCount < 1) {
      errors.add("No feature column detected in the sheet");
    }


    List<String> categoryFeatures = new LinkedList<>();
    Map<String, Boolean> categoryFeatureMap = new HashMap<>();

    try {
      categoryFeatures = dataDao.getCategoryFeatures(questionnaire.getCategoryId());

      for (String categoryFeature : categoryFeatures) {
        categoryFeatureMap.put(categoryFeature.toLowerCase(), true);
      }


    } catch (Exception e) {
      logger.error("Failed to get category features by category id {}", questionnaire.getCategoryId(), e);
    }


    int lastRowNum = productSheet.getLastRowNum();

    List<QuestionType> validQuestionTypes = dataDao.getAllQuestionTypes();
    Map<String, Boolean> questionTypeMap = new HashMap<>();
    for (QuestionType validQuestionType : validQuestionTypes) {
      questionTypeMap.put(validQuestionType.name(), true);
    }


    Integer questionColumn = columnNameToColumnIndexMapping.get("question");
    Integer questionTypeColumn = columnNameToColumnIndexMapping.get("questiontype");
    Integer answerColumn = columnNameToColumnIndexMapping.get("answer");
    Integer reasonColumn = columnNameToColumnIndexMapping.get("reason");
    Integer answerValueColumn = columnNameToColumnIndexMapping.get("answervalue");
    Integer answerImageColumn = columnNameToColumnIndexMapping.get("answer-image");;
    Integer questionGroupColumn = columnNameToColumnIndexMapping.get("questiongroup");
    List<Map<String, Object>> answers = new LinkedList<>();


    Map<String, Integer> answerToRowMapping = new HashMap<>();

    Question currentQuestion = null;
    for (int i = 1; i < lastRowNum; i++) {

      XSSFRow row = productSheet.getRow(i);

      XSSFCell questionColumnContent = row.getCell(questionColumn);
      XSSFCell questionTypeColumnContent = row.getCell(questionTypeColumn);
      XSSFCell questionGroupColumnContent = row.getCell(questionGroupColumn);

      if (questionColumnContent != null && questionColumnContent.getStringCellValue().trim().length() > 0) {

        if (currentQuestion != null) {

          if (currentQuestion.getType().equals("multi_string_choice")
              || currentQuestion.getType().equals("single_string_choice")
              || currentQuestion.getType().equals("multi_image_choice")
              || currentQuestion.getType().equals("single_image_choice")) {
            JSONObject object = new JSONObject();
            object.put("options", answers);
            answers = new LinkedList<>();
            dataDao.updateQuestionData(currentQuestion.getId(), object.toString());
          }
        }

        String currentQuestionString = questionColumnContent.getStringCellValue().trim();
        String currentQuestionTypeString = questionTypeColumnContent.getStringCellValue().trim();
        String questionGroupColumnString = questionGroupColumnContent.getStringCellValue().trim();

        if (!questionTypeMap.containsKey(currentQuestionTypeString)) {
          errors.add(String.format("Invalid question type value for question [%s] => %s", currentQuestionString, currentQuestionTypeString));
          break;
        }


        currentQuestion = dataDao.addQuestion(currentQuestionString, userId, userGroup.getId());
        dataDao.updateQuestionType(currentQuestion.getId(), currentQuestionTypeString);
        dataDao.updateQuestionGroup(currentQuestion.getId(), questionGroupColumnString);

        currentQuestion.setType(QuestionType.valueOf(currentQuestionTypeString));
        try {
          dataDao.addQuestionToQuestionnaire(questionnaire.getReferenceId(), currentQuestion.getId().intValue(), userId);
        } catch (Exception e) {
          logger.error("Failed to add new question [{}] to questionniare [{}]", currentQuestion.getId(), questionnaire.getReferenceId());
          break;
        }
      }

      boolean questionTypeAllowedCondition = currentQuestion.getType().equals("multi_string_choice")
                                      || currentQuestion.getType().equals("single_string_choice")
                                      || currentQuestion.getType().equals("multi_image_choice")
                                      || currentQuestion.getType().equals("single_image_choice");

      if (!questionTypeAllowedCondition) {
        currentQuestion = null;
        continue;
      }

      XSSFCell answerColumnValueString = row.getCell(answerColumn);
      XSSFCell reasonColumnValueString = row.getCell(reasonColumn);
      XSSFCell answerValueColumnValueString = row.getCell(answerValueColumn);
      XSSFCell answerImageColumnValueString = row.getCell(answerImageColumn);

      String answerString;
      if (answerColumnValueString != null) {
        answerColumnValueString.setCellType(Cell.CELL_TYPE_STRING);
        answerString = answerColumnValueString.getStringCellValue().trim() + " ";
      } else {
        answerString = "";
      }

      String reasonString = "";
      if (reasonColumnValueString != null) {
        reasonColumnValueString.setCellType(Cell.CELL_TYPE_STRING);
        reasonString = reasonColumnValueString.getStringCellValue().trim() + " ";
      }

      if (answerValueColumnValueString == null) {
        errors.add(String.format("No answer value for the row [%d]", i + 1));
        break;
      }

      answerValueColumnValueString.setCellType(Cell.CELL_TYPE_STRING);
      String answerValueString = answerValueColumnValueString.getStringCellValue().trim();

      List<String> imageTokens = null;
      if (answerImageColumnValueString != null &&
              (currentQuestion.getType().equals("multi_image_choice")
              || currentQuestion.getType().equals("single_image_choice"))) {
        answerImageColumnValueString.setCellType(Cell.CELL_TYPE_STRING);
        //Single image token is supported
        String imageTokensStr = answerImageColumnValueString.getStringCellValue().trim();
        if(!imageTokensStr.isEmpty()) {
          imageTokens = new LinkedList<String>();
          imageTokens.add(imageTokensStr);
        }
      }

      HashMap<String,Object> answerOptionInfoMap = new HashMap<String,Object>();
      answerOptionInfoMap.put("value", answerValueString);
      answerOptionInfoMap.put("label", answerString);

      if(imageTokens!=null) {
        answerOptionInfoMap.put("image_tokens", imageTokens);
      }

      // {"Feature 1":{"formula":"1 + sin(x)","symbols":["x"],"substitutions":[],"$$hashKey":"object:332"}};

      Map<String, SymbolicFunction> featureFunctionMap = new HashMap<>();
      if (featureCount > 0) {
        for (String featureName : featureMap.keySet()) {

          if (!categoryFeatureMap.containsKey(featureName.toLowerCase())) {
            errors.add(String.format("Category [%s] does not have feature of name [%s]", questionnaire.getCategoryId(), featureName));
            continue;
          }

          Integer featureColumnIndex = featureMap.get(featureName);
          XSSFCell functionCell = row.getCell(featureColumnIndex);
          if (functionCell == null) {
            continue;
          }


          functionCell.setCellType(Cell.CELL_TYPE_STRING);
          if (functionCell.getStringCellValue().trim().length() < 1) {
            continue;
          }

          String functionString = functionCell.getStringCellValue().trim();

          List<String> symbols = getAllMatches(functionString, "(?<![a-z])([a-z]{1})(?![a-z])");
          Set<String> uniqueSymbols = new HashSet<>(symbols);

          SymbolicFunction symbolicFunction = new SymbolicFunction(functionString);
          symbolicFunction.setSymbols(uniqueSymbols);
          featureFunctionMap.put(featureName, symbolicFunction);
          String sfjson = "{}";
          try {
            sfjson = objectMapper.writeValueAsString(symbolicFunction);
          } catch (JsonProcessingException e) {
            logger.error("Failed to convert sym function to json", e);
          }


        }
      }

      String sfJson = "{}";
      try {
        sfJson = objectMapper.writeValueAsString(featureFunctionMap);
      } catch (JsonProcessingException e) {
        logger.error("Failed to convert symbolic function to json, should not happen", e);
      }

      dataDao.updateAnswerFunctionMappingData(currentQuestion.getId(), questionnaire.getId(), answerValueString, answerString, reasonString, sfJson, userId, userGroup.getId());

      answerOptionInfoMap.put("data", featureFunctionMap);
      answers.add(answerOptionInfoMap);
    }


    if (currentQuestion != null) {

      if (currentQuestion.getType().equals("multi_string_choice")
              || currentQuestion.getType().equals("single_string_choice")
              || currentQuestion.getType().equals("multi_image_choice")
              || currentQuestion.getType().equals("single_image_choice")) {
        JSONObject object = new JSONObject();
        object.put("options", answers);
        dataDao.updateQuestionData(currentQuestion.getId(), object.toString());
      }
    }


    response.put("errors", errors);
    response.put("status", "ok");
    return response;
  }

  public HashMap<String, Object> updateCategoryFromXls(XSSFWorkbook workbook, String fileName, Category category) {

    HashMap<String, Object> response = new HashMap<String, Object>();


//    XSSFSheet categorySheet = workbook.getSheet("category");
//    XSSFSheet featureSheet = workbook.getSheet("features");
    XSSFSheet productSheet = workbook.getSheet("products");

    if (productSheet == null) {
      productSheet = workbook.getSheetAt(0);
    }


    XSSFRow featuresRow = productSheet.getRow(0);

    short lastCellNumber = featuresRow.getLastCellNum();

    boolean isOkayToInsert = true;
    List<String> errors = new LinkedList<>();

    List<String> featureList = new LinkedList<>();
    Map<String, Integer> featureMap = new HashMap<>();
    for (int i = 1; i <= lastCellNumber; i++) {
      XSSFCell cell = featuresRow.getCell(i);
      if (cell == null) {
        lastCellNumber = (short) (i - 1);
        break;
//        String err = String.format("feature name in cell [%d] for xls [%s] is missing", i, fileName);
//        errors.add(err);
//        logger.error(err);
//        isOkayToInsert = false;
//        continue;
      }
      String stringCellValue = cell.getStringCellValue();
      if (stringCellValue == null) {
        String err = String.format("feature name in cell [%d] for xls [%s] is missing", i, fileName);
        errors.add(err);
        logger.error(err);
        isOkayToInsert = false;
        stringCellValue = "";
      }
      String featureName = stringCellValue.trim();
      if (featureName.length() < 1) {
        String err = String.format("feature name in cell [%d] for xls [%s] is missing", i, fileName);
        errors.add(err);
        logger.error(err);
        isOkayToInsert = false;
      }

      if (featureMap.containsKey(featureName)) {
        String err = String.format("Feature [%s] already present at column [%d] in xls [%s]", featureName, featureMap.get(featureName), fileName);
        errors.add(err);
        logger.error(err);
        isOkayToInsert = false;
      }
      featureMap.put(featureName, i);
      featureList.add(featureName);
    }

    if (!featureMap.containsKey("subcategory")) {
      isOkayToInsert = false;
      errors.add("'subcategory' column missing from the 'products' sheet");
    }


    if (!isOkayToInsert) {
      response.put("errors", errors);
      response.put("status", "failed");
      return response;
    }

    int lastRowNumber = productSheet.getLastRowNum();
    logger.info("Total {} rows in the sheet {}", lastRowNumber, fileName);

    int byTen = lastRowNumber / 10;

    Integer subCategoryColumnId = featureMap.get("subcategory");
    featureMap.remove("subcategory");
    for (int rowNumber = 1; rowNumber <= lastRowNumber; rowNumber++) {

      if (rowNumber % byTen == 0) {
        logger.info("Complated {} / {} rows", rowNumber, lastRowNumber);
      }
      XSSFRow row = productSheet.getRow(rowNumber);


      row.getCell(0).setCellType(Cell.CELL_TYPE_STRING);
      String productName = row.getCell(0).getStringCellValue();
      row.getCell(subCategoryColumnId).setCellType(Cell.CELL_TYPE_STRING);
      String subCategoryName = row.getCell(subCategoryColumnId).getStringCellValue();

      if (productName == null || productName.length() < 1) {
        break;
      }

      for (String featureName : featureMap.keySet()) {
        Integer featureColumnNumber = featureMap.get(featureName);
        XSSFCell cell = row.getCell(featureColumnNumber);
        if (cell == null) {
          continue;
        }

        String featureValue;
        Double numericValue;
        try {
          numericValue = cell.getNumericCellValue();
          featureValue = cell.toString().trim();
          if(Objects.equals(featureValue, "")){
            numericValue = null;
          }
        } catch (Exception e) {
          featureValue = cell.getStringCellValue().trim();
          numericValue = null;//valueTransformer.transform(featureValue);
        }

        String numericValueStr = numericValue==null?null:String.valueOf(numericValue);

        dataDao.updateProductInfo(
            category.getId(),
            subCategoryName, productName,
            featureName, featureValue, numericValueStr,
            "xls;" + fileName);
      }
    }

    response.put("status", "ok");
    return response;
  }

  public void setDataDao(DataDao dataDao) {
    this.dataDao = dataDao;
  }

  public Double transformValueToNumber(String value) {
    return valueTransformer.transform(value);
  }
}
