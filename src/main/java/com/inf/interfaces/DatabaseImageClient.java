package com.inf.interfaces;

import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.LinkedList;
import java.util.List;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.inf.cache.CacheInterface;
import com.inf.data.Product;
import com.inf.data.ProductFeature;
import com.inf.data.dao.impl.DataDao;
import com.inf.exceptions.InvalidCategoryException;
import com.inf.imageclient.getty.ImageNotFound;

public class DatabaseImageClient extends AbstractImageCacherClient {
  private final DataDao dataDao;

  public DatabaseImageClient(CacheInterface<String, String> cache, ObjectMapper objectMapper, DataDao dataDao) {
    super(cache, objectMapper);
    this.dataDao = dataDao;
  }

  private static String getMD5Hex(final String inputString) {

    MessageDigest md = null;
    try {
      md = MessageDigest.getInstance("MD5");
    } catch (NoSuchAlgorithmException e) {
      // should never happen
      e.printStackTrace();
    }
    md.update(inputString.getBytes());

    byte[] digest = md.digest();

    return convertByteToHex(digest);
  }

  private static String convertByteToHex(byte[] byteData) {

    StringBuilder sb = new StringBuilder();
    for (byte aByteData : byteData) {
      sb.append(Integer.toString((aByteData & 0xff) + 0x100, 16).substring(1));
    }
    return sb.toString();
  }

  @Override
  public List<ViewImage> getImagesForQuery(String token) throws ImageNotFound, Exception {
    throw new Exception("not implemented for database image client");
  }

  @Override
  public String uploadImage(InputStream inputStream) throws IOException {
    throw new IOException("not implemented for database image client");
  }

  @Override
  public List<ViewImage> getImagesForQuery(Integer category, String company, String product) throws InvalidCategoryException {

    Product prd = new Product();
    prd.setCompanyName(company);
    prd.setName(product);
    prd.setCategoryId(category);
    ProductFeature productFeature = dataDao.getProductSingleFeatureValue(prd, "image_url");

    if (productFeature == null) {
      return new LinkedList<>();
    }

    LinkedList<ViewImage> returnValue = new LinkedList<ViewImage>();
    ViewImage vi = new ViewImage();

    vi.setToken(getMD5Hex(productFeature.getValue()));
    vi.setUrl(productFeature.getValue());
    returnValue.add(vi);
    PutCache(AbstractImageCacherClient.CACHE_PREFIX_IMAGE_REF + vi.getToken(), vi.getUrl());

    return returnValue;
  }
}
