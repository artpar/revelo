package com.inf.interfaces;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by artpar
 */
public class Oauth2Util {

  private static final Logger logger = LoggerFactory.getLogger(Oauth2Util.class);
  private static ObjectMapper objectMapper;


  public static Oauth2Token GetOauth2Token(String apiKey, String secret) {
    URLConnection connection = null;
    Oauth2Token token1 = null;
    try {
      connection = new URL("https://api.gettyimages.com/oauth2/token").openConnection();
      connection.setDoOutput(true);
      connection.setDoInput(true);
    } catch (IOException e) {
      logger.error("Failed", e);
      return null;
    }
    Map<String, Object> map = new HashMap<>();
    map.put("grant_type", "client_credentials");
    map.put("client_id", apiKey);
    map.put("client_secret", secret);
    try {
      connection.getOutputStream().write(mapToFormData(map).getBytes());
      InputStream is = connection.getInputStream();
      token1 = objectMapper.readValue(is, Oauth2Token.class);
    } catch (IOException e) {
      logger.error("Failed to get write stream", e);
    }
    return token1;
  }

  private static String mapToFormData(Map<String, Object> map) {
    String query = "";
    for (String s : map.keySet()) {
      query = s + "=" + String.valueOf(map.get(s)) + "&" + s;
    }
    return query;

  }

  public static void setObjectMapper(ObjectMapper objectMapper) {
    Oauth2Util.objectMapper = objectMapper;
  }
}
