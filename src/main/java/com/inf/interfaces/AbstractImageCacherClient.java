package com.inf.interfaces;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.inf.cache.CacheInterface;
import com.inf.imageclient.getty.ImageNotFound;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Base64;
import java.util.List;

/**
 * Created by artpar
 */
abstract public class AbstractImageCacherClient implements ImageManager {
  public static final String CACHE_PREFIX_IMAGE_URL = "image_url_";
  public static final String CACHE_PREFIX_IMAGE_REF = "image_ref_";
  public static final String CACHE_PREFIX_IMAGE_SET = "image_set_";
  public static final String WEED_PREFIX_IMAGE_SET = "image_weed_";
  private static final Logger logger = LoggerFactory.getLogger(AbstractImageCacherClient.class);
  private static CacheInterface<String, String> cache;
  private static ObjectMapper objectMapper;

  public AbstractImageCacherClient(CacheInterface<String, String> cache, ObjectMapper objectMapper) {
    AbstractImageCacherClient.cache = cache;
    AbstractImageCacherClient.objectMapper = objectMapper;
  }

  @Override
  public byte[] getImageByReferenceId(String referenceId) throws ImageNotFound {

//    throw new NotImplementedException();

    String url = new CheckCache<String>(CACHE_PREFIX_IMAGE_REF + referenceId).invoke().getCachedObject();

    if (url == null) {
      throw new ImageNotFound(referenceId);
    }
    URL url1 = null;
    try {
      url1 = new URL(url);
    } catch (MalformedURLException e) {
      throw new ImageNotFound(referenceId, e);
    }

    CheckCache<String> checkImage = new CheckCache<String>(CACHE_PREFIX_IMAGE_URL + url).invoke();
    if (checkImage.is()) return Base64.getDecoder().decode(checkImage.getCachedObject());


    HttpURLConnection connection;
    try {
      logger.info("Url protocol: {}", url1.getProtocol());
      connection = (HttpURLConnection) url1.openConnection();
      connection.setRequestProperty("User-agent", "Revelo");
    } catch (IOException e) {
      throw new ImageNotFound("Getty : " + referenceId + " -- " + url + "\n", e);
    }
    try {

      int statusCode = connection.getResponseCode(); //get response code

      if (statusCode == 301 || statusCode == 302) {
        connection = (HttpURLConnection) new URL(connection.getHeaderField("Location")).openConnection();
        connection.setRequestProperty("User-agent", "Revelo");
      }

      InputStream inputStream = connection.getInputStream();
      InputStream in = new BufferedInputStream(inputStream);
      byte[] buff = new byte[5000000];
      int len = IOUtils.read(in, buff);
      if (len > 0) {

        byte[] toSave = new byte[len];
        System.arraycopy(buff, 0, toSave, 0, len);
        PutCache(CACHE_PREFIX_IMAGE_URL + url, Base64.getEncoder().encodeToString(toSave));
        return toSave;
      }
      return new byte[0];
    } catch (IOException e) {
      throw new ImageNotFound("Getty : " + referenceId + " -- " + url + "\n", e);
    }
  }


  protected void PutCache(String k, Object images) {

    try {
      cache.set(k, objectMapper.writeValueAsString(images));
    } catch (JsonProcessingException e) {
      logger.error("Failed to write value as string to cache", e);
    }
  }


  public static class CheckCache<T> {
    private final TypeReference<T> ilistTypeReference;
    private boolean myResult;
    private String token;
    private String k;
    private T cachedObject;

    public CheckCache(String token) {
      this.token = token;
      ilistTypeReference = new TypeReference<T>() {
      };
    }

    public boolean is() {
      return myResult;
    }

    public String getK() {
      return k;
    }

    public T getCachedObject() {
      return cachedObject;
    }

    public CheckCache<T> invoke() {
      k = token;
      String s = cache.get(k);
      if (s != null && s.length() > 0) {
        try {
          cachedObject = objectMapper.readValue(s, ilistTypeReference);

          if (String.valueOf(cachedObject).length() > 5) {
            myResult = true;
          }
          return this;
        } catch (IOException e) {
          logger.error("Failed to read cache value as list of view image", e);
        }
      }
      myResult = false;
      return this;
    }
  }
}
