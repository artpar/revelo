package com.inf.interfaces;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import com.inf.imageclient.getty.ImageNotFound;

/**
 * Created by artpar
 */
public interface ImageManager {
  List<ViewImage> getImagesForQuery(String token) throws ImageNotFound, Exception;

  byte[] getImageByReferenceId(String referenceId) throws ImageNotFound;

  String uploadImage(InputStream inputStream) throws IOException;

  List<ViewImage> getImagesForQuery(Integer category, String company, String product) throws ImageNotFound, Exception;

}
