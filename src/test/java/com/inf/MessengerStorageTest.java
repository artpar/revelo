package com.inf;

import java.util.HashMap;
import java.util.List;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.inf.graphlike.english.SentenceImpl;
import com.inf.graphlike.english.TokenManager;
import com.inf.graphlike.english.phrases.NounPhrase;
import com.inf.messenger.MessengerStorage;
import com.inf.messenger.NlpService;
import com.inf.messenger.sentence.Po;
import com.inf.messenger.sentence.Sentence;
import com.inf.messenger.sentence.SentenceParse;
import org.hypergraphdb.HGHandle;
import org.hypergraphdb.HyperGraph;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;
import redis.clients.jedis.JedisPool;

class MessengerStorageTest {
    private static HyperGraph graph;

    private static MessengerStorage messengerStorage;

    private static NlpService nlpService;

    private Logger logger = LoggerFactory.getLogger(MessengerServiceTest.class);

    @AfterAll
    static void close() {
        graph.close();
    }

    @BeforeAll
    static void setup() {
        nlpService = new NlpService("http://localhost:8088", new RestTemplate());
        JedisPool jedis = new JedisPool("localhost", 6379);
        String password = "";
        ObjectMapper objectMapper = new ObjectMapper();
        graph = new HyperGraph("/tmp/graph");
        messengerStorage = new MessengerStorage(jedis, password, objectMapper, graph);

    }

    @Test
    void setObjectAttribute() {

        SentenceParse sentenceParse = nlpService.parseSentence("My name is Parth.");
        Sentence sentence1 = sentenceParse.getSentences().get(0);
        SentenceImpl sentence = new SentenceImpl(sentence1, generateTokenManager(sentence1));

        List<HGHandle> noun = messengerStorage.getNounPhrases("parth");
        if (noun == null) {
            messengerStorage.storeSentence("parth", sentence);
            noun = messengerStorage.getNounPhrases("parth");
        }
        logger.info("Get relations for: {}", noun.toString());
        List<NounPhrase> relations = messengerStorage.getRelations(noun, "be");
        for (NounPhrase relation : relations) {
            logger.info("Noun == {}", relation);
        }
    }

    public TokenManager generateTokenManager(Sentence sentence) {

        HashMap<Double, Po> tokenMap = new HashMap<Double, Po>();

        for (Po po : sentence.getPos()) {
            tokenMap.put(po.getOrth(), po);
        }
        return new TokenManager(tokenMap);

    }

    @Test
    void getNounPhrases() {
    }
}