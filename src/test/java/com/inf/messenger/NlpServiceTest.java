package com.inf.messenger;

import ch.qos.logback.classic.Level;
import com.inf.messenger.sentence.Po;
import com.inf.messenger.sentence.Sentence;
import com.inf.messenger.sentence.SentenceParse;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

class NlpServiceTest {
    private Logger logger = LoggerFactory.getLogger(NlpServiceTest.class);

    @BeforeAll
    static void setup() {
        ch.qos.logback.classic.Logger rootLogger = (ch.qos.logback.classic.Logger) LoggerFactory
                .getLogger(ch.qos.logback.classic.Logger.ROOT_LOGGER_NAME);
        rootLogger.setLevel(Level.toLevel("info"));

    }

    @Test
    void parseSentence() {

        NlpService nlpService = new NlpService("http://localhost:8088/", new RestTemplate());

        printSentence(nlpService.parseSentence("My name is Parth."));
        printSentence(nlpService.parseSentence("Thank you for your support."));
        printSentence(nlpService.parseSentence("I need help."));
        printSentence(nlpService.parseSentence("What is the time?"));
        printSentence(nlpService.parseSentence("What day is it?"));

    }

    private void printSentence(SentenceParse sentenceParse) {

        for (Sentence sentence : sentenceParse.getSentences()) {

            StringBuilder builder = new StringBuilder();
            for (Po po : sentence.getPos()) {
                builder.append(po.getText()).append("/").append(po.getPos()).append("/").append(po.getDep()).append(" ");
            }
            logger.info("Sentence: {}", builder.toString());

        }

    }
}