package com.inf;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import java.util.LinkedList;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.messenger4j.exception.MessengerApiException;
import com.github.messenger4j.exception.MessengerIOException;
import com.inf.drop.MessengerConfig;
import com.inf.messenger.ChatEngine;
import com.inf.messenger.ChatMessage;
import com.inf.messenger.ChatState;
import com.inf.messenger.MessengerService;
import com.inf.messenger.MessengerStorage;
import com.inf.messenger.NlpService;
import com.inf.survey.SurveyManager;
import com.inf.survey.SurveyManagerImpl;
import org.hypergraphdb.HyperGraph;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;
import redis.clients.jedis.JedisPool;

class MessengerServiceTest {
    private Logger logger = LoggerFactory.getLogger(MessengerServiceTest.class);

    @Test
    void replyTextMessage() throws MessengerApiException, MessengerIOException {

        NlpService nlpService = new NlpService("http://localhost:8088", new RestTemplate());
        JedisPool jedis = new JedisPool("localhost", 6379);
        String password = "";
        logger.info("{} active redis threads", jedis.getNumActive());
        ObjectMapper objectMapper = new ObjectMapper();
        HyperGraph graph = new HyperGraph("/tmp/graph");
        MessengerStorage messengerStorage = new MessengerStorage(jedis, password, objectMapper, graph);

        SurveyManager surverManager = new SurveyManagerImpl();
        ChatEngine chatEngine = new ChatEngine(nlpService, messengerStorage, surverManager);
        MessengerService service = new MessengerService(new MessengerConfig("x", "w", "d"), messengerStorage, chatEngine, new SurveyManagerImpl());
        String senderId = "parth";
        //        List<Payload> responses = service.replyTextMessage("artpar", "What is my name?", chatState);
        //        printResponses(responses);
        //        printResponses(service.replyTextMessage("artpar", "What is my name?", chatState));
        //        printResponses(service.replyTextMessage("artpar", "My name is Parth.", chatState));
        //        printResponses(service.replyTextMessage("artpar", "Hi", chatState));
        ChatState chatState = new ChatState();
        //        printResponses(service.replyTextMessage(senderId, "Yash is great.", chatState));
        //        printResponses(service.replyTextMessage(senderId, "With massive volumes of written text being produced every second, how do we make sure that we have the most recent and relevant information available to us?", chatState));
        //        printResponses(service.replyTextMessage(senderId, "What is the time to go home?", chatState));
        //        printResponses(service.replyTextMessage(senderId, "Where is he running?", chatState));
        //        printResponses(service.replyTextMessage(senderId, "What do you like?", chatState));
        //        printResponses(service.replyTextMessage(senderId, "My name is khan.", chatState));
        //        printResponses(service.replyTextMessage(senderId, "I am looking for something shiny.", chatState));
        //        printResponses(service.replyTextMessage(senderId, "I am looking for mobiles.", chatState));
        //        printResponses(service.replyTextMessage(senderId, "I saw my friend in the street eating bananas.", chatState));
        //        printResponses(service.replyTextMessage(senderId, "I saw my friend in the street eating bananas when I was driving the car.", chatState));
        //        printResponses(service.replyTextMessage(senderId, "Super Bowl 50 was an American football game to determine the champion of the National Football League (NFL) for the 2015 season.", chatState));
        //        printResponses(service.replyTextMessage(senderId, "Please cooperate with me.", chatState));
        //        printResponses(service.replyTextMessage(senderId, "Give me some food.", chatState));
        //        printResponses(service.replyTextMessage(senderId, "Was Abraham Lincoln the sixteenth President of the United States?", chatState));
        //        printResponses(service.replyTextMessage(senderId, "Did his mother die of pneumonia?", chatState));
        //        printResponses(service.replyTextMessage(senderId, "A kangaroo is a marsupial from the family Macropodidae (macropods, meaning 'large foot'). In common use the term is used to describe the largest species from this family, the Red Kangaroo, the Antilopine Kangaroo, and the Eastern and Western Grey Kangaroo of the Macropus genus. The family also includes many smaller species which include the wallabies, tree-kangaroos, wallaroos, pademelons and the Quokka, some 63 living species in all.  Kangaroos are endemic to the continent of Australia, while the smaller macropods are found in Australia and New Guinea.", chatState));
        //        printResponses(service.replyTextMessage(senderId, "My name is Parth.", chatState));
        //        printResponses(service.replyTextMessage(senderId, "imperative sentences may involve other formatting quirks.", chatState));

        //        printResponses(service.replyTextMessage(senderId, "My name is Parth", chatState));
        //        printResponses(service.replyTextMessage(senderId, "what is my name?", chatState));
        //
        //        printResponses(service.replyTextMessage(senderId, "Your age is 32.", chatState));
        //        printResponses(service.replyTextMessage(senderId, "What is your age?", chatState));
        //        printResponses(service.replyTextMessage(senderId, "What is 32?", chatState));
        printResponses(service.replyTextMessage(senderId,
                "If you want to solve that raise your taxes on the rich and lift up those that are at the lowest end of the scale. ", chatState, ""));

        //        printResponses(service.replyTextMessage(senderId, "My name was Parth", chatState));
        //        printResponses(service.replyTextMessage(senderId, "His name was Parth", chatState));
        //        printResponses(service.replyTextMessage(senderId, "His age was 32", chatState));
        //        printResponses(service.replyTextMessage(senderId, "His has a car", chatState));
        //        printResponses(service.replyTextMessage(senderId, "You have a friend", chatState));
        //        printResponses(service.replyTextMessage(senderId, "Remind me tomorrow", chatState));
        //        printResponses(service.replyTextMessage(senderId, "What is his age?", chatState));
        //        printResponses(service.replyTextMessage(senderId, "Do you have friends?", chatState));
        //        printResponses(service.replyTextMessage(senderId, "What is your name?", chatState));
        //        printResponses(service.replyTextMessage(senderId, "How old are you?", chatState));
        //        printResponses(service.replyTextMessage(senderId, "Playing guitar is all about practice.", chatState));
        //        printResponses(service.replyTextMessage(senderId, "Tell me something about the guitar ?", chatState));
        //        printResponses(service.replyTextMessage(senderId, "Who do you work for?", chatState));
        //        printResponses(service.replyTextMessage(senderId, "Was Abraham Lincoln alive in 1978?", chatState));
        //        printResponses(service.replyTextMessage("artpar", "Yash is a boy.", chatState));
        //        printResponses(service.replyTextMessage("artpar", "Yash is a boy", chatState));
        //        printResponses(service.replyTextMessage("artpar", "Who is Yash?", chatState));

    }

    private void printResponses(LinkedList<ChatMessage> responses) {
        //        assertNotEquals(responses.size(), 0);
        for (ChatMessage respons : responses) {
            String responseText = respons.getSentence().getSentence();
            //            String responseText = ((MessagePayload) respons).message().quickReplies().get().get(0).toString().toString();
            logger.info("Response message: {}", responseText);
            assertNotNull(responseText);
        }
    }
}