//package com.inf.data.dao;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.inf.cache.RedisCache;
//import com.inf.data.dao.impl.AbstractWebApiInfoDataDao;
//import com.inf.data.dao.impl.FreebaseInfoDataDao;
//import org.springframework.web.client.RestTemplate;
//import redis.clients.jedis.JedisPool;
//
//import java.util.HashMap;
//import java.util.Map;
//
///**
// * Created by parth on 22/4/16.
// */
//public class FreebaseInfoDataDaoTest {
//
//    @org.junit.Test
//    public void testQuery() throws Exception {
//
//        AbstractWebApiInfoDataDao abstractWebApiInfoDataDao = new FreebaseInfoDataDao(new RestTemplate(), new RedisCache(new JedisPool(), "test"), null, new ObjectMapper());
//        String query = "[{\"id\":\"/en/earth\",\"name\":null,\"type\":\"/astronomy/planet\"}]";
//        Map<String, String> keys = new HashMap<>();
//        keys.put("query", query);
//        keys.put("key", FreebaseInfoDataDao.ApiKey);
//
//        System.out.println(abstractWebApiInfoDataDao.query(keys, "https://www.googleapis.com/freebase/v1/mqlread?query={query}&key={key}", false));
//    }
//}

