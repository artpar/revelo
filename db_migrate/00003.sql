INSERT INTO usergroup (reference_id, name) VALUE (uuid(), 'category-agent');

INSERT INTO usergroup (reference_id, name) VALUE (uuid(), 'questionnaire-agent');

INSERT INTO usergroup (reference_id, name) VALUE (uuid(), 'views-value-transformers');

INSERT INTO usergroup (reference_id, name) VALUE (uuid(), 'views-raw-data');

INSERT INTO user_usergroup (user_id, usergroup_id)
  (SELECT
     34333,
     id
   FROM usergroup
   WHERE name NOT LIKE 'home%' AND name NOT LIKE 'admin');