

create table usergroup (
  id int(11) auto_increment primary key,
  reference_id varchar(50) unique not null,
  name varchar(50) unique not null,
  created_at timestamp default CURRENT_TIMESTAMP,
  status varchar (20) default 'active',
  user_id int(11) references user(id)
);

create table user_usergroup (
  user_id int(11) references user(id),
  usergroup_id int(11) references usergroup(id),
  status varchar(20) default 'pending',
  primary key (user_id, usergroup_id),
  created_at timestamp default CURRENT_TIMESTAMP,
  updated_at timestamp null default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP
);



alter table user add column reference_id varchar(50);
update user set reference_id = uuid() where id > 0;
alter table user add constraint reference_id_uniq unique(reference_id);